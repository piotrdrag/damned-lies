from django.urls import path

from . import views

complete_translatable_path = (
    "modules/<name:module_name>/branches/<path:branch_name>/domains/<name:domain_name>/languages/<locale:lang>"
)

urlpatterns = [
    path("", views.APIHomeView.as_view(), name="api-v1-home"),
    # Liveliness checks
    path("check/", views.check, name="api-v1-check"),
    # Modules
    path("modules/", views.ModulesView.as_view(), name="api-v1-list-modules"),
    path("modules/<name:module_name>", views.ModuleView.as_view(), name="api-v1-info-module"),
    path(
        "modules/<name:module_name>/branch/<path:branch_name>",
        views.ModuleBranchView.as_view(),
        name="api-v1-info-module-branch",
    ),
    # Teams
    path("teams/", views.TeamsView.as_view(), name="api-v1-list-teams"),
    path("teams/<locale:team_name>", views.TeamView.as_view(), name="api-v1-info-team"),
    # Languages
    path("languages/", views.LanguagesView.as_view(), name="api-v1-list-languages"),
    # Releases
    path("releases/", views.ReleasesView.as_view(), name="api-v1-list-releases"),
    path("releases/<name:release>", views.ReleaseView.as_view(), name="api-v1-info-release"),
    path("releases/<name:release>/stats", views.ReleaseStatsView.as_view(), name="api-v1-info-release-stats"),
    path(
        "releases/<name:release>/languages/<locale:locale>",
        views.ReleaseLanguageView.as_view(),
        name="api-v1-info-release-language",
    ),
    # Vertimus workflow
    path(complete_translatable_path, views.ModuleLangStatsView.as_view(), name="api-v1-module-in-lang-statistics"),
    # APIs to reserve translation and upload translation, as could be used by an external translation tool.
    path(
        f"{complete_translatable_path}/reserve", views.ReserveTranslationView.as_view(), name="api-v1-reserve-module"
    ),
    path(f"{complete_translatable_path}/upload", views.UploadTranslationView.as_view(), name="api-v1-upload-file"),
    # Notifications from the software forge. Read the documentation to see how to trigger this endpoint.
    path(
        "modules/<name:module_name>/branches/<path:branch_name>/ping",
        views.refresh_module_branch,
        name="api-v1-refresh-module-branch",
    ),
]
