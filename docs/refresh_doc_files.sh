#!/usr/bin/env bash

# The purpose of this file is to automatically generate package named rst files

declare -x SPHINX_APIDOC_OPTIONS='members,private-members,show-inheritance'

declare -a DAMNED_LIES_PACKAGES=(api common feeds languages people stats teams vertimus)

for package_name in "${DAMNED_LIES_PACKAGES[@]}"; do
    sphinx-apidoc -o source/"${package_name}".package ../"${package_name}"
done
