teams package
=============

Subpackages
-----------

.. toctree::
   :maxdepth: 4

Submodules
----------

teams.admin module
------------------

.. automodule:: teams.admin
   :members:
   :private-members:
   :show-inheritance:

teams.forms module
------------------

.. automodule:: teams.forms
   :members:
   :private-members:
   :show-inheritance:

teams.models module
-------------------

.. automodule:: teams.models
   :members:
   :private-members:
   :show-inheritance:

teams.views module
------------------

.. automodule:: teams.views
   :members:
   :private-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: teams
   :members:
   :private-members:
   :show-inheritance:
