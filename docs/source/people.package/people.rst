people package
==============

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   people.templatetags

Submodules
----------

people.admin module
-------------------

.. automodule:: people.admin
   :members:
   :private-members:
   :show-inheritance:

people.forms module
-------------------

.. automodule:: people.forms
   :members:
   :private-members:
   :show-inheritance:

people.models module
--------------------

.. automodule:: people.models
   :members:
   :private-members:
   :show-inheritance:

people.views module
-------------------

.. automodule:: people.views
   :members:
   :private-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: people
   :members:
   :private-members:
   :show-inheritance:
