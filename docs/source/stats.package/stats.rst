stats package
=============

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   stats.management
   stats.templatetags

Submodules
----------

stats.admin module
------------------

.. automodule:: stats.admin
   :members:
   :private-members:
   :show-inheritance:

stats.doap module
-----------------

.. automodule:: stats.doap
   :members:
   :private-members:
   :show-inheritance:

stats.forms module
------------------

.. automodule:: stats.forms
   :members:
   :private-members:
   :show-inheritance:

stats.models module
-------------------

.. automodule:: stats.models
   :members:
   :private-members:
   :show-inheritance:

stats.potdiff module
--------------------

.. automodule:: stats.potdiff
   :members:
   :private-members:
   :show-inheritance:

stats.repos module
------------------

.. automodule:: stats.repos
   :members:
   :private-members:
   :show-inheritance:

stats.signals module
--------------------

.. automodule:: stats.signals
   :members:
   :private-members:
   :show-inheritance:

stats.utils module
------------------

.. automodule:: stats.utils
   :members:
   :private-members:
   :show-inheritance:

stats.views module
------------------

.. automodule:: stats.views
   :members:
   :private-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: stats
   :members:
   :private-members:
   :show-inheritance:
