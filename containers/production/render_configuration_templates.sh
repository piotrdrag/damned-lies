#!/bin/bash

# Render a specific configuration. All the files will be rendered in the KIND_OF_DEPLOYMENT directory

set +C

# KIND_OF_DEPLOYMENT: kind of deployment to render (the configuration will depend on this parameter).
# Possible values are test or production (the default), the names of the configuration files in JSON
declare -r KIND_OF_DEPLOYMENT="${1:-production}"

DAMNED_LIES_VERSION_LONG_HASH="$(git rev-parse --verify HEAD)"
declare -r DAMNED_LIES_VERSION_LONG_HASH

# THIS_SCRIPT_DIRECTORY: the current script directory name
THIS_SCRIPT_DIRECTORY="$(realpath "$(dirname "${BASH_SOURCE[0]}")")"
declare -r THIS_SCRIPT_DIRECTORY

mkdir -p "${THIS_SCRIPT_DIRECTORY}/${KIND_OF_DEPLOYMENT}"
for template in "${THIS_SCRIPT_DIRECTORY}"/configuration/*.jinja2; do
    jinja2 \
        --strict \
        "${template}" \
        "${THIS_SCRIPT_DIRECTORY}/configuration/${KIND_OF_DEPLOYMENT}.json" \
        > "${THIS_SCRIPT_DIRECTORY}/${KIND_OF_DEPLOYMENT}/$(basename "${template}" .jinja2)"
done

echo "VERSION_LONG_HASH = \"${DAMNED_LIES_VERSION_LONG_HASH}\"" >> "${THIS_SCRIPT_DIRECTORY}/${KIND_OF_DEPLOYMENT}/local_settings.py"
