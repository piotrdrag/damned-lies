#!/usr/bin/env bash

# DAMNED_LIES_IMAGE_NAME: the target image name (without any tag)
declare -r DAMNED_LIES_IMAGE_NAME="damned-lies-runtime"

# FEDORA_BASE_IMAGE: version of Fedora to use
declare -r FEDORA_BASE_IMAGE=40

# THIS_SCRIPT_DIRECTORY: the current script directory name
THIS_SCRIPT_DIRECTORY="$(realpath "$(dirname "${BASH_SOURCE[0]}")")"
declare -r THIS_SCRIPT_DIRECTORY

# DAMNED_LIES_LOCAL_CODE_DIRECTORY: there is located the source code on the local system that build the container
DAMNED_LIES_LOCAL_CODE_DIRECTORY="$(realpath "$(dirname "${BASH_SOURCE[0]}")"/../..)"

# CURRENT_VCS_REF_NAME: the name of the current branch, that is used to name the created image
if [[ -z ${CURRENT_VCS_REF_NAME+x}  ]]; then
    CURRENT_VCS_REF_NAME="$(git branch --show-current)"
fi
declare -r CURRENT_VCS_REF_NAME

#######################################################################################################################

# Stop the shell script if at least one command fails
set -e

declare -r PYTHON_PACKAGES="python3 python3-setuptools python3-pip python3-wheel python3-mysqlclient python3-pyicu"
declare -r HTTPD_PACKAGES="httpd mod_wsgi"
declare -r INTERNATIONALISATION_TOOLS="gettext intltool itstool libicu-devel enchant yelp-tools"
declare -r VCS="git"
declare -r BUILD_UTILITIES="automake autoconf make glibc-langpack-en"
declare -r SYSTEM_TOOLS="which diffutils"
declare -r PACKAGES_TO_INSTALL="${PYTHON_PACKAGES} ${HTTPD_PACKAGES} ${INTERNATIONALISATION_TOOLS} ${VCS} ${BUILD_UTILITIES} ${SYSTEM_TOOLS}"

container=$(buildah from quay.io/fedora/fedora-minimal:${FEDORA_BASE_IMAGE})

buildah config --author "Guillaume Bernard" "${container}"
buildah config --label 'maintainer="Guillaume Bernard <associations@guillaume-bernard.fr>"' "${container}"
buildah config --label "license=GPLv3" "${container}"
buildah config --label "name=damned-lies-runtime" "${container}"
buildah config --label "vendor=The GNOME Project contributors" "${container}"

# Install dependencies
buildah run "${container}" /bin/sh -c  "microdnf -y update"
buildah run "${container}" /bin/sh -c  "microdnf -y --setopt=tsflags=nodocs install ${PACKAGES_TO_INSTALL}"
buildah run "${container}" /bin/sh -c  "microdnf -y clean all"

# Install dependencies in the virtual environment
buildah add \
    --chmod 400 \
    --contextdir "${DAMNED_LIES_LOCAL_CODE_DIRECTORY}" \
    "${container}" \
    "${THIS_SCRIPT_DIRECTORY}/../" /tmp/dl-tmp
buildah run "${container}" -- pip install --no-cache-dir --upgrade pip
buildah run "${container}" -- pip install --no-cache-dir -e /tmp/dl-tmp
buildah run "${container}" -- rm -rf /tmp/dl-tmp

# Commit image
buildah commit "${container}" "${DAMNED_LIES_IMAGE_NAME}:${CURRENT_VCS_REF_NAME}-${FEDORA_BASE_IMAGE}"
buildah tag "${DAMNED_LIES_IMAGE_NAME}:${CURRENT_VCS_REF_NAME}-${FEDORA_BASE_IMAGE}" "${DAMNED_LIES_IMAGE_NAME}:${CURRENT_VCS_REF_NAME}-latest"
