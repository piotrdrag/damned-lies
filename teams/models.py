import abc
from datetime import timedelta
from typing import TYPE_CHECKING, Any

from django.conf import settings
from django.db import models
from django.db.models import QuerySet
from django.urls import reverse
from django.utils import timezone, translation
from django.utils.translation import gettext as _
from django.utils.translation import gettext_lazy

from common.utils import send_mail
from people.models import Person

if TYPE_CHECKING:
    from languages.models import Language


class TeamManager(models.Manager):
    def all_with_coordinator(self: "TeamManager") -> QuerySet:
        """
        Returns all teams with the coordinator already prefilled. Use that
        function to reduce the size of extracted data and the numbers of objects
        built or use all_with_roles() if you need information about the other
        roles.
        """
        teams = self.all()
        roles = Role.objects.select_related("person").filter(role="coordinator")
        role_dict = {}

        for role in roles:
            role_dict.setdefault(role.team_id, []).append(role.person)

        for team in teams:
            try:
                team.roles = {"coordinator": role_dict[team.id]}
            except KeyError:
                # Abnormal because a team must have a coordinator but of no
                # consequence
                pass
        return teams

    def all_with_roles(self: "TeamManager") -> QuerySet:
        """
        This method prefills team.coordinator/committer/reviewer/translator to
        reduce subsequent database access.
        """
        teams = self.all()
        roles = Role.objects.select_related("person").filter(is_active=True)
        role_dict = {}

        for role in roles:
            if role.team_id not in role_dict:
                role_dict[role.team_id] = [role]
            else:
                role_dict[role.team_id].append(role)

        for team in teams:
            try:
                for role in role_dict[team.id]:
                    team.fill_role(role.role, role.person)
            except KeyError:
                # Abnormal because a team must have a coordinator but of no
                # consequence
                pass
        return teams


class AbstractTeam:
    @abc.abstractmethod
    def get_absolute_url(self: "AbstractTeam") -> str:
        pass

    @abc.abstractmethod
    def can_edit(self: "AbstractTeam", _: "Person") -> bool:  # PLR6301
        """
        Whether the team can be edited.
        """

    @abc.abstractmethod
    def get_description(self: "AbstractTeam") -> str:
        """
        Team description: a locale, a string…
        """

    @abc.abstractmethod
    def get_languages(self: "AbstractTeam") -> tuple["Language"]:
        """
        The languages the team members translate.
        """

    @abc.abstractmethod
    def get_coordinators(self: "AbstractTeam") -> list[Person]:  # PLR6301
        return []


class Team(models.Model, AbstractTeam):
    """The lang_code is generally used for the name of the team."""

    name = models.CharField(max_length=80)
    description = models.TextField()
    use_workflow = models.BooleanField(default=True)
    presentation = models.TextField(blank=True, verbose_name=_("Presentation"))
    members = models.ManyToManyField(Person, through="Role", related_name="teams")
    webpage_url = models.URLField(null=True, blank=True, verbose_name=_("Web page"))
    mailing_list = models.EmailField(null=True, blank=True, verbose_name=_("Mailing list"))
    mailing_list_subscribe = models.URLField(null=True, blank=True, verbose_name=_("URL to subscribe"))
    objects = TeamManager()

    class Meta:
        db_table = "team"
        ordering = ("description",)

    def __init__(self: "Team", *args, **kwargs) -> None:  # noqa ANN003
        models.Model.__init__(self, *args, **kwargs)
        self.roles: dict[str, list[Person]] = {}

    def __str__(self: "Team") -> str:
        return self.description

    def get_absolute_url(self: "Team") -> str:
        return reverse("team_slug", args=[self.name])

    def can_edit(self: "Team", user: "Person") -> bool:
        """Return True if user is allowed to edit this team
        user is a User (from request.user), not a Person
        """
        return user.is_authenticated and user.username in [p.username for p in self.get_coordinators()]

    def fill_role(self: "Team", role: str, person: "Person") -> None:
        """Used by TeamManager to prefill roles in team"""
        if not self.roles:
            self.roles = {"coordinator": [], "committer": [], "reviewer": [], "translator": []}
        self.roles[role].append(person)

    def get_description(self: "Team") -> str:
        return _(self.description)

    def get_languages(self: "Team") -> tuple["Language"]:
        return tuple(self.language_set.all())

    def get_members_by_role_exact(self: "Team", role: str, only_active: bool = True) -> list[Person]:
        """Return a list of active members"""
        try:
            return self.roles[role]
        except KeyError:
            if only_active:
                members = Person.objects.filter(role__team__id=self.id, role__role=role, role__is_active=True)
            else:
                members = Person.objects.filter(role__team__id=self.id, role__role=role)
            return list(members)

    def get_coordinators(self: "Team") -> list[Person]:
        return self.get_members_by_role_exact("coordinator", only_active=False)

    def get_committers_exact(self: "Team") -> list[Person]:
        return self.get_members_by_role_exact("committer")

    def get_reviewers_exact(self: "Team") -> list[Person]:
        return self.get_members_by_role_exact("reviewer")

    def get_translators_exact(self: "Team") -> list[Person]:
        return self.get_members_by_role_exact("translator")

    def get_members_by_roles(self: "Team", roles: list[str], only_active: bool = True) -> list[Person]:
        """Requires a list of roles in argument"""
        try:
            members = []
            for role in roles:
                members += self.roles[role]
        except KeyError:
            if only_active:
                members = Person.objects.filter(role__team__id=self.id, role__role__in=roles, role__is_active=True)
            else:
                members = Person.objects.filter(role__team__id=self.id, role__role__in=roles)
        return list(members)

    def get_committers(self: "Team") -> list[Person]:
        return self.get_members_by_roles(["coordinator", "committer"])

    def get_reviewers(self: "Team") -> list[Person]:
        return self.get_members_by_roles(["coordinator", "committer", "reviewer"])

    def get_translators(self: "Team") -> list[Person]:
        """Don't use get_members_by_roles to provide an optimization"""
        try:
            members = []
            for role in ["coordinator", "committer", "reviewer", "translator"]:
                members += self.roles[role]
        except KeyError:
            # Not necessary to filter as for other roles
            members = list(self.members.all())
        return members

    def get_inactive_members(self: "Team") -> list[Person]:
        """Return the inactive members"""
        return list(Person.objects.filter(role__team__id=self.id, role__is_active=False))

    def send_mail_to_coordinator(
        self: "Team", subject: str, message: str, message_kwargs: dict[str, Any] | None = None
    ) -> None:
        """Send a message to the coordinator, in her language if available
        and if subject and message are lazy strings"""
        recipients = [pers.email for pers in self.get_coordinators() if pers.email]
        if not recipients:
            return
        with translation.override(self.language_set.first().locale):
            message = "%s\n--\n" % (message % (message_kwargs or {}),)  # noqa: UP031
            message += _("This is an automated message sent from %s.") % settings.SITE_DOMAIN
            send_mail(str(subject), message, to=recipients, headers={settings.EMAIL_HEADER_NAME: "coordinator-mail"})


class FakeTeam(AbstractTeam):
    """
    This is a class replacing a Team object when a language
    has no team attached.
    """

    fake = 1

    def __init__(self: "FakeTeam", language: "Language") -> None:
        self.language = language
        self.description = _("No team for locale %s") % self.language.locale

    def get_absolute_url(self: "FakeTeam") -> str:
        return reverse("team_slug", args=[self.language.locale])

    def get_description(self: "FakeTeam") -> str:
        return self.language.locale

    def can_edit(self: "FakeTeam", _: "Person") -> bool:  # noqa PLR6301 (no self use)
        return False

    def get_languages(self: "FakeTeam") -> tuple["Language"]:
        return (self.language,)

    def get_coordinators(self: "FakeTeam") -> list[Person]:  # noqa PLR6301 (no self use)
        return []


ROLE_CHOICES = (
    ("translator", gettext_lazy("Translator")),
    ("reviewer", gettext_lazy("Reviewer")),
    ("committer", gettext_lazy("Committer")),
    ("coordinator", gettext_lazy("Coordinator")),
)


class Role(models.Model):
    """
    This is the intermediary class between Person and Team to attribute roles to
    Team members.
    """

    team = models.ForeignKey(Team, on_delete=models.CASCADE)
    person = models.ForeignKey(Person, on_delete=models.CASCADE)
    role = models.CharField(max_length=15, choices=ROLE_CHOICES, default="translator")
    is_active = models.BooleanField(default=True)

    class Meta:
        db_table = "role"
        unique_together = ("team", "person")

    def __str__(self: "Role") -> str:
        return f"{self.person.name} is {self.role} in {self.team.description} team"

    @classmethod
    def inactivate_unused_roles(cls: "Role") -> None:
        """Inactivate the roles when login older than 180 days"""
        last_login = timezone.now() - timedelta(days=30 * 6)
        cls.objects.filter(person__last_login__lt=last_login, is_active=True).update(is_active=False)
