from datetime import timedelta

from django.conf import settings
from django.core import mail
from django.test import TestCase, Client
from django.urls import reverse
from django.utils import timezone
from django.utils.translation import gettext_lazy

from languages.models import Language
from people.models import Person
from teams.models import Role, Team


class TeamsAndRolesMixin:
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.pn = Person(first_name="John", last_name="Nothing", email="jn@devnull.com", username="jn")
        cls.pn.set_password("password")
        cls.pn.save()

        cls.translator = Person.objects.create(
            first_name="John", last_name="Translator", email="jt@tf1.com", username="jt"
        )
        cls.translator.set_password("password")
        cls.translator.save()
        cls.reviewer = Person.objects.create(
            first_name="John", last_name="Reviewer", email="jr@csa.com", username="jr"
        )
        cls.reviewer.set_password("password")
        cls.reviewer.save()

        # active person, but in limit date
        cls.committer = Person.objects.create(
            first_name="John",
            last_name="Committer",
            email="jc@alinsudesonpleingre.fr",
            username="jc",
            last_login=timezone.now() - timedelta(days=30 * 6 - 1),
        )

        cls.coordinator = Person(
            first_name="John", last_name="Coordinator", email="jcoo@imthebigboss.fr", username="jcoo"
        )
        cls.coordinator.set_password("password")
        cls.coordinator.save()

        cls.team_french = Team.objects.create(name="fr", description="French", mailing_list="french_ml@example.org")
        cls.team_portuguese = Team.objects.create(name="po", description="Portuguese")
        cls.team_italian = Team.objects.create(name="it", description="Italiano")

        cls.language_italian = Language.objects.create(name="Italien", locale="it", team=cls.team_italian)
        cls.language = Language.objects.create(name="French", locale="fr", team=cls.team_french)

        Role.objects.bulk_create([
            Role(team=cls.team_french, person=cls.translator),
            Role(team=cls.team_portuguese, person=cls.translator, role="reviewer"),
            Role(team=cls.team_french, person=cls.reviewer, role="reviewer"),
            Role(team=cls.team_french, person=cls.committer, role="committer"),
            Role(team=cls.team_french, person=cls.coordinator, role="coordinator"),
        ])


class TeamTests(TeamsAndRolesMixin, TestCase):
    def test_get_members_by_role_exact(self):
        members = self.team_french.get_members_by_role_exact("committer")
        t = Team.objects.get(name="fr")
        self.assertEqual(len(members), 1)
        self.assertEqual(members[0], self.committer)

        role = Role.objects.get(person=self.committer, team=t)
        role.is_active = False
        role.save()

        members = self.team_french.get_members_by_role_exact("committer")
        self.assertEqual(len(members), 0)

    def test_get_inactive_members(self):
        members = self.team_french.get_inactive_members()
        self.assertEqual(len(members), 0)

        t = Team.objects.get(name="fr")
        role = Role.objects.get(person=self.committer, team=t)
        role.is_active = False
        role.save()

        members = self.team_french.get_inactive_members()
        self.assertEqual(len(members), 1)
        self.assertEqual(members[0], self.committer)

    def run_roles_exact_test(self, team):
        pcoords = team.get_coordinators()
        self.assertEqual(pcoords[0], self.coordinator)

        members = team.get_committers_exact()
        self.assertEqual(len(members), 1)
        self.assertEqual(members[0], self.committer)

        members = team.get_reviewers_exact()
        self.assertEqual(len(members), 1)
        self.assertEqual(members[0], self.reviewer)

        members = team.get_translators_exact()
        self.assertEqual(len(members), 1)
        self.assertEqual(members[0], self.translator)

    def test_roles_exact(self):
        self.run_roles_exact_test(self.team_french)

    def test_roles_exact_prefilled_coordinator(self):
        self.run_roles_exact_test(Team.objects.all_with_coordinator()[0])

    def test_roles_exact_prefilled_all(self):
        self.run_roles_exact_test(Team.objects.all_with_roles()[0])

    def run_roles_test(self, team):
        """
        Tests the hierarchy of roles
        """
        members = team.get_committers()
        self.assertEqual(len(members), 2)
        for pc in members:
            self.assertTrue(pc in {self.coordinator, self.committer})

        members = team.get_reviewers()
        self.assertEqual(len(members), 3)
        for pc in members:
            self.assertTrue(pc in {self.coordinator, self.committer, self.reviewer})

        members = team.get_translators()
        self.assertEqual(len(members), 4)
        for pc in members:
            self.assertTrue(pc in {self.coordinator, self.committer, self.reviewer, self.translator})

    def test_roles(self):
        self.run_roles_test(self.team_french)

    def test_roles_prefilled_coordinator(self):
        self.run_roles_test(Team.objects.all_with_coordinator()[0])

    def test_roles_prefilled_all(self):
        self.run_roles_test(Team.objects.all_with_roles()[0])

    def test_join_team(self):
        response = self.client.post("/login/", data={"username": self.pn.username, "password": "password"})
        # Display team join page
        team_join_url = reverse("person_team_join", current_app="people")
        response = self.client.get(team_join_url)
        self.assertContains(response, "<select ")
        # Post for joining
        response = self.client.post(team_join_url, {"teams": [str(self.team_french.pk)]})
        # Test user is member of team
        self.assertTrue(self.pn.is_translator(self.team_french))
        # Test coordinator receives email
        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(mail.outbox[0].recipients()[0], self.coordinator.email)
        # Mail should be sent in the target team's language (i.e. French here)
        self.assertIn("rejoindre", mail.outbox[0].body)
        # Double submission should not crash
        self.client.post(team_join_url, {"teams": [str(self.team_french.pk)]})

    def test_leave_team(self):
        Role.objects.create(team=self.team_french, person=self.pn, role="translator")
        self.client.login(username="jn", password="password")
        response = self.client.post(reverse("person_team_leave", args=[self.team_french.name]))
        self.assertRedirects(response, reverse("person_detail_username", args=[self.pn.username]))
        self.pn.refresh_from_db()
        self.assertEqual(self.pn.role_set.count(), 0)

    def test_edit_team(self):
        """Test team edit form"""
        edit_url = reverse("team_edit", args=["fr"], current_app="teams")
        with self.assertLogs("django.request", level="WARNING"):
            response = self.client.get(edit_url)
        self.assertEqual(response.status_code, 403)
        # Login as team coordinator
        response = self.client.post("/login/", data={"username": self.coordinator.username, "password": "password"})
        # Try team modification
        response = self.client.post(
            edit_url,
            {
                "webpage_url": "http://www.gnomefr.org/",
                "mailing_list": "gnomefr@traduc.org",
                "mailing_list_subscribe": "",
            },
        )
        team = Team.objects.get(name="fr")
        self.assertEqual(team.webpage_url, "http://www.gnomefr.org/")

    def test_edit_team_roles(self):
        team_url = reverse("team_slug", args=["fr"])
        # Login as team coordinator
        self.client.force_login(self.coordinator)
        # Team member role modification
        self.client.post(
            team_url,
            {
                "form_type": "reviewer",
                "%d" % Role.objects.get(team=self.team_french, person=self.reviewer).pk: "committer",
            },
        )
        self.assertEqual(len(mail.outbox), 1)
        self.assertIn("Role changed", mail.outbox[0].subject)
        self.assertIn(
            "Your role in the French team on %s has been set to “Committer”" % settings.SITE_DOMAIN,
            mail.outbox[0].body,
        )
        mail.outbox = []
        # Team member removal
        self.client.post(
            team_url,
            {
                "form_type": "translator",
                "%d" % Role.objects.get(team=self.team_french, person=self.translator).pk: "remove",
            },
        )
        self.assertEqual(len(mail.outbox), 1)
        self.assertIn("Removed from team", mail.outbox[0].subject)
        self.assertIn("You have been removed from the French team on %s" % settings.SITE_DOMAIN, mail.outbox[0].body)

    def test_send_mail_to_coordinator(self):
        self.team_french.send_mail_to_coordinator(subject="foo", message="bar")
        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(mail.outbox[0].subject, "%sfoo" % settings.EMAIL_SUBJECT_PREFIX)
        self.assertEqual(mail.outbox[0].extra_headers, {settings.EMAIL_HEADER_NAME: "coordinator-mail"})
        # the message is sent in the language of the team
        self.team_french.send_mail_to_coordinator(subject=gettext_lazy("About Damned Lies"), message="...")
        self.assertEqual(len(mail.outbox), 2)
        self.assertEqual(mail.outbox[1].subject, "%sÀ propos de Damnés Mensonges" % settings.EMAIL_SUBJECT_PREFIX)

    def test_team_suggestion_for_non_authenticated_user_in_french(self):
        self.client.cookies.load({settings.LANGUAGE_COOKIE_NAME: "fr"})
        response = self.client.get(reverse("teams"))
        self.assertFalse(response.context.get("user").is_authenticated)
        self.assertContains(response, "team-suggestions-title")
        self.assertContains(response, "team-info-base-fr-suggestion")
        self.assertNotContains(response, "member-of-team-fr")
        self.assertContains(response, "suggestion-for-team-fr")
        self.assertNotContains(response, "team-info-base-it-suggestion")
        self.assertNotContains(response, "member-of-team-it")
        self.assertNotContains(response, "suggestion-for-team-it")
        self.assertNotContains(response, "team-info-base-po-suggestion")
        self.assertNotContains(response, "member-of-team-po")
        self.assertNotContains(response, "suggestion-for-team-po")

    def test_team_suggestion_for_authenticated_user_in_french(self):
        self.coordinator.teams.clear()
        self.client.cookies.load({settings.LANGUAGE_COOKIE_NAME: "fr"})
        self.client.login(username="jcoo", password="password")
        response = self.client.get(reverse("teams"))
        self.assertTrue(response.context.get("user").is_authenticated)
        self.assertContains(response, "team-suggestions-title")
        self.assertContains(response, "team-info-base-fr-suggestion")
        self.assertNotContains(response, "member-of-team-fr")
        self.assertContains(response, "suggestion-for-team-fr")
        self.assertNotContains(response, "team-info-base-it-suggestion")
        self.assertNotContains(response, "member-of-team-it")
        self.assertNotContains(response, "suggestion-for-team-it")
        self.assertNotContains(response, "team-info-base-po-suggestion")
        self.assertNotContains(response, "member-of-team-po")
        self.assertNotContains(response, "suggestion-for-team-po")

    def test_team_suggestion_because_user_is_already_member_of_french_team_plus_italian_suggestion(self):
        self.translator.teams.clear()
        Role.objects.create(team=self.team_french, person=self.translator, is_active=True, role="translator")
        self.client.cookies.load({settings.LANGUAGE_COOKIE_NAME: "it"})
        self.client.login(username=self.translator.username, password="password")
        response = self.client.get(reverse("teams"))
        self.assertTrue(response.context.get("user").is_authenticated)
        self.assertContains(response, "team-suggestions-title")
        self.assertContains(response, "team-info-base-fr-suggestion")
        self.assertContains(response, "member-of-team-fr")
        self.assertNotContains(response, "suggestion-for-team-fr")
        self.assertContains(response, "team-info-base-it-suggestion")
        self.assertNotContains(response, "member-of-team-it")
        self.assertContains(response, "suggestion-for-team-it")
        self.assertNotContains(response, "team-info-base-po-suggestion")
        self.assertNotContains(response, "member-of-team-po")
        self.assertNotContains(response, "suggestion-for-team-po")


class JSONTeamsTest(TeamsAndRolesMixin, TestCase):
    def setUp(self):
        super().setUp()
        t3 = Team.objects.create(name="gl", description="Galician")
        coor1 = Person.objects.create(
            first_name="Marcos",
            last_name="Coordinator",
            email="marc@imthebigboss.fr",
            username="marcos",
            forge_account="thegitaccount",
        )
        coor2 = Person.objects.create(
            first_name="Pepe", last_name="Coordinator", email="pepe@imthebigboss.es", username="pepe"
        )
        Role.objects.create(team=t3, person=coor1, role="coordinator")
        Role.objects.create(team=t3, person=coor2, role="coordinator")

    def test_json_teams(self):
        """Test JSON teams interface"""
        response = self.client.get(reverse("teams", args=["json"]))
        self.assertEqual(response.status_code, 200)
        expected_json = """[
            {
                "id":"fr",
                "description":"French",
                "coordinators": [
                {
                    "name":"John Coordinator"
                }]
            },
            {
                "id":"gl",
                "description":"Galician",
                "coordinators": [
                    {
                        "name":"Marcos Coordinator",
                        "vcs":"thegitaccount"
                    },
                    {
                        "name":"Pepe Coordinator"
                    }
                ]
            },
            {
                "id":"it",
                "description":"Italiano",
                "coordinators": []
            },
            {
                "id":"po",
                "description":"Portuguese",
                "coordinators": []
            }
            ]"""
        self.assertJSONEqual(response.content.decode("utf-8"), expected_json)


class RoleTest(TeamsAndRolesMixin, TestCase):
    def setUp(self):
        super().setUp()

        self.translator.last_login = timezone.now() - timedelta(days=10)  # active person
        self.translator.save()

        self.reviewer.last_login = timezone.now() - timedelta(days=30 * 6 + 1)  # inactive person
        self.reviewer.save()

        # active person, but in limit date
        self.committer.last_login = timezone.now() - timedelta(days=30 * 6 - 1)
        self.committer.save()

        self.role = Role.objects.get(team=self.team_french, person=self.translator)
        self.role2 = Role.objects.get(team=self.team_portuguese, person=self.translator)
        self.role_inactive = Role.objects.get(team=self.team_french, person=self.reviewer)
        self.role_limit_date = Role.objects.get(team=self.team_french, person=self.committer)

    def test_inactivating_roles(self):
        # Testing if is_active is True by default
        self.assertTrue(self.role.is_active)
        self.assertTrue(self.role2.is_active)
        self.assertTrue(self.role_limit_date.is_active)
        self.assertTrue(self.role_inactive.is_active)

        Role.inactivate_unused_roles()

        # Getting roles from database after update the unused roles
        self.role = Role.objects.get(team=self.team_french, person=self.translator)
        self.role2 = Role.objects.get(team=self.team_portuguese, person=self.translator)
        self.role_inactive = Role.objects.get(team=self.team_french, person=self.reviewer)
        self.role_limit_date = Role.objects.get(team=self.team_french, person=self.committer)

        self.assertTrue(self.role.is_active)
        self.assertTrue(self.role2.is_active)
        self.assertTrue(self.role_limit_date.is_active)
        self.assertFalse(self.role_inactive.is_active)


class TeamViewsTest(TestCase):

    fixtures = ("test_data.json", )

    def setUp(self):
        self.client = Client()
        self.language = Language.objects.get(pk=1)

    def test_view_team_detail_language_without_team(self):
        self.assertIsNone(self.language.team)
        response = self.client.get(reverse("team_slug", kwargs={"team_slug": self.language.team.name}))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, f"{self.language} Translation Team")
        self.assertNotContains(response, "There is currently no established team for this language.")

    def test_view_team_detail_language_without_team(self):
        Language.objects.create(
            name="Maltese",
            locale="mt",
            plurals="nplurals=4; plural=(n==1 ? 0 : n==0 || ( n%100>1 && n%100<11) ? 1 : (n%100>10 && n%100<20 ) ? 2 : 3)"
        )
        response = self.client.get(reverse("team_slug", kwargs={"team_slug": "mt"}))
        self.assertEqual(response.status_code, 200)
        self.assertNotContains(response, f"{self.language} Translation Team")
        self.assertContains(response, "There is currently no established team for this language.")
