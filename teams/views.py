from typing import Any

from django.contrib.auth.decorators import login_required
from django.http import (
    HttpRequest,
    HttpResponse,
    HttpResponseBadRequest,
    HttpResponseForbidden,
    HttpResponseRedirect,
)
from django.shortcuts import get_object_or_404, redirect, render
from django.urls import reverse
from django.utils.translation import get_language
from django.utils.translation import gettext_lazy as _
from django.views.decorators.http import require_http_methods

from common import utils
from languages.models import Language
from people.models import Person
from teams.forms import EditMemberRoleForm, EditTeamDetailsForm
from teams.models import FakeTeam, Role, Team


def context_data_for_view_get_person_teams_from_request(request: HttpRequest) -> dict[str, Any]:
    """
    Create context data (a dictionary with key and values to give to a template) with the teams of the
    current connected user, if any.

    The resulting dict will contain three keys: user_teams, user_teams_suggestions, user_teams_all.
    """
    language_teams_for_the_current_user = set()
    if request.user.is_authenticated:
        user_teams = Person.get_by_user(request.user).teams.all()
        language_teams_for_the_current_user.update(user_teams)

    language_teams_suggestions_for_the_current_user = set()
    try:
        current_user_language = get_language()
        language_teams_suggestions_for_the_current_user.update(
            set(Team.objects.filter(language__locale=current_user_language).all())
        )
    except Language.DoesNotExist:
        pass

    user_teams = utils.trans_sort_object_list(language_teams_for_the_current_user, "description")
    user_teams_suggestions = utils.trans_sort_object_list(
        language_teams_suggestions_for_the_current_user, "description"
    )
    return {
        "user_teams": user_teams,
        "user_teams_suggestions": user_teams_suggestions,
        "user_teams_all": list(set(user_teams + user_teams_suggestions)),
    }


def teams(request: HttpRequest, format_requested: str = "html") -> HttpResponse:
    all_teams_with_coordinator = Team.objects.all_with_coordinator()

    if format_requested in {"xml", "json"}:
        return render(
            request,
            f"teams/team_list.{format_requested}",
            {"teams": all_teams_with_coordinator},
            content_type=utils.MIME_TYPES[format_requested],
        )

    if format_requested in {
        "html",
    }:
        context = {
            "pageSection": "teams",
            "teams": utils.trans_sort_object_list(all_teams_with_coordinator, "description"),
        }
        context.update(context_data_for_view_get_person_teams_from_request(request))
        return render(request, "teams/team_list.html", context)

    return HttpResponseBadRequest(
        _("The return format type you indicated is not allowed. Allowed values are: html, json, xml.")
    )


def team(request: HttpRequest, team_slug: str) -> HttpResponse:
    try:
        team = Team.objects.get(name=team_slug)
        mem_groups = (
            {
                "id": "committers",
                "title": _("Committers"),
                "members": team.get_committers_exact(),
                "form": None,
                "no_member": _("No committers"),
            },
            {
                "id": "reviewers",
                "title": _("Reviewers"),
                "members": team.get_reviewers_exact(),
                "form": None,
                "no_member": _("No reviewers"),
            },
            {
                "id": "translators",
                "title": _("Translators"),
                "members": team.get_translators_exact(),
                "form": None,
                "no_member": _("No translators"),
            },
            {
                "id": "inactive_members",
                "title": _("Inactive members"),
                "members": team.get_inactive_members(),
                "form": None,
                "no_member": _("No inactive members"),
            },
        )
    except Team.DoesNotExist:
        lang = get_object_or_404(Language, locale=team_slug)
        team = FakeTeam(lang)
        mem_groups = ()

    context = {
        "pageSection": "teams",
        "team": team,
        "can_edit_team": False,
    }
    if team.can_edit(request.user):
        if request.method == "POST":
            form_type = request.POST["form_type"]
            roles = Role.objects.filter(team=team, role=form_type, is_active=True)
            form = EditMemberRoleForm(roles, request.POST)
            if form.is_valid():
                form.save(request)
        # Create forms for template
        commit_roles = Role.objects.filter(team=team, role="committer", is_active=True)
        if commit_roles:
            mem_groups[0]["form"] = EditMemberRoleForm(commit_roles)
        review_roles = Role.objects.filter(team=team, role="reviewer", is_active=True)
        if review_roles:
            mem_groups[1]["form"] = EditMemberRoleForm(review_roles)
        translate_roles = Role.objects.filter(team=team, role="translator", is_active=True)
        if translate_roles:
            mem_groups[2]["form"] = EditMemberRoleForm(translate_roles)
        context["can_edit_team"] = True

    context["mem_groups"] = mem_groups
    context["can_edit_details"] = context["can_edit_team"] or utils.is_site_admin(request.user)
    if isinstance(team, Team):
        context["user_is_member"] = (
            request.user.is_authenticated
            and isinstance(team, Team)
            and Role.objects.filter(person=request.user, team=team).exists()
        )
    return render(request, "teams/team_detail.html", context)


def team_edit(request: HttpRequest, team_slug: str) -> HttpResponse:
    team = get_object_or_404(Team, name=team_slug)
    if not (team.can_edit(request.user) or utils.is_site_admin(request.user)):
        return HttpResponseForbidden("You are not allowed to edit this team.")
    form = EditTeamDetailsForm(request.user, request.POST or None, instance=team)
    if request.method == "POST":
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse("team_slug", args=[team_slug]))
    context = {"team": team, "form": form}
    return render(request, "teams/team_edit.html", context)


@require_http_methods(["POST"])
@login_required
def connected_user_join_team(request: HttpRequest, team_slug: str) -> HttpResponseRedirect:
    person = get_object_or_404(Person, username=request.user.username)
    target_team = get_object_or_404(Team, name=team_slug)

    # Check if the user is already part of the team
    if Role.objects.filter(team=target_team, person=person).exists():
        return redirect("team_slug", team_slug=team_slug)

    # Create a new role for the user in the team
    role = Role(team=target_team, person=person)
    role.save()
    return redirect("team_slug", team_slug=team_slug)


@require_http_methods(["POST"])
@login_required
def connected_user_leave_team(request: HttpRequest, team_slug: str) -> HttpResponseRedirect:
    person = get_object_or_404(Person, username=request.user.username)
    target_team = get_object_or_404(Team, name=team_slug)
    role = get_object_or_404(Role, team=target_team, person=person)
    role.delete()
    return redirect("team_slug", team_slug=team_slug)
