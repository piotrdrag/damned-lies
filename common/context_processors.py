from django.conf import settings


def utils(request):
    return {
        "damned_lies_bug_url": settings.DAMNED_LIES_BUG_URL,
        "internationalisation_team_bug_url": settings.INTERNATIONALISATION_TEAM_BUG_URL,
    }
