import operator
from datetime import timedelta
from unittest import TestCase, skipUnless
from unittest.mock import MagicMock, patch

from django.conf import settings
from django.core.management import call_command
from django.test import TestCase as DjangoTestCase
from django.urls import reverse
from django.utils import timezone, translation

from people.models import Person
from teams.models import Role, Team

from .utils import lc_sorted, pyicu_present, trans_sort_object_list


class CommonTest(DjangoTestCase):
    def test_admin_login(self):
        response = self.client.get(reverse("admin:index"))
        self.assertRedirects(response, reverse("admin:login") + "?next=" + reverse("admin:index"))

    def test_help_pages(self):
        response = self.client.get(reverse("help", args=["reduced_po", 1]))
        self.assertContains(response, '<div class="modal-body">')

        response = self.client.get(reverse("help", args=["reduced_po", 0]))
        self.assertNotContains(response, '<div class="modal-body">')

        response = self.client.get(reverse("help", args=["reduced_po"]))
        self.assertNotContains(response, '<div class="modal-body">')

        response = self.client.get(reverse("help", args=["unexisting", 1]))
        self.assertEqual(response.status_code, 404)

    def test_house_keeping(self):
        pn = Person.objects.create(
            first_name="John",
            last_name="Note",
            email="jn@devnull.com",
            username="jn",
            activation_key="a_activation_key",
        )
        pn.set_password("password")

        # Unactivated person
        Person.objects.create(
            first_name="John",
            last_name="Reviewer",
            username="jr",
            date_joined=timezone.now() - timedelta(days=11),
            activation_key="non-activated-key",
        )

        inactive = Person.objects.create(
            first_name="John",
            last_name="Translator",
            username="jt",
            last_login=timezone.now() - timedelta(days=30 * 6 + 1),
        )

        t1 = Team.objects.create(name="fr", description="French")
        t2 = Team.objects.create(name="fr2", description="French")

        Role.objects.create(team=t1, person=inactive)
        Role.objects.create(team=t2, person=inactive)

        response = self.client.get("/register/activate/a_activation_key", follow=True)
        self.assertContains(response, "Your account has been activated.")

        call_command("run-maintenance")

        # Testing if the non-activated accounts were deleted
        self.assertTrue(Person.objects.filter(last_name="Note").exists())
        self.assertFalse(Person.objects.filter(last_name="Reviewer").exists())

        # Testing if the inactive roles were updated
        jt = Person.objects.get(first_name="John", last_name="Translator")
        for role in Role.objects.filter(person=jt):
            self.assertFalse(role.is_active)

    def test_pull_code(self):
        push_data = {
            "object_kind": "push",
            "ref": "refs/heads/master",
            # GitLab will send a lot more, but we don't care.
        }
        with patch("common.views.run_shell_command", return_value=(0, "", "")) as rsc_patched:
            with patch("common.views.call_command") as cc_patched:
                response = self.client.post("/pull_code/", data=push_data)
                self.assertEqual(response.status_code, 403)
                response = self.client.post(
                    "/pull_code/",
                    data=push_data,
                    HTTP_X_GITLAB_EVENT="Push Hook",
                    HTTP_X_GITLAB_TOKEN=settings.GITLAB_TOKEN,
                )
        self.assertEqual(rsc_patched.call_count, 2)
        self.assertEqual(cc_patched.call_count, 1)
        self.assertEqual(response.content, b"OK")


class LcSortedTest(TestCase):
    @skipUnless(pyicu_present, "PyICU package is required for this test")
    def test_lc_sorted_en_case_insensitive(self):
        with translation.override("en"):
            self.assertEqual(["A", "b", "C", "z"], lc_sorted(["z", "C", "b", "A"]))

    @skipUnless(pyicu_present, "PyICU package is required for this test")
    def test_lc_sorted_fr_eacute_between_d_and_f(self):
        with translation.override("fr"):
            self.assertEqual(["d", "é", "f", "z"], lc_sorted(["é", "z", "d", "f"]))

    @skipUnless(pyicu_present, "PyICU package is required for this test")
    def test_lc_sorted_sv_ouml_after_z(self):
        with translation.override("sv"):
            self.assertEqual(["a", "o", "z", "ö"], lc_sorted(["z", "ö", "o", "a"]))

    def test_lc_sorted_with_custom_key(self):
        mykey = operator.itemgetter(1)
        with translation.override("fr"):
            self.assertEqual([("z", "a"), ("a", "z")], lc_sorted([("a", "z"), ("z", "a")], key=mykey))


class TransSortObjectListTest(TestCase):
    def make_item(self, name):
        item = MagicMock(name=name)
        item.name = name
        return item

    @patch("common.utils._", return_value="bar")
    def test_translate_object_name(self, gettext):
        """
        The given field is translated using gettext and the translation is stored
        in translated_name.
        """
        item = self.make_item("foo")
        trans_sort_object_list([item], "name")
        self.assertEqual("bar", item.translated_name)

    @patch("common.utils._", side_effect=lambda s: s[::-1])
    def test_sort_list_according_to_translation(self, _):
        """
        The list is sorted according to the computed translated_name
        (here gettext reverses the string so foo (oof) comes before bar (rab)).
        """
        item_foo, item_bar = self.make_item("foo_item"), self.make_item("bar_item")
        actual = trans_sort_object_list([item_foo, item_bar], "name")
        self.assertEqual([item_foo, item_bar], actual, "wrong order")

    @skipUnless(pyicu_present, "PyICU package is required for this test")
    @patch("common.utils._", side_effect=lambda x: x)
    def test_uses_localized_ordering(self, _):
        """Consider the current locale when ordering elements."""
        d, eacute, f = self.make_item("d"), self.make_item("é"), self.make_item("f")
        with translation.override("fr"):
            actual = trans_sort_object_list([f, eacute, d], "name")
            self.assertEqual([d, eacute, f], actual)


class IndexViewsTest(DjangoTestCase):
    fixtures = ("test_data.json",)

    def test_join_team_suggestion_text_exists_on_index_page_if_user_not_authenticated(self):
        response = self.client.get(reverse("home"))
        self.assertFalse(response.context.get("user").is_authenticated)
        self.assertContains(response, "create-account-for-unknown-user")

    def test_join_team_suggestion_text_exists_on_index_page_if_user_authenticated_but_no_team(self):
        john = Person.objects.get(username="john")
        john.teams.clear()
        self.client.login(username="john", password="john")
        response = self.client.get(reverse("home"))
        self.assertTrue(response.context.get("user").is_authenticated)
        self.assertContains(response, "create-account-for-unknown-user")

    def test_join_team_suggestion_text_not_exist_because_user_authenticated_and_has_teams(self):
        self.client.login(username="john", password="john")
        response = self.client.get(reverse("home"))
        self.assertTrue(response.context.get("user").is_authenticated)
        self.assertNotContains(response, "create-account-for-unknown-user")

    def test_team_suggestion_for_non_authenticated_user_in_french(self):
        self.client.cookies.load({settings.LANGUAGE_COOKIE_NAME: "fr"})
        john = Person.objects.get(username="john")
        john.teams.clear()
        response = self.client.get(reverse("home"))
        self.assertFalse(response.context.get("user").is_authenticated)
        self.assertContains(response, "team-suggestions-title")
        self.assertContains(response, "team-info-base-fr-suggestion")
        self.assertNotContains(response, "member-of-team-fr")
        self.assertContains(response, "suggestion-for-team-fr")
        self.assertNotContains(response, "team-info-base-it-suggestion")
        self.assertNotContains(response, "member-of-team-it")
        self.assertNotContains(response, "suggestion-for-team-it")
        self.assertNotContains(response, "team-info-base-po-suggestion")
        self.assertNotContains(response, "member-of-team-po")
        self.assertNotContains(response, "suggestion-for-team-po")

    def test_team_suggestion_for_authenticated_user_in_french(self):
        self.client.cookies.load({settings.LANGUAGE_COOKIE_NAME: "fr"})
        self.client.login(username="john", password="john")
        john = Person.objects.get(username="john")
        john.teams.clear()
        response = self.client.get(reverse("home"))
        self.assertTrue(response.context.get("user").is_authenticated)
        self.assertContains(response, "team-suggestions-title")
        self.assertContains(response, "team-info-base-fr-suggestion")
        self.assertNotContains(response, "member-of-team-fr")
        self.assertContains(response, "suggestion-for-team-fr")
        self.assertNotContains(response, "team-info-base-it-suggestion")
        self.assertNotContains(response, "member-of-team-it")
        self.assertNotContains(response, "suggestion-for-team-it")
        self.assertNotContains(response, "team-info-base-po-suggestion")
        self.assertNotContains(response, "member-of-team-po")
        self.assertNotContains(response, "suggestion-for-team-po")

    def test_team_suggestion_because_user_is_already_member_of_french_team_plus_italian_suggestion(self):
        self.client.cookies.load({settings.LANGUAGE_COOKIE_NAME: "it"})
        self.client.login(username="john", password="john")
        john = Person.objects.get(username="john")
        john.teams.clear()
        Role.objects.create(team=Team.objects.get(name="fr"), person=john, is_active=True, role="translator")
        response = self.client.get(reverse("home"))
        self.assertTrue(response.context.get("user").is_authenticated)
        self.assertContains(response, "team-suggestions-title")
        self.assertContains(response, "team-info-base-fr-suggestion")
        self.assertContains(response, "member-of-team-fr")
        self.assertNotContains(response, "suggestion-for-team-fr")
        self.assertContains(response, "team-info-base-it-suggestion")
        self.assertNotContains(response, "member-of-team-it")
        self.assertContains(response, "suggestion-for-team-it")
        self.assertNotContains(response, "team-info-base-po-suggestion")
        self.assertNotContains(response, "member-of-team-po")
        self.assertNotContains(response, "suggestion-for-team-po")
