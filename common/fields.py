# Unused since April 2021
# Keep this file until migrations are squashed and don't refer any more to these fields

# From: https://djangosnippets.org/snippets/1979/
import json

from django import forms
from django.core import exceptions
from django.core.serializers.json import DjangoJSONEncoder
from django.db import models


class DictionaryField(models.Field):
    description = "Dictionary object"

    def get_internal_type(self):  # noqa: PLR6301
        return "TextField"

    def from_db_value(self, value, *args):
        if value is None:
            return None
        if not value:
            return {}
        if isinstance(value, str):
            try:
                return dict(json.loads(value))
            except (ValueError, TypeError) as exc:
                raise exceptions.ValidationError(self.error_messages["invalid"]) from exc

        if isinstance(value, dict):
            return value
        return {}

    def get_prep_value(self, value):  # noqa: PLR6301
        if not value:
            return ""
        if isinstance(value, str):
            return value
        return json.dumps(value)

    def value_to_string(self, obj):
        value = self._get_val_from_obj(obj)
        return self.get_prep_value(value)

    def clean(self, value, model_instance):
        value = super().clean(value, model_instance)
        return self.get_prep_value(value)

    def formfield(self, **kwargs):
        defaults = {"widget": forms.Textarea}
        defaults.update(kwargs)
        return super().formfield(**defaults)


# From https://djangosnippets.org/snippets/1478/


class JSONField(models.TextField):
    """JSONField is a generic textfield that neatly serializes/unserializes
    JSON objects seamlessly"""

    def from_db_value(self, value, *args):
        """Convert our string value to JSON after we load it from the DB"""

        if not value:
            return None

        try:
            if isinstance(value, str):
                return json.loads(value)
        except ValueError:
            pass

        return value

    def get_db_prep_save(self, value, connection):
        """Convert our JSON object to a string before we save"""

        if not value:
            return None

        if isinstance(value, dict | list):
            value = json.dumps(value, cls=DjangoJSONEncoder)

        return super().get_db_prep_save(value, connection=connection)

    def value_to_string(self, obj):
        return json.dumps(self._get_val_from_obj(obj), cls=DjangoJSONEncoder)
