from smtplib import SMTPException
from threading import Thread

from django.conf import settings
from django.contrib import messages
from django.contrib.auth.views import LoginView as AuthLoginView
from django.core.management import call_command
from django.http import (
    Http404,
    HttpResponse,
    HttpResponseForbidden,
    HttpResponseRedirect,
)
from django.shortcuts import render
from django.template.loader import TemplateDoesNotExist, get_template
from django.urls import reverse
from django.utils.translation import gettext as _
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.debug import sensitive_variables

from people.forms import LoginForm, RegistrationForm
from people.models import Person, obfuscate_email
from teams.models import Role
from teams.views import context_data_for_view_get_person_teams_from_request

from common.utils import check_gitlab_request, run_shell_command


def index(request):
    """Homepage view"""
    context = {"pageSection": "home"}
    context.update(context_data_for_view_get_person_teams_from_request(request))
    return render(request, "index.html", context)


def about(request):
    translator_credits = _("translator-credits")
    if translator_credits == "translator-credits":
        translator_credits = ""
    else:
        translator_credits = [obfuscate_email(line) for line in translator_credits.split("\n")]
    context = {
        "pageSection": "home",
        "translator_credits": translator_credits,
    }
    if hasattr(settings, "VERSION_LONG_HASH"):
        context["damned_lies_version_string"] = settings.VERSION_LONG_HASH[0:10]
        context["damned_lies_version_url"] = f"{settings.DAMNED_LIES_COMMIT_BASE_URL}/{settings.VERSION_LONG_HASH}"

    return render(request, "about.html", context)


class LoginView(AuthLoginView):
    form_class = LoginForm
    redirect_field_name = "referer"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if not context["referer"]:
            context["referer"] = self.request.META.get("HTTP_REFERER", "")
        return context

    def get_redirect_url(self):
        url = super().get_redirect_url()
        if not url:
            url = self.request.META.get("HTTP_REFERER", "")
        return url

    def form_valid(self, form):
        response = super().form_valid(form)
        if Role.objects.filter(person__username=self.request.user.username).count() < 1:
            message = _(
                "You have not joined any translation team yet. "
                'You can do it from <a href="%(url)s">your profile</a>.'
            ) % {
                "url": reverse("person_team_join"),
            }
            messages.info(self.request, message)
        return response

    def form_invalid(self, form):
        messages.error(self.request, _("Login unsuccessful. Please verify your username and password."))
        return super().form_invalid(form)


@sensitive_variables("password1", "password2")
def site_register(request):
    if request.method == "POST":
        form = RegistrationForm(data=request.POST)
        if form.is_valid():
            try:
                form.save(request)
            except SMTPException as exc:
                messages.error(
                    request,
                    _("An error occurred while sending mail to %s (%s)") % form.cleaned_data["email"],
                    str(exc),
                )
            else:
                return HttpResponseRedirect(reverse("register_success"))
    else:
        form = RegistrationForm()
    context = {
        "pageSection": "home",
        "form": form,
    }
    return render(request, "registration/register.html", context)


def activate_account(request, key):
    """Activate an account through the link a requestor has received by email"""
    try:
        person = Person.objects.get(activation_key=key)
    except Person.DoesNotExist:
        return render(request, "error.html", {"error": _("Sorry, the key you provided is not valid.")})
    person.activate()
    messages.success(request, _("Your account has been activated."))
    return HttpResponseRedirect(reverse("login"))


def help_view(request, topic, modal=None):
    template = "help/%s.html" % topic
    try:
        get_template(template)
    except TemplateDoesNotExist as exc:
        raise Http404 from exc
    return render(request, template, {"base": "base_modal.html" if modal and int(modal) else "base.html"})


# CSRF skipped, verification using a secret token.
@csrf_exempt
def pull_code(request):
    """GitLab Webhook endpoint to update code after a repository push."""
    verified = check_gitlab_request(request)
    if not verified:
        return HttpResponseForbidden()

    # Run effective work in a separate thread to prevent timeouts
    thread = Thread(target=pull_code_real)
    thread.start()
    return HttpResponse("OK")


def pull_code_real():
    cwd = settings.BASE_DIR / "damnedlies"
    run_shell_command(["git", "pull", "--rebase"], cwd=cwd)
    call_command("compile-trans", verbosity=0)
    run_shell_command(["touch", "wsgi.py"], cwd=cwd)
