On `master`:
[![pipeline status](https://gitlab.gnome.org/Infrastructure/damned-lies/badges/master/pipeline.svg)](https://gitlab.gnome.org/Infrastructure/damned-lies/-/commits/master)
[![coverage report](https://gitlab.gnome.org/Infrastructure/damned-lies/badges/master/coverage.svg)](https://gitlab.gnome.org/Infrastructure/damned-lies/-/commits/master)

On `develop`:
[![pipeline status](https://gitlab.gnome.org/Infrastructure/damned-lies/badges/develop/pipeline.svg)](https://gitlab.gnome.org/Infrastructure/damned-lies/-/commits/develop)
[![coverage report](https://gitlab.gnome.org/Infrastructure/damned-lies/badges/develop/coverage.svg)](https://gitlab.gnome.org/Infrastructure/damned-lies/-/commits/develop)

# `damned-lies`

`damned-lies` is a **translation-tracking Web application** originally aimed to help the GNOME Translation project. It
is written in Python and is based on the [Django](https://www.djangoproject.com) framework.

## Contribute and install

Please, refer to the [Contribution guide](CONTRIBUTING.md) in order to know how to install `damned-lies` in a
development environment and feel free to contact us on our [Matrix channel](https://matrix.to/#/#i18n:gnome.org).

## Documentation

We provide **two different documentation**.

If you wish to help developing Damned Lies, please refer to the [code documentation that is hosted on GitLab Pages](https://infrastructure.pages.gitlab.gnome.org/damned-lies). It contains API documentation, as well as the REST API endpoints documentation. Still, if you’re a developer, you might find useful documentation on the [project Wiki](../../wikis).

## Production environments

Damned Lies is deployed in production through a CI/CD process, automated by GitLab. We provide three different environments to ensure the changes do not affect the production instance.

| **Environment** | `testing`                                                             | `staging`                                                                                                                                                       | `production`                                                              |
|------------------|-----------------------------------------------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------|---------------------------------------------------------------------------|
| **URL**          | https://damned-lies-testing.apps.openshift4.gnome.org/                | https://damned-lies-staging.apps.openshift4.gnome.org/                                                                                                          | https://l10n.gnome.org/                                                   |
| **Branch**       | `develop`                                                             | `master`                                                                                                                                                        | `master`                                                                  |
| **Image**        | [`gnome_infrastructure/damned-lies:testing`](https://quay.io/repository/gnome_infrastructure/damned-lies?tab=tags) | [`gnome_infrastructure/damned-lies:staging`](https://quay.io/repository/gnome_infrastructure/damned-lies?tab=tags)                                              | [`gnome_infrastructure/damned-lies:latest`](https://quay.io/repository/gnome_infrastructure/damned-lies?tab=tags) |
| **Description**  | On-going changes. Will be included in `master` after a merge request. | Validated changes, waiting to be included in production. Time to check that the changes do not generate any regression and are compatible with production data. | Triggered manually after the deployment to `staging` if everything is ok. |

### Logs

We use a [Splunk instance hosted at GNOME to monitor the logs of the production system](https://splunk.gnome.org/app/search/damned_lies_production_logs). If you’re a developer and wish to access it, please open an issue.
