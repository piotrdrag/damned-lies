from typing import TYPE_CHECKING, ClassVar, Union

from django.conf import settings
from django.db import models
from django.db.models import Q
from django.http import HttpRequest
from django.urls import NoReverseMatch, reverse
from django.utils.translation import gettext as _

from damnedlies import logger
from teams.models import FakeTeam, Team

if TYPE_CHECKING:
    from stats.models import Statistics


class NoLanguageFoundError(Exception):
    """
    When no language is found in a lookup.
    """


class Language(models.Model):
    """
    The representation of a language in the system.
    It is intended to be associated to translations.
    """

    name = models.CharField(max_length=50, unique=True)
    locale = models.CharField(max_length=15, unique=True)
    team = models.ForeignKey(Team, null=True, blank=True, default=None, on_delete=models.SET_NULL)
    plurals = models.CharField(max_length=200, blank=True)

    # Mapping for code differences between GNOME and Django standards.
    lang_mapping: ClassVar[dict[str, str]] = {
        "zh_CN": "zh_Hans",
        "zh_TW": "zh_Hant",
    }

    class Meta:
        db_table = "language"
        ordering = ("name",)

    def __str__(self: "Language") -> str:
        return f"{self.name} ({self.locale})"

    @classmethod
    def django_locale(cls: "Language", locale: str) -> str:
        return cls.lang_mapping.get(locale, locale)

    @classmethod
    def get_language_from_iana_code(cls: "Language", iana_language_code: str) -> "Language":
        """
        Return a matching Language object corresponding to LANGUAGE_CODE (BCP47-formatted).
        This converts BCP47 to ISO639 conversion function

        :raises NoLanguageFoundError: when no language corresponding to the IANA language code is found.

        .. seealso: `IANA Language Subtag Registry <https://www.iana.org/assignments/language-subtag-registry/language-subtag-registry>_`

        """
        iana_split = iana_language_code.split("-", 1)
        language_code, iana_suffix = iana_split[0], iana_split[1] if len(iana_split) > 1 else ""
        iana_suffix = iana_suffix.replace("Latn", "latin").replace("Cyrl", "cyrillic")

        candidate_languages = cls.objects.filter(Q(locale=language_code) | Q(locale__regex=rf"^{language_code}[@_].+"))

        if len(candidate_languages) == 0:
            raise NoLanguageFoundError(
                _("No corresponding Language was found for this IANA BCP47 code (%s).") % iana_language_code
            )

        best_candidate_language = candidate_languages[0]
        if len(candidate_languages) > 1:
            # Find the best language to return
            for candidate_language in candidate_languages:
                if candidate_language.get_suffix():
                    if iana_suffix.lower() == candidate_language.get_suffix().lower():
                        best_candidate_language = candidate_language
                        break
                else:
                    best_candidate_language = candidate_language

        return best_candidate_language

    def get_name(self: "Language") -> str:
        """
        Get the locale of the Language object, and if it differs from the locale,
        translate it.
        """
        if self.name != self.locale:
            return _(self.name)
        return self.locale

    def get_suffix(self: "Language") -> str:
        """
        Get the locale suffix, for instance ‘BR’ for ‘pt_BR’ or ‘CA’ for ‘fr_CA’.
        """
        split_locale = self.locale.replace("@", "_").split("_")
        if len(split_locale) > 1:
            return split_locale[-1]
        return ""

    def get_plurals(self: "Language") -> str:
        """
        Get the plural form of the language. If none, return ‘Unknown’, translated for the user.
        """
        # Translators: this concerns an unknown plural form
        return self.plurals or _("Unknown")

    def bugs_url_enter(self: "Language") -> str:
        return settings.ENTER_LANGUAGE_BUG_URL % {"locale": self.locale}

    def bugs_url_show(self: "Language") -> str:
        return settings.BROWSE_LANGUAGE_BUG_URL % {"locale": self.locale}

    def get_team_url(self: "Language") -> str:
        if self.team:
            return self.team.get_absolute_url()
        try:
            return FakeTeam(self).get_absolute_url()
        except NoReverseMatch:
            return ""

    def get_release_statistics_in_a_given_language(self) -> list["Statistics"]:
        # Imported here to avoid cyclic import
        from stats.models import get_release_statistics_in_a_given_language  # noqa: PLC0415

        return get_release_statistics_in_a_given_language(self, archives=False)

    def get_absolute_url(self: "Language") -> str:
        """
        Get URL of the Language teams associated to the current language, otherwise
        the default language view.
        """
        if self.team:
            return reverse("team_slug", args=[self.team.name])
        return reverse("languages")


def get_user_language_from_request(request: "HttpRequest") -> Union["Language", None]:
    """
    Get the Language, and so locale that is active for a given Django request.
    """
    current_language = None

    try:
        current_language = Language.get_language_from_iana_code(request.LANGUAGE_CODE)
    except NoLanguageFoundError as no_language_found_error:
        logger.debug(no_language_found_error)

    if current_language and current_language.locale == "en":
        current_language = None

    return current_language
