from django.urls import path

from languages import views
from teams import views as team_views

urlpatterns = [
    path("", views.languages, name="languages"),
    path("<locale:locale>/all/<slug:domain_type>/", views.language_all, name="language_all"),
    path("<locale:locale>/<name:release_name>/<slug:domain_type>/", views.language_release, name="language_release"),
    path(
        "<locale:locale>/<name:release_name>/<slug:domain_type>.tar.gz",
        views.language_release_tar,
        name="language_release_tar",
    ),
    path("<locale:team_slug>/", team_views.team),
    # Ajax URLs
    path("<locale:locale>/rel-archives/", views.ajax_release_archives, name="language_release_archives"),
]
