import logging
from datetime import datetime

logger = logging.getLogger("damnedlies")
__version__ = datetime.now().strftime("%Y%m%d%H%M")
