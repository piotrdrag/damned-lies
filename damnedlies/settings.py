# Django settings for djamnedlies project.
import os
from pathlib import Path

from django.conf import global_settings

gettext_noop = lambda s: s

DEBUG = True
STATIC_SERVE = True
USE_DEBUG_TOOLBAR = False

BASE_DIR = Path(__file__).resolve().parent.parent

ADMINS = (("Your Name", "your_address@example.org"),)

DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.sqlite3",
        "NAME": BASE_DIR / "database.db",
    }
}
# Please refer to the README file to create an UTF-8 database with MySQL.
DEFAULT_AUTO_FIELD = "django.db.models.AutoField"

EMAIL_HOST = "localhost"
EMAIL_HOST_USER = ""
EMAIL_HOST_PASSWORD = ""
EMAIL_SUBJECT_PREFIX = "[Damned Lies] "
EMAIL_HEADER_NAME = "X-Vertimus"
DEFAULT_FROM_EMAIL = "gnomeweb@gnome.org"
SERVER_EMAIL = "gnomeweb@gnome.org"
# When in STRINGFREEZE, where to send notifications (gnome-i18n@gnome.org) on any POT changes
NOTIFICATIONS_TO = ["gnome-i18n@gnome.org"]

DAMNED_LIES_BASE_PROJECT_URL = "https://gitlab.gnome.org/Infrastructure/damned-lies"
DAMNED_LIES_BUG_URL = f"{DAMNED_LIES_BASE_PROJECT_URL}/-/issues/new"
DAMNED_LIES_COMMIT_BASE_URL = f"{DAMNED_LIES_BASE_PROJECT_URL}/-/commit"
INTERNATIONALISATION_TEAM_BUG_URL = DAMNED_LIES_BUG_URL

ENTER_LANGUAGE_BUG_URL = "https://gitlab.gnome.org/Teams/Translation/%(locale)s/issues/new"
BROWSE_LANGUAGE_BUG_URL = "https://gitlab.gnome.org/Teams/Translation/%(locale)s/issues?state=opened"

# Local time zone for this installation. Choices can be found here:
# https://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# If running in a Windows environment this must be set to the same as your
# system time zone.
TIME_ZONE = "Europe/Zurich"

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = "en"
LANGUAGES = global_settings.LANGUAGES + [
    # Add here languages with translations for D-L but not for Django
    ("fur", gettext_noop("Friulian")),
    ("gu", gettext_noop("Gujarati")),
    ("ku", gettext_noop("Kurdish")),
]

USE_I18N = True
LOCALE_PATHS = [BASE_DIR / "locale"]

USE_TZ = True

# Absolute path to the directory that holds media.
MEDIA_ROOT = BASE_DIR / "media"

# URL that handles the media served from MEDIA_ROOT.
MEDIA_URL = "/media/"

STATIC_ROOT = BASE_DIR / "static"

STATIC_URL = "/static/"

# Local directory path for VCS checkout
SCRATCHDIR = Path("scratch")
POT_DIR = SCRATCHDIR / "POT"
LOG_DIR = SCRATCHDIR / "logs"

# By default, Django stores files locally, using the MEDIA_ROOT and MEDIA_URL settings
UPLOAD_DIR = "upload"
UPLOAD_ARCHIVED_DIR = "upload-archived"
FILE_UPLOAD_PERMISSIONS = 0o600

LOGIN_URL = "/login/"

# Make this unique, and don't share it with anybody.
# To be filled in local_settings.py
SECRET_KEY = ""

TEMPLATES = [
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "DIRS": [BASE_DIR / "templates"],
        "APP_DIRS": True,
        "OPTIONS": {
            "context_processors": [
                # Default list:
                "django.contrib.auth.context_processors.auth",
                "django.template.context_processors.debug",
                "django.template.context_processors.i18n",
                "django.template.context_processors.media",
                "django.template.context_processors.static",
                "django.template.context_processors.tz",
                "django.contrib.messages.context_processors.messages",
                # Added processors:
                "django.template.context_processors.request",
                "common.context_processors.utils",
                "damnedlies.context_processors.running_integration_instance",
            ],
            "builtins": [
                "django.templatetags.i18n",
                "django.templatetags.static",
            ],
        },
    },
]

MIDDLEWARE = [
    "allow_cidr.middleware.AllowCIDRMiddleware",
    "django.contrib.sessions.middleware.SessionMiddleware",
    "django.middleware.locale.LocaleMiddleware",
    "django.middleware.common.CommonMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "common.middleware.TokenAuthenticationMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "django.middleware.clickjacking.XFrameOptionsMiddleware",
]

ATOMIC_REQUESTS = True

ROOT_URLCONF = "damnedlies.urls"

INSTALLED_APPS = [
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.sessions",
    "django.contrib.admin",
    "django.contrib.humanize",
    "django.contrib.messages",
    "django.contrib.staticfiles",
    "common",
    "languages",
    "people",
    "stats",
    "teams",
    "vertimus",
    "feeds",
    "api",
    "bootstrap5",
]


INTERNAL_IPS = ("127.0.0.1",)

SITE_DOMAIN = "l10n.gnome.org"

LOGIN_REDIRECT_URL = "/"

# Members of this group can edit all team's details and change team coordinatorship
ADMIN_GROUP = ""

# https://www.gnu.org/software/gettext/manual/html_node/Preparing-ITS-Rules.html#Preparing-ITS-Rules
GETTEXT_ITS_DATA = {
    "gtk": [os.path.join("tools", "gtk4builder.its"), os.path.join("tools", "gtk4builder.loc")],
    "glib": [os.path.join("gio", "gschema.its"), os.path.join("gio", "gschema.loc")],
    "gnome-control-center": [
        os.path.join("gettext", "its", "gnome-keybindings.its"),
        os.path.join("gettext", "its", "gnome-keybindings.loc"),
    ],
    "polkit": [
        os.path.join("gettext", "its", "polkit.its"),
        os.path.join("gettext", "its", "polkit.loc"),
    ],
    "shared-mime-info": [
        os.path.join("data", "its", "shared-mime-info.its"),
        os.path.join("data", "its", "shared-mime-info.loc"),
    ],
    # Copy of https://github.com/ximion/appstream/tree/master/data/its (2023-11-29)
    # minus the release/description line (#149).
    "damned-lies": [
        os.path.join("common", "static", "metainfo.its"),
        os.path.join("common", "static", "metainfo.loc"),
    ],
}

GNOME_GITLAB_DOMAIN_NAME = "gitlab.gnome.org"
# TODO: remove GITLAB_TOKEN in favor of GITLAB_API_TOKENS
GITLAB_TOKEN = "fill_with_real_token"
GITLAB_API_TOKENS = {"gitlab.gnome.org": "fill_with_real_token"}
# String freeze breaks are sent to a GitLab instance through the API
GITLAB_COORDINATION_TEAMS_URL = GNOME_GITLAB_DOMAIN_NAME
GITLAB_COORDINATION_TEAMS_PROJECT = "PATH/TO/GITLAB/COORDINATION/PROJECT"
GNOME_GITLAB_USER_URL = f"https://{GNOME_GITLAB_DOMAIN_NAME}"

# GNOME Forum is a Discourse instance
GNOME_FORUM_DOMAIN_NAME = "discourse.gnome.org"
GNOME_FORUM_USER_URL = f"https://{GNOME_FORUM_DOMAIN_NAME}/u"

LOCK_DIR = Path("/var/lock")

try:
    from .local_settings import *
except ImportError:
    pass

LOG_DIR.mkdir(parents=True, exist_ok=True)

LOGGING = {
    "version": 1,
    "disable_existing_loggers": False,
    "formatters": {
        "base": {
            "format": "%(asctime)s - PID:%(process)d - %(name)s - %(levelname)s - %(funcName)s in %(module)s: %(message)s",
            "style": "%",
        },
    },
    "handlers": {
        "logger_console": {"class": "logging.StreamHandler", "level": "INFO", "formatter": "base"},
        "mail_admins": {
            "level": "CRITICAL",
            "class": "django.utils.log.AdminEmailHandler",
            "email_backend": "django.core.mail.backends.filebased.EmailBackend",
            "formatter": "base",
        },
        "logger_file": {
            "level": "DEBUG",
            "class": "logging.handlers.TimedRotatingFileHandler",
            "filename": f"{LOG_DIR}/damned-lies.log",
            "formatter": "base",
            "encoding": "utf-8",
            "when": "midnight",
            "backupCount": 60
        },
    },
    "loggers": {
        "django": {"handlers": ["logger_file"], "propagate": True},
        "django.request": {"handlers": ["logger_console", "mail_admins"], "propagate": True},
        "django.server": {"handlers": ["logger_console", "logger_file"], "propagate": True},
        "damnedlies": {"handlers": ["logger_file", "logger_console"], "level": "DEBUG", "propagate": True},
    },
}

# When running in development mode, print emails on stdout instead of trying
# to send them
if DEBUG:
    EMAIL_BACKEND = "django.core.mail.backends.console.EmailBackend"

if USE_DEBUG_TOOLBAR:
    MIDDLEWARE.insert(0, "debug_toolbar.middleware.DebugToolbarMiddleware")
    INSTALLED_APPS.append("debug_toolbar")
