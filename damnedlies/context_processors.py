def running_integration_instance(request):
    running_production_system = False
    try:
        from django.conf import settings

        if settings.RUNNING_PRODUCTION_SYSTEM and isinstance(settings.RUNNING_PRODUCTION_SYSTEM, bool):
            running_production_system = settings.RUNNING_PRODUCTION_SYSTEM
    except (ImportError, AttributeError):
        pass
    return {
        "RUNNING_PRODUCTION_SYSTEM": running_production_system,
    }
