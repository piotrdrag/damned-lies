<!--
⚠️ This template is for modules that are part of GNOME Core, Incubator, or Circle.
Addition to Damned Lies comes as a perk for members of this group.
For other apps, file a request here:
https://gitlab.gnome.org/Teams/Translation/Coordination/-/issues/new?issuable_template=Add%20app%20to%20Damned%20Lies

Use the following format for your issue title:

Add module-name to Damned Lies
-->

<!-- Edit this line with the appropriate information -->
module-name is now part of Core/Incubator/Circle ([reference]()).

GitLab project: <!-- URL here -->

* [ ] Request is made or approved by one of the maintainers
* [ ] `@translations` has been given commit access
* [ ] Ensure the `.doap` file in your project [matches the requirements](https://wiki.gnome.org/Git/FAQ).

## Branching

Our development branch is `main`/`master`.
We don’t have stable branches at this point.
Our latest stable branch is `module-42`.

## Expectations

* [ ] Strings follow the [Human Interface Guidelines on Typography](https://developer.gnome.org/hig/guidelines/typography.html)

⚠️ Developers should *never* touch po files.
If changes are made to strings, they should only be made in source code.
Tooling will apply the changes to po files, marking strings as fuzzy or untranslated as necessary.
Translators will then review them and make the appropriate changes.

⚠️ Translations must only come from Damned Lies.
Merge requests must no longer be accepted.
Developers must not pull translations from other platforms either.
Damned Lies as the sole translation platform for the project ensures no work is duplicated and no effort is wasted.

ℹ️ Releases are announced in advance on [Discourse](https://discourse.gnome.org/) (App category, i18n tag).
A period of around two weeks is generally considered good.
This ensures translators know when it’s the right time to focus on the app, and have time to do the work.
We recommend you refrain from making any changes to strings at this point until after the release, but fixing issues with strings themselves is probably still a good idea, as problematic strings will make translation work more difficult if not impossible.

* [ ] I have read the above expectations and will do my best to meet them
