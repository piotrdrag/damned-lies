**Please read this!**

Before opening a new issue, make sure to search for keywords in the issues filtered by the ~"l10n.gnome.org" label, and check the issue you’re about to submit isn’t a duplicate. Remove this notice if you’re confident your issue isn’t a duplicate.

Please take time to fill in the section that are relevant to your situation. If a section is not relevant for your special case, feel free to remove it

------

### Summary

(summarize the bug encountered concisely)

### Where to find the issue

(please, provide a link to the [l10n.gnome.org](https://l10n.gnome.org) where you noticed the bug. Please, also provide your **language settings** (the issue might be language dependent)

### What you were doing

(describe here the actions you were doing, if it is associated to a **Vertimus action**. What was the status of the module you are working on, the team language, your role inside the team, etc)

### What is the current *bug* behavior?

(what actually happens)

### What is the expected *correct* behavior?

(what you think you should see instead)

### The screenshot that illustrates the issue

(please, upload one or more screenshots that illustrate the issue you encountered.)

/label ~"Bug"
/label ~"l10n.gnome.org"
