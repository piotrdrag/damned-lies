from operator import itemgetter
from typing import Any

from django.conf import settings
from django.conf.locale import LANG_INFO
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import PasswordChangeForm
from django.db import IntegrityError, transaction
from django.http import HttpRequest, HttpResponse, HttpResponseRedirect
from django.shortcuts import get_object_or_404, render
from django.urls import reverse
from django.utils.safestring import mark_safe
from django.utils.translation import gettext as _
from django.utils.translation import gettext_lazy
from django.views.decorators.http import require_POST
from django.views.generic import DetailView, ListView, UpdateView

from common.utils import lc_sorted
from people.forms import DetailForm, TeamJoinForm
from people.models import Person
from teams.models import Role, Team
from vertimus.models import State


class PeopleListView(ListView):
    model = Person
    template_name = "people/person_list.html"

    def get_context_data(self: "PeopleListView", **kwargs) -> dict[str, Any]:  # noqa: ANN003
        context = super().get_context_data(**kwargs)
        context["pageSection"] = "teams"
        return context


class PersonDetailView(DetailView):
    model = Person
    slug_field = "username"
    context_object_name = "person"
    template_name = "people/person_detail.html"

    def get_context_data(self: "PersonDetailView", **kwargs) -> dict[str, Any]:  # noqa: ANN003
        context = super().get_context_data(**kwargs)
        states = State.objects.filter(action__person=self.object).distinct()
        user_on_own_page = self.request.user.is_authenticated and self.object.username == self.request.user.username
        context.update({
            "pageSection": "teams",
            "on_own_page": user_on_own_page,
            "states": [(s, s.stats) for s in states],
        })
        is_coordinator_of_any_team = any(role.role == "coordinator" for role in self.object.role_set.all())
        user_has_forge_account = bool(self.object.forge_account)
        if user_on_own_page and is_coordinator_of_any_team and not user_has_forge_account:
            messages.add_message(
                self.request,
                messages.WARNING,
                mark_safe(  # noqa: S308 (XSS vulnerability)
                    _(
                        "You did not register a GitLab account in your profile and are the coordinator of a "
                        "translation team. If you do not have any yet, please register on the GNOME GitLab platform "
                        "(%s) and indicate your username in your Damned Lies profile."
                    )
                    % f'<a class="alert-link" href="https://{settings.GNOME_GITLAB_DOMAIN_NAME}">'
                    f"{settings.GNOME_GITLAB_DOMAIN_NAME}</a>"
                ),
            )

        if user_on_own_page and not self.object.has_set_identity:
            messages.add_message(
                self.request,
                messages.WARNING,
                mark_safe(  # noqa: S308 (XSS vulnerability)
                    _(
                        "You did not fill your real name in your profile. Your work can only be added to the "
                        "project in your name if you fill it in."
                    )
                ),
            )

        if user_on_own_page and is_coordinator_of_any_team and user_has_forge_account:
            if not self.object.forge_account_exists:
                messages.add_message(
                    self.request,
                    messages.ERROR,
                    _(
                        "The GNOME GitLab username you set in your profile does not exist on %s. "
                        "Please check your username or create an account if this is not done yet."
                    )
                    % f'<a class="alert-link" href="https://{settings.GNOME_GITLAB_DOMAIN_NAME}">{settings.GNOME_GITLAB_DOMAIN_NAME}</a>',
                )

        return context


class PersonEditView(UpdateView):
    model = Person
    slug_field = "username"
    form_class = DetailForm
    template_name = "people/person_detail_change_form.html"

    def get_object(self: "PersonEditView", **kwargs) -> "Person":  # noqa: ANN003, ARG002
        self.kwargs["slug"] = self.request.user.username
        return super().get_object()

    def get_context_data(self: "PersonEditView", **kwargs) -> dict[str, Any]:  # noqa: ANN003
        context = super().get_context_data(**kwargs)
        context["pageSection"] = "teams"
        context["on_own_page"] = self.object.username == self.request.user.username
        all_languages = [
            (lg[0], LANG_INFO.get(lg[0], {"name_local": lg[1]})["name_local"]) for lg in settings.LANGUAGES
        ]
        context["all_languages"] = lc_sorted(all_languages, key=itemgetter(1))
        return context

    def form_invalid(self: "PersonEditView", form: DetailForm) -> HttpResponse:
        messages.error(self.request, _("Sorry, the form is not valid."))
        return super().form_invalid(form)


@login_required
def person_team_join(request: HttpRequest) -> HttpResponse:
    """Handle the form to join a team"""
    person = get_object_or_404(Person, username=request.user.username)
    if request.method == "POST":
        form = TeamJoinForm(request.POST)
        if form.is_valid():
            team = form.cleaned_data["teams"]
            # Role default to 'translator'
            new_role = Role(team=team, person=person)
            try:
                with transaction.atomic():
                    new_role.save()
                messages.success(request, _("You have successfully joined the team “%s”.") % team.get_description())
                team.send_mail_to_coordinator(
                    subject=gettext_lazy("A new person joined your team"),
                    message=gettext_lazy("%(name)s has just joined your translation team on %(site)s"),
                    message_kwargs={"name": person.name, "site": settings.SITE_DOMAIN},
                )
            except IntegrityError:
                messages.info(request, _("You are already member of this team."))
    else:
        form = TeamJoinForm()

    context = {
        "pageSection": "teams",
        "on_people_manage_team_membership_view": True,
        "person": person,
        "on_own_page": person.username == request.user.username,
        "form": form,
    }
    return render(request, "people/person_team_join_form.html", context)


@login_required
@require_POST
def person_team_leave(request: HttpRequest, team_slug: str) -> HttpResponseRedirect:
    person = get_object_or_404(Person, username=request.user.username)
    team = get_object_or_404(Team, name=team_slug)
    try:
        role = Role.objects.get(team=team, person=person)
        role.delete()
        messages.success(request, _("You have been removed from the team “%s”.") % team.get_description())
    except Role.DoesNotExist:
        # Message no i18n'ed, should never happen under normal conditions
        messages.error(request, _("You are not a member of this team."))
    # redirect to normal person detail
    return HttpResponseRedirect(reverse("person_detail_username", args=(person.username,)))


@login_required
def person_password_change(request: HttpRequest) -> HttpResponse:
    """Handle the generic form to change the password"""
    person = get_object_or_404(Person, username=request.user.username)

    if request.method == "POST":
        form = PasswordChangeForm(request.user, request.POST)
        if form.is_valid():
            messages.success(request, _("Your password has been changed."))
            form.save()
    else:
        form = PasswordChangeForm(request.user)

    context = {
        "pageSection": "teams",
        "person": person,
        "on_own_page": person.username == request.user.username,
        "form": form,
    }
    return render(request, "people/person_password_change_form.html", context)


@login_required
@require_POST
def person_create_token(request: HttpRequest) -> HttpResponseRedirect:
    person = get_object_or_404(Person, username=request.user.username)
    person.auth_token = Person.generate_token()
    person.save()
    return HttpResponseRedirect(reverse("person_detail_username", args=[person.username]))


@login_required
@require_POST
def person_delete_token(request: HttpRequest) -> HttpResponseRedirect:
    person = get_object_or_404(Person, username=request.user.username)
    person.auth_token = ""
    person.save()
    return HttpResponseRedirect(reverse("person_detail_username", args=[person.username]))
