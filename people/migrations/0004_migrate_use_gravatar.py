from django.db import migrations


def migrate_gravatar(apps, schema_editor):
    Person = apps.get_model("people", "Person")
    Person.objects.filter(use_gravatar=True).update(avatar_service="gravatar.com")


class Migration(migrations.Migration):
    dependencies = [
        ("people", "0003_person_avatar_service"),
    ]

    operations = [migrations.RunPython(migrate_gravatar)]
