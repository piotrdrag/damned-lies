import hashlib

from django import template
from django.templatetags.static import static
from django.utils.html import format_html, format_html_join
from django.utils.http import urlencode
from django.utils.translation import gettext as _

from people.models import AVATAR_SERVICES, Person

register = template.Library()


@register.filter
def people_list(list_of_people: list[Person]) -> str:
    """
    Format a people list, with clickable person name.
    """
    # Translators: string used as separator in person list
    return format_html_join(
        _(", "), '<a href="{}">{}</a>', ((pers.get_absolute_url(), pers.name) for pers in list_of_people)
    )


@register.filter
def people_image(person: Person) -> str:
    """
    For Gravatar, see: https://docs.gravatar.com/gravatar-images/django/

    :param person: the person for which to get the avatar
    """
    nobody = static("img/nobody.png")
    if person and person.avatar_service:
        digest = hashlib.md5(person.email.lower().encode("utf-8"), usedforsecurity=False).hexdigest()
        url = AVATAR_SERVICES[person.avatar_service].format(
            hash=digest,
            # Size, default image, rating
            qs=urlencode([("s", "80"), ("d", "identicon"), ("r", "g")]),
        )
        tag = format_html(
            '<img class="rounded-circle" width="48" src="{url}" alt="{alt}" crossorigin="anonymous">',
            url=url,
            alt=_("avatar icon"),
        )
    elif person and person.image:
        tag = format_html(
            '<img class="rounded-circle" width="48" src="{}" alt="{}" onerror="this.onerror = null; this.src=\'{}\'">',
            person.image,
            person.name,
            nobody,
        )
    else:
        tag = format_html(
            '<img class="rounded-circle" width="48" src="{url}" alt="{alt}">', url=nobody, alt=_("generic person icon")
        )
    return tag
