from datetime import timedelta
from pathlib import Path
from unittest import mock, skipUnless
from unittest.mock import patch

from django.contrib import auth

# pylint: disable=imported-auth-user
from django.contrib.auth.models import User
from django.contrib.messages import get_messages
from django.contrib.staticfiles.finders import find
from django.core import mail
from django.core.exceptions import ValidationError
from django.test import Client, TestCase, override_settings
from django.urls import reverse
from django.utils import timezone
from django.utils.safestring import SafeData
from django.utils.translation import gettext as _
from requests import Response

from common.utils import pyicu_present
from languages.models import Language
from people import forms
from people.forms import DetailForm
from people.models import MatrixNetworkField, Person, obfuscate_email
from people.templatetags import people
from stats.models import Branch, Domain, Module
from teams.models import Role, Team
from vertimus.models import StateTranslating


class MatrixFieldTest(TestCase):
    def test_valid_matrix_username(self):
        user_field = MatrixNetworkField(_("Matrix Account"), max_length=50, null=True, blank=True)
        try:
            user_field.validate("@user:gnome.org", None)
            user_field.validate("@user:matrix.org", None)
        except ValidationError:
            self.fail("Validation error should not occur.")

    def test_invalid_matrix_username_without_at_prefix(self):
        user_field = MatrixNetworkField(_("Matrix Account"), max_length=50, null=True, blank=True)
        with self.assertRaises(ValidationError):
            user_field.validate("user:gnome.org", None)

    def test_invalid_matrix_username_without_double_dots(self):
        user_field = MatrixNetworkField(_("Matrix Account"), max_length=50, null=True, blank=True)
        with self.assertRaises(ValidationError):
            user_field.validate("@usergnome.org", None)


class ForgeAccountFieldTest(TestCase):
    def test_valid_forge_account_username(self):
        valid_forge_account_names = ["test", "test.tes12", "12_12.98", "12", "1.1-test", "te", "a" * 255]
        for index, username in enumerate(valid_forge_account_names):
            p = Person(
                username=f"random{index}",
                email="random@example.org",
                forge_account=username,
                password="password",
            )
            p.full_clean()
            self.assertEqual(p.forge_account, username)

    def test_invalid_forge_account_username_special_character(self):
        invalid_forge_account_names = [
            "test@test",
            "https://",
            "<script>console.log('echo')</script>",
            "s",
            "t,",
            "a" * 256,
        ]
        for username in invalid_forge_account_names:
            with self.assertRaises(ValidationError):
                p = Person(username="random", email="random@example.org", forge_account=username, password="password")
                p.full_clean()


class PeopleTestCase(TestCase):

    @staticmethod
    def _create_person(seq="", **kwargs):
        pn = Person(first_name="John", last_name="Nothing", email="jn%s@devnull.com" % seq, username="jn%s" % seq)
        for key, arg in kwargs.items():
            setattr(pn, key, arg)
        pn.set_password("password")
        pn.save()
        return pn

    def test_register(self):
        username = "tèst-01"
        response = self.client.post(
            reverse("register"),
            {"username": username, "password1": "1234567", "password2": "1234567", "email": "test01@example.org"},
        )
        self.assertRedirects(response, reverse("register_success"))
        self.assertEqual(Person.objects.filter(username=username).count(), 1)
        self.assertEqual(len(mail.outbox), 1)
        self.assertIn(_("Account activation"), mail.outbox[0].subject)

    def test_activate_account(self):
        # Testing if is_active is False by default
        response = self.client.post(
            reverse("register"),
            {"username": "newuser", "email": "newuser@example.org", "password1": "blah012", "password2": "blah012"},
        )

        self.newu = Person.objects.get(username="newuser")
        self.assertFalse(self.newu.is_active)

        # Testing with a invalid activation key
        response = self.client.get("/register/activate/a_invalid_key")
        self.assertContains(response, "Sorry, the key you provided is not valid.")

        response = self.client.get("/register/activate/%s" % self.newu.activation_key, follow=True)
        self.assertContains(response, "Your account has been activated.")

        self.newu = Person.objects.get(username="newuser")
        self.assertTrue(self.newu.is_active)

    def test_login_message(self):
        self.pn = self._create_person()
        response = self.client.get(reverse("login"), HTTP_REFERER="http://foo/bar")
        self.assertContains(response, '<input type="hidden" name="referer" value="http://foo/bar">', html=True)
        response = self.client.post(reverse("login"), data={"username": "jn", "password": "password"}, follow=True)
        self.assertContains(
            response,
            "You have not joined any translation team yet. "
            'You can do it from <a href="/users/team_join/">your profile</a>.',
        )
        response = self.client.get(reverse("login"))
        self.assertContains(response, "You are already logged in as jn.")
        self.assertNotContains(response, 'id="login-form"')

    def test_login_by_email(self):
        self.pn = self._create_person()
        self.client.post(reverse("login"), data={"username": "notexist@devnull.com", "password": "password"})
        user = auth.get_user(self.client)
        self.assertFalse(user.is_authenticated)
        self.client.post(reverse("login"), data={"username": "jn@devnull.com", "password": "password"})
        user = auth.get_user(self.client)
        self.assertTrue(user.is_authenticated)

    def test_person_list(self):
        self.pn = self._create_person()
        response = self.client.get(reverse("people"))
        self.assertContains(response, "GNOME is being developed by following people:")
        self.assertContains(response, "John Nothing")

    def test_login_link(self):
        pn = self._create_person()
        self.client.force_login(pn)
        response = self.client.get(reverse("home"))

        # There is a dropdown link with the default avatar
        self.assertContains(
            response,
            '<a id="navbar-user-menu" href="#" class="nav-link px-0 py-0 avatar" role="button" '
            'data-bs-toggle="dropdown" aria-expanded="false">'
            '<img class="rounded-circle" width="48" src="/static/img/nobody.png" alt="generic person icon">'
            "</a>",
            html=True,
        )

        # There is a link to access the logged-in user settings
        self.assertContains(
            response,
            '<a class="dropdown-item" href="%s">'
            "User Settings"
            "</a>" % reverse("person_detail_username", args=[pn.username]),
            html=True,
        )

    def test_forum_account_is_displayed_on_user_page(self):
        Person.objects.create(username="bob", forum_account="bob_forum")
        response = self.client.get(reverse("person_detail_username", kwargs={"slug": "bob"}))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, ">@bob_forum</a>")

    def test_edit_details_with_forum_account(self):
        bob = Person.objects.create(username="bob")
        self.assertEqual(None, bob.forum_account)
        self.client.force_login(bob)
        response = self.client.get(reverse("person_detail_change"))
        self.assertContains(response, "forge_account")
        self.assertEqual(response.status_code, 200)
        post_data = {"username": "bob", "forum_account": "bob_forum"}
        _ = self.client.post(reverse("person_detail_change"), post_data)
        bob.refresh_from_db()
        self.assertEqual("bob_forum", bob.forum_account)

    def test_forge_account_is_displayed_on_user_page(self):
        Person.objects.create(username="bob", forge_account="bob_forge")
        response = self.client.get(reverse("person_detail_username", kwargs={"slug": "bob"}))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, ">@bob_forge</a>")

    def test_edit_details_with_forge_account(self):
        bob = Person.objects.create(username="bob")
        self.assertEqual(None, bob.forge_account)
        self.client.force_login(bob)
        response = self.client.get(reverse("person_detail_change"))
        self.assertContains(response, "forge_account")
        self.assertEqual(response.status_code, 200)
        post_data = {"username": "bob", "forge_account": "bob_forge"}
        _ = self.client.post(reverse("person_detail_change"), post_data)
        bob.refresh_from_db()
        self.assertEqual("bob_forge", bob.forge_account)

    def test_im_account_is_displayed_on_user_page(self):
        Person.objects.create(username="bob", im_username="@bob:gnome.org")
        response = self.client.get(reverse("person_detail_username", kwargs={"slug": "bob"}))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, ">@bob:gnome.org</a>")

    def test_edit_details_with_im_account(self):
        bob = Person.objects.create(username="bob")
        self.assertEqual(None, bob.im_username)
        self.client.force_login(bob)
        response = self.client.get(reverse("person_detail_change"))
        self.assertContains(response, "im_username")
        self.assertEqual(response.status_code, 200)
        post_data = {"username": "bob", "im_username": "@bob:gnome.org"}
        _ = self.client.post(reverse("person_detail_change"), post_data)
        bob.refresh_from_db()
        self.assertEqual("@bob:gnome.org", bob.im_username)

    def test_edit_details(self):
        pn = self._create_person()
        self.client.force_login(pn)
        response = self.client.get(reverse("person_detail_change"))
        self.assertContains(response, "First name")
        post_data = {
            "username": "jn2",
            "first_name": "Johnny",
            "last_name": "Nothing",
            "email": "test02@example.org",
            "image": "",
            "webpage_url": "http://www.example.org/",
        }
        response = self.client.post(reverse("person_detail_change"), post_data)
        pn.refresh_from_db()
        self.assertEqual(pn.username, "jn2")
        self.assertEqual(pn.email, "test02@example.org")
        # bad image url
        post_data["image"] = "http://http://www.example.org/image.jpg"
        form = forms.DetailForm(post_data, instance=pn)
        self.assertFalse(form.is_valid())
        self.assertTrue("image" in form.errors)

    def _test_edit_details_with_forge_account(self, forge_account_username: str, expected_forge_account_username: str):
        person = self._create_person()
        self.client.force_login(person)
        self.client.get(reverse("person_detail_change"))
        post_data = {"username": "jn2", "forge_account": forge_account_username}
        # Test form
        self.assertTrue(DetailForm(data=post_data).is_valid())

        # Test query itself
        self.client.post(reverse("person_detail_change"), post_data)
        person.refresh_from_db()
        self.assertEqual(person.forge_account, expected_forge_account_username)

    def test_edit_details_with_forge_account_username_starting_with_at_sign(self):
        self._test_edit_details_with_forge_account(
            forge_account_username="@user", expected_forge_account_username="user"
        )

    def test_edit_details_with_forge_account_username_not_starting_with_at_sign(self):
        self._test_edit_details_with_forge_account(
            forge_account_username="user", expected_forge_account_username="user"
        )

    def test_create_token(self):
        pn = self._create_person()
        self.assertEqual(pn.auth_token, "")
        self.client.force_login(pn)
        response = self.client.post(reverse("person_create_token"))
        self.assertRedirects(response, reverse("person_detail_username", args=[pn.username]))
        pn.refresh_from_db()
        self.assertNotEqual(pn.auth_token, "")

    def test_delete_token(self):
        pn = self._create_person(auth_token="askhdaksudakckascjbaskdaiwue")
        self.client.force_login(pn)
        response = self.client.post(reverse("person_delete_token"))
        self.assertRedirects(response, reverse("person_detail_username", args=[pn.username]))
        pn.refresh_from_db()
        self.assertEqual(pn.auth_token, "")

    def test_token_authentication(self):
        pn = self._create_person(auth_token="askhdaksudakckascjbaskdaiwue")
        response = self.client.get(reverse("home"), HTTP_AUTHENTICATION="Bearer askhdaksudakckascjbaskdaiwue")
        self.assertFalse(response.context["user"].is_anonymous)
        self.assertEqual(response.context["user"], pn)

    def test_obsolete_accounts(self):
        module = Module.objects.create(name="gnome-hello")
        Branch.checkout_on_creation = False
        branch = Branch(name="gnome-2-24", module=module)
        branch.save(update_statistics=False)
        domain = Domain.objects.create(module=module, name="po", layout="po/{lang}.po")
        team = Team.objects.create(name="fr", description="French", mailing_list="french_ml@example.org")
        lang = Language.objects.create(name="French", locale="fr", team=team)

        # Create Users
        p1 = self._create_person(seq="1", last_login=timezone.now() - timedelta(days=700))
        p2 = self._create_person(seq="2", last_login=timezone.now() - timedelta(days=800))
        Role.objects.create(team=team, person=p2, role="coordinator")
        p3 = self._create_person(seq="3", last_login=timezone.now() - timedelta(days=800))
        module.maintainers.add(p3)
        p4 = self._create_person(seq="4", last_login=timezone.now() - timedelta(days=800))
        StateTranslating.objects.create(branch=branch, domain=domain, language=lang, person=p4)
        self._create_person(seq="5", last_login=timezone.now() - timedelta(days=800))
        self._create_person(seq="6", date_joined=timezone.now() - timedelta(days=200), last_login=None)
        # Test only p5/p6 should be deleted
        self.assertEqual(Person.objects.all().count(), 6)
        Person.clean_obsolete_accounts()
        self.assertEqual(Person.objects.all().count(), 4)
        self.assertEqual(set(Person.objects.all()), {p1, p2, p3, p4})

    def test_obfuscate_email(self):
        self.assertEqual(obfuscate_email(""), "")
        self.assertEqual(
            obfuscate_email("me.company@domain.org"), "me&nbsp;dot&nbsp;company&nbsp;at&nbsp;domain&nbsp;dot&nbsp;org"
        )
        self.assertIsInstance(obfuscate_email("me.company@domain.org"), SafeData)
        self.assertEqual(
            obfuscate_email("George P. McLain <george@domain.org"),
            "George&nbsp;P.&nbsp;McLain&nbsp;&lt;george&nbsp;at&nbsp;domain&nbsp;dot&nbsp;org",
        )
        self.assertEqual(
            obfuscate_email("Me <me.company@domain.org>\nYou <some-address@example.com>"),
            "Me&nbsp;&lt;me&nbsp;dot&nbsp;company&nbsp;at&nbsp;domain&nbsp;dot&nbsp;org&gt;\n"
            "You&nbsp;&lt;some-address&nbsp;at&nbsp;example&nbsp;dot&nbsp;com&gt;",
        )

    def test_people_image_tag(self):
        """
        Test the people_image template tag.
        """
        pn = self._create_person()
        self.assertHTMLEqual(
            people.people_image(pn),
            '<img class="rounded-circle" width="48" src="/static/img/nobody.png" alt="generic person icon">',
        )
        pn.image = "http://www.example.org/my_image.png"
        self.assertHTMLEqual(
            people.people_image(pn),
            '<img class="rounded-circle" width="48" alt="John Nothing" onerror="this.onerror = null; '
            'this.src=\'/static/img/nobody.png\'" src="http://www.example.org/my_image.png" />',
        )
        pn.last_name = "<script>Some XSS content</script>"
        self.assertIn("&lt;script&gt;Some XSS content&lt;/script&gt;", people.people_image(pn))
        self.assertHTMLEqual(
            people.people_image(pn),
            '<img class="rounded-circle" width="48" alt="John &lt;script&gt;Some XSS content&lt;/script&gt;" '
            "onerror=\"this.onerror = null; this.src='/static/img/nobody.png'\" "
            'src="http://www.example.org/my_image.png" />',
        )
        pn.avatar_service = "gravatar.com"
        self.assertHTMLEqual(
            people.people_image(pn),
            '<img class="rounded-circle" width="48" alt="avatar icon" crossorigin="anonymous" '
            'src="https://secure.gravatar.com/avatar/618b8b6c1c973c780ec218242c49cbe7.jpg?s=80&d=identicon&r=g">',
        )
        pn.avatar_service = "libravatar.org"
        self.assertHTMLEqual(
            people.people_image(pn),
            '<img class="rounded-circle" width="48" alt="avatar icon"  crossorigin="anonymous" '
            'src="https://seccdn.libravatar.org/avatar/618b8b6c1c973c780ec218242c49cbe7?s=80&d=identicon&r=g">',
        )

    @mock.patch("people.forms.requests.get")
    def test_get_image_size(self, requests_get):
        # pylint: disable=unspecified-encoding
        with Path.open(find("img/nobody.png"), mode="rb") as fh:
            response = Response()
            response.status_code = 200
            response.raw = fh
            requests_get.return_value = response

            self.assertEqual(forms.get_image_size_from_url("https://l10n.gnome.org/static/img/nobody.png"), (48, 48))

    def test_invalid_images(self):
        invalid_urls = (
            "absolutely invalid",
            "http://hopefullythisurlshouldnotexist.com/grstzqwer.jpg",
            "file:///etc/services",
        )
        for url in invalid_urls:
            with self.assertRaises(ValidationError):
                forms.get_image_size_from_url(url)

    @skipUnless(pyicu_present, "PyICU package is required for this test")
    def test_all_languages_list_order(self):
        """
        The order of the languages should be case insensitive.
        This tests that "français" appears before "Gaeilge".
        """
        pn = Person.objects.create(username="jn")
        self.client.force_login(pn)
        url = reverse("person_detail_change")
        response = self.client.get(url)
        all_languages = response.context["all_languages"]
        self.assertGreater(
            all_languages.index(("ga", "Gaeilge")),
            all_languages.index(("fr", "français")),
            "français is not before Frisian",
        )

    def test_user_has_forge_account_and_is_active(self):
        with patch("people.models.requests.get") as patched_method:
            patched_method.return_value.json.return_value = [
                {
                    "id": 1,
                    "username": "john",
                    "name": "John Coordinator",
                    "state": "active",
                    "avatar_url": "https://gitlab.gnome.org/uploads/-/system/user/avatar/1/avatar.png",
                    "web_url": "https://gitlab.gnome.org/john",
                }
            ]
            person = Person.objects.create(username="john", forge_account="john")
            self.assertTrue(person.forge_account_exists)

    def test_user_has_forge_account_is_inactive(self):
        with patch("people.models.requests.get") as patched_method:
            patched_method.return_value.json.return_value = [
                {
                    "id": 1,
                    "username": "john",
                    "name": "John Coordinator",
                    "state": "inactive",
                    "avatar_url": "https://gitlab.gnome.org/uploads/-/system/user/avatar/1/avatar.png",
                    "web_url": "https://gitlab.gnome.org/john",
                }
            ]
            person = Person.objects.create(username="john", forge_account="john")
            self.assertFalse(person.forge_account_exists)

    def test_user_does_not_have_forge_account(self):
        with patch("people.models.requests.get") as patched_method:
            patched_method.return_value.json.return_value = []
            person = Person.objects.create(username="john", forge_account="john")
            self.assertFalse(person.forge_account_exists)

    @staticmethod
    # noinspection PyMethodMayBeStatic
    def _test_user_coordinator_with_or_without_forge_account_prepare_data() -> tuple["Person", "Team"]:
        person = Person.objects.create(username="john", forge_account=None, first_name="John", last_name="Doe")
        language = Language.objects.create(name="French", locale="fr", plurals="plurals=2; plural=(n > 1)")
        team = Team.objects.create(name="French", use_workflow=True)
        team.language_set.add(language)
        return person, team

    def test_user_is_coordinator_of_any_team_without_forge_account(self):
        person, team = self._test_user_coordinator_with_or_without_forge_account_prepare_data()
        Role.objects.create(person=person, team=team, role="coordinator", is_active=True)
        self.client.force_login(person)
        response = self.client.get(reverse("person_detail_username", kwargs={"slug": person.username}))
        messages = [m.message for m in get_messages(response.wsgi_request)]
        self.assertEqual(len(messages), 1)
        self.assertIn("You did not register a GitLab account in your profile", messages[0])

    def test_user_is_coordinator_of_any_team_with_forge_account(self):
        person, _ = self._test_user_coordinator_with_or_without_forge_account_prepare_data()
        self.client.force_login(person)
        with patch("people.models.requests.get") as patched_method:
            patched_method.return_value.json.return_value = [
                {
                    "id": 1,
                    "username": "john",
                    "name": "John Coordinator",
                    "state": "active",
                    "avatar_url": "https://gitlab.gnome.org/uploads/-/system/user/avatar/1/avatar.png",
                    "web_url": "https://gitlab.gnome.org/john",
                }
            ]
            response = self.client.get(reverse("person_detail_username", kwargs={"slug": person.username}))
            messages = [m.message for m in get_messages(response.wsgi_request)]
            self.assertEqual(len(messages), 0)

    def test_person_member_of_teams_displayed_on_user_page(self):
        person = Person.objects.create(username="john", forge_account=None)
        language_fr = Language.objects.create(name="French", locale="fr", plurals="plurals=2; plural=(n > 1)")
        team_fr = Team.objects.create(name="fr", description="French", use_workflow=True)
        team_fr.language_set.add(language_fr)
        language_it = Language.objects.create(name="Italian", locale="it", plurals="plurals=2; plural=(n > 1)")
        team_it = Team.objects.create(name="it", description="Italiano", use_workflow=True)
        team_it.language_set.add(language_it)
        Role.objects.create(person=person, team=team_fr, role="translator", is_active=True)
        Role.objects.create(person=person, team=team_it, role="reviewer", is_active=True)

        self.client.force_login(person)
        response = self.client.get(reverse("person_detail_username", kwargs={"slug": person.username}))
        self.assertContains(response, "person-member-of-fr-team")
        self.assertContains(response, "Translator</span> − <a href='/teams/fr/'>French</a>")
        self.assertContains(response, "role-translator-in-fr-team")
        self.assertContains(response, "person-member-of-it-team")
        self.assertContains(response, "Reviewer</span> − <a href='/teams/it/'>Italiano</a>")
        self.assertContains(response, "role-reviewer-in-it-team")

    def test_person_maintains_modules_displayed_on_user_page(self):
        person = Person.objects.create(username="john", forge_account=None)
        module = Module.objects.create(name="gnome-hello")
        module.maintainers.add(person)
        self.client.force_login(person)
        response = self.client.get(reverse("person_detail_username", kwargs={"slug": person.username}))
        self.assertContains(response, "person-maintains-module-gnome-hello")
        self.assertContains(response, '<a href="/module/gnome-hello/">gnome-hello</a>')

    def test_has_set_identity_empty_strings(self):
        self.coordinator = self._create_person()
        self.coordinator.first_name = ""
        self.coordinator.last_name = ""
        self.coordinator.save()
        self.assertEqual(False, self.coordinator.has_set_identity)

    def test_has_set_identity_first_name_only(self):
        self.coordinator = self._create_person()
        self.coordinator.first_name = "John"
        self.coordinator.last_name = ""
        self.coordinator.save()
        self.assertEqual(True, self.coordinator.has_set_identity)
        self.assertEqual("John", self.coordinator.name)

    def test_has_set_identity_last_name_only(self):
        self.coordinator = self._create_person()
        self.coordinator.first_name = ""
        self.coordinator.last_name = "Coordinator"
        self.coordinator.save()
        self.assertEqual(True, self.coordinator.has_set_identity)
        self.assertEqual("Coordinator", self.coordinator.name)

    def test_has_set_identity_first_and_last_names(self):
        self.coordinator = self._create_person()
        self.coordinator.first_name = "John"
        self.coordinator.last_name = "Coordinator"
        self.coordinator.save()
        self.assertEqual(True, self.coordinator.has_set_identity)
        self.assertEqual("John Coordinator", self.coordinator.name)


class PeopleViewsTestCase(TestCase):
    fixtures = ("test_data.json",)

    def setUp(self) -> None:
        self.coordinator = Person.objects.get(username="john")
        self.reviewer = Person.objects.get(username="bob")
        self.client = Client()

    def test_auth_token_displayed_on_own_page(self):
        self.client.force_login(self.coordinator)
        auth_token = self.coordinator.auth_token = Person.generate_token()
        self.coordinator.save()
        with patch("people.models.requests.get") as patched_method:
            patched_method.return_value.json.return_value = [{}]
            response = self.client.get(reverse("person_detail_username", kwargs={"slug": self.coordinator.username}))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context["person"].auth_token, auth_token)
        self.assertTrue(response.context["on_own_page"])
        self.assertContains(response, 'id="person_auth_token"')

    def test_auth_token_no_displayed_when_not_on_own_page(self):
        self.client.force_login(self.reviewer)
        auth_token = self.coordinator.auth_token = Person.generate_token()
        self.coordinator.save()
        with patch("people.models.requests.get") as patched_method:
            patched_method.return_value.json.return_value = [{}]
            response = self.client.get(reverse("person_detail_username", kwargs={"slug": self.coordinator.username}))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context["person"].auth_token, auth_token)
        self.assertFalse(response.context["on_own_page"])
        self.assertNotContains(response, 'id="person_auth_token"')

    def test_show_message_for_coordinator_of_a_team_without_any_forge_account(self):
        # Remove forge_account if any
        self.coordinator.forge_account = None
        self.coordinator.save()
        self.coordinator = Person.objects.get(username="john")

        # User has to be connected and see his/her own page
        self.client.force_login(self.coordinator)
        response = self.client.get(reverse("person_detail_username", kwargs={"slug": self.coordinator.username}))

        self.assertContains(
            response,
            "You did not register a GitLab account in your profile and are the coordinator of a translation team.",
        )
        self.assertNotContains(response, "The GNOME GitLab username you set in your profile does not exist on")

    def test_show_message_for_coordinator_of_a_team_with_forge_account_missing_on_forge(self):
        # Remove forge_account if any
        self.coordinator.forge_account = "random_username_that_is_surely_not_registered_6545464"
        self.coordinator.save()
        self.coordinator = Person.objects.get(username="john")

        # User has to be connected and see his/her own page
        self.client.force_login(self.coordinator)

        with patch("people.models.requests.get") as patched_method:
            patched_method.return_value.json.return_value = [{}]
            response = self.client.get(reverse("person_detail_username", kwargs={"slug": self.coordinator.username}))

        self.assertNotContains(
            response,
            "You did not register a GitLab account in your profile and are the coordinator of a translation team.",
        )
        self.assertContains(response, "The GNOME GitLab username you set in your profile does not exist on")

    def test_admin_action_send_messages_to_coordinators_without_any_forge_account(self):
        # Remove forge_account if any
        self.coordinator.forge_account = None
        self.coordinator.save()

        superuser = User.objects.get(username="root")
        self.client.force_login(superuser)
        data = {
            "action": "send_notification_to_coordinators_without_forge_accounts",
            "_selected_action": Person.objects.all().values_list("id", flat=True),
        }
        self.client.post(reverse("admin:people_person_changelist"), data)
        self.assertEqual(len(mail.outbox), 1)
        self.assertIn(f"Dear {self.coordinator.username}", mail.outbox[0].body)
        self.assertIn("to inform you that your profile is missing your username on GitLab", mail.outbox[0].body)

    @override_settings(LANGUAGE_CODE="en")
    def test_person_get_languages_default_language_and_team(self):
        languages = self.reviewer.get_languages()
        self.assertEqual(2, len(languages))
        self.assertIn(Language.objects.get(locale="en"), languages)
        self.assertIn(Language.objects.get(locale="fr"), languages)

    @override_settings(LANGUAGE_CODE="fr")
    def test_person_get_languages_default_language_no_team(self):
        self.reviewer.teams.clear()
        self.assertEqual(0, self.reviewer.teams.count())
        languages = self.reviewer.get_languages()
        self.assertEqual(1, len(languages))
        self.assertNotIn(Language.objects.get(locale="en"), languages)
        self.assertIn(Language.objects.get(locale="fr"), languages)

    @override_settings(LANGUAGE_CODE="fr")
    def test_person_get_languages_no_duplicate(self):
        """
        Default setting is ’fr’ and the team locale is ‘fr’.
        """
        self.assertIn(Team.objects.get(language__locale="fr"), self.reviewer.teams.all())
        self.assertEqual(1, self.reviewer.teams.count())
        languages = self.reviewer.get_languages()
        self.assertEqual(1, len(languages))
        self.assertNotIn(Language.objects.get(locale="en"), languages)
        self.assertIn(Language.objects.get(locale="fr"), languages)

    def test_person_warning_is_showed_if_real_name_not_provided(self):
        self.coordinator.first_name = ""
        self.coordinator.last_name = ""
        self.coordinator.save()
        self.client.force_login(self.coordinator)
        response = self.client.get(reverse("person_detail_username", kwargs={"slug": self.coordinator.username}))

        self.assertContains(
            response,
            "You did not fill your real name in your profile.",
        )
