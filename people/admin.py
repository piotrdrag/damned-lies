from django.contrib import admin
from django.contrib.admin import SimpleListFilter
from django.contrib.auth.admin import UserAdmin

# pylint: disable=imported-auth-user
from django.contrib.auth.models import User
from django.db.models import Q, QuerySet
from django.http import HttpRequest
from django.utils.translation import gettext as _

from common.utils import send_mail
from damnedlies import settings
from people.models import Person
from teams.models import Role


class CoordinatorWithoutForgeAccountNotification:
    def __init__(self: "CoordinatorWithoutForgeAccountNotification", username: str) -> None:
        self.title = _("Your profile is missing your GitLab forge account on Damned Lies")
        self.body = _(
            "Dear %(username)s,\n\n"
            "This is an automatic email from Damned Lies (%(site_domain)s) to inform you that your profile is missing "
            "your username on GitLab (%(forge_url)s). As you are the coordinator of at least one team, you are asked "
            "to register on this platform and update your Damned Lies profile with your GitLab account.\n\n"
            "The %(site_domain)s administrators."
        ) % {"username": username, "site_domain": settings.SITE_DOMAIN, "forge_url": settings.GNOME_GITLAB_DOMAIN_NAME}


@admin.action(description=_("Send messages to coordinators that did not set their GitLab profile username."))
def send_notification_to_coordinators_without_forge_accounts(
    modeladmin: admin.ModelAdmin,  # noqa: ARG001
    request: HttpRequest,  # noqa: ARG001
    queryset: QuerySet,
) -> None:
    users_without_forge_account = queryset.filter(Q(forge_account=None)).values_list("id")
    coordinators_without_forge_account = [
        role.person for role in Role.objects.filter(Q(person__in=users_without_forge_account) & Q(role="coordinator"))
    ]
    for person in coordinators_without_forge_account:
        notification = CoordinatorWithoutForgeAccountNotification(person.username)
        send_mail(notification.title, notification.body, to=[person.email])


class PersonFilter(SimpleListFilter):
    title = _("custom lookups")
    parameter_name = "custom_lookups"

    def lookups(self, request: "HttpRequest", model_admin: "PersonAdmin") -> list[tuple[str, str]]:  # noqa: ARG002, PLR6301
        return [
            ("potential_spam", _("Suspected Spam Accounts")),
            ("unactivated_accounts", _("Unactivated Accounts")),
            ("obsolete_accounts", _("Obsolete Accounts")),
        ]

    def queryset(self, request: "HttpRequest", queryset: QuerySet) -> QuerySet:  # noqa: ARG002
        filtered_queryset = queryset
        match self.value():
            case "potential_spam":
                filtered_queryset = Person.objects.potential_spam_accounts()
            case "unactivated_accounts":
                filtered_queryset = Person.objects.unactivated_accounts()
            case "obsolete_accounts":
                filtered_queryset = Person.objects.obsolete_accounts()
            case _:
                pass
        return filtered_queryset


class RoleInTeamInline(admin.TabularInline):
    model = Role
    extra = 0


@admin.register(Person)
class PersonAdmin(admin.ModelAdmin):
    search_fields = ("username", "first_name", "last_name", "email", "webpage_url", "forge_account")
    list_display = (
        "username",
        "first_name",
        "last_name",
        "email",
        "last_login",
        "webpage_url",
        "forge_account",
        "in_teams",
        "modules_maintained",
        "number_of_actions",
    )
    actions = (send_notification_to_coordinators_without_forge_accounts,)
    inlines = (RoleInTeamInline,)
    list_filter = ("is_staff", "date_joined", "last_login", PersonFilter)

    def get_queryset(self, request: "HttpRequest") -> QuerySet:
        return super().get_queryset(request).prefetch_related("teams", "maintains_modules", "state_set")

    def in_teams(self, obj: "Person") -> list[str]:  # noqa: PLR6301
        return [team.name for team in obj.teams.all()]

    def modules_maintained(self, obj: "Person") -> list[str]:  # noqa: PLR6301
        return [module.name for module in obj.maintains_modules.all()]

    def number_of_actions(self, obj: "Person") -> int:  # noqa: PLR6301
        return obj.state_set.count()


UserAdmin.list_display = ("username", "email", "last_name", "first_name", "is_active", "last_login")

admin.site.unregister(User)
admin.site.register(User, UserAdmin)
