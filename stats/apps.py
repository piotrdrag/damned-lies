import signal

from django.apps import AppConfig


class StatsConfig(AppConfig):
    name = "stats"

    def ready(self) -> None:
        from stats.models import ModuleLock

        signal.signal(signal.SIGTERM, lambda *args: ModuleLock.clean_locks())
