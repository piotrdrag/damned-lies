import abc
import os
import re
import shutil
import sys
import threading
from collections import Counter, OrderedDict
from collections.abc import Collection, Iterable
from datetime import datetime, timezone as dt_timezone
from functools import lru_cache, total_ordering
from operator import itemgetter
from pathlib import Path
from time import sleep
from typing import Any
from urllib import request
from urllib.error import URLError

from django.conf import settings
from django.core.exceptions import ValidationError
from django.core.management import call_command
from django.core.validators import RegexValidator
from django.db import models
from django.db.models.functions import Coalesce
from django.templatetags.static import static
from django.urls import reverse
from django.utils import dateformat, timezone
from django.utils.encoding import force_str
from django.utils.functional import cached_property
from django.utils.translation import gettext as _
from django.utils.translation import gettext_noop, ngettext
from translate.convert import sub2po

from common.utils import is_site_admin, run_shell_command
from damnedlies import logger
from languages.models import Language
from people.models import Person

from stats import repos, signals, utils
from stats.doap import update_doap_infos
from stats.potdiff import PoFileDiffStatus, POTFile
from stats.repos import VCSRepository, VCSRepositoryType

# These args should be kept in sync with
# https://github.com/mesonbuild/meson/blob/master/mesonbuild/modules/i18n.py#L25
GLIB_PRESET = (
    "--keyword=_",
    "--keyword=N_",
    "--keyword=C_:1c,2",
    "--keyword=NC_:1c,2",
    "--keyword=g_dcgettext:2",
    "--keyword=g_dngettext:2,3",
    "--keyword=g_dpgettext2:2c,3",
    "--flag=N_:1:pass-c-format",
    "--flag=C_:2:pass-c-format",
    "--flag=NC_:2:pass-c-format",
    "--flag=g_dngettext:2:pass-c-format",
    "--flag=g_strdup_printf:1:c-format",
    "--flag=g_string_printf:2:c-format",
    "--flag=g_string_append_printf:2:c-format",
    "--flag=g_error_new:3:c-format",
    "--flag=g_set_error:4:c-format",
)

# Standard Django slug validation but also accept '+' (for gtk+)
slug_re = re.compile(r"^[-\+a-zA-Z0-9_]+\Z")
validate_slug = RegexValidator(
    slug_re, "Enter a valid 'slug' consisting of letters, numbers, underscores, hyphens or plus signs.", "invalid"
)

BRANCH_HEAD_NAMES = (
    "HEAD",
    "trunk",
    "main",
    "mainline",
    "master",
)


class UnableToCommitError(Exception):
    pass


class Module(models.Model):
    name = models.CharField(max_length=50, unique=True, validators=[validate_slug])
    homepage = models.URLField(blank=True, help_text="Automatically updated if the module contains a doap file.")
    description = models.TextField(blank=True)
    comment = models.TextField(blank=True)
    bugs_base = models.CharField(max_length=250, blank=True)
    # URLField is too restrictive for vcs_root
    vcs_root = models.CharField(max_length=200)
    vcs_web = models.URLField()
    ext_platform = models.URLField(blank=True, help_text="URL to external translation platform, if any")
    archived = models.BooleanField(default=False)

    maintainers = models.ManyToManyField(
        Person,
        db_table="module_maintainer",
        related_name="maintains_modules",
        blank=True,
        help_text="Automatically updated if the module contains a doap file.",
    )

    class Meta:
        db_table = "module"
        ordering = ("name",)

    def __str__(self) -> str:
        return self.name

    def __lt__(self, other):
        return self.name < other.name

    def get_absolute_url(self):
        return reverse("module", args=[self.name])

    def get_description(self):
        if self.description:
            return f"{self.name} — {_(self.description)}"
        return self.name

    def get_comment(self):
        comment = _(self.comment) if self.comment else ""
        if self.ext_platform:
            if comment:
                comment += "<br/>"
            comment = "{comment}<em>{message_with_link}</em>".format(
                comment=comment,
                message_with_link=_(
                    'Translations for this module are externally hosted. Please go to the <a href="%(link)s">'
                    "external platform</a> to see how you can submit your translation."
                )
                % {"link": self.ext_platform},
            )
        return comment

    def get_bugs_i18n_url(self, content=None):
        if "gitlab" in self.bugs_base:
            link = utils.url_join(self.bugs_base, "?state=opened&label_name[]=8.%20Translation")
            if content:
                link += "&search={}".format(content.replace(" ", "+"))
            return link
        return None

    def get_bugs_enter_url(self):
        if "gitlab" in self.bugs_base:
            return utils.url_join(self.bugs_base, "new")
        return self.bugs_base

    def get_branches(self, reverse=False):
        """Return module branches, in ascending order by default (descending order if reverse == True)"""
        return sorted(self.branch_set.all(), reverse=reverse)

    def get_head_branch(self) -> "Branch":
        """Returns the HEAD (trunk, master, ...) branch of the module"""
        branch = self.branch_set.filter(name__in=BRANCH_HEAD_NAMES).first()
        if not branch:
            branch = self.get_branches(reverse=False)[0]
        return branch

    def can_edit_branches(self, user) -> bool:
        """Returns True for superusers, users with adequate permissions or maintainers of the module"""
        if is_site_admin(user) or user.username in [p.username for p in self.maintainers.all()]:
            return True
        return False

    def reset_hard(self) -> None:
        """Remove all module checkouts and re-create them."""
        self.remove_all_branches()
        for branch in self.get_branches():
            branch._repository.init_checkout()

    def remove_all_branches(self) -> None:
        """
        Remove all the branches associated to this module.
        """
        branches = self.get_branches()
        for branch in branches:
            branch.delete_checkout()


class ModuleLock:
    """Weird things happen when multiple updates run in parallel for the same module
    We use filesystem directories creation/deletion to act as global lock mechanism
    """

    dir_prefix = "updating-"

    def __init__(self, mod) -> None:
        self.module = mod
        self.dirpath = settings.LOCK_DIR / f"{self.dir_prefix}{self.module.name}"

    def __enter__(self):
        while True:
            try:
                self.dirpath.mkdir(parents=True)
                break
            except FileExistsError:
                if (datetime.now() - datetime.fromtimestamp(self.dirpath.stat().st_ctime)).days > 1:
                    # After more than one day, something was blocked, force release the lock.
                    self.dirpath.rmdir()
                else:
                    # Something is happening on the module, let's wait.
                    sleep(10)
        return self

    def __exit__(self, *exc_info):
        try:
            self.dirpath.rmdir()
        except OSError:
            pass

    @property
    def is_locked(self):
        return self.dirpath.exists()

    @classmethod
    def clean_locks(cls) -> None:
        # In case of urgent stops (SIGTERM, etc.)
        for subdir in settings.LOCK_DIR.iterdir():
            if subdir.name.startswith(cls.dir_prefix):
                try:
                    subdir.rmdir()
                except OSError:
                    continue


@total_ordering
class Branch(models.Model):
    """
    Branch of a module as in any Version Control System.
    For instance with Git: https://git-scm.com/docs/git-branch
    """

    name: str = models.CharField(_("Name"), max_length=50)
    vcs_subpath: str = models.CharField(_("Version Control System subpath"), max_length=50, blank=True)
    module: Module = models.ForeignKey(Module, on_delete=models.CASCADE)
    weight: int = models.IntegerField(_("Weight"), default=0, help_text=_("Smaller weight is displayed first"))
    file_hashes = models.JSONField(_("File Hashes"), blank=True, null=True, editable=False)
    # 'releases' is the backward relation name from Release model

    # May be set to False by test suite
    checkout_on_creation: bool = True

    class Meta:
        db_table = "branch"
        verbose_name_plural = "branches"
        ordering = ("name",)
        unique_together = ("name", "module")

    def __init__(self, *args, **kwargs) -> None:
        super().__init__(*args, **kwargs)
        self._ui_stats = None
        self._doc_stats = None

    @cached_property
    def _repository(self) -> VCSRepository:
        return repos.VCSRepository.create_from_repository_type(VCSRepositoryType.GIT, self)

    def clean(self) -> None:
        """
        Checkout the branch and clear all the non-committed changes.
        """
        if self.checkout_on_creation and not self.module.archived:
            try:
                with ModuleLock(self.module):
                    self.checkout()
            except Exception as exc:
                raise ValidationError(
                    _("Branch not valid: error while checking out the branch (%s).") % sys.exc_info()[1]
                ) from exc

    def save(self, update_statistics=True, **kwargs) -> None:
        old_name = Branch.objects.get(pk=self.pk).name if self.pk else None
        super().save(**kwargs)
        if old_name and old_name != self.name:
            with ModuleLock(self.module):
                self._repository.rename(old_name, self.name)
        if update_statistics and not self.module.archived:
            # The update command is launched asynchronously in a separate thread
            upd_thread = threading.Thread(target=self.update_statistics, kwargs={"force": True})
            upd_thread.start()

    def delete(self, **kwargs) -> None:
        self.delete_checkout()
        super().delete()

    def delete_checkout(self) -> None:
        """
        Remove the repository checkout.
        """
        self._repository.remove()
        # Remove the pot/po generated files
        if os.access(str(self.output_directory("ui")), os.W_OK):
            shutil.rmtree(str(self.output_directory("ui")))

    def __str__(self) -> str:
        return f"{self.name} ({self.module})"

    def __hash__(self):
        return hash(self.pk)

    def __eq__(self, other):
        if not isinstance(other, self.__class__):
            return False
        return (self.module.name, self.name) == (other.module.name, other.name)

    def __lt__(self, other):
        if self.name in BRANCH_HEAD_NAMES:
            return True
        if other.name in BRANCH_HEAD_NAMES:
            return False
        # Splitting so gnome-3-2 < gnome-3-10
        return ((-self.weight, *split_name(self.name))) > ((-other.weight, *split_name(other.name)))

    @property
    def uses_meson(self):
        return Path(self.checkout_path, "meson.build").exists()

    @property
    def img_url_prefix(self) -> str:
        return "raw"

    @property
    def is_head(self):
        return self.name in BRANCH_HEAD_NAMES

    def warnings(self) -> str:
        if self.releases.count() < 1:
            return _("This branch is not linked from any release")
        return ""

    def file_changed(self, file_path: Path) -> bool:
        """
        Determines if some file has changed based on its hash
        Always returns true if this is the first time the path is checked.

        :param file_path: the release path in the checkout directory
        :type file_path: Path
        :return: whether a file in the repository has changed.
        :rtype: bool
        """
        full_path = self.checkout_path / file_path
        if not full_path.exists():
            return False  # FIXME: Raise exception?
        new_hash = utils.compute_md5(full_path)
        if self.file_hashes and self.file_hashes.get(str(file_path), None) == new_hash:
            return False

        if not self.file_hashes:
            self.file_hashes = {}
        self.file_hashes[str(file_path)] = new_hash
        self.save(update_statistics=False)
        return True

    @property
    def has_string_frozen(self):
        """Indicates whether the branch is contained in at least one string frozen release."""
        return bool(self.releases.filter(string_frozen=True).count())

    @property
    def is_archive_only(self) -> bool:
        """Indicates whether the branch only appears in 'archived' releases."""
        return bool(self.releases.filter(weight__gt=0).count())

    @property
    def is_vcs_readonly(self):
        return "ssh://" not in self.module.vcs_root and not self.module.vcs_root.startswith("git@")

    @property
    def vcs_web_url(self):
        return self.module.vcs_web

    @property
    def vcs_web_log_url(self):
        """Link to browsable commit log"""
        return utils.url_join(self.module.vcs_web, "commits", self.name)

    def vcs_web_file_url_root(self, ref=None):
        """
        Root of the link to view a file in the VCS web interface.
        The path to the file needs to be added at the end to have the full link.

        :param ref: the reference at which to view the file.
        """
        if not ref:
            ref = self.name
        return utils.url_join(self.module.vcs_web, "blob", ref)

    @cached_property
    def checkout_path(self) -> Path:
        """Returns the path of the local checkout for the branch"""
        return settings.SCRATCHDIR / "git" / self.module.name

    @property
    def name_escaped(self) -> str:
        """Branch name suitable for including in file system paths."""
        return self.name.replace("/", "_")

    @property
    def connected_domains(self) -> dict[str, "Domain"]:
        """
        Return all domains that this branch applies to.
        """
        branch_domains = OrderedDict()
        all_associated_domains = (
            Domain.objects.filter(module=self.module).select_related("branch_from", "branch_to").all()
        )
        for domain in all_associated_domains:
            if self.is_domain_connected(domain):
                branch_domains[domain.name] = domain
        return branch_domains

    def is_domain_connected(self, domain: "Domain") -> bool:
        """
        Indicates whether the domain is connected to the branch. A domain may apply on a branch if it’s connected
        to it.
        """
        # generate query only if branch_from/branch_to are defined.
        if domain.branch_from_id or domain.branch_to_id:
            if (domain.branch_from and self > domain.branch_from) or (domain.branch_to and self < domain.branch_to):
                return False
        return True

    def domain_path(self, domain: "Domain") -> Path:
        """
        Associated domain path. Concatenation of the Branch checkout path and the Domain base directory.
        """
        return self.checkout_path / domain.base_directory

    def output_directory(self, domain_type: str) -> Path:
        """
        Directory where generated pot and po files are written on local system

        :param domain_type: Domain.DOMAIN_TYPE_CHOICES. 'ui' or 'doc'.
        :type domain_type: str
        :return: where generated pot and po files are written on local system
        :rtype: Path
        """
        subdirectory = {"ui": "", "doc": "docs"}[domain_type]
        directory_name = settings.POT_DIR / f"{self.module.name}.{self.name_escaped}" / subdirectory
        directory_name.mkdir(parents=True, exist_ok=True)
        return directory_name

    def get_statistics(
        self, domain_type: str, mandatory_languages: Iterable[Language] = ()
    ) -> OrderedDict[str, list["StatisticsBase"]]:
        """
        Get statistics list of type ``domain_type`` (``ui`` or ``doc``), in a dict of lists, key is domain.name
        (POT in 1st position)

        :param domain_type: Domain.DOMAIN_TYPE_CHOICES. 'ui' or 'doc'.
        :type domain_type: str
        :param mandatory_languages: an iterable of Languages in which we wish statistics even if no translation exists.
        :type mandatory_languages: Iterable[Language]
        :return: statistics list of type ``domain_type``
        :rtype:
            ::
                stats = {'po':      [potstat, polang1, polang2, ...],
                         'po-tips': [potstat, polang1, polang2, ...]}
        """
        branch_statistics = OrderedDict()
        statistics_in_languages = {}
        domain_primary_keys = [domain.pk for domain in self.connected_domains.values() if domain.dtype == domain_type]

        pot_statistics = self._get_pot_statistics_connected_to_domains(domain_primary_keys)
        for statistic in pot_statistics:
            branch_statistics[statistic.domain.name] = [statistic]
            statistics_in_languages[statistic.domain.name] = []

        translation_statistics = self._get_translation_statistics_connected_to_domains(domain_primary_keys)
        for statistic in translation_statistics:
            branch_statistics[statistic.domain.name].append(statistic)
            statistics_in_languages[statistic.domain.name].append(statistic.language)

        # Check if all mandatory languages are present
        for language in mandatory_languages:
            # Branch Statistics Keys are domain names.
            for domain_name, domain in branch_statistics.items():
                domain_is_not_translated = language not in statistics_in_languages[domain_name]
                branch_statistics_in_domain_has_at_least_one_translation = domain[0].full_po
                if domain_is_not_translated and branch_statistics_in_domain_has_at_least_one_translation:
                    domain.append(FakeLangStatistics(domain[0], language))
        # Sort
        for domains in branch_statistics.values():
            # Sort stats, pot first, then translated (desc), then language name
            domains.sort(key=lambda st: (int(st.language is not None), -st.translated(), st.get_lang()))
        return branch_statistics

    def _get_pot_statistics_connected_to_domains(self, domain_primary_keys: list[int]) -> Collection["Statistics"]:
        """POT Statistics object are objects without any language given.

        :param domain_primary_keys: the domain primary keys for which to look for statistics
        :type domain_primary_keys: List[int]
        :return: a collection of Statistic objects
        :rtype: QuerySet
        """
        return (
            Statistics.objects.select_related("language", "domain", "branch", "full_po")
            .filter(branch=self, language__isnull=True, domain__pk__in=domain_primary_keys)
            .order_by("domain__name")
            .all()
        )

    def _get_translation_statistics_connected_to_domains(
        self, domain_primary_keys: list[int]
    ) -> Collection["Statistics"]:
        """Language Statistics objects.

        :param domain_primary_keys: the domain primary keys for which to look for statistics
        :type domain_primary_keys: List[int]
        :return: a collection of Statistic objects
        :rtype: QuerySet
        """
        return (
            Statistics.objects.select_related("language", "domain", "branch", "full_po")
            .filter(branch=self, language__isnull=False, domain__pk__in=domain_primary_keys)
            .all()
        )

    @cached_property
    def documentation_statistics(self) -> list["Statistics"]:
        """The documentation statistics. For the return format, see Branch.get_branch_statistics"""
        statistics = self.get_statistics("doc").get("help", [])
        if len(statistics) == 0:
            raise ValueError(_("There is no documentation statistics for this branch."))
        return statistics

    @lru_cache(maxsize=32)
    def documentation_statistics_for_language(self, language: "Language") -> "Statistics":
        for statistic in self.documentation_statistics:
            if statistic.language == language:
                return statistic
        raise ValueError(_("There is no documentation statistics for this branch in this language."))

    @cached_property
    def ui_statistics(self) -> list["Statistics"]:
        """The user interface statistics. For the return format, see Branch.get_branch_statistics"""
        statistics = self.get_statistics("ui").get("po", [])
        if len(statistics) == 0:
            raise ValueError(_("There is no user interface statistics for this branch."))
        return statistics

    @lru_cache(maxsize=32)
    def ui_statistics_for_language(self, language: "Language") -> "Statistics":
        for statistic in self.ui_statistics:
            if statistic.language == language:
                return statistic
        raise ValueError(_("There is no user interface statistics for this branch in this language."))

    @lru_cache(maxsize=4)
    def get_documentation_statistics(
        self, mandatory_languages: tuple[Language] | None = ()
    ) -> OrderedDict[str, list["StatisticsBase"]]:
        """The documentation statistics in languages. For the return format, see Branch.get_branch_statistics"""
        return self.get_statistics("doc", mandatory_languages)

    @lru_cache(maxsize=4)
    def get_ui_statistics(
        self, mandatory_languages: tuple[Language] | None = ()
    ) -> OrderedDict[str, list["StatisticsBase"]]:
        """The user interface statistics in languages. For the return format, see Branch.get_branch_statistics"""
        return self.get_statistics("ui", mandatory_languages)

    def update_statistics(self, force: bool, checkout: bool = True, domain: "Domain" = None) -> None:
        """Update statistics for all po files from the branch"""
        with ModuleLock(self.module):
            checkout_errors = []
            if checkout:
                try:
                    self.checkout()
                except OSError as err:
                    if self._repository.exists():
                        checkout_errors.append(("warn", f"Unable to update branch: {err}"))
                    else:
                        raise

            domains = [domain] if domain is not None else self.connected_domains.values()
            string_frozen = self.has_string_frozen
            for domain in domains:
                # 1. Initial settings
                # *******************
                domain_path = self.domain_path(domain)
                if not domain_path.exists():
                    # Delete existing stats, if any
                    Statistics.objects.filter(branch=self, domain=domain).delete()
                    continue
                errors = checkout_errors.copy()

                # 2. Pre-check, if available (e.g. intltool-update -m)
                # **************************
                if domain.dtype == "ui" and (
                    domain.pot_method == "auto" or (domain.pot_method == "gettext" and domain.layout.count("/") < 2)
                ):
                    # Run intltool-update -m to check for some errors
                    errors.extend(utils.check_potfiles(domain_path))

                # 3. Generate a fresh pot file
                # ****************************
                potfile, errs = domain.generate_pot_file(self)
                errors.extend(errs)
                linguas = domain.get_linguas(self)
                if linguas["langs"] is None and linguas["error"]:
                    errors.append(("warn", linguas["error"]))

                # Prepare statistics object
                try:
                    pot_stat = Statistics.objects.get(language=None, branch=self, domain=domain)
                    pot_stat.information_set.all().delete()  # Reset errors
                except Statistics.DoesNotExist:
                    pot_stat = Statistics.objects.create(language=None, branch=self, domain=domain)
                for err in errors:
                    pot_stat.set_error(*err)

                # 4. Compare with old pot files, various checks
                # *****************************
                previous_pot = self.output_directory(domain.dtype) / (f"{domain.pot_base()}.{self.name_escaped}.pot")
                if not potfile:
                    logger.error("Can’t generate template file (POT) for %s/%s.", self.module.name, domain.name)
                    if previous_pot.exists():
                        # Use old POT file
                        potfile = previous_pot
                        pot_stat.set_error("error", gettext_noop("Can’t generate template file (POT), using old one."))
                    else:
                        pot_stat.set_error(
                            "error", gettext_noop("Can’t generate template file (POT), statistics aborted.")
                        )
                        continue

                # 5. Check if pot changed
                # ***********************************
                changed_status = PoFileDiffStatus.CHANGED_WITH_ADDITIONS

                if previous_pot.exists():
                    # Compare old and new POT
                    previous_pot_file = POTFile(Path(previous_pot))
                    new_pot_file = POTFile(Path(potfile))

                    changed_status, diff = previous_pot_file.diff_with(new_pot_file)
                    if (
                        string_frozen
                        and domain.dtype == "ui"
                        and changed_status == PoFileDiffStatus.CHANGED_WITH_ADDITIONS
                    ):
                        utils.StringFreezeBreakNotifierOnGitLab().notify(
                            utils.StringFreezeBreakNotification(self, diff)
                        )

                # 6. Generate pot stats and update DB
                # ***********************************
                pot_stat.update_statistics(potfile)

                if potfile != previous_pot:
                    try:
                        shutil.copyfile(str(potfile), str(previous_pot))
                    except Exception:  # FIXME: too broad exception
                        pot_stat.set_error(
                            "error", gettext_noop("Can’t copy new template file (POT) to public location.")
                        )

                # Send pot_has_changed signal
                if previous_pot.exists() and changed_status != PoFileDiffStatus.NOT_CHANGED:
                    signals.pot_has_changed.send(sender=self, potfile=potfile, branch=self, domain=domain)

                # 7. Update language po files and update DB
                # *****************************************
                stats_with_ext_errors = Statistics.objects.filter(
                    branch=self, domain=domain, information__type__endswith="-ext"
                )
                langs_with_ext_errors = [stat.language.locale for stat in stats_with_ext_errors]
                dom_langs = domain.get_lang_files(self.checkout_path)
                for lang, pofile in dom_langs:
                    outpo = self.output_directory(domain.dtype) / (
                        f"{domain.pot_base()}.{self.name_escaped}.{lang}.po"
                    )
                    if (
                        not force
                        and changed_status in {PoFileDiffStatus.NOT_CHANGED, PoFileDiffStatus.CHANGED_ONLY_FORMATTING}
                        and outpo.exists()
                        and pofile.stat().st_mtime < outpo.stat().st_mtime
                        and lang not in langs_with_ext_errors
                    ):
                        continue

                    # msgmerge po with recent pot
                    run_shell_command(["msgmerge", "--previous", "-o", str(outpo), str(pofile), str(potfile)])

                    # Get Statistics object
                    try:
                        stat = Statistics.objects.get(language__locale=lang, branch=self, domain=domain)
                        stat.information_set.all().delete()  # Reset errors
                    except Statistics.DoesNotExist:
                        try:
                            language = Language.objects.get(locale=lang)
                        except Language.DoesNotExist:
                            if self.is_head:
                                language = Language.objects.create(name=lang, locale=lang)
                            else:
                                # Do not create language (and therefore ignore stats) for an 'old' branch
                                continue
                        stat = Statistics.objects.create(language=language, branch=self, domain=domain)

                    errs = utils.check_po_conformity(outpo)
                    for err in errs:
                        stat.set_error(*err)
                    if not errs:
                        stat.update_statistics(outpo)

                    if linguas["langs"] is not None and lang not in linguas["langs"]:
                        stat.set_error("warn-ext", linguas["error"])

                # Delete stats for unexisting langs
                Statistics.objects.filter(branch=self, domain=domain).exclude(
                    models.Q(language__isnull=True) | models.Q(language__locale__in=[dl[0] for dl in dom_langs])
                ).delete()
            # Check if doap file changed
            if self.is_head and self.file_changed(Path(f"{self.module.name}.doap")):
                update_doap_infos(self.module)

    def checkout(self) -> None:
        """Do a checkout or an update of the VCS files"""
        logger.debug("Checking “%s.%s” out to “%s”…", self.module.name, self.name, self.checkout_path)
        self._repository.checkout()

    def update_repository(self) -> None:
        """
        Update existing repository checkout. This cleans the current changes and updates the repository with the
        latest changes.

        .. warning::
            the calling method should acquire a lock for the module to not mix checkouts in different
            threads/processes.
        """
        logger.debug("Updating “%s.%s” (in “%s”)…", self.module.name, self.name, self.checkout_path)
        self._repository.update()

    def commit_po(self, po_file: Path, domain: "Domain", language: "Language", author: "Person") -> str:
        """
        Commit the file 'po_file' in the branch VCS repository

        :param po_file: the po file path to commit
        :param domain: domain of the po file to commit
        :param language: language of the po file
        :param author: author of the commit
        :return: the commit hash
        """
        with ModuleLock(self.module):
            self.update_repository()
            po_file_absolute_path, po_file_already_exists, linguas_path = domain.commit_info(self, language)
            self._create_po_parent_directory_to_commit_file_if_not_exists(po_file_absolute_path)
            self._copy_po_file_into_repository(po_file_absolute_path, po_file)
            commit_hash = self._commit_po_file(
                po_file_absolute_path, po_file_already_exists, linguas_path, language, author
            )
        self._update_branch_stats_after_committing_file(
            po_file_absolute_path, po_file_already_exists, language, domain
        )
        return force_str(commit_hash)

    # noinspection PyMethodMayBeStatic
    def _create_po_parent_directory_to_commit_file_if_not_exists(self, po_file_absolute_path: Path) -> None:
        if not po_file_absolute_path.parent.exists():
            po_file_absolute_path.parent.mkdir(parents=True)

    # noinspection PyMethodMayBeStatic
    def _copy_po_file_into_repository(self, po_file_absolute_path: Path, po_file: Path) -> None:
        shutil.copyfile(str(po_file), str(po_file_absolute_path))

    # noinspection PyMethodMayBeStatic
    def _commit_po_file(
        self,
        po_file_absolute_path: Path,
        po_file_already_exists: bool,
        linguas_path: Path,
        language: "Language",
        author: "Person",
    ) -> str:
        files_to_commit = [po_file_absolute_path.relative_to(self.checkout_path)]
        if linguas_path:
            # Add locale to LINGUAS
            utils.insert_locale_in_linguas(linguas_path, language.locale)
            files_to_commit.append(linguas_path.relative_to(self.checkout_path))
        if po_file_already_exists:
            commit_message = f"Update {language.name} translation"
        else:
            commit_message = f"Add {language.name} translation"
        return self._repository.commit_files(files_to_commit, commit_message, author=author)

    def _update_branch_stats_after_committing_file(
        self, po_file_absolute_path: Path, po_file_already_exists: bool, language: "Language", domain: "Domain"
    ) -> None:
        # Finish by updating stats
        if po_file_already_exists:
            try:
                stat = Statistics.objects.get(language=language, branch=self, domain=domain)
            except Statistics.DoesNotExist:
                self.update_statistics(force=False, checkout=False, domain=domain)
            else:
                stat.update_statistics(po_file_absolute_path)
        else:
            self.update_statistics(force=False, checkout=False, domain=domain)

    def cherrypick_commit(self, commit_hash: str) -> bool:
        """
        Try to cherry-pick a branch commit `commit_hash` into main branch.

        :param commit_hash: the hash of the git commit in the same repository to cherry-pick.
        :return: whether the cherry-picking succeeded or not.
        """
        with ModuleLock(self.module):
            self.update_repository()
            return self._repository.cherry_pick(commit_hash)


class Domain(models.Model):
    DOMAIN_TYPE_CHOICES = (("ui", "User Interface"), ("doc", "Documentation"))
    POT_METHOD_CHOICES = (
        ("auto", "auto detected"),
        ("gettext", "gettext"),
        ("intltool", "intltool"),
        ("shell", "shell command"),
        ("url", "URL"),
        ("in_repo", ".pot in repository"),
        ("subtitles", "subtitles"),
    )
    module = models.ForeignKey(Module, on_delete=models.CASCADE)
    name = models.CharField(max_length=50)
    description = models.TextField(blank=True)
    dtype = models.CharField(max_length=5, choices=DOMAIN_TYPE_CHOICES, default="ui")
    layout = models.CharField(
        max_length=100, help_text="UI standard is 'po/{lang}.po', doc standard is 'help/{lang}/{lang}.po'"
    )
    pot_method = models.CharField(max_length=20, choices=POT_METHOD_CHOICES, default="auto")
    pot_params = models.CharField(
        max_length=100,
        blank=True,
        help_text="pot_method='url': URL, pot_method='shell': shell command, pot_method='gettext': optional params",
    )
    extra_its_dirs = models.TextField(
        blank=True, help_text="colon-separated directories containing extra .its/.loc files for gettext"
    )
    linguas_location = models.CharField(
        max_length=50,
        blank=True,
        help_text=(
            "Use 'no' for no LINGUAS check, or path/to/file#variable for a non-standard location.\n"
            "Leave blank for standard location (ALL_LINGUAS in LINGUAS/configure.ac/.in for UI and DOC_LINGUAS "
            "in Makefile.am for DOC)"
        ),
    )
    red_filter = models.TextField(
        blank=True,
        help_text="pogrep filter to strip po file from unprioritized strings (format: location|string,"
        " “-” for no filter)",
    )
    # Allow to specify the branches to which this domain applies
    branch_from = models.ForeignKey(Branch, null=True, blank=True, on_delete=models.PROTECT, related_name="+")
    branch_to = models.ForeignKey(Branch, null=True, blank=True, on_delete=models.PROTECT, related_name="+")

    class Meta:
        db_table = "domain"
        ordering = ("-dtype", "name")

    def __str__(self) -> str:
        return f"{self.name} ({self.module.name}/{self.get_dtype_display()})"

    @property
    def base_directory(self) -> str:
        """
        Directory where are stored all the PO files and the POT files in a ‘normal’ project structure.
        """
        return self.layout[: self.layout.find("{lang}")]

    def pot_base(self) -> str:
        """
        Name of the generated pot file, without extension.
        (--default-domain for xgettext, --gettext-package for intltool)

        For instance, ‘ui’, ‘gimp-help’, etc.

        For GIMP specifically, there are subdomains as listed here: https://gitlab.gnome.org/GNOME/gimp,
        that are ‘po-python’ and, for instance, returned as ‘gimp-po-python’.
        """
        # FIXME: is this quickfix still needed?
        if self.name[:2] == "po":
            # e.g. replace po by gimp (for ui), or po-plugins by gimp-plugins
            return self.module.name + self.name[2:]

        if self.name == "help":
            return f"{self.module.name}-help"

        return self.name

    def get_description(self):
        if self.description:
            return _(self.description)
        return self.pot_base()

    def has_standard_location(self):
        return (self.dtype == "ui" and self.layout == "po/{lang}.po") or (
            self.dtype == "doc" and self.layout == "help/{lang}/{lang}.po"
        )

    def can_build_docs(self, branch):
        try:
            return self.dtype == "doc" and self.doc_format(branch)
        except utils.UndetectableDocFormatError:
            return False

    def get_po_path(self, locale):
        """
        Return the relative filesystem path to the po file, existing or not.
        """
        return self.layout.format(lang=locale)

    @lru_cache(100)
    def doc_format(self, branch):
        """Return a DocFormat instance, or None."""
        if self.dtype == "ui":
            return None
        return utils.DocFormat(self, branch)

    def get_lang_files(self, base_path):
        """
        Returns a list of language files on filesystem, as a list of tuples:
        [(lang, lang_file), ...] -> lang_file as Path.
        """
        flist = []

        def extract_lang(path):
            path_str = str(path)
            start = len(str(base_path)) + self.layout.find("{lang}") + 1
            return re.split(r"\.|/", path_str[start:])[0]

        for item in base_path.glob(self.layout.replace("{lang}", "*")):
            flist.append((extract_lang(item), item))
        return flist

    def generate_pot_file(self, current_branch: "Branch") -> tuple[str, tuple[tuple[str, Any]]]:
        """
        Return the pot file generated (in the checkout tree), and the error if any

        :param current_branch: the module’s branch from which to refresh the POT file.
        """
        logger.info(
            "Refreshing POT files for ‘%s’ in repository ‘%s’", current_branch.name, current_branch.module.name
        )

        po_files_directory = current_branch.checkout_path / self.base_directory

        # The POT file is, by default, with the other PO files
        pot_file: Path = po_files_directory / (self.pot_base() + ".pot")

        errors: list[tuple[str, str]] = []

        # This is a special case for Damned Lies, as some strings are directly extracted
        # from the database, it has to be running to extract messages.
        if self.module.name == "damned-lies":
            call_command("update-trans", "en", verbosity=0)
            pot_file = Path("./po") / "damned-lies.pot"
            return pot_file, ()

        # Another special case, the method is auto and the domain is a documentation.
        # The messages are extracted automatically
        if self.pot_method == "auto" and self.dtype == "doc":
            pot_file, errs = utils.generate_doc_pot_file(current_branch, self)
            errors.extend(errs)

        else:
            # Determine manually what’s supposed to be the behavior or the automatic detection.
            pot_method = self.pot_generation_type(current_branch)

            environment_variables = None
            pot_command = None

            match pot_method:
                # Get POT from URL and save file locally
                case "url":
                    req = request.Request(self.pot_params)
                    try:
                        handle = request.urlopen(req)
                    except URLError:
                        return "", (("error", gettext_noop("Error retrieving pot file from URL.")),)
                    with pot_file.open("wb") as f:
                        f.write(handle.read())

                # If the POT file is supposed to be in the repository, check whether it’s true. It not, try
                # using the POT params. Otherwise, it’s impossible to locate the mentionned POT file.
                case "in_repo":
                    pot_file_exists = (current_branch.checkout_path / pot_file).is_file()
                    if not pot_file_exists and (current_branch.checkout_path / self.pot_params).is_file():
                        pot_file = Path(self.pot_params)

                case "subtitles":
                    srt_files = [p for p in po_files_directory.iterdir() if p.is_file() and p.name.endswith(".srt")]
                    if not srt_files:
                        # Try once more at parent level
                        srt_files = [
                            p for p in po_files_directory.parent.iterdir() if p.is_file() and p.name.endswith(".srt")
                        ]
                        if not srt_files:
                            return "", (("error", gettext_noop("No subtitle files found.")),)
                    with srt_files[0].open(mode="r") as po_fh, pot_file.open(mode="wb") as pot_fh:
                        sub2po.convertsub(po_fh, pot_fh)

                # All the cases below create a command line instruction to extract the POT file.
                case "gettext":
                    try:
                        pot_command, environment_variables = self.get_xgettext_command(current_branch)
                    except Exception as err:
                        errors.append(
                            ("error", str(err))  # error is a tuple of a level and a message
                        )

                case "intltool":
                    pot_command = ["intltool-update", "-g", self.pot_base(), "-p"]
                    if self.module.bugs_base:
                        environment_variables = {"XGETTEXT_ARGS": f'"--msgid-bugs-address={self.module.bugs_base}"'}

                case "shell":
                    pot_command = self.pot_params

            if pot_command:
                status, output, errs = run_shell_command(
                    pot_command, env=environment_variables, cwd=po_files_directory
                )
                if status != utils.STATUS_OK:
                    return "", (
                        (
                            "error",
                            gettext_noop(
                                "Error regenerating template file (POT) for %(file)s:\n<pre>%(cmd)s\n%(output)s</pre>"
                            )
                            % {
                                "file": self.pot_base(),
                                "cmd": " ".join(pot_command) if isinstance(pot_command, list) else pot_command,
                                "output": force_str(errs),
                            },
                        ),
                    )
                if not pot_file.exists():
                    # Try to get POT file from command output, with path relative to checkout root
                    m = re.search(r"([\w/-]*\.pot)", output)
                    if m:
                        pot_file = current_branch.checkout_path / m.group(0)
                    else:
                        # Try to find .pot file in /po dir
                        for file_ in po_files_directory.iterdir():
                            if file_.match("*.pot"):
                                pot_file = file_
                                break
                elif pot_method == "gettext":
                    # Filter out strings NOT to be translated, typically icon file names.
                    utils.exclude_untrans_messages(pot_file)

        if errors:
            return "", errors
        if not pot_file.exists():
            return "", (("error", gettext_noop("Unable to generate template file (POT)")),)
        return pot_file, ()

    def pot_generation_type(self, branch: Branch) -> str:
        """
        Get the POT generation method after inferring the auto method to extract messages.
        """
        pot_method = self.pot_method
        if self.pot_method == "auto" and self.dtype == "ui":
            pot_method = "gettext" if branch.uses_meson else "intltool"
        return pot_method

    def get_xgettext_command(self, branch) -> tuple[list[str], dict[str, str]]:
        # Note: the command will be run from the po directory.
        xgettext_args = []
        vcs_path = branch.checkout_path / self.base_directory
        if self.pot_params:
            xgettext_args.extend(self.pot_params.split())
        for opt, value in [
            ("--directory", str(branch.checkout_path)),
            ("--from-code", "utf-8"),
            ("--add-comments", ""),
            ("--output", f"{self.pot_base()}.pot"),
        ]:
            if opt not in xgettext_args:
                xgettext_args.extend([opt, value] if value else [opt])
        if (vcs_path / "POTFILES.in").exists():
            xgettext_args = ["--files-from", "POTFILES.in", *xgettext_args]
        elif (vcs_path / "POTFILES").exists():
            xgettext_args = ["--files-from", "POTFILES", *xgettext_args]
        else:
            raise RuntimeError(f"No POTFILES file found in {self.base_directory}")

        if not utils.ITS_DATA_DIR.exists():
            utils.collect_its_data()
        env = {"GETTEXTDATADIRS": str(utils.ITS_DATA_DIR.parent)}
        if self.extra_its_dirs:
            env["GETTEXTDATADIRS"] = ":".join(
                [env["GETTEXTDATADIRS"]]
                + [str(branch.checkout_path / path) for path in self.extra_its_dirs.split(":")]
            )

        # Parse and use content from: "XGETTEXT_OPTIONS = --keyword=_ --keyword=N_"
        makefile = utils.MakefileWrapper.find_file(branch, [vcs_path], file_name="Makevars")
        if makefile:
            kwds_vars = makefile.read_variable("XGETTEXT_OPTIONS")
            if kwds_vars:
                xgettext_args.extend(kwds_vars.split())
        else:
            makefile = utils.MakefileWrapper.find_file(branch, [vcs_path], file_name="meson.build")
            if makefile:
                if makefile.read_variable("gettext.preset") == "glib" or not makefile.readable:
                    xgettext_args.extend(GLIB_PRESET)
                extra_args = makefile.read_variable("gettext.args")
                if extra_args:
                    xgettext_args.extend([extra_args] if isinstance(extra_args, str) else extra_args)
                datadirs = makefile.read_variable("gettext.data_dirs")
                if datadirs:
                    env["GETTEXTDATADIRS"] = ":".join(
                        [env["GETTEXTDATADIRS"]] + [str(branch.checkout_path / path) for path in datadirs]
                    )
        # Added last as some chars in it may disturb CLI parsing
        if self.module.bugs_base:
            xgettext_args.extend(["--msgid-bugs-address", self.module.bugs_base])
        return ["xgettext", *xgettext_args], env

    def commit_info(self, branch: "Branch", language: "Language") -> tuple[Path, bool, Path]:
        """
        :return: a 3-tuple:

            * absolute path to po file,
            * boolean telling if the file is new,
            * linguas_path -> if linguas edition needed, else None

        :raises UnableToCommit: file cannot be committed.
        """
        if branch.is_vcs_readonly:
            raise UnableToCommitError(
                _("The repository %s is read only. It is not possible to create commits.") % branch.module.name
            )

        absolute_po_path = branch.checkout_path / self.get_po_path(language.locale)
        po_file_already_exists = absolute_po_path.exists()
        linguas_file_path = None
        if not po_file_already_exists and self.linguas_location != "no":
            linguas_file_path = branch.checkout_path / self.base_directory / "LINGUAS"
            if not linguas_file_path.exists():
                raise UnableToCommitError(
                    _("Sorry, adding new translations when the LINGUAS file is not known is not supported.")
                )
        return absolute_po_path, po_file_already_exists, linguas_file_path

    def get_linguas(self, branch):
        """Return a linguas dict like this: {'langs':['lang1', lang2], 'error':"Error"}"""
        base_path = branch.checkout_path
        if self.linguas_location:
            # Custom (or no) linguas location
            if self.linguas_location == "no":
                return {"langs": None, "error": ""}
            if self.linguas_location.rsplit("/", maxsplit=1)[-1] == "LINGUAS":
                return utils.read_linguas_file(base_path / self.linguas_location)

            variable = "ALL_LINGUAS"
            if "#" in self.linguas_location:
                file_path, variable = self.linguas_location.split("#")
            else:
                file_path = self.linguas_location
            linguas_file = utils.MakefileWrapper(branch, base_path / file_path)
            langs = linguas_file.read_variable(variable)
            return {
                "langs": langs.split() if langs is not None else None,
                "error": _("Entry for this language is not present in %(var)s variable in %(file)s file.")
                % {"var": variable, "file": file_path},
            }
        # Standard linguas location
        if self.dtype == "ui":
            return utils.get_ui_linguas(branch, self.base_directory)
        if self.dtype == "doc":
            return utils.get_doc_linguas(branch, self.base_directory)
        raise ValueError("Domain dtype should be one of 'ui', 'doc'")


RELEASE_STATUS_CHOICES = (("official", "Official"), ("unofficial", "Unofficial"), ("xternal", "External"))


class Release(models.Model):
    name = models.SlugField(max_length=20, unique=True)
    description = models.CharField(max_length=50)
    string_frozen = models.BooleanField(default=False)
    status = models.CharField(max_length=12, choices=RELEASE_STATUS_CHOICES)
    # weight is used to sort releases, higher on top, below 0 in archives
    weight = models.IntegerField(default=0)
    branches = models.ManyToManyField(Branch, through="Category", related_name="releases")

    class Meta:
        db_table = "release"
        ordering = ("status", "-name")

    def __str__(self) -> str:
        return self.description

    def get_description(self):
        return _(self.description)

    @cached_property
    def excluded_domains(self):
        # Compute domains which doesn't apply for this release due to limited domain
        # (by branch_from/branch_to).
        limited_stats = (
            Statistics.objects.select_related("branch", "domain")
            .filter(branch__releases=self)
            .filter(language__isnull=True)
            .filter(models.Q(domain__branch_from__isnull=False) | models.Q(domain__branch_to__isnull=False))
        )
        return {st.domain for st in limited_stats if not st.branch.is_domain_connected(st.domain)}

    @classmethod
    def total_by_releases(cls, dtype, releases):
        """Get summary stats for all languages and ``releases``

        :return: ``stats`` dict with each language locale as the key
        :rtype:
            ::

                stats{
                  'll': {'lang': <language object>,
                         'stats': [percentage for release 1, percentage for release 2, ...],
                         'diff': difference in % between first and last release,
                        }
                  'll': ...
                }
        """
        stats = {}
        totals = [0] * len(releases)
        lang_dict = dict((lang.locale, lang) for lang in Language.objects.all())
        for rel in releases:
            query = (
                Statistics.objects.filter(domain__dtype=dtype, branch__releases=rel)
                .exclude(domain__in=rel.excluded_domains)
                .values("language__locale")
                .annotate(
                    trans=models.Sum("full_po__translated"),
                    fuzzy=models.Sum("full_po__fuzzy"),
                    untrans=models.Sum("full_po__untranslated"),
                )
                .order_by("language__name")
            )
            for line in query:
                locale = line["language__locale"]
                if locale and locale not in stats:
                    stats[locale] = {"lang": lang_dict[locale], "stats": [0] * len(releases)}
                if locale is None:  # POT stats
                    totals[releases.index(rel)] = line["untrans"]
                else:
                    stats[locale]["stats"][releases.index(rel)] = line["trans"]

        # Compute percentages
        def compute_percentage(x, y):
            return int(x / y * 100)

        for key, stat in stats.items():
            stat["stats"] = list(map(compute_percentage, stat["stats"], totals))
            stat["diff"] = stat["stats"][-1] - stat["stats"][0]
        return stats

    def total_strings(self):
        """Returns the total number of strings in the release as a tuple (doc_total, ui_total)"""
        # Use pot stats to compute total sum
        qs = (
            Statistics.objects.filter(branch__category__release=self, language__isnull=True)
            .exclude(domain__in=self.excluded_domains)
            .values("domain__dtype")
            .annotate(untrans=models.Sum("full_po__untranslated"))
        )
        totals = Counter()
        for line in qs:
            totals[line["domain__dtype"]] += line["untrans"]
        return totals["doc"], totals["ui"]

    def total_part_for_all_langs(self):
        """Return total partial UI strings for each language"""
        total_part_ui_strings = {}
        all_ui_pots = (
            Statistics.objects.select_related("part_po")
            .exclude(domain__in=self.excluded_domains)
            .filter(language__isnull=True, branch__releases=self, domain__dtype="ui")
        )
        all_ui_stats = (
            Statistics.objects.select_related("part_po", "language")
            .exclude(domain__in=self.excluded_domains)
            .filter(language__isnull=False, branch__releases=self, domain__dtype="ui")
            .values(
                "branch_id",
                "domain_id",
                "language__locale",
                "part_po__translated",
                "part_po__fuzzy",
                "part_po__untranslated",
            )
        )
        stats_d = {
            f"{st['branch_id']}-{st['domain_id']}-{st['language__locale']}": sum([
                st["part_po__translated"] or 0,
                st["part_po__fuzzy"] or 0,
                st["part_po__untranslated"] or 0,
            ])
            for st in all_ui_stats
        }
        for lang in Language.objects.all():
            total_part_ui_strings[lang.locale] = self.total_part_for_lang(lang, all_ui_pots, stats_d)
        return total_part_ui_strings

    def total_part_for_lang(self, lang, all_pots=None, all_stats_d=None):
        """For partial UI stats, the total number can differ from lang to lang, so we
        are bound to iterate each stats to sum it"""
        if all_pots is None:
            all_pots = (
                Statistics.objects.select_related("part_po")
                .exclude(domain__in=self.excluded_domains)
                .filter(language__isnull=True, branch__releases=self, domain__dtype="ui")
            )
        if all_stats_d is None:
            all_stats = (
                Statistics.objects.select_related("part_po", "language")
                .exclude(domain__in=self.excluded_domains)
                .filter(language=lang, branch__releases=self, domain__dtype="ui")
                .values(
                    "branch_id",
                    "domain_id",
                    "language__locale",
                    "part_po__translated",
                    "part_po__fuzzy",
                    "part_po__untranslated",
                )
            )
            all_stats_d = {
                "%d-%d-%s" % (st["branch_id"], st["domain_id"], st["language__locale"]): sum(
                    filter(None, [st["part_po__translated"], st["part_po__fuzzy"], st["part_po__untranslated"]])
                )
                for st in all_stats
            }
        total = 0
        for stat in all_pots:
            key = "%d-%d-%s" % (stat.branch_id, stat.domain_id, lang.locale)
            total += all_stats_d.get(key, (stat.part_po and stat.part_po.untranslated) or 0)
        return total

    def total_for_lang(self, lang):
        """Returns total translated/fuzzy/untranslated strings for a specific
        language"""

        total_doc, total_ui = self.total_strings()
        total_ui_part = self.total_part_for_lang(lang)
        query = (
            Statistics.objects.filter(language=lang, branch__releases=self)
            .exclude(domain__in=self.excluded_domains)
            .values("domain__dtype")
            .annotate(
                trans=Coalesce(models.Sum("full_po__translated"), models.Value(0)),
                fuzzy=Coalesce(models.Sum("full_po__fuzzy"), models.Value(0)),
                trans_p=Coalesce(models.Sum("part_po__translated"), models.Value(0)),
                fuzzy_p=Coalesce(models.Sum("part_po__fuzzy"), models.Value(0)),
            )
        )
        stats = {
            "id": self.id,
            "name": self.name,
            "description": _(self.description),
            "ui": {
                "translated": 0,
                "fuzzy": 0,
                "total": total_ui,
                "translated_perc": 0,
                "fuzzy_perc": 0,
                "untranslated_perc": 0,
            },
            "ui_part": {
                "translated": 0,
                "fuzzy": 0,
                "total": total_ui_part,
                "translated_perc": 0,
                "fuzzy_perc": 0,
                "untranslated_perc": 0,
            },
            "doc": {
                "translated": 0,
                "fuzzy": 0,
                "total": total_doc,
                "translated_perc": 0,
                "fuzzy_perc": 0,
                "untranslated_perc": 0,
            },
        }
        for res in query:
            if res["domain__dtype"] == "ui":
                stats["ui"]["translated"] = res["trans"]
                stats["ui"]["fuzzy"] = res["fuzzy"]
                stats["ui_part"]["translated"] = res["trans_p"]
                stats["ui_part"]["fuzzy"] = res["fuzzy_p"]
            if res["domain__dtype"] == "doc":
                stats["doc"]["translated"] = res["trans"]
                stats["doc"]["fuzzy"] = res["fuzzy"]
        stats["ui"]["untranslated"] = total_ui - (stats["ui"]["translated"] + stats["ui"]["fuzzy"])
        stats["ui_part"]["untranslated"] = total_ui_part - (stats["ui_part"]["translated"] + stats["ui_part"]["fuzzy"])
        if total_ui > 0:
            stats["ui"]["translated_perc"] = int(100 * stats["ui"]["translated"] / total_ui)
            stats["ui"]["fuzzy_perc"] = int(100 * stats["ui"]["fuzzy"] / total_ui)
            stats["ui"]["untranslated_perc"] = int(100 * stats["ui"]["untranslated"] / total_ui)
        if total_ui_part > 0:
            stats["ui_part"]["translated_perc"] = int(100 * stats["ui_part"]["translated"] / total_ui_part)
            stats["ui_part"]["fuzzy_perc"] = int(100 * stats["ui_part"]["fuzzy"] / total_ui_part)
            stats["ui_part"]["untranslated_perc"] = int(100 * stats["ui_part"]["untranslated"] / total_ui_part)
        stats["doc"]["untranslated"] = total_doc - (stats["doc"]["translated"] + stats["doc"]["fuzzy"])
        if total_doc > 0:
            stats["doc"]["translated_perc"] = int(100 * stats["doc"]["translated"] / total_doc)
            stats["doc"]["fuzzy_perc"] = int(100 * stats["doc"]["fuzzy"] / total_doc)
            stats["doc"]["untranslated_perc"] = int(100 * stats["doc"]["untranslated"] / total_doc)
        return stats

    def get_global_stats(self):
        """Get statistics for all languages in a release, grouped by language
        Returns a sorted list: (language name and locale, ui, ui-part and doc stats dictionaries)"""

        query = (
            Statistics.objects.filter(language__isnull=False, branch__releases=self)
            .exclude(domain__in=self.excluded_domains)
            .values("domain__dtype", "language")
            .annotate(
                trans=Coalesce(models.Sum("full_po__translated"), models.Value(0)),
                fuzzy=Coalesce(models.Sum("full_po__fuzzy"), models.Value(0)),
                trans_p=Coalesce(models.Sum("part_po__translated"), models.Value(0)),
                fuzzy_p=Coalesce(models.Sum("part_po__fuzzy"), models.Value(0)),
                locale=models.Min("language__locale"),
                lang_name=models.Min("language__name"),
            )
            .order_by("domain__dtype", "trans")
        )
        stats = {}
        total_docstrings, total_uistrings = self.total_strings()
        total_uistrings_part = self.total_part_for_all_langs()
        for row in query:
            locale = row["locale"]
            if locale not in stats:
                # Initialize stats dict
                stats[locale] = {
                    "lang_name": row["lang_name"],
                    "lang_locale": locale,
                    "ui": {
                        "translated": 0,
                        "fuzzy": 0,
                        "untranslated": total_uistrings,
                        "translated_perc": 0,
                        "fuzzy_perc": 0,
                        "untranslated_perc": 100,
                    },
                    "ui_part": {
                        "translated": 0,
                        "fuzzy": 0,
                        "untranslated": total_uistrings_part[locale],
                        "translated_perc": 0,
                        "fuzzy_perc": 0,
                        "untranslated_perc": 100,
                    },
                    "doc": {
                        "translated": 0,
                        "fuzzy": 0,
                        "untranslated": total_docstrings,
                        "translated_perc": 0,
                        "fuzzy_perc": 0,
                        "untranslated_perc": 100,
                    },
                }
            if row["domain__dtype"] == "doc":
                stats[locale]["doc"]["translated"] = row["trans"]
                stats[locale]["doc"]["fuzzy"] = row["fuzzy"]
                stats[locale]["doc"]["untranslated"] = total_docstrings - (row["trans"] + row["fuzzy"])
                if total_docstrings > 0:
                    stats[locale]["doc"]["translated_perc"] = int(100 * row["trans"] / total_docstrings)
                    stats[locale]["doc"]["fuzzy_perc"] = int(100 * row["fuzzy"] / total_docstrings)
                    stats[locale]["doc"]["untranslated_perc"] = int(
                        100 * stats[locale]["doc"]["untranslated"] / total_docstrings
                    )
            if row["domain__dtype"] == "ui":
                stats[locale]["ui"]["translated"] = row["trans"]
                stats[locale]["ui"]["fuzzy"] = row["fuzzy"]
                stats[locale]["ui"]["untranslated"] = total_uistrings - (row["trans"] + row["fuzzy"])
                stats[locale]["ui_part"]["translated"] = row["trans_p"]
                stats[locale]["ui_part"]["fuzzy"] = row["fuzzy_p"]
                stats[locale]["ui_part"]["untranslated"] = total_uistrings_part[locale] - (
                    row["trans_p"] + row["fuzzy_p"]
                )
                if total_uistrings > 0:
                    stats[locale]["ui"]["translated_perc"] = int(100 * row["trans"] / total_uistrings)
                    stats[locale]["ui"]["fuzzy_perc"] = int(100 * row["fuzzy"] / total_uistrings)
                    stats[locale]["ui"]["untranslated_perc"] = int(
                        100 * stats[locale]["ui"]["untranslated"] / total_uistrings
                    )
                if total_uistrings_part.get(locale, 0) > 0:
                    stats[locale]["ui_part"]["translated_perc"] = int(
                        100 * row["trans_p"] / total_uistrings_part[locale]
                    )
                    stats[locale]["ui_part"]["fuzzy_perc"] = int(100 * row["fuzzy_p"] / total_uistrings_part[locale])
                    stats[locale]["ui_part"]["untranslated_perc"] = int(
                        100 * stats[locale]["ui_part"]["untranslated"] / total_uistrings_part[locale]
                    )

        results = list(stats.values())
        # Sort by most translated first
        results.sort(key=lambda st: (-st["ui"]["translated"], -st["doc"]["translated"], st["lang_name"]))
        return results

    def get_lang_stats(self, lang):
        """Get statistics for a specific language, producing the stats data structure
        Used for displaying the language-release template"""

        return {
            "doc": Statistics.get_lang_stats_by_type(lang, "doc", self),
            "ui": Statistics.get_lang_stats_by_type(lang, "ui", self),
        }

    def get_lang_files(self, lang, dtype):
        """Return a list of all po files of a lang for this release, preceded by the more recent modification date
        It uses the POT file if there is no po for a module"""
        partial = False
        if dtype == "ui-part":
            dtype, partial = "ui", True
        pot_stats = Statistics.objects.exclude(domain__in=self.excluded_domains).filter(
            language=None, branch__releases=self, domain__dtype=dtype, full_po__isnull=False
        )
        po_stats = {
            f"{st.branch_id}-{st.domain_id}": st
            for st in Statistics.objects.filter(language=lang, branch__releases=self, domain__dtype=dtype)
        }
        lang_files = []
        last_modif_date = datetime(1970, 1, 1, tzinfo=dt_timezone.utc)
        # Create list of files
        for stat in pot_stats:
            last_modif_date = max(stat.full_po.updated, last_modif_date)
            key = f"{stat.branch_id}-{stat.domain_id}"
            lang_stat = po_stats.get(key, stat)
            file_path = lang_stat.po_path(reduced=partial)
            if os.access(file_path, os.R_OK):
                lang_files.append(file_path)
        return last_modif_date, lang_files

    def get_absolute_url(self: "Release") -> str:
        return reverse("release", args=[self.name])


class CategoryName(models.Model):
    name = models.CharField(max_length=30, unique=True)

    class Meta:
        db_table = "categoryname"

    def __str__(self) -> str:
        return self.name


class Category(models.Model):
    release = models.ForeignKey(Release, on_delete=models.CASCADE)
    branch = models.ForeignKey(Branch, on_delete=models.CASCADE)
    name = models.ForeignKey(CategoryName, on_delete=models.PROTECT)

    class Meta:
        db_table = "category"
        verbose_name_plural = "categories"
        unique_together = ("release", "branch")

    def __str__(self) -> str:
        return f"{self.name} ({self.release}, {self.branch})"


class PoFile(models.Model):
    # File type fields of Django may not be flexible enough for our use case
    path = models.CharField(max_length=255, blank=True)
    updated = models.DateTimeField(auto_now_add=True)
    translated = models.IntegerField(default=0)
    fuzzy = models.IntegerField(default=0)
    untranslated = models.IntegerField(default=0)
    # List of figure dict
    figures = models.JSONField(blank=True, null=True)
    # words statistics
    translated_words = models.IntegerField(default=0)
    fuzzy_words = models.IntegerField(default=0)
    untranslated_words = models.IntegerField(default=0)

    class Meta:
        db_table = "pofile"

    def __str__(self) -> str:
        return f"{self.path} ({self.translated}/{self.fuzzy}/{self.untranslated})"

    @property
    def url(self):
        return utils.url_join(settings.MEDIA_URL, settings.UPLOAD_DIR, self.filename())

    @property
    def prefix(self):
        return settings.SCRATCHDIR

    @property
    def full_path(self):
        return self.prefix / self.path.lstrip("/")

    def filename(self):
        return Path(self.path).name

    def pot_size(self, words=False):
        if words:
            return self.translated_words + self.fuzzy_words + self.untranslated_words
        return self.translated + self.fuzzy + self.untranslated

    def fig_count(self):
        """If stat of a document type, get the number of figures in the document"""
        return len(self.figures) if self.figures else 0

    def tr_percentage(self):
        pot_size = self.pot_size()
        if pot_size == 0:
            return 0
        return int(100 * self.translated / pot_size)

    def tr_word_percentage(self):
        pot_size = self.pot_size(words=True)
        if pot_size == 0:
            return 0
        return int(100 * self.translated_words / pot_size)

    def fu_percentage(self):
        pot_size = self.pot_size()
        if pot_size == 0:
            return 0
        return int(100 * self.fuzzy / pot_size)

    def fu_word_percentage(self):
        pot_size = self.pot_size(words=True)
        if pot_size == 0:
            return 0
        return int(100 * self.fuzzy_words / pot_size)

    def un_percentage(self):
        pot_size = self.pot_size()
        if pot_size == 0:
            return 0
        return int(100 * self.untranslated / pot_size)

    def un_word_percentage(self):
        pot_size = self.pot_size(words=True)
        if pot_size == 0:
            return 0
        return int(100 * self.untranslated_words / pot_size)

    def update_statistics(self) -> None:
        stats = utils.po_file_statistics(Path(self.full_path))
        self.translated = stats["translated"]
        self.fuzzy = stats["fuzzy"]
        self.untranslated = stats["untranslated"]

        self.translated_words = stats["translated_words"]
        self.fuzzy_words = stats["fuzzy_words"]
        self.untranslated_words = stats["untranslated_words"]

        self.save()


class StatisticsBase:
    @property
    @abc.abstractmethod
    def is_fake(self):
        pass

    @abc.abstractmethod
    def get_lang(self):
        pass

    @abc.abstractmethod
    def po_url(self, pot_file=False, reduced=False):
        pass


class Statistics(models.Model, StatisticsBase):
    @property
    def is_fake(self) -> bool:
        return False

    branch = models.ForeignKey(Branch, on_delete=models.CASCADE)
    domain = models.ForeignKey(Domain, on_delete=models.CASCADE)
    language = models.ForeignKey(Language, null=True, on_delete=models.CASCADE)

    full_po = models.OneToOneField(PoFile, null=True, related_name="stat_f", on_delete=models.SET_NULL)
    part_po = models.OneToOneField(PoFile, null=True, related_name="stat_p", on_delete=models.SET_NULL)

    class Meta:
        db_table = "statistics"
        verbose_name = "statistics"
        verbose_name_plural = verbose_name
        unique_together = ("branch", "domain", "language")

    def __init__(self, *args, **kwargs) -> None:
        super().__init__(*args, **kwargs)
        self.partial_po = False  # True if part of a multiple po module
        self.info_list = []

    def __str__(self) -> str:
        return (
            f"{self.branch.module.name} ({self.domain.dtype}-{self.domain.name}) "
            f"{self.branch.name} ({self.get_lang()})"
        )

    def translated(self, scope="full"):
        return getattr(self.part_po if "part" in scope else self.full_po, "translated", 0)

    def translated_words(self, scope="full"):
        return getattr(self.part_po if "part" in scope else self.full_po, "translated_words", 0)

    def fuzzy(self, scope="full"):
        return getattr(self.part_po if "part" in scope else self.full_po, "fuzzy", 0)

    def fuzzy_words(self, scope="full"):
        return getattr(self.part_po if scope == "part" else self.full_po, "fuzzy_words", 0)

    def untranslated(self, scope="full"):
        return getattr(self.part_po if scope == "part" else self.full_po, "untranslated", 0)

    def untranslated_words(self, scope="full"):
        return getattr(self.part_po if scope == "part" else self.full_po, "untranslated_words", 0)

    def is_pot_stats(self):
        return self.language_id is None

    def get_type(self):
        """Returns the type of the domain (ui, docbook, mallard)"""
        if self.domain.dtype == "ui":
            return "ui"
        return self.domain.doc_format(self.branch).format

    def tr_percentage(self, scope="full"):
        if "full" in scope and self.full_po:
            return self.full_po.tr_percentage()
        if "part" in scope and self.part_po:
            return self.part_po.tr_percentage()
        return 0

    def tr_word_percentage(self, scope="full"):
        if "full" in scope and self.full_po:
            return self.full_po.tr_word_percentage()
        if "part" in scope and self.part_po:
            return self.part_po.tr_word_percentage()
        return 0

    def fu_percentage(self, scope="full"):
        if "full" in scope and self.full_po:
            return self.full_po.fu_percentage()
        if "part" in scope and self.part_po:
            return self.part_po.fu_percentage()
        return 0

    def fu_word_percentage(self, scope="full"):
        if "full" in scope and self.full_po:
            return self.full_po.fu_word_percentage()
        if "part" in scope and self.part_po:
            return self.part_po.fu_word_percentage()
        return 0

    def un_percentage(self, scope="full"):
        if "full" in scope and self.full_po:
            return self.full_po.un_percentage()
        if "part" in scope and self.part_po:
            return self.part_po.un_percentage()
        return 0

    def un_word_percentage(self, scope="full"):
        if "full" in scope and self.full_po:
            return self.full_po.un_word_percentage()
        if "part" in scope and self.part_po:
            return self.part_po.un_word_percentage()
        return 0

    def get_lang(self):
        if not self.is_pot_stats():
            return _("%(lang_name)s (%(lang_locale)s)") % {
                "lang_name": _(self.language.name),
                "lang_locale": self.language.locale,
            }
        return "pot file"

    @property
    def module_name(self):
        return self.branch.module.name

    @property
    def module_description(self):
        return self.branch.module.get_description()

    def has_reducedstat(self):
        return bool(self.part_po is not None and self.part_po != self.full_po)

    def filename(self, potfile=False, reduced=False) -> str:
        if not self.is_pot_stats() and not potfile:
            return (
                f"{self.domain.pot_base()}.{self.branch.name_escaped}."
                f"{self.language.locale}.{'reduced.' if reduced else ''}po"
            )
        return f"{self.domain.pot_base()}.{self.branch.name_escaped}.{'reduced.' if reduced else ''}pot"

    def pot_stats_summary(self) -> str:
        """
        Returns a string with the statistics of the po file. For instance:
        (20 messages — 0 words, 2 figures) — updated 09/09/2015 18:27+0000
        """
        if self.full_po:
            pot_size = self.full_po.pot_size()
            pot_words_size = self.full_po.pot_size(words=True)
            fig_count = self.full_po.fig_count()
            # Return stat table header: 'POT file (n messages) - updated on ??-??-???? tz'
            msg_text = ngettext("%(count)s message", "%(count)s messages", pot_size) % {"count": pot_size}
            upd_text = _("updated on %(date)s") % {
                # Date format syntax is similar to PHP http://www.php.net/date
                "date": dateformat.format(self.full_po.updated, _("Y-m-d g:i a O"))
            }
            words_text = ngettext("%(count)s word", "%(count)s words", pot_words_size) % {"count": pot_words_size}
            if fig_count:
                fig_text = ngettext("%(count)s figure", "%(count)s figures", fig_count) % {"count": fig_count}
                text = _("(%(messages)s — %(words)s, %(figures)s) — %(updated)s") % {
                    "messages": msg_text,
                    "figures": fig_text,
                    "updated": upd_text,
                    "words": words_text,
                }
            else:
                text = _("(%(messages)s — %(words)s) — %(updated)s") % {
                    "messages": msg_text,
                    "updated": upd_text,
                    "words": words_text,
                }
            return text
        raise ValueError(_("Unable to summarise the POT statistics, the file is missing."))

    def has_figures(self):
        return bool(self.full_po and self.full_po.figures)

    def get_figures(self):
        """Return an enriched list of figure dicts (used in module_images.html):
        [{'path':, 'hash':, 'fuzzy':, 'translated':, 'translated_file':}, ...]"""
        figures = []
        if self.full_po and self.domain.dtype == "doc" and self.full_po.figures:
            # something like: "https://gitlab.gnome.org/GNOME/vinagre / raw / master / help / %s / %s"
            url_model = utils.url_join(
                self.branch.vcs_web_url,
                self.branch.img_url_prefix,
                self.branch.name,
                self.domain.base_directory,
                "%s",
                "%s",
            )
            for fig in self.full_po.figures:
                fig2 = fig.copy()
                fig2["orig_remote_url"] = url_model % ("C", fig["path"])
                fig2["translated_file"] = False
                # Check if a translated figure really exists or if the English one is used
                if (
                    self.language
                    and (
                        self.branch.checkout_path / self.domain.base_directory / self.language.locale / fig["path"]
                    ).exists()
                ):
                    fig2["trans_remote_url"] = url_model % (self.language.locale, fig["path"])
                    fig2["translated_file"] = True
                figures.append(fig2)
        return figures

    def fig_stats(self) -> dict[str, int | float]:
        stats = {"fuzzy": 0, "translated": 0, "total": 0, "prc": 0}
        if self.full_po and self.full_po.figures:
            for fig in self.full_po.figures:
                stats["total"] += 1
                if fig.get("fuzzy", 0):
                    stats["fuzzy"] += 1
                elif fig.get("translated", 0):
                    stats["translated"] += 1
        stats["untranslated"] = stats["total"] - (stats["translated"] + stats["fuzzy"])
        if stats["total"] > 0:
            stats["prc"] = 100 * stats["translated"] / stats["total"]
        return stats

    def vcs_web_path(self):
        """Return the Web interface path of file on remote vcs"""
        return utils.url_join(self.branch.vcs_web_url, self.domain.base_directory)

    def po_path(self, potfile=False, reduced=False):
        """Return path of (merged) po file on local filesystem"""
        subdir = ""
        if self.domain.dtype == "doc":
            subdir = "docs"
        path = (
            settings.POT_DIR
            / f"{self.module_name}.{self.branch.name_escaped}"
            / subdir
            / self.filename(potfile, reduced)
        )
        if reduced and not path.exists():
            path = self.po_path(potfile=potfile, reduced=False)
        return path

    def po_url(self, pot_file=False, reduced=False):
        """Return URL of (merged) po file, e.g. for downloading the file"""
        subdir = ""
        if self.domain.dtype == "doc":
            subdir = "docs/"
        return utils.url_join(
            "/POT/", f"{self.module_name}.{self.branch.name_escaped}", subdir, self.filename(pot_file, reduced)
        )

    def pot_url(self):
        return self.po_url(pot_file=True)

    def update_statistics(self, file_path=None) -> None:
        """
        Update the Statistics instance with the gettext stats of the target
        po/pot file.
        Also try to generate a "reduced" po file.
        """
        if file_path is None and self.full_po:
            file_path = self.full_po.full_path
        stats = utils.po_file_statistics(file_path)
        for err in stats["errors"]:
            self.set_error(*err)
        # If stats are 100%, compare the total number of strings between
        # committed po (pofile) and merged po (outpo).
        # Until https://savannah.gnu.org/bugs/index.php?50910 is fixed.
        if self.language and stats["fuzzy"] + stats["untranslated"] == 0:  # Fully translated po file
            abs_po_path = self.branch.checkout_path / self.domain.get_po_path(self.language.locale)
            if abs_po_path.exists():
                git_stats = utils.po_file_statistics(abs_po_path)
                if stats["translated"] > git_stats["translated"]:
                    self.set_error(
                        "warn",
                        gettext_noop(
                            "The original file with strings to translate (.pot) has been updated by the software "
                            "developers, and the changes have to be merged to the translation file (.po). Please "
                            "commit the translation file again in order to fix this problem."
                        ),
                    )
        try:
            df = self.domain.doc_format(self.branch)
        except Exception:
            df = None
        fig_stats = utils.get_fig_stats(file_path, df)

        relpath = os.path.relpath(file_path, settings.SCRATCHDIR)
        if not self.full_po:
            self.full_po = PoFile.objects.create(path=relpath)
            self.save()
        self.full_po.path = relpath
        self.full_po.translated = int(stats["translated"])
        self.full_po.fuzzy = int(stats["fuzzy"])
        self.full_po.untranslated = int(stats["untranslated"])
        self.full_po.translated_words = int(stats["translated_words"])
        self.full_po.fuzzy_words = int(stats["fuzzy_words"])
        self.full_po.untranslated_words = int(stats["untranslated_words"])
        self.full_po.figures = fig_stats
        self.full_po.updated = timezone.now()
        self.full_po.save()

        if self.domain.dtype == "ui":

            def part_po_equals_full_po() -> None:
                if self.part_po == self.full_po:
                    return
                if self.part_po and self.part_po != self.full_po:
                    self.part_po.delete()
                self.part_po = self.full_po
                self.save()

            # Try to compute a reduced po file
            if (
                (self.full_po.fuzzy + self.full_po.untranslated) > 0
                and not self.branch.is_archive_only
                and self.domain.red_filter != "-"
            ):
                # Generate partial_po and store partial stats
                if self.full_po.path.endswith(".pot"):
                    part_po_path = self.full_po.full_path.with_suffix(".reduced.pot")
                else:
                    part_po_path = self.full_po.full_path.with_suffix(".reduced.po")
                utils.po_grep(self.full_po.full_path, str(part_po_path), self.domain.red_filter)
                part_stats = utils.po_file_statistics(part_po_path)
                if (
                    part_stats["translated"] + part_stats["fuzzy"] + part_stats["untranslated"]
                    == stats["translated"] + stats["fuzzy"] + stats["untranslated"]
                ):
                    # No possible gain, set part_po = full_po so it is possible to
                    # compute complete stats at database level
                    part_po_equals_full_po()
                    part_po_path.unlink()
                else:
                    utils.add_custom_header(part_po_path, "X-DamnedLies-Scope", "partial")
                    relpath = os.path.relpath(part_po_path, settings.SCRATCHDIR)
                    if not self.part_po or self.part_po == self.full_po:
                        self.part_po = PoFile.objects.create(path=relpath)
                        self.save()
                    self.part_po.path = relpath
                    self.part_po.translated = part_stats["translated"]
                    self.part_po.fuzzy = part_stats["fuzzy"]
                    self.part_po.untranslated = part_stats["untranslated"]
                    self.part_po.translated_words = part_stats["translated_words"]
                    self.part_po.fuzzy_words = part_stats["fuzzy_words"]
                    self.part_po.untranslated_words = part_stats["untranslated_words"]
                    self.part_po.updated = timezone.now()
                    self.part_po.save()
            else:
                part_po_equals_full_po()

        elif self.domain.dtype == "doc" and self.language is not None:
            fig_errors = utils.check_identical_figures(
                fig_stats, self.branch.domain_path(self.domain), self.language.locale
            )
            for err in fig_errors:
                self.set_error(*err)

        logger.debug("%s:\n%s", self.language, str(stats))

    def set_error(self, tp, description) -> None:
        Information.objects.create(statistics=self, type=tp, description=description)

    def most_important_message(self):
        """Return a message of type 1.'error', or 2.'warn, or 3.'warn"""
        error = None
        for e in self.information_set.all():
            if (
                not error
                or e.type in {"error", "error-ext"}
                or (e.type in {"warn", "warn-ext"} and error.type == "info")
            ):
                error = e
        return error

    @classmethod
    def get_lang_stats_by_type(cls, lang, dtype, release):
        """Cook statistics for an entire release, a domain type ``dtype`` and the language ``lang``.

        :return: ``stats`` dictionary
        :rtype:
            ::

                stats = {
                    'dtype':dtype, # 'ui' or 'doc'
                    'total': 0,
                    'totaltrans': 0,
                    'totalfuzzy': 0,
                    'totaluntrans': 0,
                    'totaltransperc': 0,
                    'totalfuzzyperc': 0,
                    'totaluntransperc': 0,
                    'categs': {  # OrderedDict
                        <categkey>: {
                            'catname': <catname>, # translated category name
                            'cattotal': 0,
                            'cattrans': 0,
                            'catfuzzy': 0,
                            'catuntrans': 0,
                            'cattransperc': 0,
                            'modules': { # OrderedDict
                                <module>: {
                                    <branchname>:
                                        [(<domname>, <stat>), ...], # List of tuples (domain name, Statistics object)
                                               # First element is a placeholder for a FakeSummaryStatistics object
                                               # only used for summary if module has more than 1 domain
                                    }
                                }
                            }
                        }
                    },
                    'all_errors':[]
                }
        """
        # Import here to prevent a circular dependency
        # pylint: disable=import-outside-toplevel
        from vertimus.models import Action, State

        scope = "full"
        if dtype.endswith("-part"):
            dtype = dtype[:-5]
            scope = "part"

        stats = {
            "dtype": dtype,
            "totaltrans": 0,
            "totalfuzzy": 0,
            "totaluntrans": 0,
            "totaltransperc": 0,
            "totalfuzzyperc": 0,
            "totaluntransperc": 0,
            "categs": OrderedDict(),
            "all_errors": [],
        }
        # Sorted by module to allow grouping ('fake' stats)
        pot_stats = Statistics.objects.select_related("domain", "branch__module", "full_po", "part_po")
        if release:
            pot_stats = pot_stats.filter(language=None, branch__releases=release, domain__dtype=dtype).order_by(
                "branch__module__name"
            )
            categ_names = {
                cat.branch_id: cat.name.name
                for cat in Category.objects.select_related("branch", "name").filter(release=release)
            }
        else:
            pot_stats = pot_stats.filter(language=None, domain__dtype=dtype).order_by("branch__module__name")

        tr_stats = Statistics.objects.select_related("domain", "language", "branch__module", "full_po", "part_po")
        if release:
            tr_stats = tr_stats.filter(language=lang, branch__releases=release, domain__dtype=dtype).order_by(
                "branch__module__id"
            )
        else:
            tr_stats = tr_stats.filter(language=lang, domain__dtype=dtype).order_by("branch__module__id")
        tr_stats_dict = {f"{st.branch_id}-{st.domain_id}": st for st in tr_stats}

        infos_dict = Information.get_info_dict(lang)

        # Prepare State objects in a dict (with "branch_id-domain_id" as key), to save database queries later
        vt_states = State.objects.select_related("branch", "domain")
        if release:
            vt_states = vt_states.filter(language=lang, branch__releases=release, domain__dtype=dtype)
        else:
            vt_states = vt_states.filter(language=lang, domain__dtype=dtype)
        vt_states_dict = {f"{vt.branch_id}-{vt.domain_id}": vt for vt in vt_states}

        # Get comments from last action of State objects
        actions = Action.objects.filter(state_db__in=vt_states, comment__isnull=False).order_by("created")
        actions_dict = {action.state_db_id: action for action in actions}
        for vt_state in vt_states_dict.values():
            if vt_state.id in actions_dict:
                vt_state.last_comment = actions_dict[vt_state.id].comment

        for stat in pot_stats:
            if not stat.branch.is_domain_connected(stat.domain):
                continue
            categ_key = "Default"
            if release:
                categ_key = categ_names.get(stat.branch_id)
            domname = _(stat.domain.description) if stat.domain.description else ""
            branchname = stat.branch.name
            module = stat.branch.module
            if categ_key not in stats["categs"]:
                stats["categs"][categ_key] = {
                    "cattrans": 0,
                    "catfuzzy": 0,
                    "catuntrans": 0,
                    "cattransperc": 0,
                    "catname": _(categ_key),
                    "modules": OrderedDict(),
                }
            # Try to get translated stat, else stick with POT stat
            br_dom_key = "%d-%d" % (stat.branch.id, stat.domain.id)
            if br_dom_key in tr_stats_dict:
                stat = tr_stats_dict[br_dom_key]
            stat.active = lang is not None and not module.archived

            # Match stat with error list
            if stat.id in infos_dict:
                stat.info_list = infos_dict[stat.id]
                stats["all_errors"].extend(stat.info_list)

            # Search if a state exists for this statistic
            if br_dom_key in vt_states_dict:
                stat.state = vt_states_dict[br_dom_key]
                if not stat.active and stat.state.name != "None":
                    stat.active = True

            stats["totaltrans"] += stat.translated(scope)
            stats["totalfuzzy"] += stat.fuzzy(scope)
            stats["totaluntrans"] += stat.untranslated(scope)
            stats["categs"][categ_key]["cattrans"] += stat.translated(scope)
            stats["categs"][categ_key]["catfuzzy"] += stat.fuzzy(scope)
            stats["categs"][categ_key]["catuntrans"] += stat.untranslated(scope)
            if module not in stats["categs"][categ_key]["modules"]:
                # first element is a placeholder for a fake stat
                stats["categs"][categ_key]["modules"][module] = {branchname: [[" fake", None], [domname, stat]]}
            elif branchname not in stats["categs"][categ_key]["modules"][module]:
                # first element is a placeholder for a fake stat
                stats["categs"][categ_key]["modules"][module][branchname] = [[" fake", None], [domname, stat]]
            else:
                # Here we add the 2nd or more stat to the same module-branch
                if len(stats["categs"][categ_key]["modules"][module][branchname]) == 2:
                    # Create a fake statistics object for module summary
                    stats["categs"][categ_key]["modules"][module][branchname][0][1] = FakeSummaryStatistics(
                        stat.domain.module, stat.branch, dtype
                    )
                    stats["categs"][categ_key]["modules"][module][branchname][0][1].trans(
                        stats["categs"][categ_key]["modules"][module][branchname][1][1]
                    )
                stats["categs"][categ_key]["modules"][module][branchname].append([domname, stat])
                stats["categs"][categ_key]["modules"][module][branchname][0][1].trans(stat)

        # Compute percentages and sorting
        stats["total"] = stats["totaltrans"] + stats["totalfuzzy"] + stats["totaluntrans"]
        if stats["total"] > 0:
            stats["totaltransperc"] = int(100 * stats["totaltrans"] / stats["total"])
            stats["totalfuzzyperc"] = int(100 * stats["totalfuzzy"] / stats["total"])
            stats["totaluntransperc"] = int(100 * stats["totaluntrans"] / stats["total"])
        stats["categs"] = OrderedDict(sorted(stats["categs"].items(), key=lambda t: t[0]))
        for categ in stats["categs"].values():
            categ["cattotal"] = categ["cattrans"] + categ["catfuzzy"] + categ["catuntrans"]
            if categ["cattotal"] > 0:
                categ["cattransperc"] = int(100 * categ["cattrans"] / categ["cattotal"])
            # Sort domains
            for mod in categ["modules"].values():
                for doms in mod.values():
                    doms.sort(key=itemgetter(0))
        # Sort errors
        stats["all_errors"].sort()
        return stats


class FakeLangStatistics(StatisticsBase):
    """
    Statistics class for a non-existing lang stats.
    It renders the statistics based on what is found in the original POT statistics.
    """

    is_fake = True

    def __init__(self, pot_statistics: Statistics, language: Language) -> None:
        self.stat = pot_statistics
        self.language = language

    def __getattr__(self, attr):
        # Wrap all non-existing attribute access to self.stat
        return getattr(self.stat, attr)

    def get_lang(self):
        return _("%(lang_name)s (%(lang_locale)s)") % {
            "lang_name": _(self.language.name),
            "lang_locale": self.language.locale,
        }

    def po_url(self, potfile=False, reduced=False):
        if reduced:
            locale = f"{self.language.locale}-reduced"
        else:
            locale = self.language.locale
        return reverse(
            "dynamic_po",
            kwargs={
                "module_name": self.branch.module.name,
                "domain_name": self.domain.name,
                "branch_name": self.branch.name,
                "filename": f"{locale}.po",
            },
        )


class FakeSummaryStatistics:
    """Statistics class that sums up an entire module stats"""

    is_fake = True

    def __init__(self, module, branch, dtype) -> None:
        self.module = module
        self.branch = branch
        self.domain = module.domain_set.filter(dtype=dtype)[0]
        self._translated = 0
        self._fuzzy = 0
        self._untranslated = 0
        self.partial_po = False
        self._translated_words = 0
        self._fuzzy_words = 0
        self._untranslated_words = 0

    def translated(self, scope=None):
        return self._translated

    def translated_words(self, scope=None):
        return self._translated_words

    def fuzzy(self, scope=None):
        return self._fuzzy

    def fuzzy_words(self, scope=None):
        return self._fuzzy_words

    def untranslated(self, scope=None):
        return self._untranslated

    def untranslated_words(self, scope=None):
        return self._untranslated_words

    def trans(self, stat) -> None:
        self._translated += stat.translated()
        self._fuzzy += stat.fuzzy()
        self._untranslated += stat.untranslated()
        stat.partial_po = True

    def pot_size(self):
        return int(self._translated) + int(self._fuzzy) + int(self._untranslated)

    def tr_percentage(self, scope="full"):
        if self.pot_size() == 0:
            return 0
        return int(100 * self._translated / self.pot_size())

    def fu_percentage(self, scope="full"):
        if self.pot_size() == 0:
            return 0
        return int(100 * self._fuzzy / self.pot_size())

    def un_percentage(self, scope="full"):
        if self.pot_size() == 0:
            return 0
        return int(100 * self._untranslated / self.pot_size())


class StatisticsArchived(models.Model):
    module = models.TextField()
    type = models.CharField(max_length=3, choices=Domain.DOMAIN_TYPE_CHOICES)
    domain = models.TextField()
    branch = models.TextField()
    language = models.CharField(max_length=15)
    date = models.DateTimeField()
    translated = models.IntegerField(default=0)
    fuzzy = models.IntegerField(default=0)
    untranslated = models.IntegerField(default=0)

    class Meta:
        db_table = "statistics_archived"


INFORMATION_TYPE_CHOICES = (
    ("info", "Information"),
    ("warn", "Warning"),
    ("error", "Error"),
    # Type of warning/error external to po file itself (LINGUAS, images, etc.)
    # po files containing these are always rechecked
    ("warn-ext", "Warning (external)"),
    ("error-ext", "Error (external)"),
)


class Information(models.Model):
    statistics = models.ForeignKey("Statistics", on_delete=models.CASCADE)
    # Priority of a stats message
    type = models.CharField(max_length=10, choices=INFORMATION_TYPE_CHOICES)
    description = models.TextField()

    class Meta:
        db_table = "information"

    @classmethod
    def get_info_dict(cls, lang):
        """Return a dict (of lists) with all Information objects for a lang, with statistics_id as the key
        Used for caching and preventing db access when requesting these objects for a long list of stats"""
        info_dict = {}
        for info in Information.objects.filter(statistics__language=lang):
            if info.statistics_id in info_dict:
                info_dict[info.statistics_id].append(info)
            else:
                info_dict[info.statistics_id] = [info]
        return info_dict

    def __lt__(self, other):
        return self.statistics.module_name < other.statistics.module_name

    def get_icon(self):
        return static("img/{}.png".format(self.type.split("-")[0]))

    def get_description(self):
        text = self.description
        matches = re.findall("###([^#]*)###", text)
        if matches:
            text = re.sub("###([^#]*)###", "%s", text)

        text = _(text)

        #  FIXME: if multiple substitutions, works only if order of %s is unchanged in translated string
        for match in matches:
            text = text.replace("%s", match, 1)
        return text

    def report_bug_url(self):
        link = self.statistics.branch.module.get_bugs_enter_url()
        link += "&short_desc={short}&content={short}&comment={long}".format(
            short="Error regenerating template file (POT)",
            long=utils.ellipsize(utils.strip_html(self.get_description()), 1600),
        )
        return link


class InformationArchived(models.Model):
    statistics = models.ForeignKey("StatisticsArchived", on_delete=models.CASCADE)
    # Priority of a stats message
    type = models.CharField(max_length=10, choices=INFORMATION_TYPE_CHOICES)
    description = models.TextField()

    class Meta:
        db_table = "information_archived"


# Utilities to properly sort branch names like gnome-3-2 < gnome-3-10
def try_int(value):
    try:
        return int(value)
    except ValueError:
        return value


def split_name(value):
    return tuple(try_int(part) for part in re.split(r"[-\.]", value))


def get_release_statistics_in_a_given_language(language: "Language", archives: bool = False) -> list["Statistics"]:
    """Get summary stats for all releases"""
    if archives:
        releases = Release.objects.all().filter(weight__lt=0).order_by("status", "-weight", "-name")
    else:
        releases = Release.objects.all().filter(weight__gte=0).order_by("status", "-weight", "-name")
    stats = []
    for rel in releases:
        stats.append(rel.total_for_lang(language))
    return stats
