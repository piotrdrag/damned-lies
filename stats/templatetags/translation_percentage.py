from statistics import mean
from typing import TYPE_CHECKING

from django import template
from django.db.models import QuerySet

from stats.models import Statistics

register = template.Library()

if TYPE_CHECKING:
    from stats.models import Module


@register.simple_tag()
def translation_percentage_in_language_for_module(
    statistics_in_language: QuerySet[Statistics], module: "Module", domain_type: str
) -> float | None:
    """
    Get the translation percentage for a module in a given language.

    It there is no statistic, the function returns None and this should be considered as the module is missing this
    domain for translation.
    When there are more than one domain, the mean of all the statistics is computed to show precise information on
    the state of the translation for the module.

    :param statistics_in_language: all the Statistics files in a given language.
    :param module: the module to get statistics on
    :param domain_type: and the domain (‘ui’, ‘help’, etc.)
    """

    def is_acceptable_statistic(statistic: Statistics) -> bool:
        return statistic.domain.dtype == domain_type and statistic.branch.module.id == module.id

    selected_statistics = [statistic for statistic in statistics_in_language if is_acceptable_statistic(statistic)]
    number_of_statistics = len(selected_statistics)
    if number_of_statistics == 0:
        translation_percentage = None
    elif number_of_statistics > 1:
        # There may exist more than one domain for a branch, when the domain expires
        # (for instance the POT generation method changes). In this case, if the branch
        # as a ‘branch_to’ property that is not None, it means it’s an old domain.
        translation_percentage = round(
            mean(statistic.tr_percentage() for statistic in selected_statistics if statistic.domain.branch_to is None),
            2,
        )
    else:
        translation_percentage = selected_statistics[0].tr_percentage()
    return translation_percentage


@register.simple_tag()
def translation_percentage_color_type(translation_percentage: int) -> str:
    full_translation, almost_full_translation = 100, 96
    if translation_percentage == full_translation:
        return "success"
    if almost_full_translation <= translation_percentage < full_translation:
        return "warning"
    return "danger"
