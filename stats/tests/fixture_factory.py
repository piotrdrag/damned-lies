from tempfile import NamedTemporaryFile

from django.core.management import call_command
from django.test import TestCase

from languages.models import Language
from people.models import Person
from stats.models import (
    Branch,
    Category,
    CategoryName,
    Domain,
    Information,
    Module,
    PoFile,
    Release,
    Statistics,
)
from teams.models import Role, Team


class FixtureFactory(TestCase):
    """
    Fake Test case to create fixture.
    To create a JSON fixture, run:
    python manage.py test stats.tests.FixtureFactory.make_fixture
    """

    def make_fixture(self):
        # Creating models: Teams
        t1 = Team.objects.create(
            name="fr",
            description="French",
            webpage_url="http://gnomefr.traduc.org/",
            mailing_list="gnomefr@traduc.org",
            mailing_list_subscribe="http://www.traduc.org/mailman/listinfo/gnomefr",
            presentation="Here can come any custom description for a team",
        )
        t2 = Team.objects.create(name="it", description="Italian", webpage_url="http://www.it.gnome.org/")

        # Creating models: Languages
        Language.objects.create(name="en", locale="en", plurals="nplurals=2; plural=(n != 1)")
        l_fr = Language.objects.create(name="French", locale="fr", plurals="nplurals=2; plural=(n > 1)", team=t1)
        l_it = Language.objects.create(name="Italian", locale="it", plurals="nplurals=2; plural=(n != 1)", team=t2)
        # Lang with no team and no stats
        Language.objects.create(name="Bemba", locale="bem")

        # Creating models: Persons/Roles
        p0 = Person.objects.create(username="admin1")  # Fake person (deleted below), just not to use pk=1 for user
        p1 = Person.objects.create(
            first_name="Robert",
            last_name="Translator",
            email="bob@example.org",
            username="bob",
            im_username="bobby",
            forge_account="bob1",
        )
        p1.set_password("bob")
        Role.objects.create(team=t1, person=p1, role="translator")
        p2 = Person.objects.create(
            first_name="John",
            last_name="Coordinator",
            email="coord@example.org",
            username="coord",
            forge_account="coord_fr",
        )
        p2.set_password("coord")
        Role.objects.create(team=t1, person=p2, role="coordinator")
        p3 = Person.objects.create(
            first_name="Alessio", last_name="Reviewer", email="alessio@example.org", username="alessio"
        )
        p1.set_password("alessio")
        Role.objects.create(team=t2, person=p3, role="reviewer")
        p0.delete()

        # Creating models: Modules
        gnome_hello = Module.objects.create(
            name="gnome-hello",
            vcs_root="https://gitlab.gnome.org/GNOME/gnome-hello.git",
            vcs_web="https://gitlab.gnome.org/GNOME/gnome-hello/",
            bugs_base="https://gitlab.gnome.org/GNOME/gnome-hello/issues",
        )
        zenity = Module.objects.create(
            name="zenity",
            vcs_root="https://gitlab.gnome.org/GNOME/zenity.git",
            vcs_web="https://gitlab.gnome.org/GNOME/zenity/",
            bugs_base="https://gitlab.gnome.org/GNOME/zenity/issues",
        )
        s_m_i = Module.objects.create(
            name="shared-mime-info",
            description="Shared MIME Info",
            vcs_root="https://gitlab.freedesktop.org/xdg/shared-mime-info.git",
            vcs_web="https://gitlab.freedesktop.org/xdg/shared-mime-info",
            bugs_base="https://gitlab.freedesktop.org/xdg/shared-mime-info/issues",
            comment="This is not a GNOME-specific module. Please submit your translation "
            'through the <a href="https://www.transifex.com/freedesktop/shared-mime-info/">Transifex platform</a>.',
        )

        # Creating models: Domains
        dom = {}
        for mod in (gnome_hello, zenity, s_m_i):
            dom["%s-ui" % mod.name] = Domain.objects.create(
                module=mod, name="po", description="UI Translations", dtype="ui", layout="po/{lang}.po"
            )
            dom["%s-doc" % mod.name] = Domain.objects.create(
                module=mod, name="help", description="User Guide", dtype="doc", layout="help/{lang}/{lang}.po"
            )

        # Creating models: Branches
        Branch.checkout_on_creation = False
        b1 = Branch(name="master", module=gnome_hello)
        b1.save(update_statistics=False)
        b2 = Branch(name="gnome-3-8", module=zenity)
        b2.save(update_statistics=False)
        b3 = Branch(name="master", module=zenity)
        b3.save(update_statistics=False)
        b4 = Branch(name="master", module=s_m_i)
        b4.save(update_statistics=False)

        # Creating models: Releases/Categories
        rel1 = Release.objects.create(
            name="gnome-3-8", status="official", description="GNOME 3.8 (stable)", string_frozen=True
        )
        rel2 = Release.objects.create(
            name="gnome-dev", status="official", description="GNOME in Development", string_frozen=False
        )
        rel3 = Release.objects.create(
            name="freedesktop-org", status="xternal", description="freedesktop.org (non-GNOME)", string_frozen=False
        )

        cat_name = CategoryName.objects.create(name="Desktop")
        Category.objects.create(release=rel1, branch=b1, name=cat_name)
        Category.objects.create(release=rel2, branch=b1, name=cat_name)
        Category.objects.create(release=rel1, branch=b2, name=cat_name)
        Category.objects.create(release=rel3, branch=b4, name=cat_name)

        # Creating models: Statistics
        # gnome-hello ui, gnome-hello doc (POT, fr, it)
        pofile = PoFile.objects.create(untranslated=47)
        Statistics.objects.create(
            branch=b1, domain=dom["gnome-hello-ui"], language=None, full_po=pofile, part_po=pofile
        )
        pofile = PoFile.objects.create(translated=47)
        Statistics.objects.create(
            branch=b1, domain=dom["gnome-hello-ui"], language=l_fr, full_po=pofile, part_po=pofile
        )
        pofile = PoFile.objects.create(translated=30, fuzzy=10, untranslated=7)
        Statistics.objects.create(
            branch=b1, domain=dom["gnome-hello-ui"], language=l_it, full_po=pofile, part_po=pofile
        )
        pofile = PoFile.objects.create(
            untranslated=20,
            figures=[
                {"path": "figures/gnome-hello-new.png", "hash": "8a1fcc6f46a22a1f500cfef9ca51b481"},
                {"path": "figures/gnome-hello-logo.png", "hash": "1ae47b7a7c4fbeb1f6bb72c0cf18d389"},
            ],
        )
        Statistics.objects.create(
            branch=b1, domain=dom["gnome-hello-doc"], language=None, full_po=pofile, part_po=pofile
        )
        pofile = PoFile.objects.create(
            translated=20,
            figures=[
                {
                    "translated": False,
                    "path": "figures/gnome-hello-new.png",
                    "hash": "8a1fcc6f46a22a1f500cfef9ca51b481",
                    "fuzzy": True,
                },
                {
                    "translated": False,
                    "path": "figures/gnome-hello-logo.png",
                    "hash": "1ae47b7a7c4fbeb1f6bb72c0cf18d389",
                    "fuzzy": False,
                },
            ],
        )
        Statistics.objects.create(
            branch=b1, domain=dom["gnome-hello-doc"], language=l_fr, full_po=pofile, part_po=pofile
        )
        pofile = PoFile.objects.create(translated=20)
        Statistics.objects.create(
            branch=b1, domain=dom["gnome-hello-doc"], language=l_it, full_po=pofile, part_po=pofile
        )
        # zenity ui 3.8, zenity doc 3.8, zenity ui master, zenity doc master (POT, fr, it)
        pofile = PoFile.objects.create(untranslated=136)
        part_pofile = PoFile.objects.create(untranslated=128)
        Statistics.objects.create(
            branch=b2, domain=dom["zenity-ui"], language=None, full_po=pofile, part_po=part_pofile
        )
        pofile = PoFile.objects.create(translated=136)
        Statistics.objects.create(branch=b2, domain=dom["zenity-ui"], language=l_fr, full_po=pofile, part_po=pofile)
        pofile = PoFile.objects.create(translated=130, untranslated=6)
        part_pofile = PoFile.objects.create(translated=100, untranslated=28)
        Statistics.objects.create(
            branch=b2, domain=dom["zenity-ui"], language=l_it, full_po=pofile, part_po=part_pofile
        )
        pofile = PoFile.objects.create(untranslated=259)
        Statistics.objects.create(branch=b2, domain=dom["zenity-doc"], language=None, full_po=pofile, part_po=pofile)
        pofile = PoFile.objects.create(untranslated=259)
        Statistics.objects.create(branch=b2, domain=dom["zenity-doc"], language=l_fr, full_po=pofile, part_po=pofile)
        pofile = PoFile.objects.create(translated=259)
        Statistics.objects.create(branch=b2, domain=dom["zenity-doc"], language=l_it, full_po=pofile, part_po=pofile)
        pofile = PoFile.objects.create(untranslated=149)
        stat1 = Statistics.objects.create(
            branch=b3, domain=dom["zenity-ui"], language=None, full_po=pofile, part_po=pofile
        )
        pofile = PoFile.objects.create(translated=255, fuzzy=4)
        Statistics.objects.create(branch=b3, domain=dom["zenity-ui"], language=l_fr, full_po=pofile, part_po=pofile)
        pofile = PoFile.objects.create(translated=259)
        Statistics.objects.create(branch=b3, domain=dom["zenity-ui"], language=l_it, full_po=pofile, part_po=pofile)
        pofile = PoFile.objects.create(untranslated=259)
        Statistics.objects.create(branch=b3, domain=dom["zenity-doc"], language=None, full_po=pofile, part_po=pofile)
        pofile = PoFile.objects.create(untranslated=259)
        Statistics.objects.create(branch=b3, domain=dom["zenity-doc"], language=l_fr, full_po=pofile, part_po=pofile)
        pofile = PoFile.objects.create(translated=259)
        Statistics.objects.create(branch=b3, domain=dom["zenity-doc"], language=l_it, full_po=pofile, part_po=pofile)
        # shared-mime-info ui (POT, fr, it)
        pofile = PoFile.objects.create(untranslated=626)
        Statistics.objects.create(
            branch=b4, domain=dom["shared-mime-info-ui"], language=None, full_po=pofile, part_po=pofile
        )
        pofile = PoFile.objects.create(translated=598, fuzzy=20, untranslated=2)
        Statistics.objects.create(
            branch=b4, domain=dom["shared-mime-info-ui"], language=l_fr, full_po=pofile, part_po=pofile
        )
        pofile = PoFile.objects.create(translated=620, fuzzy=6)
        Statistics.objects.create(
            branch=b4, domain=dom["shared-mime-info-ui"], language=l_it, full_po=pofile, part_po=pofile
        )

        # Example of error
        stat1.information_set.add(
            Information(
                type="error",
                description=(
                    "Error regenerating POT file for zenity:\n<pre>intltool-update -g 'zenity' -p\nERROR:"
                    " xgettext failed to generate PO template file.</pre>"
                ),
            )
        )

        # Output fixture
        out_file = NamedTemporaryFile(suffix=".json", dir=".", delete=False)
        call_command(
            "dumpdata",
            *["auth.User", "people", "teams", "languages", "stats"],
            indent=1,
            format="json",
            stdout=out_file,
        )
        out_file.close()
        print("Fixture created in the file %s" % out_file.name)
