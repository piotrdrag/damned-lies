import json
import logging
import os
import shutil
import tempfile
from datetime import date
from pathlib import Path
from unittest import skipUnless
from unittest.mock import patch

from django.conf import settings
from django.contrib.auth.models import User
from django.core import mail
from django.core.exceptions import ValidationError
from django.template.loader import get_template
from django.test import TestCase
from django.test.utils import override_settings
from django.urls import reverse
from django.utils.translation import gettext as _

from common.utils import run_shell_command
from languages.models import Language
from people.models import Person
from stats import utils
from stats.doap import DOAPParser, update_doap_infos, DOAPElementNotFoundError
from stats.models import (
    GLIB_PRESET,
    Branch,
    CategoryName,
    Domain,
    FakeLangStatistics,
    Information,
    Module,
    ModuleLock,
    PoFile,
    Release,
    Statistics,
    UnableToCommitError,
)

from ..repos import GitRepository, VCSRepository, VCSRepositoryType
from .utils import PatchShellCommand, test_scratchdir

try:
    from translate.storage import subtitles  # NOQA

    has_translate_subtitle_support = True
except ImportError:
    has_translate_subtitle_support = False


class ModuleTestCase(TestCase):
    fixtures = ("test_data.json",)
    SYS_DEPENDENCIES = (
        ("gettext", "msgfmt"),
        ("intltool", "intltool-update"),
    )

    @classmethod
    def setUpClass(cls):
        for package, prog in cls.SYS_DEPENDENCIES:
            if not utils.check_program_presence(prog):
                raise Exception("You are missing a required system package needed by Damned Lies (%s)" % package)
        super().setUpClass()

    def setUp(self):
        Branch.checkout_on_creation = False
        self.mod = Module.objects.get(name="gnome-hello")
        self.branch = self.mod.branch_set.get(name="master")

        # Locks may produce side effects when testing. Locks have their own tests here.
        ModuleLock.clean_locks()

    def test_module_methods(self):
        self.assertEqual(self.mod.get_description(), "gnome-hello")

    def test_module_comparable(self):
        modules = [Module.objects.get(name="zenity"), Module.objects.get(name="gnome-hello")]
        self.assertEqual(sorted(modules)[0].name, "gnome-hello")

    def test_clean_module_locks(self):
        class CustomModuleLock(ModuleLock):
            dir_prefix = "updating-test-"

        module = Module.objects.get(name="gnome-hello")
        with CustomModuleLock(module) as lock:
            CustomModuleLock.clean_locks()
            self.assertFalse(lock.dirpath.exists())

    @patch("stats.models.sleep", side_effect=RuntimeError())
    def test_module_lock_sleep(self, mocked_sleep):
        module = Module.objects.get(name="gnome-hello")
        with ModuleLock(module):
            with self.assertRaises(RuntimeError):
                with ModuleLock(module):
                    pass
            mocked_sleep.assert_called_once()

    def test_module_branch(self):
        response = self.client.get(reverse("module_branch", args=["gnome-hello", "master"]))
        self.assertContains(response, '<table class="stats table')
        # Missing branch
        with self.assertLogs("django.request", level="WARNING"):
            response = self.client.get(reverse("module_branch", args=["gnome-hello", "missing"]))
        self.assertEqual(response.status_code, 404)
        # Branch name with slash is reverse-able
        with self.assertLogs("django.request", level="WARNING"):
            response = self.client.get(reverse("module_branch", args=["gnome-hello", "with/slash"]))
        self.assertEqual(response.status_code, 404)

    def test_module_bugs_reporting(self):
        self.assertEqual(
            self.mod.get_bugs_i18n_url(),
            "https://gitlab.gnome.org/GNOME/gnome-hello/issues/?state=opened&label_name[]=8.%20Translation",
        )
        self.assertEqual(
            self.mod.get_bugs_i18n_url("pot error"),
            "https://gitlab.gnome.org/GNOME/gnome-hello/issues/?state=opened&label_name[]"
            "=8.%20Translation&search=pot+error",
        )
        response = self.client.get(reverse("module", args=[self.mod.name]))
        self.assertContains(
            response,
            '<li><a href="https://gitlab.gnome.org/GNOME/gnome-hello/issues/new">Report a bug</a></li>',
            html=True,
        )
        self.mod.bugs_base = ""
        self.mod.save()
        response = self.client.get(reverse("module", args=[self.mod.name]))
        self.assertContains(response, "Sorry, no known locations to report bugs for this module.")

    @test_scratchdir
    def test_first_branch_creation(self):
        mod = Module.objects.create(name="eog", vcs_root="https://gitlab.gnome.org/GNOME/eog.git")
        br = Branch(module=mod, name="master")
        with PatchShellCommand() as cmds:
            br.checkout()
        self.assertTrue(
            "git clone https://gitlab.gnome.org/GNOME/eog.git %s/git/eog" % settings.SCRATCHDIR in cmds,
            "%s is not the expected 'git clone' command." % cmds[0],
        )

    def test_head_branch(self):
        self.assertEqual(self.mod.get_head_branch().name, "master")
        Branch.objects.filter(module=self.mod, name="master").delete()
        Branch.objects.bulk_create([
            Branch(module=self.mod, name="gnome-3-18"),
            Branch(module=self.mod, name="gnome-41"),
            Branch(module=self.mod, name="gnome-2-1"),
        ])
        self.assertEqual(self.mod.get_head_branch().name, "gnome-41")

    def test_branch_methods(self):
        branch = Branch(module=self.mod, name="master")
        self.assertTrue(branch.is_head)
        self.assertEqual(branch.vcs_web_url, "https://gitlab.gnome.org/GNOME/gnome-hello/")
        self.assertEqual(branch.vcs_web_log_url, "https://gitlab.gnome.org/GNOME/gnome-hello/commits/master")
        branch.name = "gnome-3-30"
        self.assertEqual(branch.vcs_web_log_url, "https://gitlab.gnome.org/GNOME/gnome-hello/commits/gnome-3-30")

    def test_branch_domains(self):
        """
        Test that domains can be applied to a subset of all module branches,
        based on the domain branch_form/branch_to fields.
        """
        domains = self.branch.connected_domains
        self.assertEqual(list(domains.keys()), ["po", "help"])

        b3 = Branch(name="gnome-3-3", module=self.mod)
        b3.save(update_statistics=False)
        b5 = Branch(name="gnome-3-5", module=self.mod)
        b5.save(update_statistics=False)
        b7 = Branch(name="gnome-3-7", module=self.mod)
        b7.save(update_statistics=False)
        help_domain = domains["help"]
        help_domain.branch_from = b5
        help_domain.save()
        self.assertEqual(list(b3.connected_domains.keys()), ["po"])
        self.assertFalse(b3.is_domain_connected(help_domain))
        self.assertTrue(b3.is_domain_connected(domains["po"]))
        self.assertEqual(list(b5.connected_domains.keys()), ["po", "help"])
        help_domain.branch_to = b7
        help_domain.save()
        self.assertEqual(list(self.branch.connected_domains.keys()), ["po"])

    @test_scratchdir
    def test_branch_stats(self):
        lang = Language.objects.create(name="xxx", locale="xxx")
        ghost_stat = Statistics.objects.create(
            branch=self.branch, domain=self.mod.domain_set.get(name="po"), language=lang
        )
        # Check stats
        with PatchShellCommand(only=["git "]):
            self.branch.update_statistics(force=True)
        fr_po_stat = Statistics.objects.get(branch=self.branch, domain__name="po", language__locale="fr")
        self.assertEqual(fr_po_stat.translated(), 44)
        self.assertEqual(fr_po_stat.translated(scope="part"), 44)
        self.assertEqual(fr_po_stat.translated_words(), 131)
        self.assertEqual(fr_po_stat.full_po, fr_po_stat.part_po)
        self.assertEqual(fr_po_stat.pot_url(), "/POT/gnome-hello.master/gnome-hello.master.pot")
        self.assertEqual(fr_po_stat.po_url(), "/POT/gnome-hello.master/gnome-hello.master.fr.po")
        self.assertEqual(fr_po_stat.po_url(reduced=True), "/POT/gnome-hello.master/gnome-hello.master.fr.reduced.po")

        # Partial translated file
        it_po_stat = Statistics.objects.get(branch=self.branch, domain__name="po", language__locale="it")
        self.assertEqual(it_po_stat.translated(), 9)
        self.assertEqual(it_po_stat.full_po, it_po_stat.part_po)

        # Committed file with less strings that merged one.
        de_po_stat = Statistics.objects.get(branch=self.branch, domain__name="po", language__locale="de")
        self.assertEqual(de_po_stat.information_set.count(), 1)
        self.assertTrue(
            de_po_stat.information_set.first().description.startswith(
                "The original file with strings to translate (.pot) has been"
            )
        )

        pot_doc_stat = Statistics.objects.get(branch=self.branch, domain__name="help", language=None)
        self.assertEqual(pot_doc_stat.untranslated(), 43)
        self.assertEqual(len(pot_doc_stat.full_po.figures), 2)
        fr_doc_stat = Statistics.objects.get(branch=self.branch, domain__name="help", language__locale="fr")
        # Depending on the itstool version, the "GNOME Hello" string may or may
        # not have a msgctxt marker. Allowing both here.
        self.assertIn(fr_doc_stat.fuzzy(), [1, 2])
        self.assertIn(fr_doc_stat.translated(), [41, 42])
        self.assertEqual(len(pot_doc_stat.full_po.figures), len(fr_doc_stat.full_po.figures))
        self.assertEqual(fr_doc_stat.po_url(), "/POT/gnome-hello.master/docs/gnome-hello-help.master.fr.po")
        with self.assertRaises(Statistics.DoesNotExist):
            Statistics.objects.get(pk=ghost_stat.pk)

        stats = self.branch.get_statistics("doc", mandatory_languages=[lang])
        self.assertIn("help", stats)
        self.assertTrue(stats["help"][0].is_pot_stats())  # POT first
        self.assertEqual(stats["help"][-1].language.locale, "xxx")  # Fake stat last

    @test_scratchdir
    def test_branch_not_exists(self):
        branch = Branch.objects.get(name="master", module__name="zenity")
        self.assertFalse(branch._repository.exists())

    @test_scratchdir
    def test_branch_exists(self):
        branch = Branch.objects.get(name="master", module__name="gnome-hello")
        self.assertTrue(branch._repository.exists())

    @test_scratchdir
    def test_rename_branch(self):
        branch = Branch.objects.get(name="master", module__name="gnome-hello")
        _, out, _ = run_shell_command(["git", "branch", "--list"], cwd=branch.checkout_path)
        self.assertNotIn("gnome-hello-1-4", out)
        branch.name = "gnome-hello-1-4"
        branch.save(update_statistics=False)
        _, out, _ = run_shell_command(["git", "branch", "--list"], cwd=branch.checkout_path)
        self.assertIn("gnome-hello-1-4", out)

    @test_scratchdir
    def test_delete_branch(self):
        """Deleting the master branch of a git module deletes the checkout dir"""
        checkout_path = self.branch.checkout_path
        branch = Branch(module=self.mod, name="gnome-hello-1-4")
        branch.save(update_statistics=False)
        branch.delete()
        self.assertTrue(checkout_path.exists())
        self.branch.delete()
        self.assertFalse(checkout_path.exists())

    def test_create_unexisting_branch(self):
        """Creating a non-existing branch should raise a ValidationError."""
        Branch.checkout_on_creation = True
        with patch("stats.tests.tests.Branch.checkout", side_effect=OSError()):
            branch = Branch(name="trunk2", module=self.mod)
            self.assertRaises(ValidationError, branch.clean)

    def test_edit_branches_form(self):
        coord = User.objects.get(username="john")
        john = Person.objects.get(username="john")
        # Ensure john does not maintain any module
        john.maintains_modules.clear()
        coord.set_password("john")
        coord.save()
        self.client.login(username="john", password="john")
        response = self.client.get(reverse("module_edit_branches", args=[self.mod]), follow=True)
        self.assertContains(response, "you’re not allowed to edit")
        # Test now with appropriate permissions
        coord.is_superuser = True
        coord.save()
        # Duplicate data
        cat_name = CategoryName.objects.get(name="Desktop")
        response = self.client.post(
            reverse("module_edit_branches", args=[self.mod]),
            {
                "1": "1",
                "1_cat": str(cat_name.pk),
                "2": "2",
                "2_cat": str(cat_name.pk),
                "new_branch": "master",
                "new_branch_release": "2",
                "new_branch_category": str(cat_name.pk),
            },
        )
        self.assertContains(response, "the form is not valid")

    def test_branch_sorting(self):
        b1 = Branch(name="a-branch", module=self.mod)
        b1.save(update_statistics=False)
        b2 = Branch(name="p-branch", module=self.mod)
        b2.save(update_statistics=False)
        self.assertEqual([b.name for b in sorted(self.mod.branch_set.all())], ["master", "p-branch", "a-branch"])
        b1.weight = -1
        b1.save(update_statistics=False)
        self.assertEqual([b.name for b in sorted(self.mod.branch_set.all())], ["master", "a-branch", "p-branch"])
        self.assertEqual([b.name for b in self.mod.get_branches(reverse=True)], ["p-branch", "a-branch", "master"])
        # "Clever" sorting of alphanumeric names
        b3 = Branch(name="gnome-3-2", module=self.mod)
        b3.save(update_statistics=False)
        b4 = Branch(name="gnome-3-10", module=self.mod)
        b4.save(update_statistics=False)
        b5 = Branch(name="gnome-3-20", module=self.mod)
        b5.save(update_statistics=False)
        self.assertEqual(
            [b.name for b in sorted(self.mod.branch_set.all())],
            ["master", "a-branch", "p-branch", "gnome-3-20", "gnome-3-10", "gnome-3-2"],
        )

    def __setup_test_string_frozen_mail_notify_of_a_break_change(self) -> tuple[Person, Person]:
        mail.outbox = []
        with PatchShellCommand(only=["git "]):
            self.branch.update_statistics(force=False)

        self.mod.maintainers.clear()

        bob = Person.objects.get(username="bob")
        bob.forge_account = "bob_on_gitlab"
        bob.save()
        self.mod.maintainers.add(bob)

        john = Person.objects.get(username="john")
        john.forge_account = "john_on_gitlab"
        john.save()
        self.mod.maintainers.add(john)

        self.assertIn(bob, self.mod.maintainers.all())
        self.assertIn(john, self.mod.maintainers.all())

        # Create a new file with translation
        new_file_path = self.branch.checkout_path / "dummy_file.py"
        with new_file_path.open("w") as fh:
            for string in ["Dummy string for D-L tests", "Other addition"]:
                fh.write("a = _('%s')" % string)
        # Add the new file to POTFILES.in
        with (self.branch.checkout_path / "po" / "POTFILES.in").open("a") as fh:
            fh.write("dummy_file.py\n")

        return bob, john

    def __run_tests_string_frozen_mail_notify_of_a_break_change(self, template_kwargs: dict[str, list[str]]):
        # Regenerate stats (notification should be sent)
        with patch("stats.utils.Gitlab") as gitlab:
            with PatchShellCommand(only=["git log "]):
                self.branch.update_statistics(force=False, checkout=False)
            gitlab.assert_called_once()
            gitlab.assert_called_with(
                f"https://{settings.GITLAB_COORDINATION_TEAMS_URL}",
                private_token=settings.GITLAB_API_TOKENS[settings.GITLAB_COORDINATION_TEAMS_URL],
            )

            # FIXME(gbernard): should be refactored if a better option is found.
            self.assertEqual(3, len(gitlab.mock_calls))

            # Assert get() is called to reference the target project.
            self.assertEqual(gitlab.mock_calls[1][0], "().projects.get")
            self.assertEqual(gitlab.mock_calls[1].args, ("PATH/TO/GITLAB/COORDINATION/PROJECT",))

            # Assert the issue is created with the desired notification content
            self.assertEqual(gitlab.mock_calls[2][0], "().projects.get().issues.create")
            template = get_template("freeze-notification.txt")
            new_strings = [
                "`Dummy string for D-L tests` ([../dummy_file.py:1](https://gitlab.gnome.org/GNOME/gnome-hello/blob/master/../dummy_file.py#L1))",
                "`Other addition` ([../dummy_file.py:1](https://gitlab.gnome.org/GNOME/gnome-hello/blob/master/../dummy_file.py#L1))",
            ]
            description = template.render(
                template_kwargs | {
                "module": "gnome-hello.master",
                "module_url_in_damned_lies": reverse("module", kwargs={"module_name": "gnome-hello"}),
                "ourweb": settings.SITE_DOMAIN,
                "new_strings": new_strings,
                "missing_locations": False,
                "commit_log": "https://gitlab.gnome.org/GNOME/gnome-hello/commits/master",
            })
            create_issue_args = {
                "title": "String additions to ‘gnome-hello.master’",
                "description": description,
                "labels": "freeze break",
            }
            self.assertEqual(gitlab.mock_calls[2].args[0], create_issue_args)

    @test_scratchdir
    def test_string_frozen_mail_notify_of_a_break_change_contains_missing_gitlab_usernames(self):
        """
        String change for a module of a string_frozen release should generate a message.
        One maintainer forgot to fill their own username in their profile.
        """
        bob, john = self.__setup_test_string_frozen_mail_notify_of_a_break_change()
        john.forge_account = ""
        john.save()
        self.__run_tests_string_frozen_mail_notify_of_a_break_change({
            "maintainer_usernames": ["@bob_on_gitlab"],
            "maintainer_usernames_missing": ["John Coordinator"]
        })


    @test_scratchdir
    def test_string_frozen_mail_notify_of_a_break_change_all_user_filled_gitlab_usernames(self):
        """
        String change notification for a module of a string_frozen release should generate a message.
        The maintainers have all set their GitLab username in their DL profile.
        """
        self.__setup_test_string_frozen_mail_notify_of_a_break_change()
        self.__run_tests_string_frozen_mail_notify_of_a_break_change({
            "maintainer_usernames": ["@bob_on_gitlab", "@john_on_gitlab"],
            "maintainer_usernames_missing": []
        })


    @test_scratchdir
    def test_dynamic_po(self):
        """Test the creation of a blank po file for a new language"""
        tamil = Language.objects.create(name="Tamil", locale="ta")
        with PatchShellCommand(only=["git "]):
            self.branch.update_statistics(force=False)  # At least POT stats needed
        pot_stats = Statistics.objects.get(branch=self.branch, domain__name="po", language__isnull=True)
        dyn_url = FakeLangStatistics(pot_stats, tamil).po_url()
        response = self.client.get(dyn_url)
        self.assertEqual(response["content-disposition"], "inline; filename=gnome-hello.master.ta.po")
        self.assertContains(
            response,
            """# Tamil translation for gnome-hello.
# Copyright (C) %s gnome-hello's COPYRIGHT HOLDER
# This file is distributed under the same license as the gnome-hello package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR."""
            % date.today().year,
        )
        self.assertContains(response, "Language-Team: Tamil <ta@li.org>")

        dyn_url = FakeLangStatistics(pot_stats, tamil).po_url(reduced=True)
        response = self.client.get(dyn_url)
        self.assertEqual(response["content-disposition"], "inline; filename=gnome-hello.master.ta-reduced.po")
        self.assertContains(response, "# Tamil translation for gnome-hello.")

    @test_scratchdir
    def test_dynamic_po_msginit_error(self):
        tamil = Language.objects.create(name="Tamil", locale="ta")
        with PatchShellCommand(only=["git "]):
            self.branch.update_statistics(force=False)  # At least POT stats needed
        pot_stats = Statistics.objects.get(branch=self.branch, domain__name="po", language__isnull=True)
        dyn_url = FakeLangStatistics(pot_stats, tamil).po_url()
        with patch("stats.views.run_shell_command", side_effect=OSError()):
            response = self.client.get(dyn_url)
        self.assertContains(response, "Unable to produce a new .po file for language ta")

    @test_scratchdir
    def test_commit_ability(self):
        branch = self.mod.get_head_branch()
        domain = self.mod.domain_set.get(name="po")
        fr_lang = Language.objects.get(locale="fr")

        with self.assertRaises(UnableToCommitError):
            # read-only VCS
            domain.commit_info(branch, fr_lang)

        # Setup as a writable repo
        self.mod.vcs_root = "git@gitlab.gnome.org:GNOME/%s.git" % self.mod.name
        linguas = branch.checkout_path / "po" / "LINGUAS"
        linguas.unlink()
        bem_lang = Language.objects.get(locale="bem")
        with self.assertRaises(UnableToCommitError):
            # unaccessible LINGUAS file for a new lang
            domain.commit_info(branch, bem_lang)

        domain.linguas_location = "no"
        self.assertEqual(domain.commit_info(branch, bem_lang), (branch.checkout_path / "po" / "bem.po", False, None))

        self.assertEqual(domain.commit_info(branch, fr_lang), (branch.checkout_path / "po" / "fr.po", True, None))

    @test_scratchdir
    def test_commit_po(self):
        branch = self.mod.get_head_branch()
        po_file = Path(__file__).parent / "test.po"
        domain = self.mod.domain_set.get(name="po")
        fr_lang = Language.objects.get(locale="fr")
        with PatchShellCommand():
            with self.assertRaisesRegex(UnableToCommitError, "read only"):
                with self.assertLogs("stats.models", level="ERROR"):
                    branch.commit_po(po_file, domain, fr_lang, Person.objects.get(username="john"))
        # Setup as a writable repo
        self.mod.vcs_root = "git@gitlab.gnome.org:GNOME/%s.git" % self.mod.name
        self.mod.save()

        update_repo_sequence = (
            "git checkout -f master",
            "git fetch",
            "git reset --hard origin/master",
            "git clean -dfq",
            "if [ -e .gitmodules ]; then git submodule update --init; fi",
        )
        # User interface (existing language)
        git_ops = (
            *update_repo_sequence,
            "git add po/fr.po",
            # Quoting is done at the Popen level
            "git commit -m Update French translation --author John Coordinator <coord@example.org>",
            "git log -n1 --format=oneline",
            "git push origin master",
        )
        with PatchShellCommand() as cmds:
            branch.commit_po(po_file, domain, fr_lang, Person.objects.get(username="john"))
            for idx, cmd in enumerate(git_ops):
                self.assertIn(cmd, cmds[idx])

        # User interface (new language)
        bem_lang = Language.objects.get(locale="bem")
        git_ops = (
            *update_repo_sequence,
            "git add po/bem.po",
            "git add po/LINGUAS",
            "git commit -m Add Bemba translation --author John Coordinator <coord@example.org>",
            "git log -n1 --format=oneline",
            "git push origin master",
        )
        with PatchShellCommand() as cmds:
            with self.assertLogs("damnedlies", level="ERROR"):
                branch.commit_po(po_file, domain, bem_lang, Person.objects.get(username="john"))
            for idx, cmd in enumerate(git_ops):
                self.assertIn(cmd, cmds[idx])
            self.assertIn("intltool-update -g", cmds[-1])

        # Check lang has been inserted in LINGUAS file, before 'de'
        self.assertIn("bem\nde", (branch.domain_path(domain) / "LINGUAS").read_text())

        # Documentation
        domain = self.mod.domain_set.get(name="help")
        git_ops = (
            *update_repo_sequence,
            "git add help/fr/fr.po",
            "git commit -m Update French translation --author John Coordinator <coord@example.org>",
            "git log -n1 --format=oneline",
            "git push origin master",
        )
        with PatchShellCommand() as cmds:
            branch.commit_po(po_file, domain, fr_lang, Person.objects.get(username="john"))
            for idx, cmd in enumerate(git_ops):
                self.assertIn(cmd, cmds[idx])

    @test_scratchdir
    def test_commit_and_cherrypick(self):
        """
        Committing in non-HEAD branch and checking "sync with master" will
        cherry-pick the branch commit on the master branch.
        """
        # Setup as a writable repo
        self.mod.vcs_root = "git@gitlab.gnome.org:GNOME/%s.git" % self.mod.name
        self.mod.save()
        domain = self.mod.domain_set.get(name="po")
        commit_dir = self.branch.checkout_path
        committer = Person.objects.get(username="john")
        run_shell_command(
            ["git", "config", "user.name", committer.name],
            cwd=commit_dir,
        )
        run_shell_command(
            ["git", "config", "user.email", committer.email],
            cwd=commit_dir,
        )
        run_shell_command(
            ["git", "checkout", "-b", "gnome-3-18", "origin/master"], cwd=commit_dir, raise_on_error=True
        )
        branch = Branch(module=self.mod, name="gnome-3-18")
        branch.save(update_statistics=False)
        fr_lang = Language.objects.get(locale="fr")
        # Copy stats from master
        stat = Statistics.objects.get(language=fr_lang, branch=self.branch, domain=domain)
        stat.pk, stat.full_po, stat.part_po = None, None, None
        stat.branch = branch
        stat.save()
        po_file = Path(__file__).parent / "test.po"
        self.mod.vcs_root = self.mod.vcs_root.replace("git://", "ssh://")
        self.mod.save()

        with PatchShellCommand(only=["git push", "git fetch", "git reset"]) as cmds:
            commit_hash = branch.commit_po(po_file, domain, fr_lang, committer)
        update_repo_sequence = (
            "git checkout -f master",
            "git fetch",
            "git reset --hard origin/master",
            "git clean -dfq",
            "if [ -e .gitmodules ]; then git submodule update --init; fi",
        )
        git_ops = (
            *update_repo_sequence,
            "git cherry-pick -x",
            "git push origin master",
        )
        with PatchShellCommand(only=["git push", "git fetch", "git reset"]) as cmds:
            self.branch.cherrypick_commit(commit_hash)
            for idx, cmd in enumerate(git_ops):
                self.assertIn(cmd, cmds[idx])


class TestDOAPFiles(TestCase):
    fixtures = ("test_data.json",)

    def setUp(self):
        self.module = Module.objects.get(name="gnome-hello")

        # Locks may produce side effects when testing. Locks have their own tests here.
        ModuleLock.clean_locks()

    @test_scratchdir
    def test_branch_file_changed(self):
        # file_hashes is empty in fixture, so first call should always return True
        self.assertTrue(self.module.get_head_branch().file_changed("gnome-hello.doap"))
        self.assertFalse(self.module.get_head_branch().file_changed("gnome-hello.doap"))

    @test_scratchdir
    def test_update_maintainers_from_doap_file(self):
        # Add a maintainer which will be removed
        pers = Person(username="toto")
        pers.save()
        self.module.maintainers.add(pers)
        update_doap_infos(self.module)
        self.assertEqual(self.module.maintainers.count(), 3)
        claude = self.module.maintainers.get(email="claude@2xlibre.net")
        self.assertEqual(claude.username, "claudep")

    @test_scratchdir
    def test_update_doap_infos(self):
        self.assertEqual(1, self.module.maintainers.count())
        self.assertEqual("", self.module.homepage)
        self.assertEqual("https://gitlab.gnome.org/GNOME/gnome-hello/issues", self.module.bugs_base)

        update_doap_infos(self.module)

        self.assertEqual(3, self.module.maintainers.count())
        self.assertEqual("https://gitlab.gnome.org/Archive/gnome-hello", self.module.homepage)
        self.assertEqual("https://gitlab.gnome.org/Archive/gnome-hello/issues", self.module.bugs_base)

    def test_doap_parser_list_maintainers(self):
        input_doap_file = Path(__file__).parent.resolve() / Path("doap/World_decoder.doap")
        doap_parser = DOAPParser(input_doap_file)

        maintainers = doap_parser.maintainers
        self.assertEqual(2, len(doap_parser.maintainers))
        self.assertEqual("maintainer_1", maintainers[0].account)
        self.assertEqual("maintainer_2", maintainers[1].account)

    def test_doap_parser_properties(self):
        input_doap_file = Path(__file__).parent.resolve() / Path("doap/World_decoder.doap")
        doap_parser = DOAPParser(input_doap_file)

        homepage = doap_parser.homepage
        self.assertEqual("https://gitlab.gnome.org/World/Decoder", homepage)

        bugtracker_url = doap_parser.bugtracker_url
        self.assertEqual("https://gitlab.gnome.org/World/Decoder/-/issues", bugtracker_url)

    def test_doap_parser_missing_gnome_id(self):
        input_doap_file = Path(__file__).parent.resolve() / Path("doap/World_decoder_missing_gnome_id.doap")
        doap_parser = DOAPParser(input_doap_file)

        with self.assertLogs("damnedlies", logging.WARNING) as logger:
            maintainers = doap_parser.maintainers
            self.assertIn("Missing account name", logger.output[0])

        self.assertIsNone(maintainers[0].account)

    def test_doap_parser_missing_mbx(self):
        input_doap_file = Path(__file__).parent.resolve() / Path("doap/World_decoder_missing_mbx.doap")
        doap_parser = DOAPParser(input_doap_file)

        with self.assertLogs("damnedlies", logging.WARNING) as logger:
            maintainers = doap_parser.maintainers
            self.assertIn("Missing email", logger.output[0])

        self.assertIsNone(maintainers[0].email)

    def test_doap_parser_missing_name(self):
        input_doap_file = Path(__file__).parent.resolve() / Path("doap/World_decoder_missing_name.doap")
        doap_parser = DOAPParser(input_doap_file)

        with self.assertRaises(DOAPElementNotFoundError):
            doap_parser.maintainers


@override_settings(SCRATCHDIR=Path(__file__).parent)
class TestModuleBase(TestCase):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        Branch.checkout_on_creation = False
        cls.mod = Module.objects.create(
            name="testmodule",
            bugs_base="https://gitlab.gnome.org/GNOME/testmodule/issues",
        )
        cls.branch = Branch(module=cls.mod, name="master")
        cls.branch.save(update_statistics=False)

    @classmethod
    def tearDownClass(cls):
        html_dir = settings.SCRATCHDIR / "HTML"
        if html_dir.exists():
            shutil.rmtree(str(html_dir))
        super().tearDownClass()


class DomainTests(TestModuleBase):
    def setUp(self):
        super().setUp()
        Domain.doc_format.cache_clear()

    def test_get_po_path(self):
        # Standard UI
        dom = Domain(module=self.mod, dtype="ui", layout="po/{lang}.po")
        self.assertEqual(dom.get_po_path("fr"), "po/fr.po")
        # Standard Doc
        dom.dtype = "doc"
        dom.layout = "help/{lang}/{lang}.po"
        self.assertEqual(dom.get_po_path("fr"), "help/fr/fr.po")

    def test_domain_linguas(self):
        help_domain = Domain(module=self.mod, name="help", dtype="doc", layout="help_mallard/{lang}/{lang}.po")
        help_domain.linguas_location = "help_mallard/Makefile.am"
        self.assertEqual(
            help_domain.get_linguas(self.branch),
            {
                "langs": None,
                "error": "Entry for this language is not present in ALL_LINGUAS variable in "
                "help_mallard/Makefile.am file.",
            },
        )
        help_domain.linguas_location = "help_mallard/Makefile.am#HELP_LINGUAS"
        self.assertEqual(
            help_domain.get_linguas(self.branch),
            {
                "langs": ["es", "fr"],
                "error": "Entry for this language is not present in HELP_LINGUAS variable in "
                "help_mallard/Makefile.am file.",
            },
        )
        help_domain.linguas_location = ""
        self.assertEqual(
            help_domain.get_linguas(self.branch),
            {
                "langs": ["es", "fr"],
                "error": "DOC_LINGUAS list doesn’t include this language.",
            },
        )

    def test_domain_xgettext_command(self):
        domain = Domain(module=self.mod, name="po", dtype="ui", layout="po/{lang}.po")
        self.assertEqual(
            domain.get_xgettext_command(self.branch),
            (
                [
                    "xgettext",
                    "--files-from",
                    "POTFILES.in",
                    "--directory",
                    str(settings.SCRATCHDIR / "git" / "testmodule"),
                    "--from-code",
                    "utf-8",
                    "--add-comments",
                    "--output",
                    "testmodule.pot",
                ]
                + list(GLIB_PRESET)
                + [
                    "--keyword=Description",
                    "--msgid-bugs-address",
                    "https://gitlab.gnome.org/GNOME/testmodule/issues",
                ],
                {"GETTEXTDATADIRS": str(utils.ITS_DATA_DIR.parent)},
            ),
        )

    def test_doc_domain_docbook(self):
        """
        Test Docbook-style help
        """
        domain = Domain.objects.create(
            module=self.mod, name="release-notes", dtype="doc", layout="help_docbook/{lang}/{lang}.po"
        )
        _, errs = utils.generate_doc_pot_file(self.branch, domain)
        self.assertEqual(errs, [])
        doc_format = domain.doc_format(self.branch)
        self.assertEqual(doc_format.format, "docbook")
        pot_path = self.branch.domain_path(domain) / "C" / "release-notes.pot"
        self.assertTrue(pot_path.exists())
        self.addCleanup(os.remove, str(pot_path))
        self.assertIn("#: C/rnlookingforward.xml:11", pot_path.read_text())
        res = utils.get_fig_stats(pot_path, doc_format=doc_format)
        self.assertEqual(len(res), 1)
        self.assertEqual(res[0]["path"], "figures/rnusers.nautilus.png")

    def test_doc_domain_mallard(self):
        """
        Test Mallard-style help (with itstool)
        """
        domain = Domain.objects.create(
            module=self.mod, name="release-notes", dtype="doc", layout="help_mallard/{lang}/{lang}.po"
        )
        potfile, errs = utils.generate_doc_pot_file(self.branch, domain)
        self.assertEqual(errs, [])
        doc_format = domain.doc_format(self.branch)
        self.assertEqual(potfile.name, "release-notes.pot")
        self.assertEqual(doc_format.tool, "itstool")
        self.assertEqual(doc_format.format, "mallard")
        self.addCleanup(os.remove, str(potfile))
        self.assertIn("#: C/what-is.page:7", potfile.read_text())
        res = utils.get_fig_stats(potfile, doc_format=doc_format)
        self.assertEqual(len(res), 1)
        self.assertEqual(res[0]["path"], "figures/gnome-hello-logo.png")
        self.assertEqual(res[0]["hash"], "1ae47b7a7c4fbeb1f6bb72c0cf18d389")

    def test_http_pot(self):
        dom = Domain.objects.create(
            module=self.mod,
            name="http-po",
            dtype="ui",
            pot_method="url",
            pot_params="https://l10n.gnome.org/POT/damned-lies.master/damned-lies.master.pot",
        )
        with patch("stats.models.request.urlopen") as mock_urlopen:
            with Path.open(Path(__file__).parent / "empty.pot", mode="rb") as fh:
                mock_urlopen.return_value = fh
                potfile, _ = dom.generate_pot_file(self.branch)
        self.addCleanup(os.remove, str(potfile))
        self.assertTrue(potfile.exists())

    def test_shell_pot(self):
        dom = Domain.objects.create(
            module=self.mod, name="shell-po", dtype="ui", pot_method="shell", pot_params="touch some.pot"
        )
        potfile, _ = dom.generate_pot_file(self.branch)
        self.addCleanup(os.remove, str(potfile))
        self.assertTrue(potfile.exists())

    @skipUnless(has_translate_subtitle_support, "This test needs translate-toolkit subtitles support.")
    def test_subtitles_pot(self):
        dom = Domain.objects.create(
            module=self.mod,
            name="po",
            dtype="ui",
            layout="subtitles/po/{lang}.po",
            pot_method="subtitles",
        )
        potfile, _ = dom.generate_pot_file(self.branch)
        self.addCleanup(os.remove, str(potfile))
        self.assertTrue(potfile.exists())
        self.assertEqual(
            dom.get_lang_files(self.branch.checkout_path),
            [("es", Path("%s/git/testmodule/subtitles/po/es.po" % settings.SCRATCHDIR))],
        )

    def test_repository_pot(self):
        dom = Domain.objects.create(
            module=self.mod,
            name="po",
            dtype="ui",
            layout="inrepo/{lang}.po",
            pot_method="in_repo",
        )
        potfile, errs = dom.generate_pot_file(self.branch)
        self.assertEqual(errs, ())
        self.assertTrue(potfile.exists())


class StatisticsTests(TestCase):
    fixtures = ("test_data.json",)

    def test_get_global_stats(self):
        rel = Release.objects.get(name="gnome-3-8")
        stats = rel.get_global_stats()
        self.assertEqual(len(stats), 2)  # data for French and Italian
        fr_stats = stats[0]
        self.assertEqual(fr_stats["lang_name"], "French")
        self.assertEqual(fr_stats["ui"]["translated"], 183)
        self.assertEqual(fr_stats["doc"]["untranslated"], 259)

    def test_total_stats_for_lang(self):
        rel = Release.objects.get(name="gnome-3-8")
        total_for_lang = rel.total_for_lang(Language.objects.get(locale="fr"))
        self.assertEqual(total_for_lang["ui"]["total"], total_for_lang["ui_part"]["total"])
        self.assertTrue(total_for_lang["ui"]["untranslated"] == total_for_lang["ui_part"]["untranslated"] == 0)
        total_for_lang = rel.total_for_lang(Language.objects.get(locale="bem"))
        self.assertEqual(total_for_lang["ui"]["total"] - 8, total_for_lang["ui_part"]["total"])
        self.assertEqual(total_for_lang["ui"]["untranslated"], 183)
        self.assertEqual(total_for_lang["ui_part"]["untranslated"], 175)
        # Test that excluded domains are taken into account
        zenity_po = Domain.objects.get(module__name="zenity", name="po")
        zenity_po.branch_from = Branch.objects.get(module__name="zenity", name="master")
        zenity_po.save()
        rel = Release.objects.get(name="gnome-3-8")
        total_for_lang = rel.total_for_lang(Language.objects.get(locale="bem"))
        self.assertLess(total_for_lang["ui"]["untranslated"], 183)

    @test_scratchdir
    def test_update_po_file(self):
        stats = Statistics.objects.get(
            branch__module__name="zenity", branch__name="gnome-3-8", domain__name="po", language__locale="it"
        )
        stats.full_po = PoFile.objects.create(path="git/gnome-hello/po/it.po")
        stats.save()
        stats.full_po.update_statistics()
        self.assertEqual(stats.full_po.translated, 20)
        self.assertEqual(stats.full_po.untranslated, 2)

    def test_stats_links(self):
        pot_stats = Statistics.objects.get(
            branch__module__name="zenity", branch__name="gnome-3-8", domain__name="po", language__isnull=True
        )
        self.assertEqual(pot_stats.po_url(), "/POT/zenity.gnome-3-8/zenity.gnome-3-8.pot")
        stats = Statistics.objects.get(
            branch__module__name="zenity", branch__name="gnome-3-8", domain__name="po", language__locale="it"
        )
        self.assertEqual(stats.po_url(), "/POT/zenity.gnome-3-8/zenity.gnome-3-8.it.po")
        self.assertEqual(stats.po_url(reduced=True), "/POT/zenity.gnome-3-8/zenity.gnome-3-8.it.reduced.po")
        # Same for a fake stats
        stats = FakeLangStatistics(pot_stats, Language.objects.get(locale="bem"))
        self.assertEqual(stats.po_url(), "/module/po/zenity/gnome-3-8/po/bem.po")
        self.assertEqual(stats.po_url(reduced=True), "/module/po/zenity/gnome-3-8/po/bem-reduced.po")

    def test_total_by_releases(self):
        releases = list(Release.objects.filter(name__in=("gnome-3-8", "gnome-dev")))
        stats = Release.total_by_releases("ui", releases)
        self.assertEqual(set(stats.keys()), {"fr", "it"})
        self.assertEqual(stats["fr"]["diff"], 0)
        self.assertEqual(stats["fr"]["stats"], [100, 100])
        self.assertEqual(stats["it"]["diff"], 24)
        self.assertEqual(stats["it"]["stats"], [63, 87])

    def test_get_lang_stats_by_type(self):
        mod = Module.objects.get(name="zenity")
        lang = Language.objects.get(locale="fr")
        dom = Domain.objects.create(module=mod, name="po2", dtype="ui", description="UI Translations")
        branch = mod.branch_set.get(name="gnome-3-8")
        # Add one more stat so as comparison happens on two real stat objects
        Statistics.objects.create(branch=branch, domain=dom, language=None)
        stats = Statistics.get_lang_stats_by_type(lang, "ui", Release.objects.get(name="gnome-3-8"))
        for key, value in {
            "total": 183,
            "totaltrans": 183,
            "totalfuzzy": 0,
            "totaluntrans": 0,
            "totaltransperc": 100,
            "totalfuzzyperc": 0,
            "totaluntransperc": 0,
            "dtype": "ui",
            "all_errors": [],
        }.items():
            self.assertEqual(stats[key], value)

    def _test_update_statistics(self):
        # Temporarily deactivated, since update_stats cannot receive stats any more
        from vertimus.models import State, StateTranslating

        # Get a stat that has full_po and part_po
        stat = Statistics.objects.get(
            branch__module__name="zenity", branch__name="gnome-2-30", language__locale="it", domain__dtype="ui"
        )
        pers = Person.objects.create(username="toto")
        state = StateTranslating.objects.create(
            branch=stat.branch, domain=stat.domain, language=stat.language, person=pers
        )
        # Mark file completely translated
        self.assertNotEqual(stat.part_po, stat.full_po)
        # stat.set_translation_stats('dummy', 136, 0, 0)
        self.assertEqual(stat.part_po, stat.full_po)
        # Check state is still translating
        state = State.objects.filter(branch=stat.branch, domain=stat.domain, language=stat.language)
        self.assertEqual(len(state), 1)
        self.assertTrue(isinstance(state[0], StateTranslating))

    def test_information_comparable(self):
        infos = [
            Information.objects.create(
                statistics=Statistics.objects.filter(branch__module__name="zenity").first(),
                type="info",
                description="Some information",
            ),
            Information.objects.create(
                statistics=Statistics.objects.filter(branch__module__name="gnome-hello").first(),
                type="info",
                description="Other information",
            ),
        ]
        self.assertEqual(sorted(infos)[0].statistics.branch.module.name, "gnome-hello")

    @test_scratchdir
    def test_empty_pot(self):
        pot_file = Path(__file__).parent / "empty.pot"
        shutil.copyfile(pot_file, settings.SCRATCHDIR / "POT" / "zenity.master")
        mod = Module.objects.get(name="zenity")
        dom = Domain.objects.create(module=mod, name="po2", dtype="ui", description="UI Translations")
        branch = mod.branch_set.get(name="gnome-3-8")
        stat = Statistics.objects.create(branch=branch, domain=dom, language=None)
        stat.update_statistics(pot_file)
        self.assertEqual(stat.full_po, stat.part_po)
        self.assertEqual(stat.translated(), 0)
        self.assertEqual(stat.fuzzy(), 0)
        self.assertEqual(stat.untranslated(), 0)

    def test_compare_releases(self):
        response = self.client.get(reverse("release-compare", args=["ui", "3-8/dev"]))
        self.assertContains(response, f'<h1>{_("Releases Comparison")}</h1>')
        self.assertEqual(len(response.context["releases"]), 2)
        # wrong domain type
        with self.assertLogs("django.request", "WARNING"):
            response = self.client.get(reverse("release-compare", args=["whatever", "3-8/dev"]))
        self.assertEqual(response.status_code, 404)
        # wrong release names
        with self.assertLogs("django.request", "WARNING"):
            response = self.client.get(reverse("release-compare", args=["ui", "whatever1/whatever2"]))
        self.assertEqual(response.status_code, 404)


class FigureTests(TestCase):
    fixtures = ("test_data.json",)

    def test_figure_view(self):
        url = reverse("docimages", args=["gnome-hello", "help", "master", "fr"])
        response = self.client.get(url)
        self.assertContains(response, "gnome-hello-new.png")
        # Same for a non-existing language
        Language.objects.create(name="Afrikaans", locale="af")
        url = reverse("docimages", args=["gnome-hello", "help", "master", "af"])
        response = self.client.get(url)
        self.assertContains(response, "gnome-hello-new.png")

    def test_figure_urls(self):
        """Test if figure urls are properly constructed"""
        stat = Statistics.objects.get(
            branch__module__name="gnome-hello", branch__name="master", domain__dtype="doc", language__locale="fr"
        )
        figs = stat.get_figures()
        self.assertEqual(
            figs[0]["orig_remote_url"],
            "https://gitlab.gnome.org/GNOME/gnome-hello/raw/master/help/C/figures/gnome-hello-new.png",
        )
        self.assertFalse("trans_remote_url" in figs[0])

    @test_scratchdir
    def test_identical_figure_warning(self):
        """Detect warning if translated figure is identical to original figure"""
        branch = Branch.objects.get(module__name="gnome-hello", name="master")
        pot_stat = Statistics.objects.get(branch=branch, domain__name="help", language__isnull=True)
        fig_path = str(
            pot_stat.branch.checkout_path / pot_stat.domain.base_directory / "C" / pot_stat.get_figures()[0]["path"]
        )
        shutil.copyfile(fig_path, fig_path.replace("/C/", "/fr/"))
        doc_stat = Statistics.objects.get(branch=branch, domain__name="help", language__locale="fr")
        errs = utils.check_identical_figures(
            doc_stat.get_figures(),
            branch.checkout_path / "help",
            "fr",
        )
        self.assertEqual(len(errs), 1)
        self.assertTrue(errs[0][1].startswith("Figures should not be copied"))


class UtilsTests(TestModuleBase):
    def test_read_makefile_variable(self):
        domain = Domain.objects.create(
            module=self.mod, name="help", dtype="doc", layout="help_docbook/{lang}/{lang}.po"
        )
        makefile = utils.MakefileWrapper.find_file(self.branch, [self.branch.domain_path(domain)])
        var_content = makefile.read_variable("DOC_INCLUDES")
        self.assertEqual(var_content.split(), ["rnusers.xml", "rnlookingforward.xml", "$(NULL)"])

        var_content = makefile.read_variable("HELP_FILES")
        self.assertEqual(var_content.split(), ["rnusers.xml", "rnlookingforward.xml"])

        # Test $(top_srcdir) replacement
        var_content = makefile.read_variable("SOME_VAR")
        self.assertEqual(var_content, str(self.branch.checkout_path) + "/some_path.po")

    def test_read_meson_variables(self):
        domain = Domain.objects.create(
            module=self.mod, name="help", dtype="doc", layout="help_mallard_meson/{lang}/{lang}.po"
        )
        meson_file = utils.MakefileWrapper.find_file(self.branch, [self.branch.domain_path(domain)])
        self.assertEqual(meson_file.read_variable("yelp.languages"), ["es", "fr"])
        self.assertEqual(meson_file.read_variable("yelp.sources"), ["index.page", "what-is.page", "legal.xml"])
        # UI meson file
        path = Path(__file__).parent / "meson-ui.build"
        meson_file = utils.MesonfileWrapper(self.branch, path)
        self.assertEqual(meson_file.read_variable("gettext.preset"), "glib")
        self.assertEqual(meson_file.read_variable("gettext.args"), ["--keyword=Description"])

        # UI meson file (with data_dirs)
        path = Path(__file__).parent / "meson-ui2.build"
        meson_file = utils.MesonfileWrapper(self.branch, path)
        self.assertEqual(
            meson_file.read_variable("gettext.data_dirs"),
            [
                str(self.branch.checkout_path / "src/gstyle/data"),
                str(self.branch.checkout_path / "data/style-schemes"),
            ],
        )

    def test_read_cmake_variables(self):
        domain = Domain.objects.create(
            module=self.mod, name="help", dtype="doc", layout="help_mallard/{lang}/{lang}.po"
        )
        cmake_file = utils.MakefileWrapper.find_file(
            self.branch, [self.branch.domain_path(domain)], file_name="CMakeLists.txt"
        )
        self.assertEqual(cmake_file.read_variable("HELP_FILES").split(), ["index.page", "what-is.page", "legal.xml"])

    def test_doc_format_source_files(self):
        domain = Domain.objects.create(
            module=self.mod, name="help", dtype="doc", layout="help_mallard_meson/{lang}/{lang}.po"
        )
        df = utils.DocFormat(domain, self.branch)
        self.assertEqual(df.source_files(), [Path("C/index.page"), Path("C/what-is.page"), Path("C/legal.xml")])

    @test_scratchdir
    def test_insert_locale_in_linguas(self):
        linguas_path = settings.SCRATCHDIR / "git" / "gnome-hello" / "po" / "LINGUAS"
        utils.insert_locale_in_linguas(linguas_path, "xx")
        self.assertTrue(linguas_path.read_text().endswith("\nxx\n"))
        # Try with file without ending new line
        with linguas_path.open("rb+") as fh:
            fh.seek(-1, os.SEEK_END)
            fh.truncate()
        utils.insert_locale_in_linguas(linguas_path, "xy")
        self.assertTrue(linguas_path.read_text().endswith("\nxy\n"))
        # Also works if file is initially empty
        os.truncate(linguas_path, 0)
        utils.insert_locale_in_linguas(linguas_path, "xz")
        self.assertEqual(linguas_path.read_text(), "xz\n")

    def test_exclude_messages_from_potfile(self):
        pot_file = Path(__file__).parent / "donottranslate.pot"
        with tempfile.NamedTemporaryFile(suffix=".pot") as temppot:
            shutil.copyfile(str(pot_file), temppot.name)
            utils.exclude_untrans_messages(temppot.name)
            stats = utils.po_file_statistics(Path(temppot.name))
        self.assertEqual(stats["untranslated"], 2)

    @test_scratchdir
    def test_po_grep(self):
        # Take it.po, because desktop.in.h strings are not yet translated
        it_path = settings.SCRATCHDIR / "git" / "gnome-hello" / "po" / "it.po"
        temp_file = tempfile.NamedTemporaryFile(delete=False)
        self.addCleanup(os.remove, temp_file.name)
        utils.po_grep(it_path, temp_file.name, "locations|desktop.in.h")
        with Path.open(temp_file.name, encoding="utf-8") as fh:
            content = fh.read()
        self.assertGreater(len(content), 0)
        self.assertNotIn("desktop.in.h", content)

    def test_po_quality(self):
        valid_path = Path(__file__).parent / "xml_valid.po"
        result = utils.check_po_quality(str(valid_path), ["xmltags"])
        self.assertEqual(result, "")

        error_path = Path(__file__).parent / "xml_error.po"
        result = utils.check_po_quality(str(error_path), ["xmltags"])
        self.assertIn("xmltags: Different XML tags", result)

        result = utils.check_po_quality("unexisting/path.po", ["xmltags"])
        self.assertEqual(result, "The file “unexisting/path.po” does not exist")


class OtherTests(TestCase):
    def test_release_list(self):
        Release.objects.bulk_create([
            Release(name="gnome-3-18", description="GNOME 3.18", status="official", weight=0),
            Release(name="gnome-2-32", description="GNOME 2.32", status="official", weight=-10),
        ])
        response = self.client.get(reverse("releases"))
        contents = [
            "<h1>GNOME Releases</h1>",
            "<span>Older Releases</span>",
            '<a href="%s">GNOME 3.18</a>' % reverse("release", args=["gnome-3-18"]),
        ]
        for item in contents:
            self.assertContains(response, item)
        # Also test JSON output
        response = self.client.get(reverse("releases", args=["json"]))
        content = json.loads(response.content.decode("utf-8"))
        self.assertEqual(content[0]["fields"]["name"], "gnome-3-18")


class VCSRepositoryTest(TestCase):
    fixtures = ("test_data.json",)

    def test_create_new_vcs_repository(self):
        branch = Branch.objects.get(pk=1)
        git = VCSRepository.create_from_repository_type(VCSRepositoryType.GIT, branch)
        self.assertIsInstance(git, GitRepository)
