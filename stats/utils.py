import abc
import hashlib
import os
import re
import shutil
import time
from itertools import islice
from pathlib import Path
from typing import TYPE_CHECKING, ClassVar, Never
from unittest.mock import MagicMock

from django.conf import settings
from django.core.files.base import File
from django.template.loader import get_template
from django.urls import reverse
from django.utils.functional import cached_property
from django.utils.translation import gettext as _
from django.utils.translation import gettext_noop
from gitlab import Gitlab
from translate.storage.base import TranslationStore
from translate.tools import pocount, pogrep

from common.utils import run_shell_command
from stats.potdiff import PoEntry

STATUS_OK = 0
C_ENV = {"LC_ALL": "C", "LANG": "C", "LANGUAGE": "C"}

ITSTOOL_PATH = getattr(settings, "ITSTOOL_PATH", "")
ITS_DATA_DIR = settings.SCRATCHDIR / "data" / "its"

# monkey-patch ttk (https://github.com/translate/translate/issues/2129)
orig_addunit = TranslationStore.addunit

if TYPE_CHECKING:
    from stats.models import Branch, Domain


def patched_add_unit(self, unit) -> None:
    # Prevent two header units in the same store
    if unit.isheader() and len(self.units) and self.units[0].isheader():
        unit._store = self
        self.units[0] = unit
    else:
        orig_addunit(self, unit)


TranslationStore.addunit = patched_add_unit


class UndetectableDocFormatError(Exception):
    pass


class DocFormat:
    itstool_regex = re.compile("^msgid \"external ref='(?P<path>[^']*)' md5='(?P<hash>[^']*)'\"")

    def __init__(self, domain, branch) -> None:
        self.branch = branch
        self.vcs_path = branch.domain_path(domain)
        self.makefile = MakefileWrapper.find_file(branch, [self.vcs_path])
        if self.makefile is None:
            raise UndetectableDocFormatError(
                gettext_noop("Unable to find a makefile for module %s") % branch.module.name
            )
        has_page_files = any(f.suffix == ".page" for f in self.list_c_files())
        self.format = "mallard" if has_page_files else "docbook"
        self.tool = "itstool"

    def __repr__(self) -> str:
        return f"{self.__class__} format={self.format}, tool={self.tool}>"

    @property
    def use_meson(self):
        return isinstance(self.makefile, MesonfileWrapper)

    def list_c_files(self):
        return [p.relative_to(self.vcs_path) for p in (self.vcs_path / "C").iterdir() if p.is_file()]

    def source_files(self):
        """Return help source files, with path relative to help base dir."""
        sources = []
        if self.format == "docbook":
            moduleid = self.branch.module.name
            modulename = self.makefile.read_variable(*self.module_var)
            if not modulename:
                modulename = moduleid
            for index_page in ("index.docbook", modulename + ".xml", moduleid + ".xml"):
                if (self.vcs_path / "C" / index_page).exists():
                    sources.append(index_page)
                    break
            if not sources:
                # Last try: only one xml/docbook file in C/...
                xml_files = [f for f in self.list_c_files() if f.suffix in {".xml", ".docbook"}]
                if len(xml_files) == 1:
                    sources.append(xml_files[0].name)
                else:
                    raise Exception(gettext_noop("Unable to find doc source files for this module."))

        source_list = self.makefile.read_variable(*self.include_var)
        if not source_list:
            suffix = [".page"] if self.format == "mallard" else [".xml", ".docbook"]
            # Fallback to directory listing
            return [f for f in self.list_c_files() if f.suffix in suffix]
        if isinstance(source_list, str):
            sources += source_list.split()
        elif source_list:
            sources += source_list
        return [Path("C", src) for src in sources if src not in {"", "$(NULL)"}]

    def command(self, potfile, files):
        cmd = [f"{ITSTOOL_PATH}itstool", "-o", str(potfile)]
        cmd.extend([str(f) for f in files])
        return cmd

    @property
    def module_var(self):
        if self.use_meson:
            return ["yelp.project_id"]
        return ["HELP_ID", "DOC_ID", "DOC_MODULE"]

    @property
    def include_var(self):
        if self.use_meson:
            return ["yelp.sources"]
        return ["HELP_FILES", "DOC_PAGES", "DOC_INCLUDES"]

    @property
    def img_grep(self) -> str:
        return '^msgid "external ref='

    @property
    def bef_line(self) -> int:
        # Lines to keep before matched grep to catch the ,fuzzy or #|msgid line
        return 2

    @property
    def img_regex(self):
        return self.itstool_regex


class MakefileWrapper:
    default_makefiles = (
        "Makefile.am",
        "meson.build",
        "CMakeLists.txt",
    )

    @classmethod
    def find_file(cls, branch, vcs_paths, file_name=None):
        """Return the first makefile found as a MakefileWrapper instance, or None."""
        names = [file_name] if file_name is not None else cls.default_makefiles
        for path in vcs_paths:
            for file_name in names:
                file_path = path / file_name
                if file_path.exists():
                    if file_name == "meson.build":
                        return MesonfileWrapper(branch, file_path)
                    if file_name == "CMakeLists.txt":
                        return CMakefileWrapper(branch, file_path)
                    return MakefileWrapper(branch, file_path)
        return None

    def __init__(self, branch, path) -> None:
        self.branch = branch
        self.path = path

    @cached_property
    def content(self):
        try:
            content = self.path.read_text(encoding="utf-8")
        except OSError:
            return None
        return content

    def read_variable(self, *variables):
        if self.content is None:
            return None
        non_terminated_content = ""
        found = None
        for line in self.content.splitlines():
            line = line.strip()
            if line.startswith("#"):
                continue
            if non_terminated_content:
                line = non_terminated_content + " " + line
            # Test if line is non terminated (end with \ or even quote count)
            if (len(line) > 2 and line[-1] == "\\") or line.count('"') % 2 == 1:
                if line[-1] == "\\":
                    # Remove trailing backslash
                    line = line[:-1]
                non_terminated_content = line
            else:
                non_terminated_content = ""
                # Search for variable
                for var in variables:
                    match = re.search(rf'{var}\s*[=,]\s*"?([^"]*)"?', line)
                    if match:
                        found = match.group(1)
                        break
                if found:
                    break

        # Try to expand '$(var)' variables
        if found:
            for element in found.split():
                if element.startswith("$(") and element.endswith(")") and element != "$(NULL)":
                    result = self.read_variable(element[2:-1])
                    if result:
                        found = found.replace(element, result)
            if "$(top_srcdir)" in found:
                found = found.replace("$(top_srcdir)", str(self.branch.checkout_path))
        return found


class MesonfileWrapper(MakefileWrapper):
    i18n_gettext_kwargs: ClassVar[set[str]] = {"po_dir", "data_dirs", "type", "languages", "args", "preset"}
    gnome_yelp_kwargs: ClassVar[set[str]] = {"sources", "media", "symlink_media", "languages"}
    ignorable_kwargs: ClassVar[set[str]] = {"install_dir"}
    readable = True

    @cached_property
    def content(self):
        content = super().content
        # Here be dragons: Try to make meson content look like Python
        possible_kwargs = list(self.i18n_gettext_kwargs | self.gnome_yelp_kwargs | self.ignorable_kwargs)
        content = re.sub(r"(" + "|".join(possible_kwargs) + ") ?:", r"\1=", content)
        # ignore if/endif sections
        content = re.sub(r"^if .*endif$", "", content, flags=re.M | re.S)
        return (
            content.replace("true", "True")
            .replace("false", "False")
            .replace("i18n = import('i18n')", "")
            .replace("gnome = import('gnome')", "")
        )

    @cached_property
    def _parsed_variables(self):
        caught = {}
        if self.content is None:
            return caught

        class VarCatcher:
            def yelp(self, *args, **kwargs) -> None:
                caught["yelp.project_id"] = args[0]
                for var_name in ("sources", "languages"):
                    if var_name in kwargs:
                        caught[f"yelp.{var_name}"] = kwargs[var_name]

            def gettext(self, *args, **kwargs) -> None:
                caught["gettext.project_id"] = args[0]
                for var_name in MesonfileWrapper.i18n_gettext_kwargs:
                    if var_name in kwargs:
                        caught[f"gettext.{var_name}"] = kwargs[var_name]

        catcher = VarCatcher()
        this_instance = self

        class MesonMock:
            def __getattr__(self, name):
                return MagicMock()

            def source_root(self):
                return str(this_instance.branch.checkout_path)

        meson_locals = {
            "gnome": catcher,
            "i18n": catcher,
            "GNOME": catcher,
            "I18N": catcher,
            "install_data": MagicMock(),
            "meson": MesonMock(),
            "join_paths": lambda *args: os.sep.join(args),
        }
        while True:
            try:
                exec(self.content, {}, meson_locals)
                break
            except NameError as exc:
                # Put the unknown name in the locals dict and retry exec
                m = re.search(r"name '([^']*)' is not defined", str(exc))
                if m:
                    name = m.groups()[0]
                    meson_locals[name] = MagicMock()
                else:
                    break
            except Exception:
                self.readable = False
                break
        return caught

    def read_variable(self, *variables):
        """Return the value of the first found variable name in the variables list."""
        parsed_vars = self._parsed_variables

        def strip_mock(value):
            if isinstance(value, list):
                return [v for v in value if not isinstance(v, MagicMock)]
            return value if not isinstance(value, MagicMock) else None

        for var in variables:
            if var in parsed_vars:
                value = strip_mock(parsed_vars[var])
                if value:
                    return value
        return None


class CMakefileWrapper(MakefileWrapper):
    def read_variable(self, *variables):
        # Try to read variables defined as: set(VAR_NAME content1 content2)
        if self.content is None:
            return None
        non_terminated_content = ""
        found = None
        for line in self.content.splitlines():
            line = line.strip()
            if non_terminated_content:
                line = non_terminated_content + " " + line
            # Test if line is non terminated
            if line.count("(") > line.count(")"):
                non_terminated_content = line
            else:
                non_terminated_content = ""
                # Search for variable
                for var in variables:
                    match = re.search(rf"set\s*\({var}\s*([^\)]*)\)", line)
                    if match:
                        found = match.group(1).strip()
                        break
                if found:
                    break
        return found


def sort_object_list(lst, sort_meth):
    """Sort an object list with sort_meth (which should return a translated string)"""
    templist = [(getattr(obj_, sort_meth)().lower(), obj_) for obj_ in lst]
    templist.sort()
    return [obj_ for (key1, obj_) in templist]


def multiple_replace(dct, text):
    regex = re.compile("({})".format("|".join(map(re.escape, dct.keys()))))
    return regex.sub(lambda mo: dct[mo.string[mo.start() : mo.end()]], text)


def strip_html(string):
    replacements = {"<ul>": "\n", "</ul>": "\n", "<li>": " * ", "\n</li>": "", "</li>": ""}
    return multiple_replace(replacements, string)


def ellipsize(val, length):
    if len(val) > length:
        val = f"{val[:length]}..."
    return val


def check_program_presence(prog_name):
    """Test if prog_name is an available command on the system"""
    status, _, _ = run_shell_command(["which", prog_name])
    return status == 0


def po_grep(in_file, out_file, filter_) -> None:
    if filter_ == "-":
        return
    if not filter_:
        filter_loc, filter_str = "locations", "gschema.xml(.in)?|schemas.in"
    else:
        try:
            filter_loc, filter_str = filter_.split("|")
        except Exception:
            # Probably bad filter syntax in DB (TODO: log it)
            return
    grepfilter = pogrep.GrepFilter(filter_str, filter_loc, useregexp=True, invertmatch=True, keeptranslations=True)
    with open(out_file, "wb") as out:
        try:
            pogrep.rungrep(str(in_file), out, None, grepfilter)
        except Exception:
            pass


def check_potfiles(po_path):
    """Check if there were any problems regenerating a POT file (intltool-update -m).
    Return a list of errors"""
    errors = []

    run_shell_command(["rm", "-f", "missing", "notexist"], cwd=po_path)
    status, _, _ = run_shell_command(["intltool-update", "-m"], cwd=po_path)

    if status != STATUS_OK:
        errors.append(("error", gettext_noop("Errors while running “intltool-update -m” check.")))

    missing = po_path / "missing"
    if missing.exists():
        with missing.open("r") as fh:
            errors.append((
                "warn",
                gettext_noop("There are some missing files from POTFILES.in: %s")
                % ("<ul><li>" + "</li>\n<li>".join(fh.readlines()) + "</li>\n</ul>"),
            ))

    notexist = po_path / "notexist"
    if notexist.exists():
        with notexist.open("r") as fh:
            errors.append((
                "error",
                gettext_noop(
                    "Following files are referenced in either POTFILES.in or POTFILES.skip, "
                    "yet they don’t exist: %s"
                )
                % ("<ul><li>" + "</li>\n<li>".join(fh.readlines()) + "</li>\n</ul>"),
            ))
    return errors


def generate_doc_pot_file(branch: "Branch", domain: "Domain") -> tuple[str, list[tuple[str, str]]]:
    """
    Return the pot file for a document-type domain, and the error if any
    """
    try:
        doc_format = DocFormat(domain, branch)
        files = doc_format.source_files()
    except Exception as err:
        return "", [("error", str(err))]

    potbase = domain.pot_base()
    potfile = doc_format.vcs_path / "C" / (potbase + ".pot")
    command = doc_format.command(potfile, files)
    status, _, errs = run_shell_command(command, cwd=doc_format.vcs_path)

    if status != STATUS_OK:
        return "", [
            (
                "error",
                gettext_noop("Error regenerating POT file for document %(file)s:\n<pre>%(cmd)s\n%(output)s</pre>")
                % {
                    "file": potbase,
                    "cmd": " ".join([c.replace(str(settings.SCRATCHDIR), "&lt;scratchdir&gt;") for c in command]),
                    "output": errs,
                },
            )
        ]

    if not potfile.exists():
        return "", []
    return potfile, []


def remove_diff_markup_on_list_of_additions(differences: list[str]) -> list[str]:
    """
    Remove the markup used by the diff library to show additions.
    It wraps each addition with quotes and adds a ‘+’ sign as prefix.

    For instance: str = '+ "Dummy string for D-L tests"'

    :param differences: list of diff output.
    :return: cleaned list of differences with added strings without any special markup around.
    """
    return [difference.removeprefix("+").removeprefix(' "').removesuffix('"') for difference in differences]


def check_po_conformity(pofile):
    """Return errors/warnings about pofile conformity."""
    errors = []
    # Allow pofile provided as open file (e.g. to validate a temp uploaded file)
    if isinstance(pofile, File):
        input_data = pofile.read()
        if isinstance(input_data, bytes):
            try:
                input_data = input_data.decode("utf-8")
            except UnicodeDecodeError:
                return [("warn", gettext_noop("PO file “%s” is not UTF-8 encoded.") % pofile.name)]
        input_file = "-"
    else:
        input_data = None
        input_file = str(pofile)

    command = ["msgfmt", "-cv", "-o", "/dev/null", input_file]
    status, _, _ = run_shell_command(command, env=C_ENV, input_data=input_data)
    if status != STATUS_OK:
        errors.append(("error", gettext_noop("PO file “%s” doesn’t pass msgfmt check.") % pofile.name))

    if input_file != "-" and os.access(str(pofile), os.X_OK):
        errors.append(("warn", gettext_noop("This PO file has an executable bit set.")))

    # Check if PO file is in UTF-8
    if input_file != "-":
        command = f'msgconv -t UTF-8 "{pofile}" | diff -i -I \'^#~\' -u "{pofile}" - >/dev/null'
        status, _, _ = run_shell_command(command, env=C_ENV)
        if status != STATUS_OK:
            errors.append(("warn", gettext_noop("PO file “%s” is not UTF-8 encoded.") % pofile.name))
    return errors


def check_po_quality(pofile, filters):
    """
    Check po file quality with the translate-toolkit pofilter tool.
    http://docs.translatehouse.org/projects/translate-toolkit/en/latest/commands/pofilter.html
    """
    if not Path(pofile).exists():
        return _("The file “%s” does not exist") % pofile
    status, out, errs = run_shell_command([
        "pofilter",
        "--progress=none",
        "--gnome",
        "-t",
        "accelerators",
        "-t",
        *list(filters),
        pofile,
    ])
    if status == STATUS_OK:
        return out
    return _("Error running pofilter: %s") % errs


def po_file_statistics(pofile):
    """Compute pofile translation statistics."""
    res = {
        "translated": 0,
        "fuzzy": 0,
        "untranslated": 0,
        "translated_words": 0,
        "fuzzy_words": 0,
        "untranslated_words": 0,
        "errors": [],
    }

    if not pofile.exists():
        res["errors"].append(("error", gettext_noop("PO file “%s” does not exist or cannot be read.") % pofile.name))
        return res

    try:
        status = pocount.calcstats_old(str(pofile))
    except AttributeError:
        # From translate-toolkit 3.3
        status = pocount.calcstats(str(pofile))
    if not status:
        res["errors"].append(("error", gettext_noop("Can’t get statistics for POT file “%s”.") % pofile.name))
    else:
        res["fuzzy"] = status["fuzzy"]
        res["translated"] = status["translated"]
        res["untranslated"] = status["untranslated"]
        res["fuzzy_words"] = status["fuzzysourcewords"]
        res["translated_words"] = status["translatedsourcewords"]
        res["untranslated_words"] = status["untranslatedsourcewords"]

    return res


def read_linguas_file(full_path):
    """Read a LINGUAS file (each language code on a line by itself)"""
    langs = []
    with full_path.open("r") as fh:
        [langs.extend(line.split()) for line in fh if line[:1] != "#"]
    return {
        "langs": langs,
        "error": gettext_noop("Entry for this language is not present in LINGUAS file."),
    }


def insert_locale_in_linguas(linguas_path, locale) -> None:
    temp_linguas = linguas_path.parent / (linguas_path.name + "~")
    with linguas_path.open("r") as fin, temp_linguas.open("w") as fout:
        lang_written = False
        line = "\n"
        for line in fin:
            if not lang_written and line[0] != "#" and line[:5] > locale[:5]:
                fout.write(locale + "\n")
                lang_written = True
            fout.write(line)
        if not lang_written:
            fout.write(("\n" if not line.endswith("\n") else "") + locale + "\n")
    temp_linguas.replace(linguas_path)


def get_ui_linguas(branch, subdir):
    """Get language list in one of po/LINGUAS, configure.ac or configure.in"""

    linguas_here = branch.checkout_path / subdir / "LINGUAS"
    lingua_po = branch.checkout_path / "po" / "LINGUAS"  # if we're in eg. po-locations/

    # is "lang" listed in either of po/LINGUAS, ./configure.ac(ALL_LINGUAS) or ./configure.in(ALL_LINGUAS)
    for linguas in [linguas_here, lingua_po]:
        if linguas.exists():
            return read_linguas_file(linguas)
    # AS_ALL_LINGUAS is a macro that takes all po files by default
    status, _, _ = run_shell_command(f"grep -qs AS_ALL_LINGUAS {branch.checkout_path}{os.sep}configure.*")
    if status == 0:
        return {"langs": None, "error": gettext_noop("No need to edit LINGUAS file or variable for this module")}

    configureac = branch.checkout_path / "configure.ac"
    configurein = branch.checkout_path / "configure.in"
    for configure in [configureac, configurein]:
        found = MakefileWrapper(branch, configure).read_variable("ALL_LINGUAS")
        if found is not None:
            return {
                "langs": found.split(),
                "error": gettext_noop("Entry for this language is not present in ALL_LINGUAS in configure file."),
            }
    return {
        "langs": None,
        "error": gettext_noop("Don’t know where to look for the LINGUAS variable, ask the module maintainer."),
    }


def get_doc_linguas(branch, subdir):
    """Get language list in one Makefile.am (either path)"""
    linguas = None
    po_path = branch.checkout_path / subdir

    # Prioritize LINGUAS files when it exists.
    linguas_path = po_path / "LINGUAS"
    if linguas_path.exists():
        return read_linguas_file(linguas_path)

    linguas_file = MakefileWrapper.find_file(branch, [po_path, branch.checkout_path])
    if linguas_file:
        linguas = linguas_file.read_variable("DOC_LINGUAS", "HELP_LINGUAS", "gettext.languages")
    if linguas is None:
        return {
            "langs": None,
            "error": gettext_noop("Don’t know where to look for the DOC_LINGUAS variable, ask the module maintainer."),
        }
    return {
        "langs": linguas.split() if isinstance(linguas, str) else linguas,
        "error": gettext_noop("DOC_LINGUAS list doesn’t include this language."),
    }


def collect_its_data() -> None:
    """
    Fill a data directory with ``*.loc/*its`` files needed by gettext to extract
    XML strings for GNOME modules.
    """
    from .models import Module, ModuleLock

    if not ITS_DATA_DIR.exists():
        ITS_DATA_DIR.mkdir(parents=True)
    data_to_collect = getattr(settings, "GETTEXT_ITS_DATA", {})
    for module_name, files in data_to_collect.items():
        mod = Module.objects.get(name=module_name)
        branch = mod.get_head_branch()
        with ModuleLock(mod):
            branch.checkout()
            for file_path in files:
                src = branch.checkout_path / file_path
                dest = ITS_DATA_DIR / Path(file_path).parent
                shutil.copyfile(str(src), dest)


def get_fig_stats(pofile, doc_format):
    """Extract image strings from pofile and return a list of figures dict:
    [{'path':, 'hash':, 'fuzzy':, 'translated':}, ...]"""
    if not isinstance(doc_format, DocFormat):
        return []
    # Extract image strings: beforeline/msgid/msgstr/grep auto output a fourth line
    before_lines = doc_format.bef_line
    command = f"msgcat --no-wrap {pofile}| grep -A 1 -B {before_lines} '{doc_format.img_grep}'"
    (status, output, _errs) = run_shell_command(command)
    if status != STATUS_OK or output == "":
        # FIXME: something should be logged here
        return []
    lines = output.split("\n")
    while lines[0][0] != "#":
        lines = lines[1:]  # skip warning messages at the top of the output

    figures = []
    for i, line in islice(enumerate(lines), 0, None, 3 + before_lines):
        # TODO: add image size
        fig = {"path": "", "hash": ""}
        m = doc_format.img_regex.match(lines[i + before_lines])
        if m:
            fig["path"] = m.group("path")
            fig["hash"] = m.group("hash")
        fig["fuzzy"] = line == "#, fuzzy" or line[:8] == "#| msgid"
        fig["translated"] = len(lines[i + before_lines + 1]) > 9 and not fig["fuzzy"]
        figures.append(fig)
    return figures


def check_identical_figures(fig_stats, base_path, lang):
    errors = []
    for fig in fig_stats:
        trans_path = base_path / lang / fig["path"]
        if trans_path.exists():
            trans_hash = compute_md5(trans_path)
            if fig["hash"] == trans_hash:
                errors.append((
                    "warn-ext",
                    f"Figures should not be copied when identical to original ({trans_path}).",
                ))
    return errors


def add_custom_header(po_path, header, value) -> None:
    """Add a custom po file header"""
    grep_cmd = f"""grep "{header}" {po_path}"""
    status = 1
    last_headers = ["Content-Transfer-Encoding", "Plural-Forms"]
    while status != 0 and last_headers:
        status, output, _ = run_shell_command(grep_cmd)
        if status != 0:
            # Try to add header
            cmd = f"""sed -i '/^\"{last_headers.pop()}/ a\\"{header}: {value}\\\\n"' {po_path}"""
            run_shell_command(cmd)
    if status == 0 and f"{header}: {value}" not in output:
        # Set header value
        cmd = f"""sed -i '/^\"{header}/ c\\"{header}: {value}\\\\n"' {po_path}"""
        run_shell_command(cmd)


def exclude_untrans_messages(potfile) -> None:
    """Exclude translatable strings matching some translator comments."""
    exclude_message = "translators: do not translate or transliterate this text"
    # Grep first the file to see if the message is in the file.
    status, _, _ = run_shell_command(["grep", "-i", exclude_message, potfile])
    if status != STATUS_OK:
        return

    with open(str(potfile), encoding="utf-8", mode="r+") as fh:
        lines = fh.readlines()
        fh.seek(0)
        skip_unit = False
        for line in lines:
            if exclude_message not in line.lower() and not skip_unit:
                fh.write(line)
            else:
                # A blank line is resetting skip_unit
                skip_unit = line != "\n"
        fh.truncate()


def is_po_reduced(file_path):
    status, _, _ = run_shell_command(["grep", "X-DamnedLies-Scope: partial", file_path])
    return status == 0


def compute_md5(full_path):
    m = hashlib.md5(usedforsecurity=False)
    block_size = 2**13
    with full_path.open("rb") as fh:
        while True:
            data = fh.read(block_size)
            if not data:
                break
            m.update(data)
    return m.hexdigest()


def url_join(base, *args):
    """Create an url in joining base with arguments. A lot nicer than urlparse.urljoin!"""
    url = base
    for arg in args:
        if url[-1] != "/":
            url += "/"
        url += arg
    return url


class Notification(abc.ABC):
    pass


class StringFreezeBreakNotification(Notification):
    def __init__(self, branch, new_entries: list[PoEntry]) -> None:
        self.title = f"String additions to ‘{branch.module.name}.{branch.name}’"

        _, out, _ = run_shell_command(["git", "log", "-n1", "--format=oneline"], cwd=branch.checkout_path)
        latest_commit = out.split()[0] if out else ""
        file_url_root = branch.vcs_web_file_url_root(latest_commit)
        new_strings = [entry.markdown(file_url_root) for entry in new_entries]
        missing_locations = any(not entry.locations for entry in new_entries)

        template = get_template("freeze-notification.txt")

        # Some maintainers may not have filled their GitLab username in their profile.
        maintainers_usernames, maintainers_usernames_missing = [], []
        for maintainer in branch.module.maintainers.all():
            if maintainer.forge_account is not None and not maintainer.forge_account == "":
                maintainers_usernames.append(f"@{maintainer.forge_account}")
            else:
                maintainers_usernames_missing.append(maintainer.name)

        self.description = template.render({
            "module": f"{branch.module.name}.{branch.name}",
            "module_url_in_damned_lies": reverse("module", kwargs={"module_name": branch.module.name}),
            "ourweb": settings.SITE_DOMAIN,
            "new_strings": new_strings,
            "missing_locations": missing_locations,
            "commit_log": branch.vcs_web_log_url,
            "maintainer_usernames": maintainers_usernames,
            "maintainer_usernames_missing": maintainers_usernames_missing,
        })


class StringFreezeBreakNotifier(abc.ABC):
    """
    An abstract base class to implement string freeze break notification.
    """

    @abc.abstractmethod
    def notify(self, notification: StringFreezeBreakNotification) -> Never:
        raise NotImplementedError()


class StringFreezeBreakNotifierOnGitLab(StringFreezeBreakNotifier):
    """
    Implementation of a tool to notify user that a string freeze break occurred.
    It uses the GitLab API.
    """

    GITLAB_INSTANCE_URL = settings.GITLAB_COORDINATION_TEAMS_URL
    GITLAB_INSTANCE_PROJECT = settings.GITLAB_COORDINATION_TEAMS_PROJECT

    def __init__(self) -> None:
        self.gitlab_client = Gitlab(
            f"https://{self.GITLAB_INSTANCE_URL}", private_token=settings.GITLAB_API_TOKENS[self.GITLAB_INSTANCE_URL]
        )
        self.coordination_project = self.gitlab_client.projects.get(self.GITLAB_INSTANCE_PROJECT)

    def notify(self, notification: StringFreezeBreakNotification) -> None:
        _ = self.coordination_project.issues.create(vars(notification) | {"labels": "freeze break"})


class Profiler:
    def __init__(self) -> None:
        self.start = time.clock()

    def time_spent(self):
        return time.clock() - self.start
