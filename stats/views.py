import os
from datetime import date
from itertools import chain

from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.core import serializers
from django.http import (
    Http404,
    HttpResponse,
    HttpResponseBadRequest,
    HttpResponseForbidden,
    HttpResponseRedirect,
)
from django.shortcuts import get_object_or_404, render
from django.urls import reverse
from django.utils.translation import get_language, get_language_info
from django.utils.translation import gettext as _

from common.utils import MIME_TYPES, run_shell_command
from languages.models import Language, get_user_language_from_request
from people.models import Person
from stats.forms import ModuleBranchForm
from stats.models import (
    BRANCH_HEAD_NAMES,
    Branch,
    Category,
    Domain,
    FakeLangStatistics,
    Module,
    ModuleLock,
    Release,
    Statistics,
)


def modules(request, return_format="html"):
    if return_format in {"json", "xml"}:
        data = serializers.serialize(return_format, Module.objects.all())
        return HttpResponse(data, content_type=MIME_TYPES[return_format])

    if return_format in {
        "html",
    }:
        user_language = get_language_info(get_language())

        # Looks like a complex query bug actually extracts all Statistics in the language for the HEAD branches.
        # To avoid other queries on the database, select related fields and only keep those that are used
        # later by the ‘translation_percentage_in_language_for_module’ tag.
        all_statistics_in_language = (
            Statistics.objects.filter(language__name=user_language.get("name"), branch__name__in=BRANCH_HEAD_NAMES)
            .select_related("branch__module", "domain", "full_po")
            .only(
                # Domain
                "domain_id",
                "domain__dtype",
                "domain__branch_from",
                "domain__branch_to",
                # Branch
                "branch__module_id",
                # Statistics
                "full_po__translated",
                "full_po__untranslated",
                "full_po__fuzzy",
            )
        )

        context = {
            "pageSection": "module",
            "modules": Module.objects.only("name", "description", "archived"),
            "user_language": user_language,
            "all_statistics_in_language": all_statistics_in_language,
        }
        return render(request, "module_list.html", context)

    return HttpResponseBadRequest(
        _("The return format type you indicated is not allowed. Allowed values are: html, json, xml.")
    )


def module(request, module_name: str):
    mod = get_object_or_404(Module, name=module_name)
    branches = sorted(mod.branch_set.all())

    context = {
        "pageSection": "module",
        "module": mod,
        "branches": branches,
        "can_edit_branches": mod.can_edit_branches(request.user),
        "can_refresh": can_refresh_branch(request.user),
        "user_language": get_user_language_from_request(request),
        # Will allow to display the language of the user, even if no statistic exist in
        # the user language
        "mandatory_languages": Person.get_by_user(request.user).get_languages(),
    }

    return render(request, "module_detail.html", context)


def ajax_module_branch(request, module_name, branch_name):
    """
    Dynamically load branch statistics from a view.
    """
    mod = get_object_or_404(Module, name=module_name)
    branch = get_object_or_404(mod.branch_set, name=branch_name)
    if request.user.is_authenticated:
        person = Person.get_by_user(request.user)
        langs = person.get_languages()
        # Cache the branch stats with user langs included
        branch.get_ui_statistics(mandatory_languages=langs)
        branch.get_documentation_statistics(mandatory_languages=langs)

    context = {
        "module": mod,
        "branch": branch,
    }
    return render(request, "branch_detail.html", context)


@login_required
def module_edit_branches(request, module_name):
    mod = get_object_or_404(Module, name=module_name)
    if not mod.can_edit_branches(request.user):
        messages.error(request, _("Sorry, you’re not allowed to edit this module’s branches"))
        return HttpResponseRedirect(mod.get_absolute_url())

    if request.method == "POST":
        form = ModuleBranchForm(mod, request.POST)
        if form.is_valid():
            updated = False
            # Modified or deleted release or category for branch
            for key, field in form.fields.items():
                if not getattr(field, "is_branch", False):
                    continue
                if form.fields[key].initial and not form.cleaned_data[key]:
                    # Delete category
                    Category.objects.get(pk=key).delete()
                    updated = True
                    continue
                release_has_changed = key in form.changed_data
                category_has_changed = (key + "_cat") in form.changed_data
                if form.cleaned_data[key] and (release_has_changed or category_has_changed):
                    if field.initial:
                        cat = Category.objects.get(pk=key)
                        if release_has_changed:
                            cat.release = form.cleaned_data[key]
                        cat.name = form.cleaned_data[key + "_cat"]
                    else:
                        rel = Release.objects.get(pk=form.cleaned_data[key].pk)
                        branch = Branch.objects.get(module=mod, name=key)
                        cat = Category(release=rel, branch=branch, name=form.cleaned_data[key + "_cat"])
                    cat.save()
                    updated = True
            # New branch (or new category)
            if form.cleaned_data["new_branch"]:
                if "new_category" in form.cleaned_data:
                    form.cleaned_data["new_category"].save()
                    updated = True
                else:
                    branch_name = form.cleaned_data["new_branch"]
                    try:
                        branch = Branch.objects.get(module=mod, name=branch_name)
                    except Branch.DoesNotExist:
                        # It is probably a new branch
                        try:
                            branch = Branch(module=mod, name=branch_name)
                            branch.save()
                            messages.success(request, _("The new branch %s has been added") % branch_name)
                            updated = True
                            # Send message to gnome-i18n?
                        except Exception as e:
                            messages.error(
                                request,
                                _("Error adding the branch '%(branch_name)s': %(error)s")
                                % {"branch_name": branch_name, "error": str(e)},
                            )
                            branch = None
            else:
                messages.success(request, _("Branches updated"))
            if updated:
                form = ModuleBranchForm(mod)
        else:
            messages.error(request, _("Sorry, the form is not valid"))
    else:
        form = ModuleBranchForm(mod)
    context = {
        "module": mod,
        "form": form,
    }
    return render(request, "module_edit_branches.html", context)


def branch_refresh(request, branch_id):
    if not can_refresh_branch(request.user):
        return HttpResponseForbidden("You are not allowed to refresh this branch's stats.")

    branch = get_object_or_404(Branch, pk=branch_id)
    if ModuleLock(branch.module).is_locked:
        messages.info(
            request,
            _("A statistics update is already running for branch %(branch_name)s of module %(module_name)s.")
            % {"branch_name": branch.name, "module_name": branch.module.name},
        )
    else:
        try:
            branch.update_statistics(force=True)
        except Exception as exc:
            messages.error(
                request,
                f"An error occurred while updating statistics for branch "
                f"{branch.name} of module {branch.module.name}: {exc}",
            )
        else:
            messages.success(
                request,
                _("Statistics successfully updated for branch %(branch)s of module %(module)s")
                % {"branch": branch.name, "module": branch.module.name},
            )
    return HttpResponseRedirect(reverse("module", args=[branch.module.name]))


def docimages(request, module_name, potbase, branch_name, langcode):
    mod = get_object_or_404(Module, name=module_name)
    try:
        stat = Statistics.objects.get(
            branch__module=mod.id, branch__name=branch_name, domain__name=potbase, language__locale=langcode
        )
    except Statistics.DoesNotExist:
        pot_stat = get_object_or_404(
            Statistics, branch__module=mod.id, branch__name=branch_name, domain__name=potbase, language__isnull=True
        )
        lang = get_object_or_404(Language, locale=langcode)
        stat = FakeLangStatistics(pot_stat, lang)
    context = {
        "pageSection": "module",
        "module": mod,
        "stat": stat,
        "locale": stat.language.locale,
        "figure_statistics_dict": stat.fig_stats(),
    }
    return render(request, "module_images.html", context)


def dynamic_po(request, module_name, branch_name, domain_name, filename):
    """Generates a dynamic po file from the POT file of a branch"""
    try:
        locale, _ = filename.split(".")
        if locale.endswith("-reduced"):
            locale, reduced = locale[:-8], True
        else:
            reduced = False
    except Exception as exc:
        raise Http404 from exc

    language = get_object_or_404(Language.objects.select_related("team"), locale=locale)
    branch = get_object_or_404(Branch, module__name=module_name, name=branch_name)
    try:
        domain = branch.connected_domains[domain_name]
    except KeyError as ke:
        raise Http404 from ke
    potfile = get_object_or_404(Statistics, branch=branch, domain=domain, language=None)

    file_path = potfile.po_path(reduced=reduced)
    if not os.access(file_path, os.R_OK):
        raise Http404

    dyn_content = f"""# {language.name} translation for {module_name}.
# Copyright (C) {date.today().year} {module_name}'s COPYRIGHT HOLDER
# This file is distributed under the same license as the {module_name} package.\n"""
    if request.user.is_authenticated:
        person = Person.get_by_user(request.user)
        dyn_content += f"# {person.name} <{person.email}>,  {date.today().year}.\n#\n"
    else:
        dyn_content += "# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.\n#\n"

    command = f"msginit --locale={locale} --no-translator --input={file_path} --output-file=-"
    try:
        _, output, _ = run_shell_command(command, raise_on_error=True)
    except OSError:
        return HttpResponse(f"Unable to produce a new .po file for language {locale}, msginit command failed.")
    lines = output.split("\n")
    skip_next_line = False
    for i, line in enumerate(lines):
        if skip_next_line or (line and line[0] == "#"):
            # Skip first lines of the file
            skip_next_line = False
            continue
        # Transformations
        new_line = {
            '"Project-Id-': f'"Project-Id-Version: {module_name} {branch_name}\\n"',
            '"Last-Transl': '"Last-Translator: FULL NAME <EMAIL@ADDRESS>\\n"',
            '"Language-Te': f'"Language-Team: {language.name} '
            f'<{language.team if language.team and language.team.mailing_list else f"{locale}@li.org"}>\\n"',
            '"Content-Typ': '"Content-Type: text/plain; charset=UTF-8\\n"',
            '"Plural-Form': f'"Plural-Forms: {language.plurals or "nplurals=INTEGER; plural=EXPRESSION"};\\n"',
        }.get(line[:12], line)
        if line != new_line and line[-3:] != '\\n"':
            # Skip the wrapped part of the replaced line
            skip_next_line = True
        dyn_content += new_line + "\n"
        if not line:
            # Output the remaining part of the file untouched
            dyn_content += "\n".join(lines[i + 1 :])
            break
    response = HttpResponse(dyn_content, "text/plain")
    response["Content-Disposition"] = "inline; filename={}".format(
        ".".join([domain.pot_base(), branch.name_escaped, filename])
    )
    return response


def releases(request, format="html"):
    active_releases = Release.objects.filter(weight__gte=0).order_by("status", "-weight", "-name")
    old_releases = Release.objects.filter(weight__lt=0).order_by("status", "-weight", "-name")
    if format in {"json", "xml"}:
        data = serializers.serialize(format, chain(active_releases, old_releases))
        return HttpResponse(data, content_type=MIME_TYPES[format])
    context = {
        "pageSection": "releases",
        "active_releases": active_releases,
        "old_releases": old_releases,
    }
    return render(request, "release_list.html", context)


def release(request, release_name, format="html"):
    release = get_object_or_404(Release, name=release_name)
    if format == "xml":
        return render(request, "release_detail.xml", {"release": release}, content_type=MIME_TYPES[format])
    context = {"pageSection": "releases", "user_language": get_user_language_from_request(request), "release": release}
    return render(request, "release_detail.html", context)


def compare_by_releases(request, dtype, rels_to_compare):
    if dtype not in dict(Domain.DOMAIN_TYPE_CHOICES):
        raise Http404("Wrong domain type")
    releases = list(
        Release.objects.in_bulk(
            [f"gnome-{rel_name}" for rel_name in rels_to_compare.split("/")], field_name="name"
        ).values()
    )
    if not releases:
        raise Http404("No matching releases")
    stats = Release.total_by_releases(dtype, releases)
    context = {
        "releases": releases,
        "stats": stats,
    }
    return render(request, "release_compare.html", context)


def can_refresh_branch(user: Person) -> bool:
    if user.is_authenticated and user.has_perm("stats.change_module"):
        return True

    # Otherwise, if the user is coordinator or any team
    person = Person.get_by_user(user)
    if isinstance(person, Person) and person.is_coordinator(team="any"):
        return True
    return False
