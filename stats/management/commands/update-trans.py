import itertools
import os
import re
import shutil
import subprocess

from django.conf import settings
from django.core.management import call_command
from django.core.management.base import BaseCommand
from django.db.models import F

from languages.models import Language
from stats.models import CategoryName, Domain, Module, Release
from teams.models import Team


class Command(BaseCommand):
    help = "Update translations of djamnedlies ('en' is a special case, and generate damned-lies.pot)"
    database_content_filename = "database-content.py"

    def add_arguments(self, parser) -> None:
        parser.add_argument("lang_code", help="language code ('it', 'pt_BR', etc.)")

    def handle(self, **options) -> str:
        self.lang_code = options["lang_code"]
        self.po_dir = settings.BASE_DIR / "po"
        self.locale_dir = settings.BASE_DIR / "locale" / self.lang_code / "LC_MESSAGES"
        if self.lang_code == "en":
            self.po_file = self.po_dir / "damned-lies.pot"
        else:
            self.po_file = self.po_dir / f"{self.lang_code}.po"
            self._update_django_po_with_code_strings()

            # backup code-only strings in the code.po file. Remove non code related msgid (mainly coming from database)
            django_po_file = self.locale_dir / "django.po"
            if django_po_file.is_file():
                code_po_file = self.locale_dir / "code.po"
                shutil.copy(django_po_file, code_po_file)
                subprocess.run(
                    ["/usr/bin/msgattrib", "--no-obsolete", "-o", str(code_po_file), str(code_po_file)], check=True
                )

        self._update_django_po_with_database_strings()

        # Copy locale/ll/LC_MESSAGES/django.po to po/ll.po
        shutil.copy(str(self.locale_dir / "django.po"), str(self.po_file))
        return f"po file for language '{self.lang_code}' updated."

    def _update_django_po_with_code_strings(self) -> None:
        if self.po_file.exists():
            if not self.locale_dir.is_dir():
                self.locale_dir.mkdir(parents=True, exist_ok=True)
            shutil.copy(self.po_file, self.locale_dir / "django.po")
        self._call_django_makemessages_command()

    def _call_django_makemessages_command(self) -> None:
        """
        Run makemessages -l ll
        """
        os.chdir(settings.BASE_DIR)
        call_command("makemessages", locale=[self.lang_code])

    def _update_django_po_with_database_strings(self) -> None:
        self.dbfile = settings.BASE_DIR / self.database_content_filename
        with self.dbfile.open("w", encoding="utf-8") as fh:
            for value in itertools.chain(
                Team.objects.values_list("description", flat=True),
                Language.objects.exclude(name__exact=F("locale")).values_list("name", flat=True),
                Domain.objects.distinct().values_list("description", flat=True),
                Module.objects.exclude(name__exact=F("description")).values_list("description", flat=True),
                Module.objects.exclude(comment="").values_list("comment", flat=True),
                Release.objects.values_list("description", flat=True),
                CategoryName.objects.values_list("name", flat=True),
            ):
                if value:
                    value = re.sub(r"\r\n|\r|\n", "\n", value)
                    fh.write(f'_(u"""{value}""")\n')
        self._call_django_makemessages_command()
        self.dbfile.unlink()
