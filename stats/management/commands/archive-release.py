import csv
from io import StringIO

from django.core.management.base import BaseCommand, CommandError

from stats.models import Release, Statistics


class Command(BaseCommand):
    help = "Write statistics of a release in a CSV structure (stdout)"

    def add_arguments(self, parser) -> None:
        parser.add_argument("release")

    def handle(self, **options):
        try:
            release = Release.objects.get(name=options["release"])
        except Release.DoesNotExist as exc:
            raise CommandError("The release name '{}' is not known.\n".format(options["release"])) from exc

        out = StringIO()
        headers = ["Module", "Branch", "Domain", "Language", "Translated", "Fuzzy", "Untranslated"]
        writer = csv.DictWriter(out, headers)
        header = dict(zip(headers, headers))
        writer.writerow(header)

        stats = Statistics.objects.filter(branch__category__release=release, language__isnull=False).select_related(
            "branch__module", "domain", "language"
        )
        for stat in stats:
            row = {
                "Module": stat.branch.module.name,
                "Branch": stat.branch.name,
                "Domain": stat.domain.name,
                "Language": stat.language.locale,
                "Translated": stat.translated(),
                "Fuzzy": stat.fuzzy(),
                "Untranslated": stat.untranslated(),
            }
            writer.writerow(row)
        return out.getvalue()
