import shutil

from django.conf import settings
from django.core.management import call_command
from django.core.management.base import BaseCommand

from languages.models import Language


class Command(BaseCommand):
    help = "Compile translations of djamnedlies"

    def add_arguments(self, parser) -> None:
        # Copy --locale arg from compilemessages
        parser.add_argument(
            "--locale",
            "-l",
            action="append",
            default=[],
            help="Locale(s) to process (e.g. de_AT). Default is to process all. " "Can be used multiple times.",
        )

    def handle(self, **options) -> None:
        # Copy all po/ll.po files in locale/ll/LC_MESSAGES/django.po
        podir = settings.BASE_DIR / "po"
        for pofile in podir.iterdir():
            if pofile.suffix != ".po":
                continue
            lang_code = pofile.stem
            if options["locale"] and lang_code not in options["locale"]:
                continue
            localedir = settings.BASE_DIR / "locale" / Language.django_locale(lang_code) / "LC_MESSAGES"
            if not localedir.is_dir():
                localedir.mkdir(parents=True, exist_ok=True)
            if options["verbosity"] > 1:
                self.stdout.write(f"Copying {pofile} to {localedir}")
            shutil.copy(pofile, localedir / "django.po")

        # Run compilemessages -l ll
        call_command("compilemessages", locale=[Language.django_locale(locale) for locale in options["locale"]])
