#!/usr/bin/env python3
from enum import Enum, auto
from pathlib import Path
from typing import TYPE_CHECKING

from translate.misc.multistring import multistring
from translate.storage import factory
from translate.storage.pocommon import pounit

from common.utils import run_shell_command
from damnedlies import logger

if TYPE_CHECKING:
    from translate.storage.pypo import pofile

# Output differences between two POT files


class PoDiffState(Enum):
    ADDITION = (1, "+")
    DELETION = (2, "-")

    def __str__(self) -> str:
        return self.value[1]


class PoEntry:
    """An entry in a PO/POT file."""

    msgid: str = None
    msgid_plural: str = None
    msgctxt: str = None
    # "+" for added, "-" for removed
    diff_state: PoDiffState = None

    def __init__(self, unit: pounit, diff_state: PoDiffState) -> None:
        self.diff_state = diff_state
        if isinstance(unit.source, multistring):
            self.msgid = unit.source.strings[0]
            self.msgid_plural = unit.source.strings[1]
        else:
            self.msgid = unit.source
        self.msgctxt = unit.getcontext()
        self.locations = [FileLocation(location) for location in unit.getlocations()]

    @property
    def display_str(self):
        """
        The string representation to display the entry.

        It also supports msgctxt (GNU gettext 0.15) and plural forms,
        the returning format is:

            [msgctxt::]msgid[/msgid_plural]
        """
        onemsg = ""
        if self.msgctxt:
            onemsg += self.msgctxt + "::"
        onemsg += self.msgid
        if self.msgid_plural:
            onemsg += "/" + self.msgid_plural

        return onemsg

    def markdown(self, location_link_prefix: str):
        """
        The markdown representation of the entry, including the location.

        The returning format is:

            `display_str` (location.markdown{, location.markdown…})
        """
        markdown = "`" + self.display_str + "`"

        if self.locations:
            locations = [location.markdown(location_link_prefix) for location in self.locations]
            markdown += " (" + ", ".join(locations) + ")"

        return markdown


class FileLocation:
    """A location in a file."""

    path: str = None
    line: str = None

    def __init__(self, location: str) -> None:
        split = location.rsplit(":", 1)
        self.path = split[0]
        self.line = split[1]

    @property
    def display_str(self):
        """
        The string representation to display the location.

        The returning format is:

            path:line
        """
        return self.path + ":" + self.line

    def link(self, prefix: str):
        """
        The link to the location, with the given prefix.

        The returning format is:

            prefix/path#Lline
        """
        link = prefix
        if link[-1] != "/":
            link += "/"
        return link + self.path + "#L" + self.line

    def markdown(self, link_prefix: str):
        """
        The markdown representation of the location.

        The returning format is:

            [display_str](link)
        """
        return "[" + self.display_str + "](" + self.link(link_prefix) + ")"


class PoFileDiffStatus(Enum):
    NOT_CHANGED = auto()
    CHANGED_ONLY_FORMATTING = auto()
    CHANGED_WITH_ADDITIONS = auto()
    CHANGED_NO_ADDITIONS = auto()


class POTFile:
    @property
    def path(self) -> Path:
        return self.__path_to_pot_file

    def msg_ids(self) -> list[str]:
        return [*self.pofile.getids()]

    def __init__(self, path_to_pot_file: Path) -> None:
        self.__path_to_pot_file = path_to_pot_file
        self.pofile: "pofile" = factory.getobject(str(self.__path_to_pot_file))

    def diff_with(self, other_pot_file: "POTFile") -> tuple[PoFileDiffStatus, list[PoEntry] | None]:
        """ """
        logger.debug("Checking differences between %s and %s", self.path, other_pot_file.path)

        _, number_of_lines, _ = run_shell_command(f'diff "{self.path}" "{other_pot_file.path}"|wc -l')

        MINIMAL_NUMBER_OF_LINES_IN_DIFF: int = 4
        if int(number_of_lines) <= MINIMAL_NUMBER_OF_LINES_IN_DIFF:
            return PoFileDiffStatus.NOT_CHANGED, None

        result_all, result_add_only = self.__diff(other_pot_file)

        if not len(result_all) and not len(result_add_only):
            return PoFileDiffStatus.CHANGED_ONLY_FORMATTING, None

        if len(result_add_only) > 0:
            return PoFileDiffStatus.CHANGED_WITH_ADDITIONS, result_add_only

        return PoFileDiffStatus.CHANGED_NO_ADDITIONS, result_all

    def __diff(self, other_pot_file: "POTFile") -> tuple[list[PoEntry], list[PoEntry]]:
        """
        Returns a list of differing entries between two PO/POT files.
        """
        msgid_from_po_file_a = self.msg_ids()
        msgid_from_po_file_a.sort()

        msgid_from_po_file_b = other_pot_file.msg_ids()
        msgid_from_po_file_b.sort()

        index_msgid_pofile_a, index_msgid_pofile_b = 0, 0
        result_add_only = []
        result_all = []

        logger.info(
            "%d messages found in PO file A, %d messages in PO file B.",
            len(msgid_from_po_file_a),
            len(msgid_from_po_file_b),
        )

        while index_msgid_pofile_a < len(msgid_from_po_file_a) and index_msgid_pofile_b < len(msgid_from_po_file_b):
            # Both messages are identical, so we safely skip them, it will not be included in the diff.
            if msgid_from_po_file_a[index_msgid_pofile_a] == msgid_from_po_file_b[index_msgid_pofile_b]:
                logger.debug(
                    "Identical messages: %s / %s",
                    msgid_from_po_file_a[index_msgid_pofile_a],
                    msgid_from_po_file_b[index_msgid_pofile_b],
                )
                index_msgid_pofile_a += 1
                index_msgid_pofile_b += 1

            # Compare strings based on the lexicographic order.
            # https://en.wikipedia.org/wiki/Lexicographic_order
            elif msgid_from_po_file_a[index_msgid_pofile_a] < msgid_from_po_file_b[index_msgid_pofile_b]:
                # Message has been deleted from A. It exists in B, but no longer in A.
                unit = self.pofile.findid(msgid_from_po_file_a[index_msgid_pofile_a])
                entry = PoEntry(unit, PoDiffState.DELETION)
                result_all.append(entry)
                index_msgid_pofile_a += 1
                logger.debug("Deletion detected: %s", entry)

            elif msgid_from_po_file_a[index_msgid_pofile_a] > msgid_from_po_file_b[index_msgid_pofile_b]:
                # Message has been added to B
                unit = other_pot_file.pofile.findid(msgid_from_po_file_b[index_msgid_pofile_b])
                entry = PoEntry(unit, PoDiffState.ADDITION)
                result_all.append(entry)
                result_add_only.append(entry)
                index_msgid_pofile_b += 1
                logger.debug("Addition detected: %s", entry)

        return result_all, result_add_only
