from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("stats", "0008_domain_extra_its_dirs"),
    ]

    operations = [
        migrations.AlterField(
            model_name="branch",
            name="vcs_subpath",
            field=models.CharField(blank=True, default="", max_length=50),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name="domain",
            name="description",
            field=models.TextField(blank=True, default=""),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name="domain",
            name="linguas_location",
            field=models.CharField(
                blank=True,
                default="",
                help_text="Use 'no' for no LINGUAS check, or path/to/file#variable for a non-standard location.\nLeave blank for standard location (ALL_LINGUAS in LINGUAS/configure.ac/.in for UI and DOC_LINGUAS in Makefile.am for DOC)",
                max_length=50,
            ),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name="domain",
            name="pot_method",
            field=models.CharField(
                blank=True,
                default="",
                help_text="Leave blank for autodetected method (intltool/gettext/gnome-doc-utils), or '&lt;gettext&gt;', '&lt;intltool&gt;', or real pot command to force the tool chain",
                max_length=100,
            ),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name="domain",
            name="red_filter",
            field=models.TextField(
                blank=True,
                default="",
                help_text="pogrep filter to strip po file from unprioritized strings (format: location|string, “-” for no filter)",
            ),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name="module",
            name="bugs_base",
            field=models.CharField(blank=True, default="", max_length=250),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name="module",
            name="bugs_component",
            field=models.CharField(blank=True, default="", max_length=50),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name="module",
            name="bugs_product",
            field=models.CharField(blank=True, default="", max_length=50),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name="module",
            name="comment",
            field=models.TextField(blank=True, default=""),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name="module",
            name="description",
            field=models.TextField(blank=True, default=""),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name="module",
            name="ext_platform",
            field=models.URLField(blank=True, default="", help_text="URL to external translation platform, if any"),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name="module",
            name="homepage",
            field=models.URLField(
                blank=True, default="", help_text="Automatically updated if the module contains a doap file."
            ),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name="pofile",
            name="path",
            field=models.CharField(blank=True, default="", max_length=255),
            preserve_default=False,
        ),
    ]
