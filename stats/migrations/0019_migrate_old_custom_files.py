from django.db import migrations


def migrate_figures(apps, schema_editor):
    PoFile = apps.get_model("stats", "PoFile")
    Branch = apps.get_model("stats", "Branch")
    for file_ in PoFile.objects.exclude(figures_old__isnull=False):
        file_.figures = file_.figures_old
        file_.save()
    for branch in Branch.objects.exclude(file_hashes_old=""):
        branch.file_hashes = branch.file_hashes_old
        branch.save()


class Migration(migrations.Migration):
    dependencies = [
        ("stats", "0018_new_jsonfields"),
    ]

    operations = [
        migrations.RunPython(migrate_figures),
    ]
