from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("stats", "0009_remove_null_on_text_fields"),
    ]

    operations = [
        migrations.RenameField(
            model_name="domain",
            old_name="pot_method",
            new_name="pot_method_old",
        ),
        migrations.AddField(
            model_name="domain",
            name="pot_method",
            field=models.CharField(
                choices=[
                    ("auto", "auto detected"),
                    ("gettext", "gettext"),
                    ("intltool", "intltool"),
                    ("shell", "shell command"),
                    ("url", "URL"),
                    ("in_repo", ".pot in repository"),
                    ("subtitles", "subtitles"),
                ],
                default="auto",
                max_length=20,
            ),
        ),
        migrations.AddField(
            model_name="domain",
            name="pot_params",
            field=models.CharField(
                blank=True,
                help_text="pot_method='url': URL, pot_method='shell': shell command, pot_method='gettext': optional params",
                max_length=100,
            ),
        ),
    ]
