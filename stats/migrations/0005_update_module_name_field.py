import re

import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("stats", "0004_remove_old_cat_name"),
    ]

    operations = [
        migrations.AlterField(
            model_name="module",
            name="name",
            field=models.CharField(
                unique=True,
                max_length=50,
                validators=[
                    django.core.validators.RegexValidator(
                        re.compile("^[-\\+a-zA-Z0-9_]+\\Z"),
                        "Enter a valid 'slug' consisting of letters, numbers, underscores, hyphens or plus signs.",
                        "invalid",
                    )
                ],
            ),
        ),
    ]
