import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("stats", "0003_migrate_category_names"),
    ]

    operations = [
        migrations.RemoveField(
            model_name="category",
            name="name",
        ),
        migrations.RenameField(
            model_name="category",
            old_name="name_id",
            new_name="name",
        ),
        migrations.AlterField(
            model_name="category",
            name="name",
            field=models.ForeignKey(to="stats.CategoryName", on_delete=django.db.models.deletion.PROTECT),
        ),
    ]
