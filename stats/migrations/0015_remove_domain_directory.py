from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ("stats", "0014_migrate_dir_to_layout"),
    ]

    operations = [
        migrations.RemoveField(
            model_name="domain",
            name="directory",
        ),
    ]
