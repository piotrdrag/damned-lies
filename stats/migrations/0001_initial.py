import django.db.models.deletion
from django.db import migrations, models

import common.fields


class Migration(migrations.Migration):
    dependencies = [
        ("people", "0001_initial"),
        ("languages", "0001_initial"),
    ]

    operations = [
        migrations.CreateModel(
            name="Branch",
            fields=[
                ("id", models.AutoField(verbose_name="ID", serialize=False, auto_created=True, primary_key=True)),
                ("name", models.CharField(max_length=50)),
                ("vcs_subpath", models.CharField(max_length=50, null=True, blank=True)),
                ("weight", models.IntegerField(default=0, help_text="Smaller weight is displayed first")),
                ("file_hashes", common.fields.DictionaryField(default="", editable=False, blank=True)),
            ],
            options={
                "ordering": ("name",),
                "db_table": "branch",
                "verbose_name_plural": "branches",
            },
        ),
        migrations.CreateModel(
            name="Category",
            fields=[
                ("id", models.AutoField(verbose_name="ID", serialize=False, auto_created=True, primary_key=True)),
                (
                    "name",
                    models.CharField(
                        default="default",
                        max_length=30,
                        choices=[
                            ("default", "Default"),
                            ("admin-tools", "Administration Tools"),
                            ("dev-tools", "Development Tools"),
                            ("desktop", "GNOME Desktop"),
                            ("dev-platform", "GNOME Developer Platform"),
                            ("proposed", "New Module Proposals"),
                            ("g3-core", "Core"),
                            ("g3-utils", "Utils"),
                            ("g3-apps", "Apps"),
                            ("g3-a11y", "Accessibility"),
                            ("g3-games", "Games"),
                            ("g3-backends", "Backends"),
                            ("g3-dev-tools", "Development Tools"),
                            ("g3-core-libs", "Core Libraries"),
                            ("g3-extra-libs", "Extra Libraries"),
                            ("g2-legacy", "Legacy Desktop"),
                            ("stable", "Stable Branches"),
                            ("dev", "Development Branches"),
                        ],
                    ),
                ),
                ("branch", models.ForeignKey(to="stats.Branch", on_delete=models.CASCADE)),
            ],
            options={
                "db_table": "category",
                "verbose_name_plural": "categories",
            },
        ),
        migrations.CreateModel(
            name="Domain",
            fields=[
                ("id", models.AutoField(verbose_name="ID", serialize=False, auto_created=True, primary_key=True)),
                ("name", models.CharField(max_length=50)),
                ("description", models.TextField(null=True, blank=True)),
                (
                    "dtype",
                    models.CharField(
                        default="ui", max_length=5, choices=[("ui", "User Interface"), ("doc", "Documentation")]
                    ),
                ),
                ("directory", models.CharField(max_length=50)),
                (
                    "pot_method",
                    models.CharField(
                        help_text="Leave blank for standard method (intltool for UI and gnome-doc-utils for DOC), or '&lt;gettext&gt;' for the pure xgettext-based extraction",
                        max_length=100,
                        null=True,
                        blank=True,
                    ),
                ),
                (
                    "linguas_location",
                    models.CharField(
                        help_text="Use 'no' for no LINGUAS check, or path/to/file#variable for a non-standard location.\n            Leave blank for standard location (ALL_LINGUAS in LINGUAS/configure.ac/.in for UI and DOC_LINGUAS in Makefile.am for DOC)",
                        max_length=50,
                        null=True,
                        blank=True,
                    ),
                ),
                (
                    "red_filter",
                    models.TextField(
                        help_text='pogrep filter to strip po file from unprioritized strings (format: location|string, "-" for no filter)',
                        null=True,
                        blank=True,
                    ),
                ),
            ],
            options={
                "ordering": ("-dtype", "name"),
                "db_table": "domain",
            },
        ),
        migrations.CreateModel(
            name="Information",
            fields=[
                ("id", models.AutoField(verbose_name="ID", serialize=False, auto_created=True, primary_key=True)),
                (
                    "type",
                    models.CharField(
                        max_length=10,
                        choices=[
                            ("info", "Information"),
                            ("warn", "Warning"),
                            ("error", "Error"),
                            ("warn-ext", "Warning (external)"),
                            ("error-ext", "Error (external)"),
                        ],
                    ),
                ),
                ("description", models.TextField()),
            ],
            options={
                "db_table": "information",
            },
        ),
        migrations.CreateModel(
            name="InformationArchived",
            fields=[
                ("id", models.AutoField(verbose_name="ID", serialize=False, auto_created=True, primary_key=True)),
                (
                    "type",
                    models.CharField(
                        max_length=10,
                        choices=[
                            ("info", "Information"),
                            ("warn", "Warning"),
                            ("error", "Error"),
                            ("warn-ext", "Warning (external)"),
                            ("error-ext", "Error (external)"),
                        ],
                    ),
                ),
                ("description", models.TextField()),
            ],
            options={
                "db_table": "information_archived",
            },
        ),
        migrations.CreateModel(
            name="Module",
            fields=[
                ("id", models.AutoField(verbose_name="ID", serialize=False, auto_created=True, primary_key=True)),
                ("name", models.SlugField(unique=True)),
                (
                    "homepage",
                    models.URLField(
                        help_text="Automatically updated if the module contains a doap file.", null=True, blank=True
                    ),
                ),
                ("description", models.TextField(null=True, blank=True)),
                ("comment", models.TextField(null=True, blank=True)),
                ("bugs_base", models.CharField(max_length=50, null=True, blank=True)),
                ("bugs_product", models.CharField(max_length=50, null=True, blank=True)),
                ("bugs_component", models.CharField(max_length=50, null=True, blank=True)),
                (
                    "vcs_type",
                    models.CharField(
                        max_length=5, choices=[("svn", "Subversion"), ("git", "Git"), ("hg", "Mercurial")]
                    ),
                ),
                ("vcs_root", models.CharField(max_length=200)),
                ("vcs_web", models.URLField()),
                (
                    "ext_platform",
                    models.URLField(help_text="URL to external translation platform, if any", null=True, blank=True),
                ),
                ("archived", models.BooleanField(default=False)),
                (
                    "maintainers",
                    models.ManyToManyField(
                        help_text="Automatically updated if the module contains a doap file.",
                        related_name="maintains_modules",
                        db_table="module_maintainer",
                        to="people.Person",
                        blank=True,
                    ),
                ),
            ],
            options={
                "ordering": ("name",),
                "db_table": "module",
            },
        ),
        migrations.CreateModel(
            name="PoFile",
            fields=[
                ("id", models.AutoField(verbose_name="ID", serialize=False, auto_created=True, primary_key=True)),
                ("path", models.CharField(max_length=255, null=True, blank=True)),
                ("updated", models.DateTimeField(auto_now_add=True)),
                ("translated", models.IntegerField(default=0)),
                ("fuzzy", models.IntegerField(default=0)),
                ("untranslated", models.IntegerField(default=0)),
                ("figures", common.fields.JSONField(null=True, blank=True)),
                ("translated_words", models.IntegerField(default=0)),
                ("fuzzy_words", models.IntegerField(default=0)),
                ("untranslated_words", models.IntegerField(default=0)),
            ],
            options={
                "db_table": "pofile",
            },
        ),
        migrations.CreateModel(
            name="Release",
            fields=[
                ("id", models.AutoField(verbose_name="ID", serialize=False, auto_created=True, primary_key=True)),
                ("name", models.SlugField(max_length=20)),
                ("description", models.CharField(max_length=50)),
                ("string_frozen", models.BooleanField(default=False)),
                (
                    "status",
                    models.CharField(
                        max_length=12,
                        choices=[("official", "Official"), ("unofficial", "Unofficial"), ("xternal", "External")],
                    ),
                ),
                ("weight", models.IntegerField(default=0)),
                (
                    "branches",
                    models.ManyToManyField(related_name="releases", through="stats.Category", to="stats.Branch"),
                ),
            ],
            options={
                "ordering": ("status", "-name"),
                "db_table": "release",
            },
        ),
        migrations.CreateModel(
            name="Statistics",
            fields=[
                ("id", models.AutoField(verbose_name="ID", serialize=False, auto_created=True, primary_key=True)),
                ("branch", models.ForeignKey(to="stats.Branch", on_delete=models.CASCADE)),
                ("domain", models.ForeignKey(to="stats.Domain", on_delete=models.CASCADE)),
                (
                    "full_po",
                    models.OneToOneField(
                        related_name="stat_f",
                        null=True,
                        on_delete=django.db.models.deletion.SET_NULL,
                        to="stats.PoFile",
                    ),
                ),
                ("language", models.ForeignKey(to="languages.Language", null=True, on_delete=models.CASCADE)),
                (
                    "part_po",
                    models.OneToOneField(
                        related_name="stat_p",
                        null=True,
                        on_delete=django.db.models.deletion.SET_NULL,
                        to="stats.PoFile",
                    ),
                ),
            ],
            options={
                "db_table": "statistics",
                "verbose_name": "statistics",
                "verbose_name_plural": "statistics",
            },
        ),
        migrations.CreateModel(
            name="StatisticsArchived",
            fields=[
                ("id", models.AutoField(verbose_name="ID", serialize=False, auto_created=True, primary_key=True)),
                ("module", models.TextField()),
                ("type", models.CharField(max_length=3, choices=[("ui", "User Interface"), ("doc", "Documentation")])),
                ("domain", models.TextField()),
                ("branch", models.TextField()),
                ("language", models.CharField(max_length=15)),
                ("date", models.DateTimeField()),
                ("translated", models.IntegerField(default=0)),
                ("fuzzy", models.IntegerField(default=0)),
                ("untranslated", models.IntegerField(default=0)),
            ],
            options={
                "db_table": "statistics_archived",
            },
        ),
        migrations.AddField(
            model_name="informationarchived",
            name="statistics",
            field=models.ForeignKey(to="stats.StatisticsArchived", on_delete=models.CASCADE),
        ),
        migrations.AddField(
            model_name="information",
            name="statistics",
            field=models.ForeignKey(to="stats.Statistics", on_delete=models.CASCADE),
        ),
        migrations.AddField(
            model_name="domain",
            name="module",
            field=models.ForeignKey(to="stats.Module", on_delete=models.CASCADE),
        ),
        migrations.AddField(
            model_name="category",
            name="release",
            field=models.ForeignKey(to="stats.Release", on_delete=models.CASCADE),
        ),
        migrations.AddField(
            model_name="branch",
            name="module",
            field=models.ForeignKey(to="stats.Module", on_delete=models.CASCADE),
        ),
        migrations.AlterUniqueTogether(
            name="statistics",
            unique_together=set([("branch", "domain", "language")]),
        ),
        migrations.AlterUniqueTogether(
            name="category",
            unique_together=set([("release", "branch")]),
        ),
        migrations.AlterUniqueTogether(
            name="branch",
            unique_together=set([("name", "module")]),
        ),
    ]
