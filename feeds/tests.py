from django.test import TestCase
from django.urls import reverse


class FeedTests(TestCase):
    fixtures = ("test_data.json",)

    def test_language_feed_downloaded_filename_is_correct(self):
        response = self.client.get(reverse("lang_feed", kwargs={"locale": "fr"}))
        self.assertContains(response, "Workflow actions for the French language")
        self.assertEqual("application/rss+xml; charset=utf-8", response.headers.get("Content-Type"))
        self.assertEqual(
            "attachment; filename=gnome_damned_lies_rss_feed_for_french.rss",
            response.headers.get("Content-Disposition"),
        )
