from datetime import datetime
from typing import TYPE_CHECKING

from django.conf import settings
from django.contrib.syndication.views import Feed, FeedDoesNotExist
from django.http import HttpRequest
from django.urls import reverse
from django.utils.text import slugify
from django.utils.translation import gettext_lazy as _

from languages.models import Language
from vertimus.models import Action, ActionArchived

if TYPE_CHECKING:
    from people.models import Person


class LatestActionsByLanguage(Feed):
    title_template = "feeds/actions_title.html"
    description_template = "feeds/actions_description.html"

    def __call__(self: "LatestActionsByLanguage", request: HttpRequest, *args, **kwargs):  # noqa ANN002, ANN003
        """
        Rewrite __call__ method to set the downloaded filename.
        """
        response = super().__call__(request, *args, **kwargs)
        # Object should exist or exception is already raised by super().__call__()
        language = self.get_object(request, **kwargs)
        response["Content-Disposition"] = (
            f"attachment; filename=gnome_damned_lies_rss_feed_for_{slugify(language.name)}.rss"
        )
        return response

    def get_object(self: "LatestActionsByLanguage", request: "HttpRequest", locale: str, **kwargs) -> Language:  # noqa: PLR6301, ANN003, ARG002
        return Language.objects.get(locale=locale)

    def title(self: "LatestActionsByLanguage", language: "Language") -> str:  # noqa: PLR6301
        return _("%(site)s — Workflow actions for the %(lang)s language") % {
            "site": settings.SITE_DOMAIN,
            "lang": language.name,
        }

    def link(self: "LatestActionsByLanguage", action: "Language") -> str:  # noqa: PLR6301
        """
        :raises FeedDoesNotExist: when the requested feed does not exist.
        """
        if not action:
            raise FeedDoesNotExist
        return action.get_team_url()

    def description(self: "LatestActionsByLanguage", action: "Language") -> str:  # noqa: PLR6301
        return _("Latest actions of the GNOME Translation Project for the %s language") % action.name

    def items(self: "LatestActionsByLanguage", action: "Language") -> list[Action]:  # noqa: PLR6301
        actions = (
            Action.objects.filter(state_db__language=action.id)
            .select_related("state_db")
            .union(
                ActionArchived.objects.filter(state_db__language=action.id)
                .defer("sequence")
                .select_related("state_db")
            )
        )
        return actions.order_by("-created")[:20]

    def item_link(self: "LatestActionsByLanguage", item: "Action") -> str:  # noqa: PLR6301
        link = reverse(
            "vertimus_by_names",
            args=(
                item.state_db.branch.module.name,
                item.state_db.branch.name,
                item.state_db.domain.name,
                item.state_db.language.locale,
            ),
        )
        return "%s#%d" % (link, item.id)

    def item_pubdate(self: "LatestActionsByLanguage", item: "Action") -> "datetime":  # noqa: PLR6301
        return item.created

    def item_author_name(self: "LatestActionsByLanguage", item: "Action") -> "Person":  # noqa: PLR6301
        return item.person
