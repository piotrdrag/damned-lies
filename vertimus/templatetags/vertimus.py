from typing import TYPE_CHECKING

from django import template
from django.templatetags.static import static
from django.utils.html import format_html

if TYPE_CHECKING:
    pass

register = template.Library()


@register.filter
def as_tr(field) -> str:
    help_html = ""
    if field.help_text:
        help_html = format_html('<br><span class="help">{}</span>', field.help_text)
    help_link = ""
    # This is a custom attribute possibly set in forms.py
    if hasattr(field.field, "help_link"):
        help_link = format_html(
            '<span class="help_link">'
            '<a href="{}" data-bs-target="#modal-container" role="button" data-bs-toggle="modal">'
            '<i class="fa fa-question-circle"></i>'
            "</a></span>",
            field.field.help_link,
            static("img/help.png"),
        )
    errors_html = ""
    if field.errors:
        errors_html = "".join(["%s" % err for err in field.errors])

    return format_html(
        '<tr class="tr_{}"><th>{}</th><td>'
        + ('<div class="alert alert-danger">{}</div>' if errors_html else "")
        + "{}{}{}</td></tr>",
        field.name,
        field.label_tag(),
        errors_html,
        field.as_widget(),
        help_link,
        help_html,
    )


@register.filter
def as_tr_cb(field) -> str:
    label = field.label
    if field.help_text:
        label = format_html('{} <span class="help">({})</span>', label, field.help_text)
    return format_html(
        '<tr class="tr_{}"><td></td><td>{} <label for="id_{}">{}</label></td></tr>',
        field.name,
        field.as_widget(),
        field.name,
        label,
    )


@register.simple_tag
def get_badge_bg_from_vertimus_state(vertimus_state: "State") -> str:
    from vertimus.models import (
        StateCommitting,
        StateProofread,
        StateProofreading,
        StateToReview,
        StateTranslated,
        StateTranslating,
    )

    vertimus_state_badge_color = {
        StateTranslating.description: "text-bg-primary",
        StateProofreading.description: "text-bg-primary",
        StateCommitting.description: "text-bg-primary",
        StateTranslated.description: "text-bg-warning",
        StateProofread.description: "text-bg-warning",
        StateToReview.description: "text-bg-warning",
    }
    return vertimus_state_badge_color.get(vertimus_state.description, "bg-secondary")
