import abc
import os
import shutil
import sys
from datetime import timedelta
from pathlib import Path
from typing import TYPE_CHECKING, Any, Optional

from django.conf import settings
from django.db import models
from django.db.models import Max, Q
from django.db.models.signals import post_save, pre_delete
from django.dispatch import receiver
from django.urls import reverse
from django.utils import timezone
from django.utils.translation import gettext, gettext_noop, override, to_language
from django.utils.translation import gettext_lazy as _

from common.utils import run_shell_command, send_mail
from damnedlies import logger
from languages.models import Language
from people.models import Person
from stats.models import Branch, Domain, PoFile, Statistics, UnableToCommitError
from stats.signals import pot_has_changed
from stats.utils import is_po_reduced, po_grep
from teams.models import Role

if TYPE_CHECKING:
    from django.db.models import QuerySet


class SendMailFailedError(Exception):
    """Something went wrong while sending message"""


class State(models.Model):
    """State of a module translation"""

    branch = models.ForeignKey(Branch, on_delete=models.CASCADE)
    domain = models.ForeignKey(Domain, on_delete=models.CASCADE)
    language = models.ForeignKey(Language, on_delete=models.CASCADE)
    person = models.ForeignKey(Person, blank=True, null=True, on_delete=models.SET_NULL)

    name = models.SlugField(max_length=20, default="None")
    updated = models.DateTimeField(auto_now=True, editable=False)

    class Meta:
        db_table = "state"
        verbose_name = "state"
        unique_together = ("branch", "domain", "language")

    def __init__(self: "State", *args, **kwargs) -> None:  # noqa: ANN002, ANN003
        super().__init__(*args, **kwargs)
        if self.name == "None" and getattr(self.__class__, "_name", "None") != "None":
            self.name = self.__class__._name
        self.__class__ = {
            "None": StateNone,
            "Translating": StateTranslating,
            "Translated": StateTranslated,
            "Proofreading": StateProofreading,
            "Proofread": StateProofread,
            "ToReview": StateToReview,
            "ToCommit": StateToCommit,
            "Committing": StateCommitting,
            "Committed": StateCommitted,
        }.get(self.name, State)

    def __str__(self: "State") -> str:
        return f"{self.name}: {self.branch.module.name} {self.branch.name} ({self.language.name} - {self.domain.name})"

    def get_absolute_url(self: "State") -> str:
        return reverse("vertimus_by_ids", args=[self.branch_id, self.domain_id, self.language_id])

    @property
    def stats(self: "State") -> Statistics | None:
        try:
            return Statistics.objects.get(branch=self.branch, domain=self.domain, language=self.language)
        except Statistics.DoesNotExist:
            return None

    def able_to_commit(self: "State") -> bool:
        try:
            self.domain.commit_info(self.branch, self.language)
        except UnableToCommitError:
            logger.exception("Unable to commit repository.")
            return False
        return self.get_latest_po_file_action() is not None

    def change_state(self: "State", state_class: type["State"], person: Person | None = None) -> None:
        self.name = state_class._name
        self.person = person
        self.__class__ = state_class
        self.save()

    def _get_available_actions(self: "State", person: Person, action_names: list[str]) -> list["Action"]:
        # For archived modules, the only possible action is to archive the state (by the coordinator only)
        if self.branch.module.archived:
            if self.action_set.count() > 0 and person.is_coordinator(self.language.team):
                return [ActionAA()]
            return []

        action_names.append("WC")
        # Allow the coordinator to cancel current reserved state
        if person.is_coordinator(self.language.team) and self.name.endswith("ing") and "UNDO" not in action_names:
            action_names.append("UNDO")
        if person.is_committer(self.language.team) and "IC" not in action_names:
            action_names.extend(("Separator", "IC", "AA"))
        return [eval("Action" + action_name)() for action_name in action_names]  # FIXME: eval is unsafe

    @abc.abstractmethod
    def get_available_actions(self: "State", person: Person) -> list["Action"]:
        raise NotImplementedError()

    def get_action_sequence_from_level(self: "State", level: int) -> int:
        """Get the sequence corresponding to the requested level.
        The first level is 1."""
        if level < 0:
            raise ValueError("Level must be greater than 0")

        query = (
            ActionArchived.objects.filter(state_db=self)
            .values("sequence")
            .distinct()
            .order_by("-sequence")[level - 1 : level]
        )
        sequence = None
        if len(query) > 0:
            sequence = query[0]["sequence"]
        return sequence

    def involved_persons(self: "State", extra_user: Person | None = None) -> "QuerySet[Person]":
        """
        Return all persons having posted any action on the current state.
        """
        if extra_user is not None:
            return Person.objects.filter(Q(action__state_db=self) | Q(pk=extra_user.pk)).distinct()
        return Person.objects.filter(action__state_db=self).distinct()

    def get_latest_po_file_action(self: "State") -> Optional["Action"]:
        try:
            return Action.objects.filter(file__endswith=".po", state_db=self).latest("id")
        except Action.DoesNotExist:
            return None


class StateNone(State):
    _name = "None"
    description = _("Inactive")

    class Meta:
        proxy = True

    def get_available_actions(self: "StateNone", person: Person) -> list["Action"]:
        action_names = []

        if (self.language.team and person.is_translator(self.language.team)) or person.is_maintainer_of(
            self.branch.module
        ):
            action_names = ["RT", "UT"]

        return self._get_available_actions(person, action_names)


class StateTranslating(State):
    _name = "Translating"
    description = _("Translating")

    class Meta:
        proxy = True

    def get_available_actions(self: "StateTranslating", person: Person) -> list["Action"]:
        action_names = []

        if self.person == person:
            action_names = ["UT", "UNDO"]

        return self._get_available_actions(person, action_names)


class StateTranslated(State):
    _name = "Translated"
    description = _("Translated")

    class Meta:
        proxy = True

    def get_available_actions(self: "StateTranslated", person: Person) -> list["Action"]:
        action_names = []

        if person.is_reviewer(self.language.team):
            action_names.append("RP")
            action_names.append("UP")
            action_names.append("TR")

        if person.is_translator(self.language.team):
            action_names.append("RT")

        if person.is_committer(self.language.team):
            action_names.append("TC")
            if self.able_to_commit():
                action_names.append("CI")

        return self._get_available_actions(person, action_names)


class StateProofreading(State):
    _name = "Proofreading"
    description = _("Proofreading")

    class Meta:
        proxy = True

    def get_available_actions(self: "StateProofreading", person: Person) -> list["Action"]:
        action_names = []

        if person.is_reviewer(self.language.team):
            if self.person == person:
                action_names = ["UP", "TR", "TC", "UNDO"]

        return self._get_available_actions(person, action_names)


class StateProofread(State):
    _name = "Proofread"
    # Translators: This is a status, not a verb
    description = _("Proofread")

    class Meta:
        proxy = True

    def get_available_actions(self: "StateProofread", person: "Person") -> list["Action"]:
        if person.is_reviewer(self.language.team):
            action_names = ["TC", "RP", "TR"]
        else:
            action_names = []
        if person.is_committer(self.language.team):
            action_names.append("RC")
            if self.able_to_commit():
                action_names.insert(1, "CI")

        return self._get_available_actions(person, action_names)


class StateToReview(State):
    _name = "ToReview"
    description = _("To Review")

    class Meta:
        proxy = True

    def get_available_actions(self: "StateToReview", person: Person) -> list["Action"]:
        action_names = []
        if person.is_translator(self.language.team):
            action_names.append("RT")

        return self._get_available_actions(person, action_names)


class StateToCommit(State):
    _name = "ToCommit"
    description = _("To Commit")

    class Meta:
        proxy = True

    def get_available_actions(self: "StateToCommit", person: Person) -> list["Action"]:
        if person.is_committer(self.language.team):
            action_names = ["RC", "TR", "UNDO"]
            if self.able_to_commit():
                action_names.insert(1, "CI")
        else:
            action_names = []

        return self._get_available_actions(person, action_names)


class StateCommitting(State):
    _name = "Committing"
    description = _("Committing")

    class Meta:
        proxy = True

    def get_available_actions(self: "StateCommitting", person: Person) -> list["Action"]:
        action_names = []

        if person.is_committer(self.language.team):
            if self.person == person:
                action_names = ["IC", "TR", "UNDO"]
            if self.able_to_commit():
                action_names.insert(0, "CI")

        return self._get_available_actions(person, action_names)


class StateCommitted(State):
    _name = "Committed"
    description = _("Committed")

    class Meta:
        proxy = True

    def get_available_actions(self: "StateCommitted", person: Person) -> list["Action"]:
        if person.is_committer(self.language.team):
            action_names = ["AA"]
        else:
            action_names = []

        return self._get_available_actions(person, action_names)


#
# Actions
#

ACTION_NAMES = (
    ("WC", _("Write a comment")),
    ("RT", _("Reserve for translation")),
    ("UT", _("Upload the new translation")),
    ("RP", _("Reserve for proofreading")),
    ("UP", _("Upload the proofread translation")),
    # Translators: this means the file is ready to be committed in repository
    ("TC", _("Ready for submission")),
    ("CI", _("Submit to repository")),
    # Translators: this indicates a committer is going to commit the file in the repository
    ("RC", _("Reserve to submit")),
    # Translators: this is used to indicate the file has been committed in the repository
    ("IC", _("Inform of submission")),
    # Translators: regardless of the translation completion, this file need to be reviewed
    ("TR", _("Rework needed")),
    ("AA", _("Archive the actions")),
    ("UNDO", _("Undo the last state change")),
)


def generate_upload_filename(instance: type["ActionAbstract"], filename: str) -> str:
    if isinstance(instance, ActionArchived):
        return f"{settings.UPLOAD_ARCHIVED_DIR}/{Path(filename).name}"

    # Extract the first extension (with the point)
    filepath = Path(filename)
    root, ext = Path(filepath.name), filepath.suffix
    # Check if a second extension is present
    if root.suffix == ".tar":
        ext = ".tar" + ext
    new_filename = (
        f"{instance.state_db.branch.module.name}-"
        f"{instance.state_db.branch.name}-"
        f"{instance.state_db.domain.name}-"
        f"{instance.state_db.language.locale}-"
        f"{instance.state_db.id}"
        f"{ext}"
    )
    return f"{settings.UPLOAD_DIR}/{new_filename}"


class MergedPoFile(PoFile):
    class Meta:
        proxy = True

    @property
    def prefix(self: "MergedPoFile") -> str:
        return settings.MEDIA_ROOT


class ActionAbstract(models.Model):
    """Common model for Action and ActionArchived"""

    state_db = models.ForeignKey(State, on_delete=models.CASCADE)
    person = models.ForeignKey(Person, null=True, on_delete=models.SET_NULL)

    name = models.SlugField(max_length=8)
    created = models.DateTimeField(editable=False)
    comment = models.TextField(blank=True, null=True)
    sent_to_ml = models.BooleanField(default=False)
    file = models.FileField(upload_to=generate_upload_filename, blank=True, null=True)
    merged_file = models.OneToOneField(MergedPoFile, blank=True, null=True, on_delete=models.SET_NULL)

    # A comment or a file is required
    arg_is_required = False
    comment_is_required = False
    file_is_required = False
    file_is_prohibited = False

    target_state = None
    send_mail_to_ml = False

    class Meta:
        abstract = True

    def __str__(self: "ActionAbstract") -> str:
        return f"{self.name} ({self.description}) - {self.id}"

    @property
    def description(self: "ActionAbstract") -> str:
        return dict(ACTION_NAMES).get(self.name, None)

    @property
    def person_name(self: "ActionAbstract") -> str:
        return self.person.name if self.person else gettext("deleted account")

    @property
    def most_uptodate_file(self: "ActionAbstract") -> PoFile:
        return self.merged_file if self.merged_file else self.file

    @property
    def most_uptodate_filepath(self: "ActionAbstract") -> str:
        return self.merged_file.full_path if self.merged_file else self.file.path

    @property
    def can_build(self: "ActionAbstract") -> bool:
        return not isinstance(self, ActionArchived) and self.state_db.domain.can_build_docs(self.state_db.branch)

    @property
    def build_url(self: "ActionAbstract") -> str:
        path = settings.SCRATCHDIR / "HTML" / str(self.pk) / "index.html"
        return "/" + str(path.relative_to(settings.SCRATCHDIR)) if path.exists() else None

    def get_filename(self: "ActionAbstract") -> str | None:
        if self.file:
            return Path(self.file.name).name
        return None

    def has_po_file(self: "ActionAbstract") -> bool:
        return self.file and self.file.name[-3:] == ".po"

    @classmethod
    def get_action_history(
        cls: "ActionAbstract", state: State | None = None, sequence: int | None = None
    ) -> list[tuple[Any, list[dict[str, Any]]]]:
        """
        Return action history as a list of tuples (action, file_history),
        file_history is a list of previous po files, used in vertimus view to
        generate diff links
        sequence argument is only valid on ActionArchived instances
        """
        history = []
        if state or sequence:
            file_history = [{"action_id": 0, "title": gettext("File in repository")}]
            if not sequence:
                query = cls.objects.filter(state_db__id=state.id)
            else:
                # Not necessary to filter on state with a sequence (unique)
                query = cls.objects.filter(sequence=sequence)
            for action in query.order_by("id"):
                history.append((action, list(file_history)))
                if action.file and action.file.path.endswith(".po"):
                    file_history.insert(
                        0,
                        {
                            "action_id": action.id,
                            "title": gettext("Uploaded file by %(name)s on %(date)s")
                            % {"name": action.person_name, "date": action.created},
                        },
                    )
        return history


class Action(ActionAbstract):
    default_message = gettext_noop(
        "The new state of %(module)s — %(branch)s — %(domain)s (%(language)s) is now “%(new_state)s”."
    )

    class Meta:
        db_table = "action"
        verbose_name = "action"

    def __init__(self: "Action", *args, **kwargs) -> None:  # noqa: ANN002, ANN003
        super().__init__(*args, **kwargs)
        if self.name:
            self.__class__ = eval("Action" + self.name)
        elif getattr(self.__class__, "name"):
            self.name = self.__class__.name

    @classmethod
    def new_by_name(cls: "Action", action_name: str, **kwargs) -> type["Action"]:  # noqa: ANN003
        return eval("Action" + action_name)(**kwargs)  # FIXME: eval is unsafe

    def save(self: "Action", *args, **kwargs) -> None:  # noqa: ANN002, ANN003
        if not self.id and not self.created:
            self.created = timezone.now()
        super().save(*args, **kwargs)

    def update_state(self: "Action") -> None:
        if self.target_state is not None:
            # All actions change state except Writing a comment
            self.state_db.change_state(self.target_state, self.person)
        else:
            # Force updated state date to be updated
            self.state_db.save()

        if self.target_state == StateCommitted:
            # Committed is the last state of the workflow, archive actions
            arch_action = self.new_by_name("AA", person=self.person)
            arch_action.apply_on(self.state_db, {})

    def apply_on(self: "Action", state: "State", form_data: dict[str, Any]) -> None:
        if self.name not in [a.name for a in state.get_available_actions(self.person)]:
            raise Exception("Action not allowed")
        if state.pk is None:
            state.save()
        self.state_db = state
        if self.file:
            self.file.save(Path(self.file.name).name, self.file, save=False)
        if form_data.get("comment"):
            self.comment = form_data["comment"]
        self.sent_to_ml = form_data.get("send_to_ml", False)
        self.save()
        self.update_state()  # Do not save action after that, they have been archived

        if form_data.get("send_to_ml"):
            self.send_mail_new_state(state, (state.language.team.mailing_list,))
        elif self.send_mail_to_ml or self.comment:
            # If the action is normally sent to ML (but unchecked) or if the form
            # contains a comment, send message to state participants
            recipients = set(state.involved_persons().exclude(pk=self.person.pk).values_list("email", flat=True))
            if self.comment and not recipients:
                # In any case, the coordinators should be aware of comments
                recipients = [pers.email for pers in state.language.team.get_coordinators() if pers.email]
            self.send_mail_new_state(state, recipients)

    def get_previous_action_with_po(self: "Action") -> Optional["Action"]:
        """
        Return the previous Action with an uploaded file related to the
        same state.
        """
        prev_actions_with_po = Action.objects.filter(file__endswith=".po", state_db=self.state_db)
        if self.id:
            prev_actions_with_po = prev_actions_with_po.filter(id__lt=self.id)
        try:
            return prev_actions_with_po.latest("id")
        except Action.DoesNotExist:
            return None

    def merge_file_with_pot(self: "Action", pot_file: str) -> None:
        """Merge the uploaded translated file with current pot."""
        if not self.file or not Path(pot_file).exists():
            return
        if not self.merged_file:
            merged_path = "%s.merged.po" % self.file.path[:-3]
            self.merged_file = MergedPoFile.objects.create(path=os.path.relpath(merged_path, settings.MEDIA_ROOT))
            self.save()
            return  # post_save will call merge_file_with_pot again
        merged_path = self.merged_file.full_path
        command = ["msgmerge", "--previous", "-o", merged_path, self.file.path, str(pot_file)]
        run_shell_command(command)
        # If uploaded file is reduced, run po_grep *after* merge
        if is_po_reduced(self.file.path):
            temp_path = "%s.temp.po" % self.file.path[:-3]
            shutil.copy(merged_path, temp_path)
            po_grep(temp_path, merged_path, self.state_db.domain.red_filter)
            Path(temp_path).unlink()
        self.merged_file.update_statistics()

    def send_mail_new_state(self: "Action", state: "State", recipient_list: list[str]) -> None:
        """
        Prepare and email recipient_list, informing about the action.
        """
        # Remove None and empty string items from the list
        recipient_list = filter(lambda x: x and x is not None, recipient_list)

        if not recipient_list:
            return

        url = f"""https://{settings.SITE_DOMAIN}{reverse(
            'vertimus_by_names',
            args=(state.branch.module.name, state.branch.name, state.domain.name, state.language.locale)
        )}"""
        subject = state.branch.module.name + " - " + state.branch.name
        # to_language may be unnecessary after https://code.djangoproject.com/ticket/32581 is solved
        with override(to_language(Language.django_locale(state.language.locale))):
            message = gettext("Hello,") + "\n\n" + gettext(self.default_message) + "\n%(url)s\n\n"
            message %= {
                "module": state.branch.module.name,
                "branch": state.branch.name,
                "domain": state.domain.name,
                "language": state.language.get_name(),
                "new_state": state.description,
                "url": url,
            }
            message += self.comment or gettext("Without comment")
            message += "\n\n" + self.person.name
            message += "\n--\n" + gettext("This is an automated message sent from %s.") % settings.SITE_DOMAIN
        try:
            send_mail(subject, message, to=recipient_list, headers={settings.EMAIL_HEADER_NAME: state.description})
        except Exception as exc:
            raise SendMailFailedError("Sending message failed: %r" % exc) from exc


class ActionArchived(ActionAbstract):
    # The first element of each cycle is null at creation time (and defined
    # afterward).
    sequence = models.IntegerField(null=True)

    class Meta:
        db_table = "action_archived"

    @classmethod
    def clean_old_actions(cls: "ActionArchived", days: int) -> None:
        """Delete old archived actions after some (now-days) time"""
        # In each sequence, test date of the latest action, to delete whole sequences instead of individual actions
        for action in (
            ActionArchived.objects.values("sequence")
            .annotate(max_created=Max("created"))
            .filter(max_created__lt=timezone.now() - timedelta(days=days))
        ):
            # Call each action delete() so as file is also deleted
            for act in ActionArchived.objects.filter(sequence=action["sequence"]):
                act.delete()


class ActionWC(Action):
    name = "WC"
    comment_is_required = True
    default_message = gettext_noop(
        "A new comment has been posted on %(module)s — %(branch)s — %(domain)s (%(language)s)."
    )

    class Meta:
        proxy = True


class ActionRT(Action):
    name = "RT"
    target_state = StateTranslating
    file_is_prohibited = True

    class Meta:
        proxy = True


class ActionUT(Action):
    name = "UT"
    target_state = StateTranslated
    file_is_required = True
    send_mail_to_ml = True

    class Meta:
        proxy = True


class ActionRP(Action):
    name = "RP"
    target_state = StateProofreading
    file_is_prohibited = True

    class Meta:
        proxy = True


class ActionUP(Action):
    name = "UP"
    target_state = StateProofread
    file_is_required = True
    send_mail_to_ml = True

    class Meta:
        proxy = True


class ActionTC(Action):
    name = "TC"
    target_state = StateToCommit

    class Meta:
        proxy = True

    def apply_on(self: "ActionTC", state: "State", form_data: dict[str, Any]) -> None:
        super().apply_on(state, form_data)
        # Email all committers of the team
        committers = [c.email for c in state.language.team.get_committers()]
        self.send_mail_new_state(state, committers)


class ActionCI(Action):
    name = "CI"
    target_state = StateCommitted
    file_is_prohibited = True
    send_mail_to_ml = True

    class Meta:
        proxy = True

    def apply_on(self: "ActionCI", state: "State", form_data: dict[str, Any]) -> str:
        self.state_db = state
        action_with_po = self.get_previous_action_with_po()
        author = form_data["author"] if form_data["author"] else None
        try:
            commit_hash = state.branch.commit_po(Path(action_with_po.file.path), state.domain, state.language, author)
        except Exception as exc:
            # Commit failed, state unchanged
            raise Exception(_("The commit failed. The error was: “%s”") % sys.exc_info()[1]) from exc

        msg = gettext("The file has been successfully committed to the repository.")
        if form_data.get("sync_master", False) and not state.branch.is_head:
            # Cherry-pick the commit on the master branch
            main_branch: Branch = state.branch.module.get_head_branch()
            success = main_branch.cherrypick_commit(commit_hash)
            if success:
                msg += gettext(" Additionally, the synchronization with the %(name)s branch succeeded.") % {
                    "name": main_branch.name
                }
            else:
                msg += gettext(" However, the synchronization with the %(name)s branch failed.") % {
                    "name": main_branch.name
                }
        super().apply_on(state, form_data)  # Mail sent in super
        return msg


class ActionRC(Action):
    name = "RC"
    target_state = StateCommitting
    file_is_prohibited = True

    class Meta:
        proxy = True


class ActionIC(Action):
    name = "IC"
    target_state = StateCommitted
    send_mail_to_ml = True

    class Meta:
        proxy = True


class ActionTR(Action):
    name = "TR"
    target_state = StateToReview
    arg_is_required = True
    send_mail_to_ml = True

    class Meta:
        proxy = True


class ActionAA(Action):
    name = "AA"
    target_state = StateNone

    class Meta:
        proxy = True

    def apply_on(self: "ActionAA", state: "State", form_data: dict[str, Any]) -> None:
        super().apply_on(state, form_data)
        all_actions = Action.objects.filter(state_db=state).order_by("id").all()

        sequence = None
        for action in all_actions:
            file_to_archive = None
            if action.file:
                try:
                    file_to_archive = action.file.file  # get a file object, not a filefield
                except OSError:
                    pass
            action_archived = ActionArchived(
                state_db=action.state_db,
                person=action.person,
                name=action.name,
                created=action.created,
                comment=action.comment,
                file=file_to_archive,
            )
            if file_to_archive:
                action_archived.file.save(Path(action.file.name).name, file_to_archive, save=False)

            if sequence is None:
                # The ID is available after the save()
                action_archived.save()
                sequence = action_archived.id

            action_archived.sequence = sequence
            action_archived.save()

            action.delete()  # The file is also automatically deleted, if it is not referenced elsewhere


class ActionUNDO(Action):
    name = "UNDO"

    class Meta:
        proxy = True

    def update_state(self: "ActionUNDO") -> None:
        # Look for the action to revert, excluding WC because this action is a noop on State
        actions = Action.objects.filter(state_db__id=self.state_db.id).exclude(name="WC").order_by("-id")
        skip_next = False
        for action in actions:
            if skip_next:
                skip_next = False
                continue
            if action.name == "UNDO":
                # Skip Undo and the associated action
                skip_next = True
                continue
            # Found action to revert
            self.state_db.change_state(action.target_state, action.person)
            break
        else:
            # No revertable action found, reset to the None state
            self.state_db.change_state(StateNone)


class ActionSeparator:
    """Fake action to add a separator in action menu"""

    name = None
    description = "--------"


@receiver(pot_has_changed)
def update_uploaded_files(sender, **kwargs) -> None:  # noqa: ANN003, ANN001, ARG001
    """
    Updates all the PO files that have ongoing actions using the latest POT file that is available.
    """
    actions = Action.objects.filter(
        state_db__branch=kwargs["branch"], state_db__domain=kwargs["domain"], file__endswith=".po"
    )
    for action in actions:
        action.merge_file_with_pot(kwargs["potfile"])


@receiver(post_save)
def merge_uploaded_file(sender, instance, **kwargs) -> None:  # noqa: ANN003, ANN001, ARG001
    """
    Callback for Action that automatically merge uploaded file with the latest pot file.
    """
    if not isinstance(instance, Action):
        return
    if instance.file and instance.file.path.endswith(".po"):
        try:
            stat = Statistics.objects.get(
                branch=instance.state_db.branch, domain=instance.state_db.domain, language=None
            )
        except Statistics.DoesNotExist:
            return
        potfile = stat.po_path()
        instance.merge_file_with_pot(potfile)


@receiver(pre_delete)
def delete_action_files(sender, instance, **kwargs) -> None:  # noqa: ANN003, ANN001, ARG001
    """
    Callback for Action that deletes:
    - the uploaded file
    - the merged file
    - the html dir where docs are built
    """
    if not isinstance(instance, ActionAbstract) or not getattr(instance, "file"):
        return
    if instance.file.path.endswith(".po"):
        if instance.merged_file:
            if os.access(instance.merged_file.full_path, os.W_OK):
                Path(instance.merged_file.full_path).unlink()
    if os.access(instance.file.path, os.W_OK):
        Path(instance.file.path).unlink()
    html_dir = settings.SCRATCHDIR / "HTML" / str(instance.pk)
    if html_dir.exists():
        shutil.rmtree(str(html_dir))


@receiver(pre_delete, sender=Statistics)
def clean_dangling_states(sender: object, instance: Statistics, **kwargs) -> None:  # noqa: ANN003, ARG001
    State.objects.filter(branch=instance.branch, domain=instance.domain, language=instance.language).delete()


@receiver(post_save)
def reactivate_role(instance, **kwargs) -> None:  # noqa: ANN003, ARG001, ANN001
    # Reactivating the role if needed
    if not isinstance(instance, Action):
        return
    try:
        role = instance.person.role_set.get(team=instance.state_db.language.team, is_active=False)
        role.is_active = True
        role.save()
    except Role.DoesNotExist:
        pass
