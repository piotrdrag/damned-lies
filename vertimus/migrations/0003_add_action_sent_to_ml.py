from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("vertimus", "0002_state_person_blank"),
    ]

    operations = [
        migrations.AddField(
            model_name="action",
            name="sent_to_ml",
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name="actionarchived",
            name="sent_to_ml",
            field=models.BooleanField(default=False),
        ),
    ]
