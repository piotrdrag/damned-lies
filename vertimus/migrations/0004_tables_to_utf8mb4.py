from django.db import migrations


def to_utf8mb4(apps, schema_editor):
    if not schema_editor.connection.vendor == "mysql":
        return
    tables = [
        apps.get_model("vertimus", "Action")._meta.db_table,
        apps.get_model("vertimus", "ActionArchived")._meta.db_table,
        apps.get_model("vertimus", "State")._meta.db_table,
    ]
    for table_name in tables:
        schema_editor.execute(
            "ALTER TABLE %s CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci" % table_name, params=None
        )


class Migration(migrations.Migration):
    dependencies = [
        ("vertimus", "0003_add_action_sent_to_ml"),
    ]

    operations = [migrations.RunPython(to_utf8mb4, migrations.RunPython.noop, atomic=False)]
