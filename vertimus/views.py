import difflib
import html
import os
import re
import shutil
import subprocess
import tempfile
from collections.abc import Generator
from pathlib import Path
from typing import TYPE_CHECKING, Any

from defusedxml.minidom import parse
from django.conf import settings
from django.contrib import messages
from django.http import (
    Http404,
    HttpResponseForbidden,
    HttpResponseRedirect,
    StreamingHttpResponse,
)
from django.shortcuts import get_object_or_404, render
from django.urls import reverse
from django.utils.html import escape
from django.utils.safestring import mark_safe
from django.utils.translation import gettext as _, pgettext
from django.views.generic import View

from stats.models import (
    Branch,
    Domain,
    FakeLangStatistics,
    Language,
    Module,
    ModuleLock,
    Statistics,
)
from stats.utils import (
    DocFormat,
    UndetectableDocFormatError,
    check_po_quality,
    is_po_reduced,
)
from vertimus.forms import ActionForm
from vertimus.models import Action, ActionArchived, SendMailFailedError, State, ActionUT

if TYPE_CHECKING:
    from django.http import HttpRequest, HttpResponse

from damnedlies import logger


def vertimus_by_stats_id(request: "HttpRequest", stats_id: int, lang_id: int) -> "HttpResponse":
    """Access to Vertimus view by a Statistics ID"""
    stats = get_object_or_404(Statistics, pk=stats_id)
    lang = get_object_or_404(Language, pk=lang_id)
    return vertimus(request, stats.branch, stats.domain, lang, stats)


def vertimus_by_ids(request: "HttpRequest", branch_id: int, domain_id: int, language_id: int) -> "HttpResponse":
    """Access to Vertimus view by Branch, Domain and language IDs"""
    branch = get_object_or_404(Branch, pk=branch_id)
    domain = get_object_or_404(Domain, pk=domain_id)
    language = get_object_or_404(Language, pk=language_id)
    return vertimus(request, branch, domain, language)


def vertimus_by_names(
    request: "HttpRequest", module_name: str, branch_name: str, domain_name: str, locale_name: str, level: int = 0
) -> "HttpResponse":
    """Access to Vertimus view by Branch, Domain and Language names"""
    module = get_object_or_404(Module, name=module_name)
    branch = get_object_or_404(Branch.objects.select_related("module"), name=branch_name, module__id=module.id)
    try:
        domain = branch.connected_domains[domain_name]
    except KeyError as ke:
        raise Http404 from ke
    language = get_object_or_404(Language, locale=locale_name)
    return vertimus(request, branch, domain, language, level=level)


def _exists_a_domain_type_in_this_language_for_branch(
    branch: "Branch", language: "Language", domain_name: str
) -> bool:
    """
    Indicates whether there exists a domain in the same language, for the same branch that has the given domain name.
    It could be "po" for "po-files" for the User Interface, "help" for documentation or anything else.
    """
    at_least_one_stat_exists_for_the_branch_domain_type = False
    if domain_name in branch.connected_domains.keys():
        at_least_one_stat_exists_for_the_branch_domain_type = (
            branch.connected_domains.get(domain_name).statistics_set.filter(language=language).count() > 0
        )
    return at_least_one_stat_exists_for_the_branch_domain_type


def vertimus(
    request: "HttpRequest",
    branch: "Branch",
    domain: "Domain",
    language: "Language",
    stats: Statistics | None = None,
    level: int = 0,
) -> "HttpResponse":
    """The Vertimus view and form management. Level argument is used to
    access to the previous action history, first level (1) is the
    grandparent, second (2) is the parent of the grandparent, etc."""
    level = int(level)

    pot_stats, stats, state = get_vertimus_state(branch, domain, language, stats=stats)
    # Filtering on domain.name instead of domain because we can have several domains
    # working on the same set of strings (e.g. when an extraction method changed,
    # each extraction is mapped to a different domain with branch_from/branch_to delimitations)
    other_branch_states = (
        State.objects.filter(branch__module=branch.module, domain__name=domain.name, language=language)
        .exclude(branch=branch.pk)
        .exclude(name="None")
    )

    if level == 0:
        # Current actions
        action_history = Action.get_action_history(state=state)
    else:
        sequence = state.get_action_sequence_from_level(level)
        action_history = ActionArchived.get_action_history(sequence=sequence)

    # Get the sequence of the grandparent to know if exists a previous action
    # history
    sequence_grandparent = state.get_action_sequence_from_level(level + 1) if state.pk else None
    grandparent_level = level + 1 if sequence_grandparent else None

    action_form = None
    person = request.user.person if request.user.is_authenticated else None
    if person and level == 0:
        # Only authenticated user can act on the translation, and it's not
        # possible to edit an archived workflow
        available_actions = state.get_available_actions(person)
        if request.method == "POST":
            action_form = ActionForm(request.user, state, available_actions, request.POST, request.FILES)

            if action_form.is_valid():
                # Process the data in form.cleaned_data
                action = action_form.cleaned_data["action"]

                action = Action.new_by_name(action, person=person, file=request.FILES.get("file", None))
                try:
                    msg = action.apply_on(state, action_form.cleaned_data)
                except SendMailFailedError:
                    messages.error(request, _("A problem occurred while sending mail, no mail have been sent"))
                except Exception as e:
                    messages.error(request, _("An error occurred during applying your action: %s") % e)
                else:
                    # Action applied
                    if isinstance(action, ActionUT) and not person.has_set_identity:
                        messages.warning(
                            request,
                            mark_safe(  # noqa: S308 (XSS vulnerability)
                                # Translators: The %s are HTML links (opening an closing tag). The first opens, the second closes the tag.
                                _(
                                    "You’ve just uploaded a new translation but did not fill your real name in your"
                                    " profile. If you want your work to be added to the project in your name, you "
                                    "need to fill it. "
                                    "Please, %s go to your profile %s to update your identity."
                                )
                                % (
                                    '<a class="alert-link" href="{link}">'.format(
                                        link=reverse("person_detail_id", kwargs={"pk": person.id}),
                                    ),
                                    "</a>",
                                ),
                            ),
                        )

                    if msg:
                        messages.success(request, msg)

                return HttpResponseRedirect(
                    reverse("vertimus_by_names", args=(branch.module.name, branch.name, domain.name, language.locale))
                )
        elif available_actions:
            action_form = ActionForm(request.user, state, available_actions)

    has_current_module_branch_ui_po_file = _exists_a_domain_type_in_this_language_for_branch(branch, language, "po")
    has_current_module_branch_help_po_file = _exists_a_domain_type_in_this_language_for_branch(
        branch, language, "help"
    )
    context = {
        "pageSection": "module",
        "stats": stats,
        "pot_stats": pot_stats,
        "po_url": stats.po_url(),
        "po_url_reduced": stats.po_url(reduced=True) if stats.has_reducedstat() else "",
        "branch": branch,
        "other_states": other_branch_states,
        "domain": domain,
        "language": language,
        "module": branch.module,
        "state": state,
        "is_team_member": person and language.team and person.is_translator(language.team),
        "action_history": action_history,
        "action_form": action_form,
        "level": level,
        "is_current_module_branch_ui_po_file": pot_stats.domain.dtype == "ui",
        "has_current_module_branch_ui_po_file": has_current_module_branch_ui_po_file,
        "is_current_module_branch_help_po_file": pot_stats.domain.dtype == "doc",
        "has_current_module_branch_help_po_file": has_current_module_branch_help_po_file,
        "grandparent_level": grandparent_level,
    }
    if stats.has_figures():
        context["fig_stats"] = stats.fig_stats()
        del context["fig_stats"]["prc"]
    return render(request, "vertimus/vertimus_detail.html", context)


def get_vertimus_state(
    branch: "Branch", domain: "Domain", language: "Language", stats: Statistics | None = None
) -> tuple[Statistics, Statistics, State]:
    pot_stats = get_object_or_404(Statistics, branch=branch, domain=domain, language=None)
    if not stats:
        try:
            stats = Statistics.objects.get(branch=branch, domain=domain, language=language)
        except Statistics.DoesNotExist:
            stats = FakeLangStatistics(pot_stats, language)

    # Get the state of the translation
    try:
        state = State.objects.get(branch=branch, domain=domain, language=language)
    except State.DoesNotExist:
        # No need to save the state at this stage
        state = State(branch=branch, domain=domain, language=language)
    return pot_stats, stats, state


def vertimus_diff(request: "HttpRequest", action_id_1: int, action_id_2: int, level: int = 0) -> "HttpResponse":
    """Show a diff between current action po file and previous file"""
    if int(level) != 0:
        action_real = ActionArchived
    else:
        action_real = Action
    action_1 = get_object_or_404(action_real, pk=action_id_1)
    state = action_1.state_db

    file_path_1 = action_1.most_uptodate_filepath
    reduced = is_po_reduced(file_path_1)
    if not Path(file_path_1).exists():
        raise Http404("File not found")

    descr_1 = _('<a href="%(url)s">Uploaded file</a> by %(name)s on %(date)s') % {
        "url": action_1.most_uptodate_file.url,
        "name": action_1.person_name,
        "date": action_1.created,
    }

    if action_id_2 not in {None, 0}:
        # 1) id_2 specified in URL
        action_2 = get_object_or_404(action_real, pk=action_id_2)
        file_path_2 = action_2.most_uptodate_filepath
        descr_2 = _('<a href="%(url)s">Uploaded file</a> by %(name)s on %(date)s') % {
            "url": action_2.most_uptodate_file.url,
            "name": action_2.person_name,
            "date": action_2.created,
        }
    else:
        action_2 = None
        if action_id_2 is None:
            # 2) Search previous in action history
            action_2 = action_1.get_previous_action_with_po()

        if action_2:
            file_path_2 = action_2.most_uptodate_filepath
            descr_2 = _('<a href="%(url)s">Uploaded file</a> by %(name)s on %(date)s') % {
                "url": action_2.most_uptodate_file.url,
                "name": action_2.person_name,
                "date": action_2.created,
            }
        else:
            # 3) Lastly, the file should be the more recently committed file (merged)
            try:
                stats = Statistics.objects.get(branch=state.branch, domain=state.domain, language=state.language)
                descr_2 = _('<a href="%(url)s">Latest committed file</a> for %(lang)s') % {
                    "url": stats.po_url(),
                    "lang": state.language.get_name(),
                }
            except Statistics.DoesNotExist:
                stats = get_object_or_404(Statistics, branch=state.branch, domain=state.domain, language=None)
                descr_2 = f'<a href="{stats.pot_url()}">%(text)s</a>' % {
                    "text": _("Latest POT file"),
                }
            file_path_2 = stats.po_path(reduced=reduced)
    if not Path(file_path_2).exists():
        raise Http404("File not found")

    d = difflib.HtmlDiff(wrapcolumn=80)
    with (
        Path.open(file_path_1, encoding="utf-8", errors="replace") as fh1,
        Path.open(file_path_2, encoding="utf-8", errors="replace") as fh2,
    ):
        diff_content = d.make_table(fh2.readlines(), fh1.readlines(), descr_2, descr_1, context=True)

    context = {
        "diff_content": diff_content,
        "state": state,
    }
    return render(request, "vertimus/vertimus_diff.html", context)


def latest_uploaded_po(
    request: "HttpRequest",  # noqa: ARG001
    module_name: str,
    branch_name: str,
    domain_name: str,
    locale_name: str,
) -> "HttpResponse":
    """Redirect to the latest uploaded po for a module/branch/language"""
    branch = get_object_or_404(Branch, module__name=module_name, name=branch_name)
    domain = get_object_or_404(Domain, module__name=module_name, name=domain_name)
    lang = get_object_or_404(Language, locale=locale_name)
    latest_upload = Action.objects.filter(
        state_db__branch=branch, state_db__domain=domain, state_db__language=lang, file__endswith=".po"
    ).order_by("-created")[:1]
    if not latest_upload:
        raise Http404
    return HttpResponseRedirect(latest_upload[0].merged_file.url)


def activity_by_language(request: "HttpRequest", locale: str) -> "HttpResponse":
    language = get_object_or_404(Language, locale=locale)
    states = State.objects.filter(language=language).exclude(name="None")
    context = {
        "pageSection": "languages",
        "language": language,
        "activities": states,
    }
    return render(request, "vertimus/activity_summary.html", context)


class PoFileActionBase(View):
    def get(self: "PoFileActionBase", request: "HttpRequest", *args, **kwargs) -> "HttpResponse":  # noqa: ANN002, ARG002, ANN003
        self.pofile = self.get_po_file()
        context = self.get_context_data(**kwargs)
        return render(request, self.template_name, context)

    def get_po_file(self: "PoFileActionBase") -> Path:
        pofile = None
        if self.kwargs.get("action_pk"):
            self.action = get_object_or_404(Action, pk=self.kwargs["action_pk"])
            if self.action.has_po_file():
                pofile = self.action.most_uptodate_filepath
        elif self.kwargs.get("stats_pk"):
            stats = get_object_or_404(Statistics, pk=self.kwargs["stats_pk"])
            pofile = stats.po_path()
        else:
            raise Http404("action_pk and stats_pk are both None")
        return pofile


class QualityCheckView(PoFileActionBase):
    template_name = "vertimus/quality-check.html"

    def get_context_data(self: "QualityCheckView", **kwargs) -> dict[str, Any]:  # noqa: ARG002, ANN003
        context = {"base": "base_modal.html"}
        if self.pofile is None:
            context["results"] = _("No po file to check")
        else:
            context["checks"] = ["xmltags"]
            try:
                results = check_po_quality(self.pofile, context["checks"])
            except OSError as err:
                context["results"] = "Sorry, the server was not able to run the quality checks on this file (%s)" % err
            else:
                if results:
                    context["results"] = mark_safe(
                        re.sub(
                            r"^(# \(pofilter\) .*)", r'<span class="highlight">\1</span>', escape(results), flags=re.M
                        )
                    )
                else:
                    context["results"] = _("The po file looks good!")
        return context


class MsgiddiffView(PoFileActionBase):
    HEADER = """
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <style>
            body { font-family: monospace; }
            div.warning { font-size: 200%; padding: 3em; background-color: #ddd; }
            div.diff {
                color: #444; background-color: #eee; padding: 4px;
                width: 78ch; line-height: 1.4; margin-left: 2.5em;
            }
            div.nowrap { white-space: pre; }
            span.noline { color: #aaa; }
            del { color: red; background-color: #fed4d4; white-space: pre; }
            ins { color: green; background-color: #c8f5c8; white-space: pre; }
            div.separator { margin: 1em; color: #999; font-style: italic; }
        </style>
    </head>
<body>
""" + '<div class="warning">{}</div>'.format(
        _(
            "WARNING: This file is <b>NOT</b> suitable as a base for completing this translation. "
            "It contains HTML markup to highlight differential parts of changed strings."
        )
    )
    FOOTER = "</body</html>"

    def streamed_file(self: "MsgiddiffView", po_file: Path) -> Generator[str, None, None]:
        def strip(line: str) -> str:
            if line.startswith("#| "):
                line = line[3:]
            if line.startswith("msgid "):
                line = line[6:]
            return line.rstrip("\n").strip('"')

        def line_fmt(no: int, line: str) -> str:
            return '<span class="noline">%d</span> ' % no + html.escape(line) + "<br>"

        yield self.HEADER
        prev_id = curr_id = None
        no_wrap = False
        stored_comments = []
        in_fuzzy = False
        with Path.open(po_file, encoding="utf-8") as fh:
            for noline, line in enumerate(fh.readlines(), start=1):
                if line.startswith(("#.", "#:")):
                    stored_comments.append((noline, line))
                    continue

                if line == "\n":
                    in_fuzzy = False
                    stored_comments = []
                    continue

                if line.startswith("#, fuzzy"):
                    in_fuzzy = True
                    diff_printed = False
                    prev_id = []
                    curr_id = []
                    no_wrap = "no-wrap" in line
                    yield '<div class="separator">(…)</div>\n'
                    for no, com in stored_comments:
                        yield line_fmt(no, com)
                    yield line_fmt(noline, line)
                    continue

                if in_fuzzy:
                    if line.startswith("#|"):
                        prev_id.append(line)
                        continue
                    if line.startswith('msgstr "'):
                        # Compute and display inline diff
                        sep = "\n" if no_wrap else ""
                        yield (
                            '<div class="diff%s">' % (" nowrap" if no_wrap else "")
                            + diff_strings(
                                html.escape(sep.join(strip(_line) for _line in prev_id)),
                                html.escape(sep.join(strip(_line) for _, _line in curr_id)),
                            )
                            + "</div>"
                        )
                        for no, _line in curr_id:
                            yield line_fmt(no, _line)
                        diff_printed = True
                    if not diff_printed:
                        curr_id.append((noline, line))
                        continue
                    yield line_fmt(noline, line)

        yield self.FOOTER

    def get(
        self: "MsgiddiffView",
        request: "HttpRequest",  # noqa: ARG002
        *args,  # noqa: ARG002, ANN002,
        **kwargs,  # noqa: ARG002, ANN003
    ) -> StreamingHttpResponse:
        stats = get_object_or_404(Statistics, pk=self.kwargs["stats_pk"])
        pofile = stats.po_path()
        return StreamingHttpResponse(self.streamed_file(pofile))


class BuildTranslatedDocsView(PoFileActionBase):
    http_method_names = ("post",)

    def post(
        self: "BuildTranslatedDocsView",
        request: "HttpRequest",
        *args,  # noqa: ARG002, ANN002
        **kwargs,  # noqa: ARG002, ANN003
    ) -> "HttpResponse":
        pofile = self.get_po_file()
        if pofile is None:
            raise Http404("No target po file for this action")

        html_dir = settings.SCRATCHDIR / "HTML" / str(self.kwargs["action_pk"])
        if html_dir.exists():
            # If the build already ran, redirect to the static results
            return HttpResponseRedirect(self.action.build_url)

        state = self.action.state_db
        team = state.language.team
        if not request.user.is_authenticated or not team or not request.user.person.is_translator(team):
            return HttpResponseForbidden("Only team members can build docs.")

        with ModuleLock(state.branch.module):
            state.branch.checkout()
            error_message = self.build_docs(state, pofile, html_dir)

        if error_message:
            messages.error(request, error_message)
            return HttpResponseRedirect(state.get_absolute_url())
        return HttpResponseRedirect(self.action.build_url)

    def build_docs(self: "BuildTranslatedDocsView", state: "State", po_file: Path, html_dir: Path) -> str:
        """
        Try building translated docs, return an error message or an empty string
        on success.
        """
        try:
            doc_format = DocFormat(state.domain, state.branch)
        except UndetectableDocFormatError as err:
            return str(err)

        build_error = _("Build failed (%(program)s): %(err)s")
        with tempfile.NamedTemporaryFile(suffix=".gmo") as gmo, tempfile.TemporaryDirectory() as build_dir:
            try:
                subprocess.run(["msgfmt", po_file, "-o", os.path.join(gmo.name)], capture_output=True, check=True)
            except subprocess.CalledProcessError as called_process_error:
                logger.error("Building docs: %s", called_process_error.stderr.decode(errors="replace"))
                return build_error % {
                    "program": "msgfmt",
                    "err": called_process_error.stderr.decode(errors="replace"),
                }

            sources = doc_format.source_files()
            try:
                subprocess.run(
                    [
                        "itstool",
                        "-m",
                        gmo.name,
                        "-l",
                        state.language.locale,
                        "-o",
                        str(build_dir),
                        "--strict",
                        *[str(s) for s in sources],
                    ],
                    cwd=str(doc_format.vcs_path),
                    capture_output=True,
                    check=True,
                )
            except subprocess.CalledProcessError as called_process_error:
                logger.error("Building docs: %s", called_process_error.stderr.decode(errors="replace"))
                return build_error % {
                    "program": "itstool",
                    "err": called_process_error.stderr.decode(errors="replace"),
                }

            # Now build the html version
            if not html_dir.exists():
                html_dir.mkdir(parents=True)
            if doc_format.format == "mallard":
                # With mallard, specifying the directory is enough.
                build_ref = [str(build_dir)]
            else:
                build_ref = [os.path.join(build_dir, s.name) for s in sources]

            index_html = html_dir / "index.html"
            cmd = ["yelp-build", "html", "-o", str(html_dir) + "/", "-p", str(doc_format.vcs_path / "C"), *build_ref]

            result = None
            try:
                result = subprocess.run(cmd, cwd=str(build_dir), capture_output=True, check=True)
            except subprocess.CalledProcessError as called_process_error:
                html_directory_should_be_removed_because_of_failure = (
                    not index_html.exists() and result and len(result.stderr) > 0
                )
                if html_directory_should_be_removed_because_of_failure:
                    shutil.rmtree(html_dir)
                return build_error % {
                    "program": "yelp-build",
                    "err": called_process_error.stderr.decode(errors="replace"),
                }

            if not index_html.exists() and Path(build_ref[0]).is_file():
                # Create an index.html symlink to the base html doc if needed
                try:
                    # FIXME: forbid_entities=False is a trick that should be removed if possible
                    doc = parse(build_ref[0], forbid_entities=False)
                    base_name = doc.getElementsByTagName("article")[0].attributes.get("id").value
                except (AttributeError, IndexError):
                    pass
                else:
                    html_name = "%s.html" % base_name
                    (html_dir / "index.html").symlink_to(html_dir / html_name)
        return ""


def diff_strings(previous: str, current: str) -> str:
    """
    Compute a diff between two strings, with inline differences.
    http://stackoverflow.com/questions/774316/python-difflib-highlighting-differences-inline
    >>> diff_strings(old string, new string)
    'lorem<ins> foo</ins> ipsum dolor <del>sit </del>amet'
    """
    if not previous or not current:
        return ""
    seqm = difflib.SequenceMatcher(
        a=previous.replace("\r\n", "\n"),
        b=current.replace("\r\n", "\n"),
    )
    output = []
    for opcode, a0, a1, b0, b1 in seqm.get_opcodes():
        if opcode == "equal":
            output.append(seqm.a[a0:a1])
        elif opcode == "insert":
            output.append('<ins title="New text">' + seqm.b[b0:b1] + "</ins>")
        elif opcode == "delete":
            output.append('<del title="Deleted text">' + seqm.a[a0:a1] + "</del>")
        elif opcode == "replace":
            output.append('<del title="Deleted text">' + seqm.a[a0:a1] + "</del>")
            output.append('<ins title="New text">' + seqm.b[b0:b1] + "</ins>")
        else:
            raise RuntimeError("unexpected opcode")
    return mark_safe("".join(output))
