import shutil
import tempfile
from functools import cache
from pathlib import Path
from unittest import skipUnless
from unittest.mock import patch
from xml.dom.minidom import parseString

import requests
from django.conf import settings
from django.core import mail
from django.core.files.base import ContentFile, File
from django.core.files.uploadedfile import SimpleUploadedFile
from django.core.management import call_command
from django.http import QueryDict
from django.test import TestCase
from django.test.utils import override_settings
from django.urls import reverse
from django.utils.datastructures import MultiValueDict

from languages.models import Language
from people.models import Person
from stats.models import (
    Branch,
    Category,
    CategoryName,
    Domain,
    Module,
    Release,
    Statistics,
)
from stats.tests.tests import TestModuleBase
from stats.tests.utils import PatchShellCommand, test_scratchdir
from teams.models import Role, Team
from teams.tests import TeamsAndRolesMixin
from vertimus.forms import ActionForm
from vertimus.models import (
    Action,
    ActionArchived,
    ActionCI,
    ActionUNDO,
    ActionWC,
    State,
    StateCommitted,
    StateCommitting,
    StateNone,
    StateProofread,
    StateProofreading,
    StateToCommit,
    StateToReview,
    StateTranslated,
    StateTranslating,
)

MEDIA_ROOT = Path(tempfile.mkdtemp())


@cache
def is_internet_connection_up_and_running() -> bool:
    url, timeout = "http://www.google.com", 2
    has_connection = False
    try:
        _ = requests.get(url, timeout=timeout)
        has_connection = True
    except (requests.ConnectionError, requests.Timeout):
        pass
    return has_connection


@override_settings(MEDIA_ROOT=MEDIA_ROOT)
class VertimusTest(TeamsAndRolesMixin, TestCase):
    def setUp(self):
        super().setUp()

        self.m = Module.objects.create(
            name="gnome-hello",
            description="GNOME Hello",
            bugs_base="https://gitlab.gnome.org/GNOME/gnome-hello/issues",
            vcs_root="https://gitlab.gnome.org/GNOME/gnome-hello.git",
            vcs_web="https://gitlab.gnome.org/GNOME/gnome-hello/",
        )

        Branch.checkout_on_creation = False
        self.b = Branch(name="gnome-2-24", module=self.m)
        # Block the update of Statistics by the thread
        self.b.save(update_statistics=False)

        self.r = Release.objects.create(
            name="gnome-2-24", status="official", description="GNOME 2.24 (stable)", string_frozen=True
        )

        self.c = Category.objects.create(
            release=self.r, branch=self.b, name=CategoryName.objects.create(name="Desktop")
        )

        self.d = Domain.objects.create(
            module=self.m, name="po", description="UI translations", dtype="ui", layout="po/{lang}.po"
        )
        Statistics.objects.create(language=None, branch=self.b, domain=self.d)
        self.files_to_clean = []

    def tearDown(self):
        for path in self.files_to_clean:
            if Path(path).exists():
                Path(path).unlink()

    def upload_file(self, to_state, action_code="UP", pers=None):
        """Test utility to add an uploaded file to a state"""
        test_file = ContentFile("test content")
        test_file.name = "mytestfile.po"
        action = Action.new_by_name(action_code, person=pers or self.reviewer, file=test_file)
        action.apply_on(to_state, {"send_to_ml": action.send_mail_to_ml, "comment": "Done."})
        self.files_to_clean.append(action.file.path)
        return action.file

    def test_state_none(self):
        state = StateNone(branch=self.b, domain=self.d, language=self.language)

        action_names = [a.name for a in state.get_available_actions(self.pn)]
        self.assertEqual(action_names, ["WC"])

        for p in (self.translator, self.reviewer):
            action_names = [a.name for a in state.get_available_actions(p)]
            self.assertEqual(action_names, ["RT", "UT", "WC"])

        for p in (self.committer, self.coordinator):
            action_names = [a.name for a in state.get_available_actions(p)]
            self.assertEqual(action_names, ["RT", "UT", "WC", None, "IC", "AA"])

    def test_state_translating(self):
        state = StateTranslating(branch=self.b, domain=self.d, language=self.language, person=self.translator)

        for p in (self.pn, self.reviewer):
            action_names = [a.name for a in state.get_available_actions(p)]
            self.assertEqual(action_names, ["WC"])

        action_names = [a.name for a in state.get_available_actions(self.committer)]
        self.assertEqual(action_names, ["WC", None, "IC", "AA"])

        action_names = [a.name for a in state.get_available_actions(self.coordinator)]
        self.assertEqual(action_names, ["WC", "UNDO", None, "IC", "AA"])

        # Same person
        action_names = [a.name for a in state.get_available_actions(self.translator)]
        self.assertEqual(action_names, ["UT", "UNDO", "WC"])

    def test_state_translated(self):
        state = StateTranslated(branch=self.b, domain=self.d, language=self.language, person=self.translator)

        action_names = [a.name for a in state.get_available_actions(self.pn)]
        self.assertEqual(action_names, ["WC"])

        action_names = [a.name for a in state.get_available_actions(self.translator)]
        self.assertEqual(action_names, ["RT", "WC"])

        action_names = [a.name for a in state.get_available_actions(self.reviewer)]
        self.assertEqual(action_names, ["RP", "UP", "TR", "RT", "WC"])

        for p in (self.committer, self.coordinator):
            action_names = [a.name for a in state.get_available_actions(p)]
            self.assertEqual(action_names, ["RP", "UP", "TR", "RT", "TC", "WC", None, "IC", "AA"])

    def test_state_proofreading(self):
        state = StateProofreading(branch=self.b, domain=self.d, language=self.language, person=self.reviewer)

        for p in (self.pn, self.translator):
            action_names = [a.name for a in state.get_available_actions(p)]
            self.assertEqual(action_names, ["WC"])

        action_names = [a.name for a in state.get_available_actions(self.committer)]
        self.assertEqual(action_names, ["WC", None, "IC", "AA"])

        action_names = [a.name for a in state.get_available_actions(self.coordinator)]
        self.assertEqual(action_names, ["WC", "UNDO", None, "IC", "AA"])

        # Same person and reviewer
        action_names = [a.name for a in state.get_available_actions(self.reviewer)]
        self.assertEqual(action_names, ["UP", "TR", "TC", "UNDO", "WC"])

    def test_state_proofread(self):
        state = StateProofread(branch=self.b, domain=self.d, language=self.language, person=self.reviewer)

        for p in (self.pn, self.translator):
            action_names = [a.name for a in state.get_available_actions(p)]
            self.assertEqual(action_names, ["WC"])

        action_names = [a.name for a in state.get_available_actions(self.reviewer)]
        self.assertEqual(action_names, ["TC", "RP", "TR", "WC"])

        for p in (self.committer, self.coordinator):
            action_names = [a.name for a in state.get_available_actions(p)]
            self.assertEqual(action_names, ["TC", "RP", "TR", "RC", "WC", None, "IC", "AA"])

    def test_state_to_review(self):
        state = StateToReview(branch=self.b, domain=self.d, language=self.language, person=self.translator)

        action_names = [a.name for a in state.get_available_actions(self.pn)]
        self.assertEqual(action_names, ["WC"])

        for p in (self.translator, self.reviewer):
            action_names = [a.name for a in state.get_available_actions(p)]
            self.assertEqual(action_names, ["RT", "WC"])

        for p in (self.committer, self.coordinator):
            action_names = [a.name for a in state.get_available_actions(p)]
            self.assertEqual(action_names, ["RT", "WC", None, "IC", "AA"])

    def test_state_to_commit(self):
        state = StateToCommit(branch=self.b, domain=self.d, language=self.language, person=self.reviewer)

        for p in (self.pn, self.translator, self.reviewer):
            action_names = [a.name for a in state.get_available_actions(p)]
            self.assertEqual(action_names, ["WC"])

        for p in (self.committer, self.coordinator):
            action_names = [a.name for a in state.get_available_actions(p)]
            self.assertEqual(action_names, ["RC", "TR", "UNDO", "WC", None, "IC", "AA"])

    @test_scratchdir
    def test_state_committing(self):
        state = StateProofreading(branch=self.b, domain=self.d, language=self.language, person=self.reviewer)
        state.save()
        self.upload_file(state, "UP")
        action = Action.new_by_name("RC", person=self.committer)
        action.apply_on(state, {"comment": "Reserve the commit"})
        self.assertEqual(state.name, "Committing")

        for p in (self.pn, self.translator, self.reviewer):
            action_names = [a.name for a in state.get_available_actions(p)]
            self.assertEqual(action_names, ["WC"])

        action_names = [a.name for a in state.get_available_actions(self.coordinator)]
        self.assertEqual(action_names, ["WC", "UNDO", None, "IC", "AA"])

        action_names = [a.name for a in state.get_available_actions(self.committer)]
        self.assertEqual(action_names, ["IC", "TR", "UNDO", "WC"])

        # Setup as a writable repo
        self.b.module.vcs_root = "git@gitlab.gnome.org:GNOME/%s.git" % self.b.module.name
        self.b.module.save()
        self.b.refresh_from_db()
        self.assertTrue(state.able_to_commit())
        action_names = [a.name for a in state.get_available_actions(self.coordinator)]
        self.assertEqual(action_names, ["CI", "WC", "UNDO", None, "IC", "AA"])

    def test_state_committed(self):
        state = StateCommitted(branch=self.b, domain=self.d, language=self.language, person=self.committer)

        for p in (self.pn, self.translator, self.reviewer):
            action_names = [a.name for a in state.get_available_actions(p)]
            self.assertEqual(action_names, ["WC"])

        for p in (self.committer, self.coordinator):
            action_names = [a.name for a in state.get_available_actions(p)]
            self.assertEqual(action_names, ["AA", "WC", None, "IC", "AA"])

    def test_action_menu(self):
        state = StateNone(branch=self.b, domain=self.d, language=self.language)
        form = ActionForm(self.translator, state, state.get_available_actions(self.translator))
        self.assertHTMLEqual(
            str(form["action"]),
            '<select id="id_action" name="action">'
            '<option value="RT">Reserve for translation</option>'
            '<option value="UT">Upload the new translation</option>'
            '<option value="WC">Write a comment</option>'
            "</select>",
        )
        form = ActionForm(self.coordinator, state, state.get_available_actions(self.coordinator))
        self.assertHTMLEqual(
            str(form["action"]),
            '<select id="id_action" name="action">'
            '<option value="RT">Reserve for translation</option>'
            '<option value="UT">Upload the new translation</option>'
            '<option value="WC">Write a comment</option>'
            '<option value="" disabled>--------</option>'
            '<option value="IC">Inform of submission</option>'
            '<option value="AA">Archive the actions</option>'
            "</select>",
        )

    def test_action_wc(self):
        state = StateNone(branch=self.b, domain=self.d, language=self.language)
        # State may be initially unsaved
        prev_updated = state.updated

        action = Action.new_by_name("WC", person=self.translator)
        action.apply_on(state, {"send_to_ml": action.send_mail_to_ml, "comment": "Hi 😉"})
        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(mail.outbox[0].recipients(), [self.language.team.get_coordinators()[0].email])
        self.assertEqual(mail.outbox[0].extra_headers, {settings.EMAIL_HEADER_NAME: "Inactive"})
        # Second comment by someone else, mail sent to the other person
        action = Action.new_by_name("WC", person=self.reviewer)
        action.apply_on(state, {"send_to_ml": action.send_mail_to_ml, "comment": "Great!"})
        self.assertEqual(len(mail.outbox), 2)
        self.assertEqual(mail.outbox[1].recipients(), [self.translator.email])
        # Test that submitting a comment without text generates a validation error
        form = ActionForm(self.translator, state, [ActionWC()], data=QueryDict("action=WC&comment="))
        self.assertTrue("A comment is needed" in str(form.errors))
        self.assertNotEqual(state.updated, prev_updated)

        # Test send comment to mailing list
        mail.outbox = []
        action = Action.new_by_name("WC", person=self.translator)
        action.apply_on(state, {"send_to_ml": True, "comment": "Hi again!"})
        self.assertTrue(action.sent_to_ml)
        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(mail.outbox[0].recipients(), [self.language.team.mailing_list])
        self.assertIn("Hi again!", mail.outbox[0].body)
        self.assertEqual(mail.outbox[0].extra_headers, {settings.EMAIL_HEADER_NAME: "Inactive"})

    def test_send_mail_translated(self):
        team = Team.objects.create(name="zh", description="Chinese", mailing_list="zh@example.org")
        zh_cn = Language.objects.create(name="Chinese", locale="zh_CN", team=team)
        call_command("compile-trans", locale="zh_CN")

        state = StateNone(branch=self.b, domain=self.d, language=zh_cn)
        action = Action.new_by_name("WC", person=self.pn)
        action.apply_on(state, {"send_to_ml": True, "comment": "Comment"})
        self.assertEqual(len(mail.outbox), 1)
        self.assertIn("您好，", mail.outbox[0].body)

    def test_action_rt(self):
        state = StateNone(branch=self.b, domain=self.d, language=self.language)

        action = Action.new_by_name("RT", person=self.translator)
        action.apply_on(state, {"send_to_ml": action.send_mail_to_ml, "comment": "Reserved!"})
        self.assertIsInstance(state, StateTranslating)

    @test_scratchdir
    def test_action_ut(self):
        # Disabling the role
        role = Role.objects.get(person=self.translator, team=self.language.team)
        role.is_active = False
        role.save()

        state = StateTranslating(branch=self.b, domain=self.d, language=self.language, person=self.translator)
        state.save()

        file_path = Path(__file__).parent / "valid_po.po"
        with file_path.open() as test_file:
            action = Action.new_by_name("UT", person=self.translator, file=File(test_file))
            action.apply_on(state, {"send_to_ml": action.send_mail_to_ml, "comment": "Done by translator."})

        self.assertEqual(action.file.url, "/media/upload/gnome-hello-gnome-2-24-po-fr-%d.po" % state.id)
        self.files_to_clean.append(action.file.path)
        # Merged file will not be really produced as no pot file exists on the file system
        self.assertIsNone(action.merged_file)

        self.assertIsInstance(state, StateTranslated)
        # Mail sent to mailing list
        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(mail.outbox[0].recipients(), [self.language.team.mailing_list])
        self.assertEqual(mail.outbox[0].subject, "%sgnome-hello - gnome-2-24" % settings.EMAIL_SUBJECT_PREFIX)
        self.assertEqual(mail.outbox[0].extra_headers, {settings.EMAIL_HEADER_NAME: "Translated"})

        # Testing if the role was activated
        role = Role.objects.get(person=self.translator, team=self.language.team)
        self.assertTrue(role.is_active)

        # Setup as a writable repo and test Submit to repository is already available
        self.b.module.vcs_root = "git@gitlab.gnome.org:GNOME/%s.git" % self.b.module.name
        self.b.module.save()
        self.assertEqual(
            [act.name for act in state.get_available_actions(self.coordinator)],
            ["RP", "UP", "TR", "RT", "TC", "CI", "WC", None, "IC", "AA"],
        )

    @test_scratchdir
    def test_action_ut_already_reserved(self):
        """
        If someone reserved the module for translation, upload a translation from another person is not possible.
        """
        state = StateTranslating(branch=self.b, domain=self.d, language=self.language, person=self.translator)
        state.save()

        file_path = Path(__file__).parent / "valid_po.po"
        with file_path.open() as test_file:
            action = Action.new_by_name("UT", person=self.reviewer, file=File(test_file))
            with self.assertRaisesRegex(Exception, "Action not allowed"):
                action.apply_on(state, {"send_to_ml": False, "comment": "Done by reviewer."})

    def test_action_rp(self):
        state = StateTranslated(branch=self.b, domain=self.d, language=self.language)
        state.save()

        action = Action.new_by_name("RP", person=self.reviewer)
        action.apply_on(
            state,
            {
                "send_to_ml": action.send_mail_to_ml,
                "comment": "",
            },
        )
        self.assertIsInstance(state, StateProofreading)
        self.assertEqual(len(mail.outbox), 0)

    def test_action_rp_with_comment(self):
        state = StateTranslated.objects.create(branch=self.b, domain=self.d, language=self.language)
        action = Action.new_by_name("WC", person=self.translator)
        action.apply_on(state, {"send_to_ml": False, "comment": "Hi!"})
        self.assertFalse(action.sent_to_ml)
        # At least the coordinator receives the message
        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(mail.outbox[0].recipients(), [self.language.team.get_coordinators()[0].email])

        action = Action.new_by_name("RP", person=self.reviewer)
        action.apply_on(state, {"send_to_ml": False, "comment": "I'm reviewing this!"})
        # If a comment is set, a message should be sent
        self.assertEqual(len(mail.outbox), 2)
        self.assertEqual(mail.outbox[1].recipients(), [self.translator.email])

    def test_action_up(self):
        state = StateProofreading(branch=self.b, domain=self.d, language=self.language, person=self.reviewer)
        state.save()

        test_file = ContentFile("test content")
        test_file.name = "mytestfile.po"

        action = Action.new_by_name("UP", person=self.reviewer, file=test_file)
        action.apply_on(state, {"send_to_ml": action.send_mail_to_ml, "comment": "Done."})
        self.files_to_clean.append(action.file.path)
        self.assertIsInstance(state, StateProofread)
        # Mail sent to mailing list
        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(mail.outbox[0].recipients(), [self.language.team.mailing_list])

        # Comment made by someone else, file reviewed again, checkbox "Send to mailing list" unckecked
        # => mail sent to the comment author
        action = Action.new_by_name("WC", person=self.translator)
        action.apply_on(state, {"send_to_ml": False, "comment": "Hi!"})
        action = Action.new_by_name("RP", person=self.reviewer)
        action.apply_on(state, {"send_to_ml": False, "comment": "Reserved by a reviewer!"})
        mail.outbox = []
        action = Action.new_by_name("UP", person=self.reviewer, file=test_file)
        action.apply_on(state, {"send_to_ml": False, "comment": "Done second time."})
        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(mail.outbox[0].recipients(), [self.translator.email])

    def test_action_tc(self):
        state = StateProofread(branch=self.b, domain=self.d, language=self.language)
        state.save()

        action = Action.new_by_name("TC", person=self.reviewer)
        action.apply_on(state, {"send_to_ml": action.send_mail_to_ml, "comment": "Ready!"})
        self.assertIsInstance(state, StateToCommit)

    def test_action_rc(self):
        state = StateToCommit(branch=self.b, domain=self.d, language=self.language)
        state.save()

        action = Action.new_by_name("RC", person=self.committer)
        action.apply_on(state, {"send_to_ml": action.send_mail_to_ml, "comment": "This work is mine!"})
        self.assertIsInstance(state, StateCommitting)

    @test_scratchdir
    def test_action_ci(self):
        # Setup as a writable repo
        self.b.module.vcs_root = "git@gitlab.gnome.org:GNOME/%s.git" % self.b.module.name
        self.b.module.save()
        Branch.checkout_on_creation = False
        master = Branch(name="master", module=self.m)
        master.save(update_statistics=False)
        state = StateProofreading(branch=self.b, domain=self.d, language=self.language, person=self.reviewer)
        state.save()

        action = Action.new_by_name("WC", person=self.translator)
        action.apply_on(state, {"send_to_ml": action.send_mail_to_ml, "comment": "Hi!"})
        # Adding users with incomplete profiles
        pers_no_full_name = Person.objects.create(username="ûsername")
        action = Action.new_by_name("WC", person=pers_no_full_name)
        action.apply_on(state, {"send_to_ml": action.send_mail_to_ml, "comment": "Hello!"})
        pers_no_email = Person.objects.create(username="noemail", first_name="Sven", last_name="Brkc")
        action = Action.new_by_name("WC", person=pers_no_email)
        action.apply_on(state, {"send_to_ml": action.send_mail_to_ml, "comment": "Hello!"})

        self.upload_file(state, "UP")

        self.assertIn(ActionCI, map(type, state.get_available_actions(self.coordinator)))

        form = ActionForm(self.coordinator, state, state.get_available_actions(self.coordinator))
        self.assertEqual(len(form.fields["author"].choices), 6)
        self.assertEqual(form.fields["author"].initial, self.reviewer)
        self.assertIn("sync_master", form.fields)
        self.assertEqual(form.fields["sync_master"].label, "Sync with master")
        self.assertHTMLEqual(
            str(form["author"]),
            '<select id="id_author" name="author">'
            '<option value="">---------</option>'
            '<option disabled value="%d">ûsername (full name missing)</option>'
            '<option disabled value="%d">Sven Brkc (email missing)</option>'
            '<option value="%d">John Coordinator</option>'
            '<option selected value="%d">John Reviewer</option>'
            '<option value="%d">John Translator</option>'
            "</select>"
            % (
                pers_no_full_name.pk,
                pers_no_email.pk,
                self.coordinator.pk,
                self.reviewer.pk,
                self.translator.pk,
            ),
        )

    @test_scratchdir
    def test_action_ci_no_fullname(self):
        """
        Test that a commit works even when nobody has full name, but without --author flag.
        """
        pers = Person.objects.create(email="user@example.org", username="username_only")
        Role.objects.create(team=self.team_french, person=pers, role="reviewer")
        self.m.vcs_root = "git@gitlab.gnome.org:GNOME/gnome-hello.git"
        self.m.save()

        state = StateProofreading(branch=self.b, domain=self.d, language=self.language, person=pers)
        state.save()
        # Adding two comments from the commit author, as this might trigger a form error
        action = Action.new_by_name("WC", person=self.coordinator)
        action.apply_on(state, {"send_to_ml": False, "comment": "Looks good"})
        action = Action.new_by_name("WC", person=self.coordinator)
        action.apply_on(state, {"send_to_ml": False, "comment": "Looks good too"})

        self.upload_file(state, "UP", pers=pers)
        post_data = {"action": "CI", "author": "", "comment": "", "send_to_ml": True}
        form = ActionForm(self.coordinator, state, state.get_available_actions(self.coordinator), data=post_data)
        # Missing author
        self.assertFalse(form.is_valid())
        post_data["author"] = str(self.coordinator.pk)
        form = ActionForm(self.coordinator, state, state.get_available_actions(self.coordinator), data=post_data)
        self.assertTrue(form.is_valid())
        # path needed when copying file to commit
        (self.b.checkout_path / "po").mkdir(parents=True, exist_ok=True)
        with PatchShellCommand(only=["git "]) as cmds:
            action = Action.new_by_name("CI", person=self.coordinator)
            msg = action.apply_on(state, form.cleaned_data)
        self.assertIn("git commit -m Update French translation --author John Coordinator <jcoo@imthebigboss.fr>", cmds)
        self.assertEqual(msg, "The file has been successfully committed to the repository.")
        state.refresh_from_db()
        # All actions should have been archived
        self.assertEqual(state.action_set.count(), 0)

    def test_action_ic(self):
        state = StateProofreading(branch=self.b, domain=self.d, language=self.language, person=self.reviewer)
        state.save()

        # Upload a new file
        action_file = self.upload_file(state, "UP")
        self.assertEqual(len(mail.outbox), 1)  # Mail sent to mailing list
        mail.outbox = []

        file_path = settings.MEDIA_ROOT / action_file.name
        self.assertTrue(file_path.exists())

        action = Action.new_by_name("TC", person=self.committer)
        action.apply_on(state, {"send_to_ml": action.send_mail_to_ml, "comment": ""})
        self.assertEqual(len(mail.outbox), 1)  # Mail sent to committers
        mail.outbox = []

        action = Action.new_by_name("RC", person=self.committer)
        action.apply_on(state, {"send_to_ml": action.send_mail_to_ml, "comment": ""})

        action = Action.new_by_name("IC", person=self.committer)
        action.apply_on(state, {"send_to_ml": action.send_mail_to_ml, "comment": "Committed."})
        # Mail sent to mailing list
        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(mail.outbox[0].recipients(), [self.language.team.mailing_list])
        # Team is French (but translations may not be compiled/up-to-date)
        self.assertTrue("Commité" in mail.outbox[0].body or "Committed" in mail.outbox[0].body)

        self.assertIsInstance(state, StateNone)
        self.assertFalse(file_path.exists(), "%s not deleted" % file_path)

        # Remove test file
        action_archived = ActionArchived.objects.get(comment="Done.")
        filename_archived = settings.MEDIA_ROOT / action_archived.file.name
        action_archived.delete()
        self.assertFalse(filename_archived.exists(), "%s not deleted" % filename_archived)

    def test_action_tr(self):
        state = StateTranslated(branch=self.b, domain=self.d, language=self.language)
        state.save()

        action = Action.new_by_name("TR", person=self.committer)
        action.apply_on(state, {"send_to_ml": action.send_mail_to_ml, "comment": "Bad work :-/"})
        self.assertIsInstance(state, StateToReview)

    def test_action_aa(self):
        state = StateCommitted(branch=self.b, domain=self.d, language=self.language, person=self.reviewer)
        state.save()

        action = Action.new_by_name("AA", person=self.committer)
        action.apply_on(
            state,
            {
                "send_to_ml": action.send_mail_to_ml,
                "comment": "I don't want to disappear :)",
            },
        )

        state = State.objects.get(branch=self.b, domain=self.d, language=self.language)
        self.assertIsInstance(state, StateNone)
        self.assertEqual(state.action_set.count(), 0)

    def test_action_undo(self):
        state = StateNone(branch=self.b, domain=self.d, language=self.language)

        action = Action.new_by_name("RT", person=self.translator)
        action.apply_on(state, {"send_to_ml": action.send_mail_to_ml, "comment": "Reserved!"})

        action = Action.new_by_name("UNDO", person=self.translator)
        action.apply_on(
            state,
            {
                "send_to_ml": action.send_mail_to_ml,
                "comment": "Ooops! I don't want to do that. Sorry.",
            },
        )

        self.assertEqual(state.name, "None")

        action = Action.new_by_name("RT", person=self.translator)
        action.apply_on(state, {"send_to_ml": action.send_mail_to_ml, "comment": "Translating"})

        action = Action.new_by_name("UT", person=self.translator)
        action.apply_on(state, {"send_to_ml": action.send_mail_to_ml, "comment": "Translated"})

        action = Action.new_by_name("RT", person=self.translator)
        action.apply_on(state, {"send_to_ml": action.send_mail_to_ml, "comment": "Reserved!"})

        action = Action.new_by_name("UNDO", person=self.translator)
        action.apply_on(
            state, {"send_to_ml": action.send_mail_to_ml, "comment": "Ooops! I don't want to do that. Sorry."}
        )
        self.assertEqual(action.comment, "Ooops! I don't want to do that. Sorry.")

        self.assertEqual(state.name, "Translated")

        action = Action.new_by_name("RT", person=self.translator)
        action.apply_on(state, {"send_to_ml": action.send_mail_to_ml, "comment": "Translating 1"})

        action = Action.new_by_name("UNDO", person=self.translator)
        action.apply_on(state, {"send_to_ml": action.send_mail_to_ml, "comment": "Undo 1"})

        action = Action.new_by_name("RT", person=self.translator)
        action.apply_on(state, {"send_to_ml": action.send_mail_to_ml, "comment": "Translating 2"})

        action = Action.new_by_name("UNDO", person=self.translator)
        action.apply_on(state, {"send_to_ml": action.send_mail_to_ml, "comment": "Undo 2"})

        self.assertEqual(state.name, "Translated")

    def test_action_on_archived_module(self):
        state = StateNone.objects.create(branch=self.b, domain=self.d, language=self.language)
        action = Action.new_by_name("WC", person=self.translator)
        action.apply_on(state, {"send_to_ml": action.send_mail_to_ml, "comment": "Hi!"})
        self.m.archived = True
        self.m.save()

        # For an archived module, the only available action is to archive the state by a coord.
        self.assertEqual(state.get_available_actions(self.translator), [])
        self.assertEqual([a.name for a in state.get_available_actions(self.coordinator)], ["AA"])

        action = Action.new_by_name("AA", person=self.coordinator)
        action.apply_on(state, {"send_to_ml": False})
        self.assertEqual(state.name, "None")

    def test_delete(self):
        """Test that a whole module tree can be properly deleted"""
        state = StateNone(branch=self.b, domain=self.d, language=self.language)

        action = Action.new_by_name("WC", person=self.translator)
        action.apply_on(state, {"send_to_ml": action.send_mail_to_ml, "comment": "Hi!"})

        self.m.delete()
        self.assertEqual(Action.objects.all().count(), 0)

    def test_delete_domain(self):
        StateTranslating.objects.create(branch=self.b, domain=self.d, language=self.language, person=self.translator)
        self.d.delete()
        self.assertEqual(State.objects.all().count(), 0)

    def test_delete_statistics(self):
        """Test clean_dangling_states receiver"""
        po_stat = Statistics.objects.create(branch=self.b, domain=self.d, language=self.language)
        StateTranslating.objects.create(branch=self.b, domain=self.d, language=self.language, person=self.translator)
        po_stat.delete()
        self.assertEqual(State.objects.all().count(), 0)

    def test_vertimus_view(self):
        url = reverse("vertimus_by_ids", args=[self.b.id, self.d.id, self.language.id])
        response = self.client.get(url)
        self.assertNotContains(response, '<option value="WC">')
        self.assertContains(
            response,
            '<span class="num1">      0</span><span class="num2">     0</span><span class="num3">     0</span>',
        )

        self.client.login(username=self.pn.username, password="password")
        response = self.client.get(url)
        self.assertContains(response, '<option value="WC">')

        response = self.client.post(
            url,
            data={
                "action": "WC",
                "comment": "Graçias",
                "send_to_ml": "",
            },
            follow=True,
        )
        self.assertContains(response, "Graçias")

    def test_vertimus_view_on_going_activities(self):
        master = Branch(name="master", module=self.m)
        # Block the update of Statistics by the thread
        master.save(update_statistics=False)
        Statistics.objects.create(branch=master, domain=self.d, language=None)
        dom_alt = Domain.objects.create(
            module=self.m, name="po", description="UI translations", dtype="ui", layout="po/{lang}.po"
        )
        StateNone.objects.create(branch=master, domain=self.d, language=self.language)
        StateNone.objects.create(branch=self.b, domain=dom_alt, language=self.language)

        response = self.client.get(reverse("vertimus_by_ids", args=[master.pk, self.d.pk, self.language.pk]))
        self.assertNotContains(response, "Another branch for this module has ongoing activity.")

        state = State.objects.get(branch=self.b, domain=dom_alt, language=self.language)
        state.change_state(StateTranslated)
        response = self.client.get(reverse("vertimus_by_ids", args=[master.pk, self.d.pk, self.language.pk]))
        self.assertContains(response, "Another branch for this module has ongoing activity.")

    @test_scratchdir
    def test_diff_view(self):
        state = StateNone(branch=self.b, domain=self.d, language=self.language, person=self.reviewer)
        state.save()
        # Ensure pot file exists (used for merged file and by the diff view)
        potfile, _ = self.d.generate_pot_file(self.b)
        pot_location = self.b.output_directory(self.d.dtype) / (f"{self.d.pot_base()}.{self.b.name}.pot")
        shutil.copyfile(str(potfile), str(pot_location))

        action = Action.new_by_name("RT", person=self.translator)
        action.apply_on(state, {"send_to_ml": action.send_mail_to_ml, "comment": ""})
        with (Path(__file__).parent / "valid_po.po").open("rb") as fh:
            action = Action.new_by_name("UT", person=self.translator, file=File(fh))
            action.apply_on(state, {"send_to_ml": action.send_mail_to_ml, "comment": "Done."})
        action_history = Action.get_action_history(state=state)
        diff_url = reverse("vertimus_diff", args=[action_history[-1][0].id, "0", 0])
        response = self.client.get(diff_url)
        self.assertContains(response, '<a href="%s">Latest POT file</a>' % str(pot_location).split("scratch")[1])
        # Should also work if action persons were deleted
        self.translator.delete()
        response = self.client.get(diff_url)
        self.assertContains(response, "Latest POT file")

    def test_uploaded_file_validation(self):
        # Test a non valid po file
        post_content = QueryDict("action=WC&comment=Test1")
        post_file = MultiValueDict({"file": [SimpleUploadedFile("filename.po", b"Not valid po file content")]})
        form = ActionForm(self.translator, None, [ActionWC()], data=post_content, files=post_file)
        self.assertTrue("file" in form.errors)
        post_file = MultiValueDict({"file": [SimpleUploadedFile("filename.po", "Niña".encode("latin-1"))]})
        form = ActionForm(self.translator, None, [ActionWC()], data=post_content, files=post_file)
        self.assertTrue("file" in form.errors)

        # Test a valid po file
        with (Path(__file__).parent / "valid_po.po").open("rb") as fh:
            post_file = MultiValueDict({"file": [File(fh)]})
            form = ActionForm(self.translator, None, [ActionWC()], data=post_content, files=post_file)
            self.assertTrue(form.is_valid())

        # Test form without file
        form = ActionForm(self.translator, None, [ActionWC()], data=post_content)
        self.assertTrue(form.is_valid())

    def test_feeds(self):
        state = StateNone(branch=self.b, domain=self.d, language=self.language)

        action = Action.new_by_name("RT", person=self.translator)
        action.apply_on(state, {"send_to_ml": action.send_mail_to_ml, "comment": "Translating"})

        response = self.client.get(reverse("lang_feed", args=[self.language.locale]))
        doc = parseString(response.content)
        self.assertEqual(doc.childNodes[0].tagName, "rss")
        self.assertEqual(doc.childNodes[0].getAttribute("xmlns:atom"), "http://www.w3.org/2005/Atom")
        self.assertEqual(
            doc.getElementsByTagName("title")[1].toxml(),
            "<title>po (gnome-hello/User Interface) - gnome-hello (gnome-2-24) - Reserve for translation\n</title>",
        )
        self.assertEqual(
            doc.getElementsByTagName("guid")[0].toxml(),
            """<guid>http://testserver/vertimus/gnome-hello/gnome-2-24/po/fr/#%d</guid>""" % action.pk,
        )

    def test_activity_summary(self):
        StateTranslating.objects.create(branch=self.b, domain=self.d, language=self.language, person=self.translator)

        response = self.client.get(reverse("activity_by_language", args=[self.language.locale]))
        self.assertContains(response, self.m.description)

    @test_scratchdir
    def test_check_po_file(self):
        valid_path = Path(__file__).parent / "valid_po.po"
        state = StateProofreading(branch=self.b, domain=self.d, language=self.language, person=self.reviewer)
        state.save()
        with valid_path.open() as test_file:
            action = Action.new_by_name("UT", person=self.translator, file=File(test_file))
            action.state_db = state
            action.file.save(Path(action.file.name).name, action.file, save=False)
        action.merged_file = None
        action.save()
        response = self.client.get(reverse("action-quality-check", args=[action.pk]))
        self.assertContains(response, "The po file looks good!")

        po_stat = Statistics.objects.create(branch=self.b, domain=self.d, language=self.language)
        Path(po_stat.po_path()).parent.mkdir(parents=True, exist_ok=True)
        with valid_path.open() as valid_file, Path.open(po_stat.po_path(), "w", encoding="utf-8") as fh:
            fh.write(valid_file.read())
        response = self.client.get(reverse("stats-quality-check", args=[po_stat.pk]))
        self.assertContains(response, "The po file looks good!")

    def test_mysql(self):
        # Copied from test_action_undo() with minor changes
        state = StateNone(branch=self.b, domain=self.d, language=self.language)
        state.save()

        action = Action.new_by_name("RT", person=self.reviewer, comment="Reserved!")
        action.apply_on(state, {"send_to_ml": action.send_mail_to_ml, "comment": "Reserved!"})

        action = Action.new_by_name("UNDO", person=self.reviewer)
        action.apply_on(
            state,
            {
                "send_to_ml": action.send_mail_to_ml,
                "comment": "Ooops! I don't want to do that. Sorry.",
            },
        )

        action = Action.new_by_name("RT", person=self.reviewer)
        action.apply_on(state, {"send_to_ml": action.send_mail_to_ml, "comment": "Translating"})

        action = Action.new_by_name("UT", person=self.reviewer)
        action.apply_on(state, {"send_to_ml": action.send_mail_to_ml, "comment": "Translated"})

        action = Action.new_by_name("RP", person=self.reviewer)
        action.apply_on(state, {"send_to_ml": action.send_mail_to_ml, "comment": "Proofreading"})

        action = Action.new_by_name("UNDO", person=self.reviewer)
        action.apply_on(
            state,
            {
                "send_to_ml": action.send_mail_to_ml,
                "comment": "Ooops! I don't want to do that. Sorry.",
            },
        )

        actions_db = Action.objects.filter(state_db__id=state.id).exclude(name="WC").order_by("-id")

        # So the last action is UNDO
        self.assertIsInstance(actions_db[0], ActionUNDO)

        # Here be dragons! A call to len() workaround the Django/MySQL bug!
        len(actions_db)
        self.assertIsInstance(actions_db[0], ActionUNDO)


class VertimusViewsText(TestCase):
    fixtures = ("test_data.json",)

    def setUp(self):
        self.coordinator = Person.objects.get(username="john")

    @test_scratchdir
    def test_vertimus_translation_submitted_user_has_no_first_name_or_last_name(self):
        """
        Test that we display a warning banner when the user tries to contribute and uploads a PO file
        but does not have set their identity.
        """
        self.coordinator.first_name = ""
        self.coordinator.last_name = ""
        self.coordinator.save()
        self.client.force_login(self.coordinator)
        url = reverse(
            "vertimus_by_names",
            kwargs={"module_name": "gnome-hello", "branch_name": "master", "domain_name": "po", "locale_name": "fr"},
        )

        with (Path(__file__).parent / "valid_po.po").open(mode="r", encoding="utf-8") as file:
            response = self.client.post(
                url,
                data={
                    "action": "UT",  # upload new translation
                    "file": file,
                },
                follow=True,
            )
            self.assertRedirects(response, url)
            self.assertContains(
                response, "You’ve just uploaded a new translation but did not fill your real name in your profile"
            )

    @test_scratchdir
    def test_vertimus_translation_submitted_user_has_name(self):
        """
        Test that we display a warning banner when the user tries to contribute and uploads a PO file
        but does not have set their identity.
        """
        self.coordinator.first_name = "John"
        self.coordinator.last_name = "Coordinator"
        self.client.force_login(self.coordinator)
        url = reverse(
            "vertimus_by_names",
            kwargs={"module_name": "gnome-hello", "branch_name": "master", "domain_name": "po", "locale_name": "fr"},
        )

        with (Path(__file__).parent / "valid_po.po").open(mode="r", encoding="utf-8") as file:
            response = self.client.post(
                url,
                data={
                    "action": "UT",  # upload new translation
                    "file": file,
                },
                follow=True,
            )
            self.assertRedirects(response, url)
            self.assertNotContains(
                response, "You’ve just uploaded a new translation but did not fill your real name in your profile"
            )


class DocsBuildingTests(TeamsAndRolesMixin, TestModuleBase):
    def setUp(self):
        super().setUp()
        html_dir = settings.SCRATCHDIR / "HTML"
        if html_dir.exists():
            shutil.rmtree(str(html_dir))

    def test_doc_building(self):
        dom = Domain.objects.create(
            module=self.mod, name="help", description="User Guide", dtype="doc", layout="help_mallard/{lang}/{lang}.po"
        )
        state = StateTranslating(branch=self.branch, domain=dom, language=self.language, person=self.translator)
        state.save()

        file_path = Path(__file__).parent / "gnome-hello.help.fr.po"
        with file_path.open() as test_file:
            action = Action.new_by_name("UT", person=self.translator, file=File(test_file))
            action.apply_on(state, {"send_to_ml": action.send_mail_to_ml, "comment": "Done by translator."})
        self.assertTrue(action.can_build)
        self.assertIsNone(action.build_url)
        with patch("stats.models.Branch.checkout"):
            response = self.client.post(reverse("action-build-help", args=[action.pk]))
            self.assertEqual(response.status_code, 403)
            self.client.force_login(self.translator)
            response = self.client.post(reverse("action-build-help", args=[action.pk]))
        self.assertRedirects(response, "/HTML/%d/index.html" % action.pk, fetch_redirect_response=False)
        self.assertEqual(action.build_url, "/HTML/%d/index.html" % action.pk)
        index_file = settings.SCRATCHDIR / "HTML" / str(action.pk) / "index.html"
        with index_file.open("r") as ifile:
            self.assertIn('<h2><span class="title">À propos</span></h2>', ifile.read())

    @skipUnless(is_internet_connection_up_and_running(), "No Internet Connection available, cannot execute the test.")
    def test_docbook_building(self):
        """
        .. warning: This test requires an Internet connection, otherwise some asserts will fail.
        This is because yelp-build tool needs to retrieve external resources from the Internet in order to build
        the index.html file.
        """
        dom = Domain.objects.create(
            module=self.mod, name="help", description="User Guide", dtype="doc", layout="help_docbook/{lang}/{lang}.po"
        )
        state = StateTranslating(branch=self.branch, domain=dom, language=self.language, person=self.translator)
        state.save()

        file_path = Path(__file__).parent / "gnome-hello.help.fr.po"
        with file_path.open() as test_file:
            action = Action.new_by_name("UT", person=self.translator, file=File(test_file))
            action.apply_on(state, {"send_to_ml": action.send_mail_to_ml, "comment": "Done by translator."})
        self.assertTrue(action.can_build)
        self.assertIsNone(action.build_url)
        self.client.force_login(self.translator)
        with patch("stats.models.Branch.checkout"):
            response = self.client.post(reverse("action-build-help", args=[action.pk]))
        self.assertRedirects(response, "/HTML/%d/index.html" % action.pk, fetch_redirect_response=False)
        self.assertEqual(action.build_url, "/HTML/%d/index.html" % action.pk)
        index_file = settings.SCRATCHDIR / "HTML" / str(action.pk) / "index.html"
        with index_file.open("r") as ifile:
            self.assertIn('<h2><span class="title">À propos</span></h2>', ifile.read())
