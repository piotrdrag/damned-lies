from django.contrib import admin

from vertimus.models import Action, State


class StateAdmin(admin.ModelAdmin):
    list_filter = ("name", "language")
    raw_id_fields = (
        "branch",
        "domain",
        "person",
    )
    search_fields = ("branch__module__name",)


class ActionAdmin(admin.ModelAdmin):
    list_display = ("__str__", "state_db", "merged_file")
    raw_id_fields = ("state_db", "person", "merged_file")
    search_fields = ("comment",)


admin.site.register(State, StateAdmin)
admin.site.register(Action, ActionAdmin)
